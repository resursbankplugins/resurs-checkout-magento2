<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;
use Resursbank\Checkout\Api\Data\PaymentHistoryInterface;

/**
 * Describes properties and methods associated with search result sets of
 * payment history event entries.
 */
interface PaymentHistorySearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get payment history list.
     *
     * @return PaymentHistoryInterface[]
     */
    public function getItems(): array;

    /**
     * Set payment history list.
     *
     * @param PaymentHistoryInterface[] $items
     * @return $this
     */
    public function setItems(
        array $items
    ): PaymentHistorySearchResultsInterface;
}
