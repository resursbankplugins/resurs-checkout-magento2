<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Api\Data;

/**
 * Describes properties and methods related to payment history event entries.
 */
interface PaymentHistoryInterface
{
    /**
     * @var string
     */
    const ID = 'id';

    /**
     * Relationship to Magento\Sales\Model\Order\Payment entity.
     *
     * @var string
     */
    const PAYMENT_ID = 'payment_id';

    /**
     * @var string
     */
    const EVENT = 'event';

    /**
     * @var string
     */
    const USER = 'user';

    /**
     * @var string
     */
    const CREATED_AT = 'created_at';

    /**
     * @var string
     */
    const EXTRA = 'extra';

    /**
     * @var string
     */
    const STATE_FROM = 'state_from';

    /**
     * @var string
     */
    const STATE_TO = 'state_to';

    /**
     * @var string
     */
    const STATUS_FROM = 'status_from';

    /**
     * @var string
     */
    const STATUS_TO = 'status_to';

    /**
     * @var int
     */
    const EVENT_CREDIT_DENIED = 1;

    /**
     * @var int
     */
    const EVENT_PURCHASE_DECLINED = 2;

    /**
     * @var int
     */
    const EVENT_REDIRECTED_GATEWAY = 3;

    /**
     * @var int
     */
    const EVENT_PAYMENT_COMPLETED = 4;

    /**
     * @var int
     */
    const EVENT_ABORTED_BY_CUSTOMER = 5;

    /**
     * @var int
     */
    const EVENT_PAYMENT_ANNULLED = 6;

    /**
     * @var int
     */
    const EVENT_PAYMENT_CREDITED = 7;

    /**
     * @var int
     */
    const EVENT_PAYMENT_DEBITED = 8;

    /**
     * @var int
     */
    const EVENT_CALLBACK_UNFREEZE = 9;

    /**
     * @var int
     */
    const EVENT_CALLBACK_BOOKED = 10;

    /**
     * @var int
     */
    const EVENT_CALLBACK_UPDATE = 14;

    /**
     * @var int
     */
    const EVENT_PAYMENT_CREATING = 15;

    /**
     * @var int
     */
    const EVENT_PAYMENT_SIGNING = 16;

    /**
     * @var int
     */
    const EVENT_PAYMENT_PROCESSING = 17;

    /**
     * @var int
     */
    const EVENT_PAYMENT_PENDING = 18;

    /**
     * @var int
     */
    const USER_CUSTOMER = 1;

    /**
     * @var int
     */
    const USER_RESURS_BANK = 2;

    /**
     * @var int
     */
    const USER_CLIENT = 3;

    /**
     * @var string
     */
    const ORDER_STATUS_CREDIT_DENIED = 'resursbank_credit_denied';

    /**
     * @var string
     */
    const ORDER_STATUS_PURCHASE_DECLINED = 'resursbank_purchase_declined';

    /**
     * @var string
     */
    const ORDER_STATUS_PURCHASE_FAILED = 'resursbank_purchase_failed';

    /**
     * @var string
     */
    const ORDER_STATUS_CANCELLED = 'resursbank_purchase_canceled';

    /**
     * @var string
     */
    const ORDER_STATUS_ABORTED_BY_CUSTOMER = 'resursbank_aborted_by_customer';

    /**
     * @var string
     */
    const ORDER_STATUS_PAYMENT_REVIEW = 'resursbank_frozen';

    /**
     * @var string
     */
    const ORDER_STATUS_BOOKED = 'resursbank_booked';

    /**
     * @var string
     */
    const ORDER_STATUS_FINALIZED = 'resursbank_finalized';

    /**
     * @var string
     */
    const ORDER_STATUS_CONFIRMED = 'resursbank_confirmed';

    /**
     * @var string
     */
    const USER_CUSTOMER_LABEL = 'Customer';

    /**
     * @var string
     */
    const USER_RESURS_BANK_LABEL = 'Resurs Bank';

    /**
     * @var string
     */
    const USER_CLIENT_LABEL = 'Client';

    /**
     * @var string
     */
    const EVENT_CREDIT_DENIED_LABEL = 'The customer was denied credit';

    /**
     * @var string
     */
    const EVENT_PURCHASE_DECLINED_LABEL = 'The customer declined the purchase';

    /**
     * @var string
     */
    const EVENT_REDIRECTED_GATEWAY_LABEL =
        'The customer was redirected to payment gateway';

    /**
     * @var string
     */
    const EVENT_PAYMENT_COMPLETED_LABEL = 'The payment process was completed';

    /**
     * @var string
     */
    const EVENT_ABORTED_BY_CUSTOMER_LABEL =
        'The payment process was aborted by the customer';

    /**
     * @var string
     */
    const EVENT_PAYMENT_CREATING_LABEL = 'The payment is being created';

    /**
     * @var string
     */
    const EVENT_PAYMENT_ANNULLED_LABEL = 'Payment canceled at Resurs Bank';

    /**
     * @var string
     */
    const EVENT_PAYMENT_CREDITED_LABEL = 'Payment credited at Resurs Bank';

    /**
     * @var string
     */
    const EVENT_PAYMENT_DEBITED_LABEL = 'Payment debited at Resurs Bank';

    /**
     * @var string
     */
    const EVENT_CALLBACK_BOOKED_LABEL =
        'Callback "Booked" received from Resurs Bank';

    /**
     * @var string
     */
    const EVENT_CALLBACK_UNFREEZE_LABEL =
        'Callback "Unfreeze" received from Resurs Bank';

    /**
     * @var string
     */
    const EVENT_CALLBACK_UPDATE_LABEL =
        'Callback "Update" received from Resurs Bank';

    /**
     * @var string
     */
    const EVENT_PAYMENT_SIGNING_LABEL = 'The payment required signing';

    /**
     * @var string
     */
    const EVENT_PAYMENT_PROCESSING_LABEL = 'Payment ready, confirmed at Resurs Bank';

    /**
     * @var string
     */
    const EVENT_PAYMENT_PENDING_LABEL = 'Payment in review at Resurs Bank';

    /**
     * Get ID.
     *
     * @param int|null $default
     * @return int|null
     */
    public function getId(int $default = null);

    /**
     * Set ID.
     *
     * @param int|string|null $id
     * @return $this
     */
    public function setId($id): PaymentHistoryInterface;

    /**
     * Get payment ID.
     *
     * @param int|null $default
     * @return int|null
     */
    public function getPaymentId(int $default = null);

    /**
     * Set payment ID.
     *
     * @param int $id
     * @return $this
     */
    public function setPaymentId(int $id): PaymentHistoryInterface;

    /**
     * Get payment event.
     *
     * @param int|null $default
     * @return int|null
     */
    public function getEvent(int $default = null);

    /**
     * Set payment event.
     *
     * @param int $event
     * @return $this
     */
    public function setEvent(int $event): PaymentHistoryInterface;

    /**
     * Get user that triggered the event. Users are represented by an integer,
     * which indicates what group that user belongs to.
     *
     * @param int|null $default
     * @return int|null
     */
    public function getUser(int $default = null);

    /**
     * Set user that triggered the event. Users are represented by an integer,
     * which indicates what group that user belongs to.
     *
     * @param int $user
     * @return $this
     */
    public function setUser(int $user): PaymentHistoryInterface;

    /**
     * Get extra information about the event.
     *
     * @param string|null $default
     * @return string|null
     */
    public function getExtra(string $default = null);

    /**
     * Set extra information about the event. This may for example include the
     * name of an admin (client) that triggered an event or reasons why an event
     * was triggered.
     *
     * @param string $extra
     * @return $this
     */
    public function setExtra(string $extra): PaymentHistoryInterface;

    /**
     * Set the state that this entry went from.
     *
     * @param string $state
     * @return $this
     */
    public function setStateFrom(string $state): PaymentHistoryInterface;

    /**
     * Get the state_from information about the event.
     *
     * @param string|null $default
     * @return string|null
     */
    public function getStateFrom(string $default = null);

    /**
     * Set the state that this entry went to.
     *
     * @param string $state
     * @return $this
     */
    public function setStateTo(string $state): PaymentHistoryInterface;

    /**
     * Get the state_to information about the event.
     *
     * @param string|null $default
     * @return string|null
     */
    public function getStateTo(string $default = null);

    /**
     * Set the status that this entry went from.
     *
     * @param string $status
     * @return $this
     */
    public function setStatusFrom(string $status): PaymentHistoryInterface;

    /**
     * Get the status_from information about the event.
     *
     * @param string|null $default
     * @return string|null
     */
    public function getStatusFrom(string $default = null);

    /**
     * Set the status that this entry went to.
     *
     * @param string $status
     * @return $this
     */
    public function setStatusTo(string $status): PaymentHistoryInterface;

    /**
     * Get the status_to information about the event.
     *
     * @param string|null $default
     * @return string|null
     */
    public function getStatusTo(string $default = null);

    /**
     * Get the time when the event was created.
     *
     * @param string|null $default
     * @return string|null
     */
    public function getCreatedAt(string $default = null);

    /**
     * Set the time when the event entry was created.
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt(string $createdAt): PaymentHistoryInterface;

    /**
     * Takes the integer code for a user and returns an associated label for
     * that user. Returns an empty string if a label can't be found.
     *
     * @param int $user
     * @return string
     */
    public function getUserLabel(int $user): string;

    /**
     * Takes the integer code for an event and returns an associated label for
     * that event. Returns an empty string if a label can't be found.
     *
     * @param int $event
     * @return string
     */
    public function getEventLabel(int $event): string;
}
