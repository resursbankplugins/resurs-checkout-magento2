<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Api;

/**
 * Please see vendor/resursbank/checkout/Model/GuestPaymentManagement.php for
 * further information.
 *
 * @package Resursbank\Checkout\Api
 */
interface GuestPaymentManagementInterface
{
    /**
     * Apply submitted payment method on quote, collect new totals and return
     * the resulting collection.
     *
     * NOTE: The docblock definitions are important for the API calls to
     * function. FQN is required in the docblock for Magento to instantiate the
     * Model associated with this interface. FQN is applied in the argument list
     * for consistency.
     *
     * @param string $cartId
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @return \Magento\Checkout\Api\Data\PaymentDetailsInterface
     */
    public function collectTotals(
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
    );
}
