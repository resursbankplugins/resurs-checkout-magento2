<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Helper;

use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order as MagentoOrder;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\Order\ItemRepository;
use Resursbank\Checkout\Exception\ResursBankException;
use Resursbank\Checkout\Helper\Db\PaymentHistory as History;
use Resursbank\Checkout\Helper\Payment\Method;
use Resursbank\Checkout\Model\Payment\Standard;
use Resursbank\Checkout\Model\PaymentHistory;
use Resursbank\Checkout\Model\Total\Quote;
use Magento\Sales\Api\OrderManagementInterface;

/**
 * @package Resursbank\Checkout\Helper
 */
class Order extends AbstractHelper
{
    /**
     * Name of callback received column on order table.
     *
     * NOTE: This is a legacy flag once used by a cronjob no longer included in
     * the module. The flag is referenced by our setup script to add and later
     * drop a column from the sales order schema.
     *
     * @var string
     */
    const CALLBACK_RECEIVED_COLUMN = 'resursbank_callback_received';

    /**
     * Name of flag containig a boolean displaying whether or not we have
     * requested an order confirmation email be sent. This flag is intended
     * to prevent scenarios where multiple order confirmation emails are queued.
     * This can othrweise happen if Resurs Bank submits multiple BOOKED
     * callbacks.
     *
     * @var string
     */
    const EMAIL_REQUESTED_COLUMN = 'resursbank_email_requested';

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var OrderInterface
     */
    private $orderInterface;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var History
     */
    private $history;

    /**
     * @var Method
     */
    private $method;

    /**
     * @var OrderSender
     */
    private $orderSender;

    /**
     * @var OrderManagementInterface
     */
    private $orderManagement;

    /**
     * @var ItemRepository
     */
    private $orderItemRepository;

    /**
     * @param Context $context
     * @param OrderRepository $orderRepository
     * @param OrderInterface $orderInterface
     * @param Log $log
     * @param Method $method
     * @param OrderSender $orderSender
     * @param History $history
     */
    public function __construct(
        Context $context,
        OrderRepository $orderRepository,
        OrderInterface $orderInterface,
        Log $log,
        Method $method,
        OrderSender $orderSender,
        History $history,
        OrderManagementInterface $orderManagement,
        ItemRepository $orderItemRepository
    ) {
        $this->orderRepository = $orderRepository;
        $this->orderInterface = $orderInterface;
        $this->log = $log;
        $this->method = $method;
        $this->orderSender = $orderSender;
        $this->history = $history;
        $this->orderManagement = $orderManagement;
        $this->orderItemRepository = $orderItemRepository;

        parent::__construct($context);
    }

    /**
     * Retrieve an order through the order repository. Exceptions will be logged
     * then thrown upstream.
     *
     * @param string|int $entityId
     * @return MagentoOrder
     * @throws Exception
     */
    public function getOrder($entityId): MagentoOrder
    {
        $order = null;

        try {
            /** @var MagentoOrder $order */
            $order = $this->orderRepository->get((int)$entityId);
        } catch (Exception $e) {
            $this->log->error($e);
            throw $e;
        }

        return $order;
    }

    /**
     * Retrieve payment id from order.
     *
     * @param MagentoOrder $order
     * @return string
     * @throws ResursBankException
     */
    public function getPaymentId(MagentoOrder $order): string
    {
        $result = $order->getOriginalIncrementId() ?
            (string) $order->getOriginalIncrementId() :
            (string) $order->getIncrementId();

        if ($result === '') {
            throw new ResursBankException(
                'Failed to obtain increment id from order ' . $order->getId()
            );
        }

        return $result;
    }

    /**
     * Retrieve payment method fee excl. tax from order.
     *
     * @param MagentoOrder $order
     * @return float
     */
    public function getPaymentMethodFeeAmount(MagentoOrder $order): float
    {
        return (float) $order->getData(Quote::CODE . '_amount');
    }

    /**
     * Retrieve payment method fee tax amount from order.
     *
     * @param MagentoOrder $order
     * @return float
     */
    public function getPaymentMethodFeeTaxAmount(MagentoOrder $order): float
    {
        return (float) $order->getData(Quote::CODE . '_tax_amount');
    }

    /**
     * Returns the value of the `resursbank_environment` column for the order
     * from the `sales_order` table.
     *
     * @param MagentoOrder $order
     * @return string
     */
    public function getEnvironment(MagentoOrder $order): string
    {
        return (string) $order->getData('resursbank_environment');
    }

    /**
     * Sets the environment in the `resursbank_environment` column.
     *
     * @param MagentoOrder $order
     * @param string $environment
     * @return MagentoOrder
     */
    public function setEnvironment(
        MagentoOrder $order,
        string $environment
    ): MagentoOrder {
        return $order->setData('resursbank_environment', $environment);
    }

    /**
     * Calculate and return VAT percentage for payment method fee applied on
     * order.
     *
     * @param MagentoOrder $order
     * @return float
     */
    public function getPaymentMethodFeeVatPercentage(
        MagentoOrder $order
    ): float {
        return $this->method->calculateFeeVatPercentage(
            $this->getPaymentMethodFeeAmount($order),
            $this->getPaymentMethodFeeTaxAmount($order)
        );
    }

    /**
     * Save an order to the repository.
     *
     * @param OrderInterface $order
     * @return OrderInterface
     * @throws AlreadyExistsException
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function save(OrderInterface $order): OrderInterface
    {
        return $this->orderRepository->save($order);
    }

    /**
     * Mark the order as declined (denied credit application).
     *
     * @param MagentoOrder $order
     * @return void
     * @throws AlreadyExistsException
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws ResursBankException
     */
    public function deny(MagentoOrder $order)
    {
        $state = MagentoOrder::STATE_CANCELED;
        $status = PaymentHistory::ORDER_STATUS_CREDIT_DENIED;

        $this->createHistoryEntry(
            $order,
            PaymentHistory::EVENT_CREDIT_DENIED,
            $state,
            $status
        )->updateStatus($order, $state, $status);
    }

    /**
     * Mark the order as payment pending.
     *
     * @param MagentoOrder $order
     * @param int $user
     * @return void
     * @throws AlreadyExistsException
     * @throws InputException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     * @throws ResursBankException
     */
    public function pendingPayment(
        MagentoOrder $order,
        int $user = PaymentHistory::USER_RESURS_BANK
    ) {
        $state = MagentoOrder::STATE_NEW;
        $status = MagentoOrder::STATE_PENDING_PAYMENT;

        $this->createHistoryEntry(
            $order,
            PaymentHistory::EVENT_REDIRECTED_GATEWAY,
            $state,
            $status,
            $user
        )->updateStatus($order, $state, $status);
    }

    /**
     * This method will update the order status and state.
     *
     * @param OrderInterface $order
     * @param string $state
     * @param string $status
     * @return self
     * @throws AlreadyExistsException
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function updateStatus(
        OrderInterface $order,
        string $state,
        string $status
    ): self {
        $order->setState($state)
            ->setStatus($status);

        $this->save($order);

        return $this;
    }

    /**
     * Shorthand to create a history entry to the payment attached to the
     * provided order.
     *
     * @param OrderInterface $order
     * @param int $event
     * @param string $state
     * @param string $status
     * @param int $user
     * @param string $extra
     * @return self
     * @throws AlreadyExistsException
     * @throws LocalizedException
     * @throws ResursBankException
     */
    public function createHistoryEntry(
        OrderInterface $order,
        int $event,
        string $state,
        string $status,
        int $user = PaymentHistory::USER_RESURS_BANK,
        string $extra = ''
    ): self {
        if ($order->getPayment() === null) {
            throw new ResursBankException(
                'No payment attached to Order. Failed to create history entry.'
            );
        }

        $this->history->addEntry(
            $this->history->createEntry(
                (int) $order->getPayment()->getEntityId(),
                $event,
                $user,
                $extra,
                $order->getState(),
                $state,
                $order->getStatus() ?? '',
                $status
            )
        );

        return $this;
    }

    /**
     * Send order confirmation email.
     *
     * @param MagentoOrder $order
     * @return bool
     */
    public function sendConfirmationEmail(MagentoOrder $order)
    {
        // Submit email, if it hasn't already been sent (this avoid duplicate
        // emails from being submitted if callbacks are sent more than once).
        if ((bool) $order->getData(self::EMAIL_REQUESTED_COLUMN) === false) {
            $result = $this->orderSender->send($order);

            // Set flag in database indicating that the order confirmation email
            // has been submitted.
            $this->orderRepository->save(
                $order->setData(self::EMAIL_REQUESTED_COLUMN, true)
            );
        }
    }

    /**
     * Retrieve order from increment_id.
     *
     * @param string $id
     * @return MagentoOrder
     */
    public function getOrderFromIncrementId(string $id): MagentoOrder
    {
        return $this->orderInterface->loadByIncrementId($id);
    }

    /**
     * Cancel order.
     *
     * @param MagentoOrder $order
     * @return OrderInterface
     * @throws AlreadyExistsException
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function cancel(MagentoOrder $order): OrderInterface
    {
        foreach ($order->getItems() as $item) {
            $this->orderItemRepository->save($item->cancel());
        }

        $this->orderManagement->cancel($order->getEntityId());

        return $order;
    }

    /**
     * Has the order used a Resurs Bank payment method?
     *
     * @param MagentoOrder $order
     * @return bool
     */
    public function isResursOrder(MagentoOrder $order) : bool
    {
        $code = $order->getPayment()->getMethod();

        return substr($code, 0, strlen(Standard::CODE_PREFIX)) === Standard::CODE_PREFIX;
    }
}
