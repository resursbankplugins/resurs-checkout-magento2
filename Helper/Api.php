<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper;

use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;
use Resursbank\Checkout\Model\Api\Adapter\Simplified as SimplifiedAdapter;
use Resursbank\Checkout\Model\Api as ModelApi;

/**
 * Class Api
 * @package Resursbank\Checkout\Helper
 */
class Api extends AbstractHelper
{
    /**
     * Name of the quote table column containing the temporary Resurs Bank
     * token.
     *
     * @var string
     */
    const QUOTE_TOKEN_FIELD = 'resursbank_token';

    /**
     * Interval for card amount options.
     *
     * @var int
     */
    const CARD_AMOUNT_INTERVAL = 5000;

    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var Context
     */
    private $context;

    /**
     * Internal cached quote instance.
     *
     * @var Quote
     */
    private $quote;

    /**
     * @param Session $checkoutSession
     * @param Context $context
     * @param CartRepositoryInterface $quoteRepository
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        Session $checkoutSession,
        Context $context,
        CartRepositoryInterface $quoteRepository,
        ObjectManagerInterface $objectManager
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->context = $context;
        $this->quoteRepository = $quoteRepository;
        $this->objectManager = $objectManager;

        parent::__construct($context);
    }

    /**
     * Retrieve checkout quote.
     *
     * NOTE: we avoid fetching the quote instance directly from the session
     * because sessions use internal caching which can cause the quote to appear
     * as inactive and thus break the purchase process (this has been observed
     * on a single instance running version 2.2.8). We use our own internal
     * cache to suppress unnecessary calls to the database.
     *
     * @return Quote
     * @throws NoSuchEntityException
     */
    public function getQuote()
    {
        if ($this->quote === null) {
            $this->quote = $this->quoteRepository->get(
                $this->checkoutSession->getQuoteId()
            );
        }

        return $this->quote;
    }

    /**
     * Check if shopping cart is empty.
     *
     * @return bool
     * @throws NoSuchEntityException
     */
    public function cartIsEmpty()
    {
        return ((float) $this->getQuote()->getItemsCount() < 1);
    }

    /**
     * Clear payment session information from checkout session.
     *
     * @return $this
     */
    public function clearPaymentSession()
    {
        // Unset session data.
        $this->checkoutSession
            ->unsetData(ModelApi::PAYMENT_SESSION_ID_KEY)
            ->unsetData(ModelApi::PAYMENT_SESSION_IFRAME_KEY)
            ->unsetData(ModelApi::PAYMENT_SESSION_IFRAME_SCRIPT_KEY)
            ->unsetData(ModelApi::PAYMENT_SESSION_IFRAME_ORIGIN_KEY)
            ->unsetData(ModelApi::PAYMENT_REFERENCE)
            ->unsetData(SimplifiedAdapter::SESSION_KEY_CARD_NUMBER)
            ->unsetData(SimplifiedAdapter::SESSION_KEY_CARD_AMOUNT)
            ->unsetData(SimplifiedAdapter::SESSION_KEY_COMPANY_GOVERNMENT_ID)
            ->unsetData(SimplifiedAdapter::SESSION_KEY_CONTACT_GOVERNMENT_ID)
            ->unsetData(SimplifiedAdapter::SESSION_KEY_IS_COMPANY)
            ->unsetData(SimplifiedAdapter::SESSION_KEY_SSN)
            ->unsetData(SimplifiedAdapter::SESSION_PAYMENT_ID)
            ->unsetData(SimplifiedAdapter::SESSION_KEY_SIGNING_URL);

        return $this;
    }

    /**
     * Validates session. If the session is not valid the order should not be placed.
     *
     * @return bool
     */
    public function validateSessionData()
    {
        $id = $this->checkoutSession->getData(ModelApi::PAYMENT_SESSION_ID_KEY);
        $iframe = $this->checkoutSession->getData(ModelApi::PAYMENT_SESSION_IFRAME_KEY);

        return is_string($id) && $id !== '' && is_string($iframe) && $iframe !== '';
    }

    /**
     * Rebuild cart contents from order.
     *
     * @param Order $order
     * @return $this
     * @throws NoSuchEntityException
     */
    public function rebuildCart(Order $order)
    {
        $oldQuote = $this->quoteRepository->get($order->getQuoteId());
        $oldQuote->setIsActive(true);

        $this->checkoutSession->setQuoteId((int)$oldQuote->getId());

        /* @var Cart $cart */
        $cart = $this->objectManager->get('Magento\Checkout\Model\Cart');
        $cart->setQuote($oldQuote)->save();

        return $this;
    }

    /**
     * Deactivates the quote of the order, clears the checkout session of quote
     * data, and truncates the cart.
     *
     * Because of how we reinstate the quote to make certain functionality
     * available to the user (such as if an order fails we rebuild the cart),
     * this is required to do after a successful purchase to ensure that the
     * following do not occur:
     *
     * 1. The cart might not be displayed as empty on the order success page
     * when it should be.
     * 2. There have been instances where when making a purchase right after
     * a successful one, the products of the new purchase will be added to the
     * previous order.
     *
     * @param Order $order
     * @return $this
     * @throws NoSuchEntityException
     */
    public function clearQuoteSession(Order $order)
    {
        $oldQuote = $this->quoteRepository->get($order->getQuoteId());
        $oldQuote->setIsActive(false);
        $this->quoteRepository->save($oldQuote);
        $this->checkoutSession->clearQuote();

        /* @var Cart $cart */
        $cart = $this->objectManager->get('Magento\Checkout\Model\Cart');
        $cart->truncate()->save();

        return $this;
    }
}
