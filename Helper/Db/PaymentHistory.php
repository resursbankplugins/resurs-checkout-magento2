<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Helper\Db;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Resursbank\Checkout\Api\PaymentHistoryRepositoryInterface;
use Resursbank\Checkout\Model\PaymentHistory as Model;
use Resursbank\Checkout\Model\PaymentHistoryFactory;

/**
 * @package Resursbank\Checkout\Helper\Db
 */
class PaymentHistory extends AbstractHelper
{
    /**
     * @var PaymentHistoryFactory
     */
    private $modelFactory;

    /**
     * @var PaymentHistoryRepositoryInterface
     */
    private $repository;

    /**
     * @param Context $context
     * @param PaymentHistoryFactory $modelFactory
     * @param PaymentHistoryRepositoryInterface $repository
     */
    public function __construct(
        Context $context,
        PaymentHistoryFactory $modelFactory,
        PaymentHistoryRepositoryInterface $repository
    ) {
        $this->modelFactory = $modelFactory;
        $this->repository = $repository;

        parent::__construct($context);
    }

    /**
     * Returns an instance of PaymentHistory Model.
     *
     * @return Model
     */
    public function getModel(): Model
    {
        return $this->modelFactory->create();
    }

    /**
     * @return PaymentHistoryRepositoryInterface
     */
    public function getRepository(): PaymentHistoryRepositoryInterface
    {
        return $this->repository;
    }

    /**
     * Creates a valid entry for the database.
     *
     * @param int $paymentEntityId
     * @param int $event
     * @param int $user
     * @param string $extra
     * @param string $stateFrom
     * @param string $stateTo
     * @param string $statusFrom
     * @param string $statusTo
     * @return Model
     */
    public function createEntry(
        int $paymentEntityId,
        int $event,
        int $user,
        string $extra = '',
        string $stateFrom = '',
        string $stateTo = '',
        string $statusFrom = '',
        string $statusTo = ''
    ): Model {
        return $this->getModel()
            ->setPaymentId($paymentEntityId)
            ->setEvent($event)
            ->setUser($user)
            ->setExtra($extra)
            ->setStateFrom($stateFrom)
            ->setStateTo($stateTo)
            ->setStatusFrom($statusFrom)
            ->setStatusTo($statusTo);
    }

    /**
     * @param Model $entry
     * @return Model
     * @throws AlreadyExistsException
     * @throws LocalizedException
     */
    public function addEntry(Model $entry): Model
    {
        if ($this->isValidEntry($entry)) {
            $this->repository->save($entry);
        }

        return $entry;
    }

    /**
     * Determines whether an entry is valid to store in the database.
     *
     * @param Model $entry
     * @return bool
     */
    public function isValidEntry(Model $entry): bool
    {
        return is_int($entry->getPaymentId()) &&
            is_int($entry->getEvent()) &&
            is_int($entry->getUser()) &&
            $entry->getEvent() > 0 &&
            $entry->getUser() > 0;
    }
}
