<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Db\Account\Method;

use \InvalidArgumentException;
use \Exception;
use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\App\Helper\Context;
use \Magento\Framework\ObjectManagerInterface;
use \Magento\Framework\Exception\LocalizedException;
use \Resursbank\Checkout\Model\Account\Method\Annuity as AnnuityModel;
use \Resursbank\Checkout\Model\ResourceModel\Account\Method\Annuity\Collection;
use \Resursbank\Checkout\Helper\Db\Account\Method as MethodDb;

/**
 * @package Resursbank\Checkout\Helper\Db\Account\Method
 */
class Annuity extends AbstractHelper
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var MethodDb
     */
    private $methodDb;

    /**
     * @param Context $context
     * @param MethodDb $methodDb
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        Context $context,
        MethodDb $methodDb,
        ObjectManagerInterface $objectManager
    ) {
        $this->methodDb = $methodDb;
        $this->objectManager = $objectManager;

        parent::__construct($context);
    }

    /**
     * Returns an instance of the Annuity model.
     *
     * @return AnnuityModel
     */
    public function getModel()
    {
        /** @var AnnuityModel $model */
        $model = $this->objectManager->create(AnnuityModel::class);

        return $model;
    }

    /**
     * Get all annuities.
     *
     * @return Collection
     */
    public function getCollection()
    {
        /** @var Collection $collection */
        $collection = $this->getModel()->getCollection();

        return $collection;
    }

    /**
     * Filter annuities by provided method id.
     *
     * @param int $id
     * @return Collection
     */
    public function filterByMethodId($id)
    {
        return $this->getCollection()
            ->addFieldToFilter(AnnuityModel::METHOD_ID, $id);
    }

    /**
     * Truncates the main table.
     *
     * @return $this
     * @throws LocalizedException
     */
    public function truncate()
    {
        $model = $this->getModel();
        $model->getResource()->getConnection()->truncateTable(
            $model->getResource()->getMainTable()
        );

        return $this;
    }

    /**
     * Adds a record to the annuity table in the database.
     *
     * @param int $methodId
     * @param array $data
     * @return AnnuityModel
     * @throws Exception
     */
    public function sync($methodId, array $data)
    {
        if (!$this->validateData($data)) {
            throw new InvalidArgumentException('Argument $data is not valid.');
        }

        $annuityModel = $this->getModel();
        $annuityModel->setMethodId($methodId)
            ->setTitle($data[AnnuityModel::TITLE])
            ->setFactor($data[AnnuityModel::FACTOR])
            ->setDuration($data[AnnuityModel::DURATION])
            ->setRaw(json_encode($data));

        // Save updates.
        $annuityModel->save();

        return $annuityModel;
    }

    /**
     * Validates data that you want to add to the database.
     *
     * @param array $data
     * @return bool
     */
    public function validateData(array $data)
    {
        return isset($data[AnnuityModel::DURATION]) &&
            isset($data[AnnuityModel::TITLE]) &&
            isset($data[AnnuityModel::FACTOR]);
    }
}
