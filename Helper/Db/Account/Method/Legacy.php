<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Db\Account\Method;

use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\App\Helper\Context;
use \Magento\Framework\ObjectManagerInterface;
use \Magento\Framework\App\ResourceConnection;
use \Resursbank\Checkout\Model\Payment\Standard;
use \Resursbank\Checkout\Helper\Db\Account\Method as MethodDb;
use \Resursbank\Checkout\Helper\Log;
use \Resursbank\Checkout\Helper\Logic\Account\Method as MethodHelper;

/**
 * Collect legacy payment methods from orders.
 *
 * @package Resursbank\Checkout\Helper\Db\Account\Method
 */
class Legacy extends AbstractHelper
{
    /**
     * @var string
     */
    const CODE = 'method';

    /**
     * @var string
     */
    const ADDITIONAL_INFO = 'additional_information';

    /**
     * @var string
     */
    const TITLE = 'method_title';

    /**
     * @var string
     */
    const SALES_TABLE = 'sales_order_payment';

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var MethodDb
     */
    private $methodDb;

    /**
     * @var Log
     */
    private $log;

    /**
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param MethodDb $methodDb
     * @param Log $log
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        MethodDb $methodDb,
        Log $log
    ) {
        $this->objectManager = $objectManager;
        $this->methodDb = $methodDb;
        $this->log = $log;

        parent::__construct($context);
    }

    /**
     * Collect payment methods from order records.
     *
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function collect()
    {
        /** @var ResourceConnection $resource */
        $resource = $this->objectManager->get(ResourceConnection::class);

        // Database connection.
        $connection = $resource->getConnection();

        // Name of the table, including potential prefix/suffix.
        $table = $resource->getTablename(self::SALES_TABLE);

        // Retrieve a distinct collection of payment method codes and titles
        // used for existing orders.
        $result = $connection->query(
            "select distinct method, additional_information from " .
            $table .
            " where method like '" .
            MethodHelper::CODE_PREFIX .
            "%'"
        )->fetchAll();

        return is_array($result) ? $result : [];
    }

    /**
     * Syncs a legacy payment method from an order to the database. This is
     * necessary to support old orders that utilize payment methods which are
     * no longer available from the API.
     *
     * @param array $method
     * @return $this
     */
    public function sync(array $method)
    {
        // Some orders may have non-existent payment methods, which is why we
        // don't throw an exception here.
        if ($this->validateData($method)) {
            $model = $this->methodDb->loadByCode($method[self::CODE]);

            if (!$model->getId()) {
                $model->setCode($method[self::CODE])
                    ->setActive(0)
                    ->setTitle($this->getTitle($method))
                    ->save();
            }
        }

        return $this;
    }

    /**
     * Get payment method title from payment information extracted from order.
     *
     * @param array $method
     * @return string
     */
    private function getTitle(array $method)
    {
        $result = Standard::TITLE;
        $info = isset($method[self::ADDITIONAL_INFO]) ?
            @json_decode($method[self::ADDITIONAL_INFO], true) :
            [];

        if (is_array($info) && isset($info[self::TITLE])) {
            $result = $info[self::TITLE];
        }

        return (string) $result;
    }

    /**
     * Check if payment method extracted from order can be added to the
     * database.
     *
     * @param array $method
     * @return bool
     */
    private function validateData(array $method)
    {
        return (
            isset($method[self::CODE]) &&
            $method[self::CODE] !== Standard::CODE
        );
    }
}
