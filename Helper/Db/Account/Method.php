<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Db\Account;

use Exception;
use InvalidArgumentException;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Db\Account;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Logic\Account\Method as MethodHelper;
use Resursbank\Checkout\Model\Account\Method as MethodModel;
use Resursbank\Checkout\Model\Api\Credentials;
use Resursbank\Checkout\Model\Payment\Standard;
use Resursbank\Checkout\Model\ResourceModel\Account\Method\Collection;

/**
 * @package Resursbank\Checkout\Helper\Db\Account
 */
class Method extends AbstractHelper
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var Account
     */
    private $account;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param Account $account
     * @param Log $log
     * @param ApiConfig $apiConfig
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        Account $account,
        Log $log,
        ApiConfig $apiConfig
    ) {
        $this->objectManager = $objectManager;
        $this->account = $account;
        $this->log = $log;
        $this->apiConfig = $apiConfig;

        parent::__construct($context);
    }

    /**
     * Returns an instance of the Method model.
     *
     * @return MethodModel
     */
    public function getModel()
    {
        /** @var MethodModel $model */
        $model = $this->objectManager->create(MethodModel::class);

        return $model;
    }

    /**
     * Get all methods.
     *
     * @return Collection
     */
    public function getCollection()
    {
        /** @var Collection $collection */
        $collection = $this->getModel()->getCollection();

        return $collection;
    }

    /**
     * Load by code.
     *
     * @param string $val
     * @return MethodModel
     */
    public function loadByCode($val)
    {
        return $this->getModel()->load($val, MethodModel::CODE);
    }

    /**
     * Reset 'active' to '0' for all records.
     *
     * @return $this
     */
    public function deactivateMethods()
    {
        /** @var MethodModel $method */
        foreach ($this->getCollection() as $method) {
            $method->setActive(0)->save();
        }

        return $this;
    }

    /**
     * Collect all methods from the database and convert them to XML
     * configuration. This will be appended to the result from all XML files
     * Magento collects configuration data from.
     *
     * @return array
     */
    public function collect()
    {
        $result = [];

        if ($this->tableExists()) {
            /** @var MethodModel $method */
            foreach ($this->getCollection() as $method) {
                $result[$method->getCode()] = [
                    'identifier' => $method->getIdentifier(),
                    'code' => $method->getCode(),
                    'active' => $method->getActive(),
                    'model' => Standard::class,
                    'order_status' => $method->getOrderStatus(),
                    'title' => $method->getTitle(),
                    'group' => 'offline',
                    'allowspecific' => '1',
                    'specificcountry' => $method->getSpecificcountry(),
                    'min_order_total' => $method->getMinOrderTotal(),
                    'max_order_total' => $method->getMaxOrderTotal()
                ];
            }
        } else {
            $this->log->error(
                (
                    'Table resursbank_checkout_account_method does not ' .
                    'exist. During the installation process this is normal. ' .
                    'Please refresh your cache. If the problem persists ' .
                    'please contact us.'
                ),
                true
            );
        }

        return $result;
    }

    /**
     * Adds a record to the method table in the database.
     *
     * @param array $data
     * @param Credentials $credentials
     * @param string $country
     * @return MethodModel
     * @throws Exception
     */
    public function sync(
        array $data,
        Credentials $credentials,
        string $country
    ) {
        if (!$this->validateData($data)) {
            throw new InvalidArgumentException('Argument $data is not valid.');
        }

        $code = $data[MethodModel::CODE];
        $accountId = $this->account->getIdByCredentials($credentials);
        $model = $this->loadByCode($code);

        // Set 'code' if this is a new method currently not in the db.
        if (!$model->getId()) {
            $model->setCode($code);
        }

        // Set associated API account.
        if ($accountId !== 0) {
            $model->setAccountId($accountId);
        }

        // Update values of method record (existing or not). Please note
        // we cannot use setData as that would overwrite 'code' and
        // 'id', thus causing a unique constraint violation when saving.
        $model->setIdentifier($data[MethodModel::IDENTIFIER])
            ->setActive(1)
            ->setTitle($data[MethodModel::TITLE])
            ->setMinOrderTotal($data[MethodModel::MIN_ORDER_TOTAL])
            ->setMaxOrderTotal($data[MethodModel::MAX_ORDER_TOTAL])
            ->setOrderStatus(MethodHelper::DEFAULT_ORDER_STATUS)
            ->setRaw($data[MethodModel::RAW])
            ->setSpecificcountry($country);

        // Save updates.
        $model->save();

        return $model;
    }

    /**
     * Validates data that you want to add to the database.
     *
     * @param array $data
     * @return bool
     */
    public function validateData(array $data)
    {
        return (
            isset($data[MethodModel::CODE]) &&
            isset($data[MethodModel::IDENTIFIER]) &&
            isset($data[MethodModel::TITLE]) &&
            isset($data[MethodModel::MIN_ORDER_TOTAL]) &&
            isset($data[MethodModel::MAX_ORDER_TOTAL]) &&
            isset($data[MethodModel::RAW])
        );
    }

    /**
     * Truncates the main table.
     *
     * @return $this
     * @throws LocalizedException
     */
    public function truncate()
    {
        $model = $this->getModel();
        $model->getResource()->getConnection()->truncateTable(
            $model->getResource()->getMainTable()
        );

        return $this;
    }

    /**
     * Retrieve collection of payment methods based on active user.
     *
     * @param null|string $scopeCode
     * @return Collection
     * @throws Exception
     */
    public function getActiveCollection($scopeCode = null)
    {
        /** @var Credentials $credentials */
        $credentials = $this->apiConfig->getCredentials($scopeCode);

        // Only retrieve methods associated with the API account configured for
        // the configuration currently displayed. Filter out inactive methods.
        return $this->getCollection()->addFieldToFilter(
            MethodModel::ACCOUNT_ID,
            $this->account->getIdByCredentials($credentials)
        )->addFieldToFilter(MethodModel::ACTIVE, '1');
    }

    /**
     * Check if 'resursbank_checkout_account_method' table exist. It won't
     * during the installation process. Magento assembles the initial config
     * from XML files before executing the installation / upgrade scripts. This
     * means we can end up with a MySQL error during the installation process
     * since the table we want to collect our methods from naturally cannot
     * exist before it's been created. Hence this method.
     *
     * @return bool
     */
    private function tableExists()
    {
        /** @var ResourceConnection $resource */
        $resource = $this->objectManager->get(ResourceConnection::class);

        // Database connection.
        $connection = $resource->getConnection();

        return $connection->isTableExists(
            $connection->getTableName('resursbank_checkout_account_method')
        );
    }
}
