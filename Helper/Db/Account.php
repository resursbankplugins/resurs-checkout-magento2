<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Db;

use \Magento\Framework\App\Helper\Context;
use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\ObjectManagerInterface;
use \Resursbank\Checkout\Model\ResourceModel\Account\Collection;
use \Resursbank\Checkout\Model\Account as AccountModel;
use \Resursbank\Checkout\Model\Api\Credentials;

/**
 * @package Resursbank\Checkout\Helper\Db
 */
class Account extends AbstractHelper
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager
    ) {
        $this->objectManager = $objectManager;

        parent::__construct($context);
    }

    /**
     * Returns an instance of the Account model.
     *
     * @return AccountModel
     */
    public function getModel()
    {
        /** @var AccountModel $model */
        $model = $this->objectManager->create(AccountModel::class);

        return $model;
    }

    /**
     * Get all accounts.
     *
     * @return Collection
     */
    public function getCollection()
    {
        /** @var Collection $collection */
        $collection = $this->getModel()->getCollection();

        return $collection;
    }

    /**
     * Retrieve ID from account matching provided credentials.
     *
     * @param Credentials $credentials
     * @return int
     */
    public function getIdByCredentials(Credentials $credentials)
    {
        $model = $this->getModel()->loadByCredentials($credentials);

        return (int) $model->getId();
    }

    /**
     * @param AccountModel $account
     * @return Credentials
     */
    public function getCredentials(AccountModel $account)
    {
        /** @var Credentials $model */
        $model = $this->objectManager->create(Credentials::class);

        $model->setUsername((string) $account->getUsername())
            ->setEnvironment((int) $account->getEnvironment());

        return $model;
    }
}
