<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Config;

use Exception;
use Resursbank\Checkout\Helper\Config as ConfigHelper;

/**
 * Provides methods to simplify config handling for the resursbank_methods
 * config section.
 *
 * @package Resursbank\Checkout\Helper\Config
 */
class Methods extends ConfigHelper
{
    /**
     * @var string
     */
    protected $section = 'resursbank_methods';

    /**
     * Retrieve configured payment method fee.
     *
     * @param string $method
     * @param null|string $scopeCode
     * @return float
     * @throws Exception
     */
    public function getFee($method, $scopeCode = null)
    {
        $result = $this->getSetting(
            'fee',
            $method,
            $scopeCode
        );

        if (is_string($result)) {
            $result = preg_replace('/,/', '.', $result);
        }

        return (float) $result;
    }

    /**
     * Retrieve configured fee tax class.
     *
     * @param null|string $scopeCode
     * @return string
     * @throws Exception
     */
    public function getTaxClass($scopeCode = null)
    {
        return (string) $this->getSetting(
            'class',
            'tax',
            $scopeCode
        );
    }

    /**
     * Retrieve configured display setting for tax values associated with fees.
     *
     * @param null|string $scopeCode
     * @return int
     * @throws Exception
     */
    public function getTaxDisplay($scopeCode = null)
    {
        return (int) $this->getSetting(
            'display',
            'tax',
            $scopeCode
        );
    }

    /**
     * Retrieve maximum order total for Swsih transactions.
     *
     * @param null|string $scopeCode
     * @return string
     * @throws Exception
     */
    public function getSwishMaxOrderTotal($scopeCode = null)
    {
        return (string) $this->getSetting(
            'swish_max_order_total',
            'advanced',
            $scopeCode
        );
    }
}
