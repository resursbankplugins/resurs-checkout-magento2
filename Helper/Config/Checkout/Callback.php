<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Helper\Config\Checkout;

use Exception;
use Resursbank\Checkout\Helper\Config\Checkout as ConfigHelper;

/**
 * @package Resursbank\Checkout\Helper\Config\Checkout
 */
class Callback extends ConfigHelper
{
    /**
     * Config section this class is responsible for.
     *
     * @var string
     */
    const SECTION = 'callback';

    /**
     * Get timestamp when test callback last came in.
     *
     * @param null|string $scopeCode
     * @return int
     * @throws Exception
     */
    public function getTestReceivedAt(string $scopeCode = null): int
    {
        return (int) $this->getSetting(
            'test_received_at',
            self::SECTION,
            $scopeCode
        );
    }

    /**
     * Update timestamp when test callback last came in.
     *
     * @param int $value
     * @param int $scopeCode
     * @return void
     * @throws Exception
     */
    public function setTestReceivedAt(int $value, int $scopeCode = 0)
    {
        $this->setSetting(
            'test_received_at',
            self::SECTION,
            (string) $value,
            $scopeCode
        );
    }
}
