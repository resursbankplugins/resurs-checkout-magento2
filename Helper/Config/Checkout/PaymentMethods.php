<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Helper\Config\Checkout;

use Exception;
use Resursbank\Checkout\Helper\Config\Checkout as ConfigHelper;

/**
 * @package Resursbank\Checkout\Helper\Config\Checkout
 */
class PaymentMethods extends ConfigHelper
{
    /**
     * Config section this class is responsible for.
     *
     * @var string
     */
    const SECTION = 'methods';

    /**
     * Whether the automatic sync of payment methods in enabled.
     *
     * @param null|string $scopeCode
     * @return bool
     * @throws Exception
     */
    public function autoSyncEnabled(string $scopeCode = null): bool
    {
        return $this->getFlag(
            'auto_sync_method',
            self::SECTION,
            $scopeCode
        );
    }
}
