<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Config\Checkout;

use Exception;
use Resursbank\Checkout\Helper\Config\Checkout as CheckoutHelper;
use Resursbank\Checkout\Model\Config\Source\Flow;
use Resursbank\Checkout\Model\Api\Credentials;

/**
 * Provides methods to simplify handling API configuration values.
 *
 * @package Resursbank\Checkout\Helper\Config\Checkout
 */
class Api extends CheckoutHelper
{
    /**
     * Config section this class is responsible for.
     *
     * @var string
     */
    const SECTION = 'api';

    /**
     * Retrieve API flow.
     *
     * @param null|string $scopeCode
     * @return string
     * @throws Exception
     */
    public function getFlow($scopeCode = null)
    {
        return $this->getSetting('flow', self::SECTION, $scopeCode);
    }

    /**
     * Check whether the flow is set to simplified.
     *
     * @param null|string $scopeCode
     * @return boolean
     * @throws Exception
     */
    public function isSimplifiedFlow($scopeCode = null)
    {
        $configValue = $this->getSetting('flow', self::SECTION, $scopeCode);

        return $configValue === Flow::SIMPLIFIED;
    }

    /**
     * Check whether the flow is set to checkout.
     *
     * @param null|string $scopeCode
     * @return boolean
     * @throws Exception
     */
    public function isCheckoutFlow($scopeCode = null)
    {
        $configValue = $this->getSetting('flow', self::SECTION, $scopeCode);

        return $configValue === Flow::CHECKOUT;
    }

    /**
     * Retrieve unit measurement.
     *
     * @param null|string $scopeCode
     * @return string
     * @throws Exception
     */
    public function getUnitMeasure($scopeCode = null)
    {
        return (string) $this->getSetting(
            'unit_measure',
            self::SECTION,
            $scopeCode
        );
    }

    /**
     * Get country.
     *
     * @param null|string $scopeCode
     * @return string
     * @throws Exception
     */
    public function getCountry($scopeCode = null)
    {
        return $this->getSetting('country', self::SECTION, $scopeCode);
    }

    /**
     * Check if API is in development / test mode.
     *
     * @param null|string $scopeCode
     * @return bool
     * @throws Exception
     */
    public function isInTestMode($scopeCode = null)
    {
        return ($this->getEnvironment($scopeCode) !== 'production');
    }

    /**
     * Retrieve API environment.
     *
     * @param null|string $scopeCode
     * @return string (test|prod)
     * @throws Exception
     */
    public function getEnvironment($scopeCode = null)
    {
        return $this->getSetting('environment', self::SECTION, $scopeCode);
    }

    /**
     * Retrieve API username.
     *
     * @param null|string $scopeCode
     * @return string
     * @throws Exception
     */
    public function getUsername($scopeCode = null)
    {
        return $this->getSetting(
            ('username_' . $this->getEnvironment($scopeCode)),
            self::SECTION,
            $scopeCode
        );
    }

    /**
     * Retrieve API password.
     *
     * @param null|string $scopeCode
     * @return string
     * @throws Exception
     */
    public function getPassword($scopeCode = null)
    {
        return $this->getSetting(
            ('password_' . $this->getEnvironment($scopeCode)),
            self::SECTION,
            $scopeCode
        );
    }

    /**
     * Check if we have credentials for the API.
     *
     * @param null|string $scopeCode
     * @return bool
     * @throws Exception
     */
    public function hasCredentials($scopeCode = null)
    {
        $username = $this->getUsername($scopeCode);
        $password = $this->getPassword($scopeCode);

        return (!empty($username) && !empty($password));
    }

    /**
     * Retrieve API credentials.
     *
     * @param null|string $scopeCode
     * @return Credentials
     * @throws Exception
     */
    public function getCredentials($scopeCode = null)
    {
        $result = new Credentials();

        $result->setEnvironment($this->getEnvironment($scopeCode))
            ->setUsername($this->getUsername($scopeCode))
            ->setPassword($this->getPassword($scopeCode));

        return $result;
    }
}
