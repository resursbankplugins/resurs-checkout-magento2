<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Config\Checkout;


use Exception;
use Resursbank\Checkout\Helper\Config\Checkout as ConfigHelper;

/**
 * @package Resursbank\Checkout\Helper\Config\Checkout
 */
class Advanced extends ConfigHelper
{
    /**
     * Config section this class is responsible for.
     *
     * @var string
     */
    const SECTION = 'advanced';

    /**
     * Whether or not to display shipping rates before address has been given.
     *
     * @param null|string $scopeCode
     * @return bool
     * @throws Exception
     */
    public function isCarrierRatesRequireAddressEnabled($scopeCode = null)
    {
        return $this->getFlag(
            'carrier_rates_require_address',
            self::SECTION,
            $scopeCode
        );
    }

    /**
     * Whether or not to display the loading mask on the checkout page.
     *
     * @param null|string $scopeCode
     * @return bool
     * @throws Exception
     */
    public function isLoadingMaskHidden($scopeCode = null)
    {
        return $this->getFlag(
            'hide_loading_mask',
            self::SECTION,
            $scopeCode
        );
    }

    /**
     * Whether or not to display the loading mask specifically used for shipping
     * methods on the checkout page.
     *
     * @param null|string $scopeCode
     * @return bool
     * @throws Exception
     */
    public function isShippingLoadingMaskHidden($scopeCode = null)
    {
        return $this->getFlag(
            'hide_shipping_loading_mask',
            self::SECTION,
            $scopeCode
        );
    }

    /**
     * Whether or not to round tax percentage in payment payloads.
     *
     * This basically only applies on discount values since their excl. tax
     * value and VAT percentage value are re-calculated to be both acceptable
     * by Resurs Bank and accurate in both locations (Resurs Bank payment entity
     * and Magento Order entity).
     *
     * @param null|string $scopeCode
     * @return bool
     * @throws Exception
     */
    public function roundTaxPercentage($scopeCode = null)
    {
        return $this->getFlag(
            'round_tax_percentage',
            self::SECTION,
            $scopeCode
        );
    }

    /**
     * Whether or not to reuse canceled orders that are created during the
     * order placement process. If enabled this setting allows the system to
     * replace a canceled order that was created due to an error with the
     * current order.
     *
     * NOTE: Only works if the customer is still in the same session as the
     * canceled order when it was created.
     *
     * @param null|string $scopeCode
     * @return bool
     * @throws Exception
     */
    public function getReuseErroneouslyCreatedOrders($scopeCode = null)
    {
        return $this->getFlag(
            'reuse_erroneously_created_orders',
            self::SECTION,
            $scopeCode
        );
    }

    /**
     * Whether or not the plugin should wait for fraud control in simplfied
     * flow when booking a payment.
     *
     * @param null|string $scopeCode
     * @return bool
     * @throws Exception
     */
    public function isWaitingForFraudControl($scopeCode = null)
    {
        return $this->getFlag(
            'wait_for_fraud_control',
            self::SECTION,
            $scopeCode
        );
    }

    /**
     * Whether or not the payment should be annulled immediately if Resurs Bank returns
     * a FROZEN state in simplified flow.
     *
     * @param null|string $scopeCode
     * @return bool
     * @throws Exception
     */
    public function isAnnulIfFrozen($scopeCode = null)
    {
        return $this->getFlag(
            'annul_if_frozen',
            self::SECTION,
            $scopeCode
        );
    }

    /**
     * Whether or not the payment should be debited immediately on a booked payment
     * for simplified flow.
     *
     * @param null|string $scopeCode
     * @return bool
     * @throws Exception
     */
    public function isFinalizeIfBooked($scopeCode = null)
    {
        return $this->getFlag(
            'finalize_if_booked',
            self::SECTION,
            $scopeCode
        );
    }

    /**
     * Whether or not to validate phone number at checkout.
     *
     * @param null|string $scopeCode
     * @return bool
     * @throws Exception
     */
    public function isValidatePhoneNumberEnabled($scopeCode = null)
    {
        return $this->getFlag(
            'validate_phone_number',
            self::SECTION,
            $scopeCode
        );
    }
}
