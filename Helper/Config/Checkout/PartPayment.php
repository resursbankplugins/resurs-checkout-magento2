<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Helper\Config\Checkout;

use Exception;
use Resursbank\Checkout\Helper\Config\Checkout as ConfigHelper;

/**
 * @package Resursbank\Checkout\Helper\Config\Checkout
 */
class PartPayment extends ConfigHelper
{
    /**
     * Config section this class is responsible for.
     *
     * @var string
     */
    const SECTION = 'part_payment';

    /**
     * Retrieve enabled status.
     *
     * @param null|string $scopeCode
     * @return bool
     * @throws Exception
     */
    public function getEnabled($scopeCode = null): bool
    {
        return (bool) $this->getSetting('enabled', self::SECTION, $scopeCode);
    }

    /**
     * Retrieve selected payment method.
     *
     * @param null|string $scopeCode
     * @return int
     * @throws Exception
     */
    public function getPaymentMethod($scopeCode = null): int
    {
        return (int) $this->getSetting(
            'payment_method',
            self::SECTION,
            $scopeCode
        );
    }

    /**
     * Retrieve the duration for the selected payment method.
     *
     * @param null|string $scopeCode
     * @return int
     * @throws Exception
     */
    public function getDuration($scopeCode = null): int
    {
        return (int) $this->getSetting('duration', self::SECTION, $scopeCode);
    }

    /**
     * Retrieve the product price threshold from configuration.
     *
     * @param null|string $scopeCode
     * @return float
     * @throws Exception
     */
    public function getThreshold($scopeCode = null): float
    {
        return (float) $this->getSetting('threshold', self::SECTION, $scopeCode);
    }
}
