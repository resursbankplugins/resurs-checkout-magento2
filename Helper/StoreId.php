<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\ObjectManagerInterface;
use Magento\Catalog\Model\Session as SessionManager;

/**
 * Methods to handle store id.
 *
 * NOTE: This refers to the store id at Resurs Bank in case you've multiple
 * stores configured. It's unrelated to Magento stores / store views.
 *
 * @package Resursbank\Checkout\Helper
 */
class StoreId extends AbstractHelper
{
    /**
     * @var string
     */
    const SESSION_KEY = 'resursbank_checkout_store_id';

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @param ObjectManagerInterface $objectManager
     * @param Context $context
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        Context $context
    ) {
        $this->objectManager = $objectManager;

        parent::__construct($context);
    }

    /**
     * Set a store id in session.
     *
     * @param int $id
     * @return self
     */
    public function setStoreId(int $id): self
    {
        $this->sessionManager()->setData(self::SESSION_KEY, $id);

        return $this;
    }

    /**
     * Retrieve the store id from session.
     *
     * @return int|null
     */
    public function getStoreId()
    {
        return $this->sessionManager()->getData(self::SESSION_KEY);
    }

    /**
     * Is a store id saved in session?
     *
     * @return bool
     */
    public function hasStoreId(): bool
    {
        return (bool) $this->getStoreId();
    }

    /**
     * Clear and return the store id.
     *
     * @return int
     */
    public function removeStoreId()
    {
        return $this->sessionManager()->getData(self::SESSION_KEY, true);
    }

    /**
     * Create SessionManager object.
     *
     * @return SessionManager
     */
    protected function sessionManager(): SessionManager
    {
        return $this->objectManager->create(SessionManager::class);
    }
}
