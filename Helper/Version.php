<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper;

use \Exception;
use \Magento\Framework\Exception\FileSystemException;
use \Magento\Framework\ObjectManagerInterface;
use \Magento\Framework\Module\ModuleResource;
use \Magento\Framework\App\Helper\Context;
use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\Component\ComponentRegistrarInterface;
use \Magento\Framework\Component\ComponentRegistrar;
use \Magento\Framework\Filesystem\Directory\ReadFactory;

/**
 * Version collection methods.
 *
 * @package Resursbank\Checkout\Helper
 */
class Version extends AbstractHelper
{
    /**
     * @var string
     */
    const MODULE_NAME = 'Resursbank_Checkout';

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ComponentRegistrarInterface
     */
    private $componentRegistrar;

    /**
     * @var ReadFactory
     */
    private $readFactory;

    /**
     * @var Log
     */
    private $log;

    /**
     * @param ObjectManagerInterface $objectManager
     * @param ComponentRegistrarInterface $componentRegistrar
     * @param ReadFactory $readFactory
     * @param Log $log
     * @param Context $context
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        ComponentRegistrarInterface $componentRegistrar,
        ReadFactory $readFactory,
        Log $log,
        Context $context
    ) {
        $this->objectManager = $objectManager;
        $this->componentRegistrar = $componentRegistrar;
        $this->readFactory = $readFactory;
        $this->log = $log;

        parent::__construct($context);
    }

    /**
     * Retrieve array of all versions.
     *
     * @return array
     */
    public function getAll()
    {
        return [
            'data' => $this->getDataVersion(),
            'schema' => $this->getSchemaVersion(),
            'composer' => $this->getComposerVersion()
        ];
    }

    /**
     * Retrieve data version from setup_module.
     *
     * @return string
     */
    public function getDataVersion()
    {
        return (string) $this->getModuleResource()
            ->getDataVersion(self::MODULE_NAME);
    }

    /**
     * Retrieve schema version from setup_module.
     *
     * @return string
     */
    public function getSchemaVersion()
    {
        return (string) $this->getModuleResource()
            ->getDbVersion(self::MODULE_NAME);
    }

    /**
     * Retrieve version specified in module composer.json
     *
     * @return string
     */
    public function getComposerVersion()
    {
        $result = 'unknown';

        try {
            // Get data from composer.json
            $data = $this->getComposerData();

            // Resolve version from data array.
            if (isset($data['version']) &&
                (string) $data['version'] !== ''
            ) {
                $result = (string) $data['version'];
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Retrieve data from module composer.json
     *
     * @return array
     * @throws FileSystemException
     */
    private function getComposerData()
    {
        $baseDirectory = $this->componentRegistrar->getPath(
            ComponentRegistrar::MODULE,
            self::MODULE_NAME
        );

        $dir = $this->readFactory->create($baseDirectory);

        // Suppress PHP errors. Failure to collect information from the file
        // should be handled further up. If the composer.json file cannot be
        // read it just makes things more difficult for us to debug. An error
        // here will cause all API communication to cease functioning though,
        // so it's better we just ignore it since it's not operation critical.
        return (array) @json_decode(
            (string) $dir->readFile('composer.json'),
            true
        );
    }

    /**
     * Retrieve instance of module resource model (we use this to collect data /
     * schema version from the database).
     *
     * @return ModuleResource
     */
    private function getModuleResource()
    {
        return $this->objectManager->get(ModuleResource::class);
    }
}
