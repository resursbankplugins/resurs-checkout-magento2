<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper;

use Exception;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order as MagentoOrder;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Config\Checkout\General as GeneralConfig;
use Resursbank\Checkout\Helper\Db\Account\Method as MethodDb;
use Resursbank\Checkout\Helper\Order as OrderHelper;
use Resursbank\Checkout\Model\Account\Method as MethodModel;
use Resursbank\Checkout\Model\Api\Adapter\Simplified;
use Resursbank\Checkout\Model\Api\Adapter\Simplified as ApiAdapter;
use Resursbank\Checkout\Model\Api\Adapter\Simplified\Payment as PaymentModel;
use Resursbank\Checkout\Model\Payment\Standard;

/**
 * General payment related methods.
 */
class Payment extends AbstractHelper
{
    /**
     * @var Ecom
     */
    protected $ecom;

    /**
     * @var Api
     */
    protected $api;

    /**
     * @var ApiConfig
     */
    protected $config;

    /**
     * @var GeneralConfig
     */
    protected $generalConfig;

    /**
     * @var MethodDb
     */
    private $methodDb;

    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @var Simplified
     */
    private $apiAdapter;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var Log
     */
    private $log;

    /**
     * Payment constructor.
     *
     * @param Ecom $ecom
     * @param Api $api
     * @param ApiConfig $config
     * @param MethodDb $methodDb
     * @param OrderHelper $orderHelper
     * @param Context $context
     * @param Simplified $apiAdapter
     * @param Registry $registry
     * @param GeneralConfig $generalConfig
     * @param Session $session
     * @param Log $log
     */
    public function __construct(
        Ecom $ecom,
        Api $api,
        ApiConfig $config,
        MethodDb $methodDb,
        OrderHelper $orderHelper,
        Context $context,
        Simplified $apiAdapter,
        Registry $registry,
        GeneralConfig $generalConfig,
        Session $session,
        Log $log
    ) {
        $this->ecom = $ecom;
        $this->api = $api;
        $this->config = $config;
        $this->methodDb = $methodDb;
        $this->orderHelper = $orderHelper;
        $this->apiAdapter = $apiAdapter;
        $this->registry = $registry;
        $this->generalConfig = $generalConfig;
        $this->session = $session;
        $this->log = $log;

        parent::__construct($context);
    }

    /**
     * Validate payment:
     *
     * 1) Selected payment method was provided by Resurs Bank.
     * 2) Order grand total is above 0.
     * 3) A payment matching the order actually exists at Resurs Bank.
     *
     * NOTE: Orders with a grand total of 0 won't get payments created at Resurs
     * Bank, we therefore avoid an unnecessary API call by including a separate
     * check for this since it's a much less expensive process.
     *
     * @param Order $order
     * @return bool
     * @throws Exception
     */
    public function validate(Order $order)
    {
        return (
            $this->validateMethod($order) &&
            $order->getGrandTotal() > 0 &&
            $this->exists($order)
        );
    }

    /**
     * Validate payment method:
     *
     * 1) Payment method must have been provided by Resurs Bank.
     *
     * @param Order $order
     * @return bool
     */
    public function validateMethod(Order $order)
    {
        $code = '';

        if ($order->getPayment() !== null) {
            $code = substr(
                $order->getPayment()->getMethod(),
                0,
                strlen(Standard::CODE_PREFIX)
            );
        }

        return ($code === Standard::CODE_PREFIX);
    }

    /**
     * Check if a payment actually exists.
     *
     * @param Order $order
     * @throws Exception
     * @return bool
     */
    public function exists(Order $order)
    {
        $result = false;

        try {
            // Retrieve API credentials for the store the order was placed in.
            $credentials = $this->config->getCredentials(
                $order->getStore()->getCode()
            );

            $payment = $this->ecom->getConnection($credentials)
                ->getPayment(
                    $this->orderHelper->getPaymentId($order)
                );

            $result = ($payment !== null);
        } catch (Exception $e) {
            // If there is no payment we will receive an Exception from ECom.
            if (!$this->validateMissingPaymentException($e)) {
                throw $e;
            }
        }

        return $result;
    }

    /**
     * Validate that an Exception was thrown because a payment was actually
     * missing.
     *
     * @param Exception $e
     * @return bool
     */
    public function validateMissingPaymentException(Exception $e)
    {
        return (
            $e->getCode() === 3 ||
            $e->getCode() === 8
        );
    }

    /**
     * Check if a payment debited.
     *
     * @param mixed $payment
     * @return bool
     */
    public function isFinalized($payment)
    {
        return (
            is_object($payment) &&
            isset($payment->finalized) &&
            $payment->finalized
        );
    }

    /**
     * Check if payment can be debited.
     *
     * @param mixed $payment
     * @return bool
     */
    public function isDebitable($payment)
    {
        return $this->ecom->paymentSessionHasStatus(
            $payment,
            'DEBITABLE'
        );
    }

    /**
     * Get payment method instance based on order.
     *
     * @param Order $order
     * @return MethodModel
     */
    public function getMethod(Order $order)
    {
        return $this->methodDb->loadByCode($order->getPayment()->getMethod());
    }

    /**
     * When the order process fails we will sometimes end up with an inaccurate
     * message of what actually went wrong. This method attempts to correct the
     * error message based on a string we temporarily store in the registry.
     *
     * Basically, sometimes an error occurs during payment creation, which
     * Magento will then overwrite, thus covering up the actual problem.
     *
     *
     * @param Exception $previousException
     * @return CouldNotSaveException
     */
    public function createPaymentStatusError(Exception $previousException)
    {
        $paymentStatusError = $this->registry->registry(
            Simplified::REGISTRY_KEY_PAYMENT_STATUS_ERROR
        );

        $msg = $paymentStatusError !== null ?
            $paymentStatusError->getMessage() :
            $previousException->getMessage();

        return new CouldNotSaveException(__($msg), $previousException);
    }

    /**
     * Check whether or not a payment can be created in Simplified Flow for the
     * supplied order.
     *
     * 1. Module must be enabled.
     * 2. Must be using Simplified Flow.
     * 3. Order must not have an original increment id (it must be new).
     * 4. Payment method must stem from Resurs Bank.
     * 5. Order grand total must exceed 0.
     * 6. A payment matching the increment id must not exist at Resurs Bank.
     *
     * @param Order $order
     * @return bool
     * @throws Exception
     */
    public function canCreateSimplifiedPayment(Order $order)
    {
        return (
            $this->generalConfig->isEnabled() &&
            $this->config->isSimplifiedFlow() &&
            !$order->getOriginalIncrementId() &&
            $this->validateMethod($order) &&
            $order->getGrandTotal() > 0 &&
            !$this->exists($order)
        );
    }

    /**
     * Create payment at Resurs Bank.
     *
     * NOTE: this method currently only supports Simplified Flow.
     *
     * @param MagentoOrder $order
     * @throws Exception
     */
    public function create(MagentoOrder $order)
    {
        try {
            /** @var PaymentModel $payment */
            $payment = $this->apiAdapter->createPayment($order);

            $this->prepareSigning($payment);
        } catch (Exception $e) {
            $this->log->error($e);
            throw $e;
        }
    }

    /**
     * Prepare signing a payment. Basically, we need to tell the checkout to
     * redirect us to the payment gateway where the payment is signed (the
     * client may need to sign with their BankID or similar) and we need to
     * store the resulting paymentId after creating the payment in our checkout
     * session so we can book (complete) the payment when the customer is
     * redirected back to our success page.
     *
     * @param PaymentModel $payment
     * @throws Exception
     */
    private function prepareSigning(PaymentModel $payment)
    {
        if ($payment->getSigningUrl() !== '') {
            // Store signing URL in session for later use to redirect client.
            // See Controller/Simplified/Redirect.php
            $this->session->setData(
                ApiAdapter::SESSION_KEY_SIGNING_URL,
                $payment->getSigningUrl()
            );

            // Keep the resulting paymentId from creating the payment in mind.
            $this->session->setData(
                ApiAdapter::SESSION_PAYMENT_ID,
                $payment->getPaymentId()
            );
        }
    }
}
