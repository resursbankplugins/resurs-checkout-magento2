<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Helper\Plugin\Order;

use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;
use Resursbank\Checkout\Helper\Cart as CartHelper;
use Resursbank\Checkout\Helper\Checkout\Onepage;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Config\Checkout\General as GeneralConfig;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Order as OrderHelper;
use Resursbank\Checkout\Helper\Payment;
use Resursbank\Checkout\Helper\Api as ApiHelper;

/**
 * Contains shared business logic for Plugin/Order/AroundPlace and
 * Plugin/Order/AroundPlaceGuest.
 *
 * @package Resursbank\Checkout\Helper\Plugin\Order
 */
class Place extends AbstractHelper
{
    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @var GeneralConfig
     */
    private $generalConfig;

    /**
     * @var Payment
     */
    private $helperPayment;

    /**
     * @var OrderHelper
     */
    private $helperOrder;

    /**
     * @var Onepage
     */
    private $onepage;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var CartHelper
     */
    private $cartHelper;

    /**
     * @var ApiHelper
     */
    private $apiHelper;

    /**
     * @param Context $context
     * @param ApiConfig $apiConfig
     * @param GeneralConfig $generalConfig
     * @param Payment $helperPayment
     * @param OrderHelper $helperOrder
     * @param Onepage $onepage
     * @param Log $log
     * @param CartHelper $cartHelper
     * @param ApiHelper $apiHelper
     */
    public function __construct(
        Context $context,
        ApiConfig $apiConfig,
        GeneralConfig $generalConfig,
        Payment $helperPayment,
        OrderHelper $helperOrder,
        Onepage $onepage,
        Log $log,
        CartHelper $cartHelper,
        ApiHelper $apiHelper
    ) {
        $this->apiConfig = $apiConfig;
        $this->generalConfig = $generalConfig;
        $this->helperPayment = $helperPayment;
        $this->helperOrder = $helperOrder;
        $this->onepage = $onepage;
        $this->log = $log;
        $this->cartHelper = $cartHelper;
        $this->apiHelper = $apiHelper;

        parent::__construct($context);
    }

    /**
     * Centralized code that will handle anything before and after an order has
     * been placed, effectively acting like an `around...()` plugin method.
     *
     * @param callable $callable
     * @param mixed ...$args
     * @return string
     * @throws Exception
     * @throws CouldNotSaveException
     */
    public function place(
        callable $callable,
        ...$args
    ): string {
        $result = '';

        try {
            $result = $callable(...$args);

            /** @var Order $order */
            $order = $this->helperOrder->getOrder($result);

            if ($this->isEnabled($order)) {
                // Store active API account details on order.
                $this->helperOrder->setEnvironment(
                    $order,
                    $this->apiConfig->getEnvironment()
                );

                // Save order.
                $this->helperOrder->save($order);

                // Create payment in Simplified Flow.
                if ($this->helperPayment->canCreateSimplifiedPayment($order)) {
                    $this->helperPayment->create($order);
                }

                // Rebuild cart from previous quote.
                if ($this->apiConfig->isCheckoutFlow()) {
                    $this->cartHelper->rebuildCart($order, false);
                }
            }
        } catch (Exception $e) {
            $this->handleException($e);
        }

        return (string) $result;
    }

    /**
     * @param Exception $e
     * @throws Exception
     * @throws CouldNotSaveException
     * @throws AlreadyExistsException
     * @throws LocalizedException
     */
    private function handleException(Exception $e)
    {
        // Log error.
        $this->log->error($e);

        if ($this->onepage->hasCurrentOrder()) {
            /** @var Order $order */
            $order = $this->onepage->getCurrentOrder();

            // In Checkout Flow we assume a failure here means the credit
            // application was denied.
            if ($this->apiConfig->isCheckoutFlow()) {
                $this->helperOrder->deny($order);
            }

            // Rebuild cart from previous quote.
            $this->cartHelper->rebuildCart($order);
        }

        // Toss error upstream.
        throw $this->helperPayment->createPaymentStatusError($e);
    }

    /**
     * Check whether or not the place() is enabled.
     *
     * @param Order $order
     * @return bool
     * @throws Exception
     */
    private function isEnabled(Order $order): bool
    {
        return (
            $this->generalConfig->isEnabled() &&
            $this->helperPayment->validateMethod($order)
        );
    }
}
