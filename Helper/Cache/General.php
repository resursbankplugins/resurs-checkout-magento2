<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Cache;

use \Resursbank\Checkout\Model\Cache\Type\General as Cache;
use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\App\Helper\Context;
use \Magento\Framework\App\Cache\TypeListInterface;
use \Magento\Framework\App\Cache\Type\Config;

/**
 * Class General
 *
 * @package Resursbank\Checkout\Helper\Cache
 */
class General extends AbstractHelper
{
    /**
     * @var Cache
     */
    private $cache;

    /**
     * @var TypeListInterface
     */
    private $cacheList;

    /**
     * @param Context $context
     * @param Cache $cache
     * @param TypeListInterface $cacheList
     */
    public function __construct(
        Context $context,
        Cache $cache,
        TypeListInterface $cacheList
    ) {
        $this->cache = $cache;
        $this->cacheList = $cacheList;

        parent::__construct($context);
    }

    /**
     * Loads data from the Resurs Bank Checkout cache.
     *
     * @param string $key
     * @return string
     */
    public function load($key = '')
    {
        return $this->cache->load($key);
    }

    /**
     *
     *
     * @param mixed $data
     * @param string $key
     * @return $this
     */
    public function save($data, $key = '')
    {
        $this->cache->save(
            $data,
            $key,
            [Cache::CACHE_TAG, Config::CACHE_TAG]
        );

        return $this;
    }

    /**
     * Clear Resurs Bank cache.
     *
     * @return $this
     */
    public function clean()
    {
        $this->cacheList->cleanType(Cache::TYPE_IDENTIFIER);

        return $this;
    }

    /**
     * Clears the config cache.
     *
     * @return $this
     */
    public function cleanConfig()
    {
        $this->cacheList->cleanType(Config::TYPE_IDENTIFIER);

        return $this;
    }
}
