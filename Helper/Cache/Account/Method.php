<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Cache\Account;

use \InvalidArgumentException;
use \Resursbank\Checkout\Helper\Cache\General as Cache;

/**
 * Handle payment method collection and storing. Store methods from old orders
 * and the API within the database.
 *
 * @package Resursbank\Checkout\Helper\Cache\Account
 */
class Method extends Cache
{
    /**
     * @var string
     */
    const CACHE_KEY = 'resursbank_checkout_account_method';

    /**
     * Returns an array of the payment methods data stored in cache.
     *
     * @param string $key
     * @return array
     */
    public function load($key = self::CACHE_KEY)
    {
        $result = json_decode(parent::load($key), true);

        return is_array($result) ? $result : [];
    }

    /**
     * Saves data to the payment methods cache.
     *
     * @param array $data
     * @param string $key
     * @return $this
     * @throws InvalidArgumentException
     */
    public function save($data, $key = self::CACHE_KEY)
    {
        if (!is_array($data)) {
            throw new InvalidArgumentException(
                'Argument $data needs to be an array'
            );
        }

        parent::save(json_encode($data), $key);

        return $this;
    }
}