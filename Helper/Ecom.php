<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Helper;

use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ProductMetadataInterface;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\StoreId;
use Resursbank\Checkout\Model\Api\Credentials;
use Resursbank\RBEcomPHP\RESURS_FLOW_TYPES;
use Resursbank\RBEcomPHP\ResursBank;
use stdClass;

/**
 * This class is a wrapper for the ECom library.
 *
 * @package Resursbank\Checkout\Helper
 */
class Ecom extends AbstractHelper
{
    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @var Version
     */
    private $version;

    /**
     * @var ProductMetadataInterface
     */
    private $productMetadata;

    /**
     * @var StoreId
     */
    private $storeId;

    /**
     * @param ApiConfig $apiConfig
     * @param Version $version
     * @param ProductMetadataInterface $productMetadata
     * @param Context $context
     * @param StoreId $storeId
     */
    public function __construct(
        ApiConfig $apiConfig,
        Version $version,
        ProductMetadataInterface $productMetadata,
        Context $context,
        StoreId $storeId
    ) {
        $this->apiConfig = $apiConfig;
        $this->version = $version;
        $this->productMetadata = $productMetadata;
        $this->storeId = $storeId;

        parent::__construct($context);
    }

    /**
     * Retrieve ECom connection instance.
     *
     * @param Credentials|null $credentials
     * @return ResursBank
     * @throws Exception
     */
    public function getConnection(Credentials $credentials = null): ResursBank
    {
        $this->resolveCredentials($credentials);

        $connection = new ResursBank(
            $credentials->getUsername(),
            $credentials->getPassword(),
            $credentials->getEnvironment()
        );

        $connection->setSimplifiedPsp(true);

        $this->setUserAgent($connection);

        if ($this->storeId->hasStoreId()) {
            $connection->setStoreId($this->storeId->getStoreId());
        }

        if ($this->apiConfig->isSimplifiedFlow()) {
            $connection->setPreferredPaymentFlowService(
                RESURS_FLOW_TYPES::SIMPLIFIED_FLOW
            );
        } else {
            $connection->setPreferredPaymentFlowService(
                RESURS_FLOW_TYPES::RESURS_CHECKOUT
            );
        }

        // Turn off the bitmasking process in getPayment-status handler.
        $connection->setAutoDebitableTypes(false);

        // Deactivate getPayment validation on aftershop features.
        $connection->setGetPaymentValidation(false);

        // Deactivate order line validation on bookPayment features.
        $connection->setBookPaymentValidation(false);

        return $connection;
    }

    /**
     * @param ResursBank $connection
     * @throws Exception
     */
    private function setUserAgent(ResursBank &$connection)
    {
        $data = $this->version->getAll();
        $data['magento2'] = $this->productMetadata->getVersion();

        $connection->setUserAgent(json_encode($data));
    }

    /**
     * Resolve API credentials. If none are provided attempt to fetch them from
     * the configuration.
     *
     * @param Credentials|null $credentials
     * @throws Exception
     */
    public function resolveCredentials(Credentials &$credentials = null)
    {
        if (is_null($credentials) || !$credentials->hasCredentials()) {
            $credentials = $this->apiConfig->getCredentials();

            if (!$credentials->hasCredentials()) {
                throw new Exception('No API credentials provided.');
            }
        }
    }

    /**
     * Check if a provided payment session has a certain status applied.
     *
     * @param stdClass $session
     * @param string $status
     * @return bool
     */
    public function paymentSessionHasStatus(
        stdClass $session,
        string $status
    ): bool {
        $status = strtoupper((string) $status);

        return (is_object($session) &&
            isset($session->status) &&
            (
                (
                    is_array($session->status) &&
                    in_array($status, $session->status)
                ) ||
                (
                    is_string($session->status) &&
                    strtoupper($session->status) === $status
                )
            )
        );
    }
}
