<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Resursbank\Checkout\Helper\Order as OrderHelper;

/**
 * Methods to handle and manipulate shopping cart.
 *
 * @package Resursbank\Checkout\Helper
 */
class Cart extends AbstractHelper
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var Api
     */
    private $apiHelper;

    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @param OrderRepositoryInterface $orderRepository
     * @param Api $apiHelper
     * @param OrderHelper $orderHelper
     * @param Context $context
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        Api $apiHelper,
        OrderHelper $orderHelper,
        Context $context
    ) {
        $this->orderRepository = $orderRepository;
        $this->apiHelper = $apiHelper;
        $this->orderHelper = $orderHelper;

        parent::__construct($context);
    }

    /**
     * Rebuilds the cart from an order.
     *
     * @param Order $order
     * @param bool $cancelOrder
     * @throws NoSuchEntityException
     * @throws AlreadyExistsException
     * @throws InputException
     */
    public function rebuildCart(Order $order, bool $cancelOrder = true)
    {
        // Rebuild cart.
        $this->apiHelper->rebuildCart($order);

        // Cancel order.
        if ($cancelOrder) {
            $this->orderHelper->cancel($order);
        }
    }
}
