<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Logic\Account\Method;

use \Exception;
use \Resursbank\Checkout\Helper\Cache\Account\Method\Annuity as AnnuityCache;
use \Resursbank\Checkout\Helper\Config\Checkout\PartPayment as PartPaymentConfig;
use \Resursbank\Checkout\Helper\Db\Account\Method\Annuity as AnnuityDb;
use \Resursbank\Checkout\Helper\Db\Account\Method as MethodDb;
use \Resursbank\Checkout\Model\Account\Method as MethodModel;
use \Resursbank\Checkout\Helper\Log as Log;
use \Resursbank\Checkout\Model\ResourceModel\Account\Method\Annuity\Collection;
use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\App\Helper\Context;

/**
 * @package Resursbank\Checkout\Helper\Logic\Account\Method
 */
class Annuity extends AbstractHelper
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var AnnuityCache
     */
    private $cache;

    /**
     * @var AnnuityDb
     */
    private $annuityDb;

    /**
     * @var MethodDb
     */
    private $methodDb;

    /**
     * @var PartPaymentConfig
     */
    private $config;

    /**
     * PartPayment constructor.
     *
     * @param Context $context
     * @param Log $log
     * @param AnnuityCache $cache
     * @param AnnuityDb $annuityDb
     * @param MethodDb $methodDb
     * @param PartPaymentConfig $config
     */
    public function __construct(
        Context $context,
        Log $log,
        AnnuityCache $cache,
        AnnuityDb $annuityDb,
        MethodDb $methodDb,
        PartPaymentConfig $config
    ) {
        $this->context = $context;
        $this->log = $log;
        $this->cache = $cache;
        $this->annuityDb = $annuityDb;
        $this->methodDb = $methodDb;
        $this->config = $config;

        parent::__construct($context);
    }

    /**
     * Retrieve calculated part payment price.
     *
     * @param int $methodId
     * @param float $price
     * @param float $factor
     * @return float
     */
    public function calculatePartPaymentPrice(
        $methodId,
        $price,
        $factor
    ) {
        $price = (float) $price;
        $factor = (float) $factor;

        $result = 0;

        if ($factor > 0) {
            try {
                if ($this->isPartPaymentPrice($methodId, $price)) {
                    $result = round((($price * $factor) * 100) / 100, 2);
                }
            } catch (Exception $e) {
                $this->log->error($e);
            }
        }

        return $result;
    }

    /**
     * Check if price is eligible for part payment plans.
     *
     * @param int $methodId
     * @param float $price
     * @return bool
     * @throws Exception
     */
    public function isPartPaymentPrice($methodId, $price)
    {
        $result = false;

        $price = (float) $price;

        if ($price > 0) {
            /** @var MethodModel $method */
            $method = $this->methodDb->getModel()->load($methodId);

            $minPrice = (float)$method->getMinOrderTotal();
            $maxPrice = (float)$method->getMaxOrderTotal();
            $duration = $this->config->getDuration();

            if ($duration > 0) {
                $calculatedPrice = (float)$price / $duration;

                $result = (
                    $calculatedPrice >= $this->getThreshold() &&
                    $calculatedPrice >= $minPrice &&
                    $price <= $maxPrice
                );
            }
        }

        return $result;
    }

    /**
     * Get the product price threshold.
     *
     * @return float
     */
    public function getThreshold(): float
    {
        $result = 150.0;

        try {
            $result = $this->config->getThreshold();
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Fetches a list of all annuities for a given payment method id.
     *
     * @param int $methodId
     * @return array
     */
    public function getAnnuities($methodId)
    {
        $result = [];

        try {
            $cache = $this->cache->loadByMethodId($methodId);

            if (!empty($cache)) {
                $result = $cache;
            } else {
                /** @var Collection $collection */
                $collection = $this->annuityDb->filterByMethodId($methodId);
                $result = $collection->getData();
                $this->cache->saveByMethodId($result, $methodId);
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return is_array($result) ? $result : [];
    }

    /**
     * Get configured annuity factor for part payment price calculations.
     *
     * @param int $methodId
     * @param float $duration
     * @return float
     */
    public function getAnnuityFactorByDuration($methodId, $duration)
    {
        $factor = 0.0;
        $annuities = $this->getAnnuities($methodId);

        if (is_array($annuities)) {
            $filteredFactors = array_filter(
                $annuities,
                function ($item) use ($duration) {
                    return (float) $item['duration'] === (float) $duration;
                }
            );

            $factor = !empty($filteredFactors) ?
                array_pop($filteredFactors)['factor'] :
                0.0;
        }

        return (float) $factor;
    }
}
