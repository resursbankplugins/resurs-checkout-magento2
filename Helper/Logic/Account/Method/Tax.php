<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Logic\Account\Method;

use Exception;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Quote\Model\Quote;
use Magento\Tax\Api\Data\QuoteDetailsInterface;
use Magento\Tax\Api\Data\QuoteDetailsInterfaceFactory;
use Magento\Tax\Api\Data\QuoteDetailsItemInterface;
use Magento\Tax\Api\Data\QuoteDetailsItemInterfaceFactory;
use Magento\Tax\Api\Data\TaxClassKeyInterface;
use Magento\Tax\Api\Data\TaxClassKeyInterfaceFactory;
use Magento\Tax\Api\Data\TaxDetailsInterface;
use Magento\Tax\Api\TaxCalculationInterface;
use Magento\Tax\Model\TaxDetails\ItemDetails;
use Resursbank\Checkout\Helper\Config\Methods as MethodsConfig;
use Resursbank\Checkout\Model\Total\Quote as Total;

/**
 * Tax calculation operations for payment method fee.
 *
 * @package Resursbank\Checkout\Helper\Logic\Account\Method
 */
class Tax extends AbstractHelper
{
    /**
     * @var TaxClassKeyInterfaceFactory
     */
    private $taxClassKeyFactory;

    /**
     * @var QuoteDetailsInterfaceFactory
     */
    private $quoteDetailsFactory;

    /**
     * @var QuoteDetailsItemInterfaceFactory
     */
    private $quoteDetailsItemFactory;

    /**
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @var TaxCalculationInterface
     */
    private $taxCalculation;

    /**
     * @var MethodsConfig
     */
    private $methodsConfig;

    /**
     * @param Context $context
     * @param TaxClassKeyInterfaceFactory $taxClassKeyFactory
     * @param QuoteDetailsInterfaceFactory $quoteDetailsFactory
     * @param QuoteDetailsItemInterfaceFactory $quoteDetailsItemFactory
     * @param TaxCalculationInterface $taxCalculation
     * @param CustomerSession $customerSession
     * @param MethodsConfig $methodsConfig
     */
    public function __construct(
        Context $context,
        TaxClassKeyInterfaceFactory $taxClassKeyFactory,
        QuoteDetailsInterfaceFactory $quoteDetailsFactory,
        QuoteDetailsItemInterfaceFactory $quoteDetailsItemFactory,
        TaxCalculationInterface $taxCalculation,
        CustomerSession $customerSession,
        MethodsConfig $methodsConfig
    ) {
        $this->taxClassKeyFactory = $taxClassKeyFactory;
        $this->quoteDetailsFactory = $quoteDetailsFactory;
        $this->quoteDetailsItemFactory = $quoteDetailsItemFactory;
        $this->taxCalculation = $taxCalculation;
        $this->customerSession = $customerSession;
        $this->methodsConfig = $methodsConfig;

        parent::__construct($context);
    }

    /**
     * Retrieve tax price details object for payment method fee.
     *
     * @param float $price
     * @param Quote $quote
     * @param null $priceIncludesTax
     * @param bool $roundPrice
     * @return null|ItemDetails
     * @throws Exception
     */
    public function getTaxPrice(
        $price,
        Quote $quote,
        $priceIncludesTax = null,
        $roundPrice = true
    ) {
        $result = null;
        $price = (float) $price;

        if ($price > 0) {
            /** @var QuoteDetailsItemInterface $item */
            $item = $this->getQuoteItemDetails(
                $quote,
                $price,
                $priceIncludesTax
            );

            /** @var QuoteDetailsInterface $quoteDetails */
            $quoteDetails = $this->getQuoteDetails($quote, $item);

            /** @var TaxDetailsInterface $taxDetails */
            $taxDetails = $this->taxCalculation->calculateTax(
                $quoteDetails,
                $quote->getStoreId(),
                $roundPrice
            );

            $items = $taxDetails->getItems();

            if (!empty($items)) {
                $result = array_shift($items);
            }
        }

        return $result;
    }

    /**
     * Retrieve simulated quote item details for tax calculation.
     *
     * @param Quote $quote
     * @param float $price
     * @param float $priceIncludesTax
     * @return QuoteDetailsItemInterface
     * @throws Exception
     */
    private function getQuoteItemDetails(
        Quote $quote,
        $price,
        $priceIncludesTax
    ) {
        $taxClassKey = $this->taxClassKeyFactory->create();
        $taxClassKey->setType(TaxClassKeyInterface::TYPE_ID)
            ->setValue($this->methodsConfig->getTaxClass($quote->getStoreId()));

        $item = $this->quoteDetailsItemFactory->create();
        $item->setQuantity(1)
            ->setCode(Total::CODE)
            ->setShortDescription('Payment fee')
            ->setTaxClassKey($taxClassKey)
            ->setIsTaxIncluded($priceIncludesTax)
            ->setType('product')
            ->setUnitPrice($price);

        return $item;
    }

    /**
     * Retrieve simulated quote details for tax calculation.
     *
     * @param Quote $quote
     * @param QuoteDetailsItemInterface $item
     * @return QuoteDetailsInterface
     */
    private function getQuoteDetails(
        Quote $quote,
        QuoteDetailsItemInterface $item
    ) {
        $customerTaxClassKey = $this->taxClassKeyFactory->create();
        $customerTaxClassKey->setType(TaxClassKeyInterface::TYPE_ID)
            ->setValue($quote->getCustomerTaxClassId());

        $quoteDetails = $this->quoteDetailsFactory->create();
        $quoteDetails
            ->setShippingAddress($quote->getShippingAddress()->getDataModel())
            ->setBillingAddress($quote->getBillingAddress()->getDataModel())
            ->setCustomerTaxClassKey($customerTaxClassKey)
            ->setItems([$item])
            ->setCustomerId($this->customerSession->getCustomerId());

        return $quoteDetails;
    }
}
