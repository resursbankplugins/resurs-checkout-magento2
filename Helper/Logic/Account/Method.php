<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Helper\Logic\Account;

use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Checkout\Helper\Api\Account\Method\Annuity as ApiAnnuity;
use Resursbank\Checkout\Helper\Api\Account\Method\Annuity\Convert\Db as ApiAnnuityDbConverter;
use Resursbank\Checkout\Helper\Api\Account\Method as ApiMethod;
use Resursbank\Checkout\Helper\Api\Account\Method\Convert\Db as ApiMethodDbConverter;
use Resursbank\Checkout\Helper\Api\Credentials as ApiCredentials;
use Resursbank\Checkout\Helper\Cache\Account\Method as MethodCache;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Db\Account\Method\Annuity as AnnuityDb;
use Resursbank\Checkout\Helper\Db\Account\Method as MethodDb;
use Resursbank\Checkout\Helper\Db\Account\Method\Legacy as LegacyDb;
use Resursbank\Checkout\Helper\Log\Methods as MethodsLog;
use Resursbank\Checkout\Model\Account\Method as MethodModel;
use Resursbank\Checkout\Model\Api\Credentials;
use Zend_Db_Statement_Exception;

/**
 * Handle payment method collection and storing. Store methods from old orders
 * and the API within the database.
 *
 * @package Resursbank\Checkout\Helper\Logic\Account
 */
class Method extends AbstractHelper
{
    /**
     * @var string
     */
    const CODE_PREFIX = 'resursbank_';

    /**
     * @var float
     */
    const DEFAULT_MIN_ORDER_TOTAL = 150.0;

    /**
     * @var float
     */
    const DEFAULT_MAX_ORDER_TOTAL = 0.0;

    /**
     * @var string
     */
    const DEFAULT_ORDER_STATUS = Order::STATE_PENDING_PAYMENT;

    /**
     * @var ApiMethod
     */
    private $apiMethod;

    /**
     * @var ApiAnnuity
     */
    private $apiAnnuity;

    /**
     * @var ApiMethodDbConverter
     */
    private $apiMethodDbConverter;

    /**
     * @var ApiAnnuityDbConverter
     */
    private $apiAnnuityDbConverter;

    /**
     * @var MethodCache
     */
    private $methodCache;

    /**
     * @var MethodsLog
     */
    private $log;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ApiConfig
     */
    private $config;

    /**
     * @var MethodDb
     */
    private $methodDb;

    /**
     * @var LegacyDb
     */
    private $legacyDb;

    /**
     * @var AnnuityDb
     */
    private $annuityDb;

    /**
     * @var ApiCredentials
     */
    private $apiCredentials;

    /**
     * @param Context $context
     * @param ApiAnnuity $apiAnnuity
     * @param ApiMethod $apiMethod
     * @param ApiMethodDbConverter $apiMethodDbConverter
     * @param ApiAnnuityDbConverter $apiAnnuityDbConverter
     * @param MethodCache $methodCache
     * @param MethodsLog $log
     * @param StoreManagerInterface $storeManager
     * @param ApiConfig $config
     * @param MethodDb $methodDb
     * @param LegacyDb $legacyDb
     * @param AnnuityDb $annuityDb
     * @param ApiCredentials $apiCredentials
     */
    public function __construct(
        Context $context,
        ApiMethod $apiMethod,
        ApiAnnuity $apiAnnuity,
        ApiMethodDbConverter $apiMethodDbConverter,
        ApiAnnuityDbConverter $apiAnnuityDbConverter,
        MethodCache $methodCache,
        MethodsLog $log,
        StoreManagerInterface $storeManager,
        ApiConfig $config,
        MethodDb $methodDb,
        LegacyDb $legacyDb,
        AnnuityDb $annuityDb,
        ApiCredentials $apiCredentials
    ) {
        $this->apiMethod = $apiMethod;
        $this->apiAnnuity = $apiAnnuity;
        $this->apiMethodDbConverter = $apiMethodDbConverter;
        $this->apiAnnuityDbConverter = $apiAnnuityDbConverter;
        $this->methodCache = $methodCache;
        $this->log = $log;
        $this->storeManager = $storeManager;
        $this->config = $config;
        $this->methodDb = $methodDb;
        $this->legacyDb = $legacyDb;
        $this->annuityDb = $annuityDb;
        $this->apiCredentials = $apiCredentials;

        parent::__construct($context);
    }

    /**
     * Collect methods from the database (or cache if available).
     *
     * @return array
     * @throws Exception
     */
    public function collect(): array
    {
        // Attempt to load cached data.
        $result = $this->methodCache->load();

        if (empty($result)) {
            // Collect methods from the database.
            $result = $this->methodDb->collect();

            // Cache result. Avoid expiration since this won't change often.
            $this->methodCache->save($result);
        }

        return is_array($result) ? $result : [];
    }

    /**
     * Updates the database with legacy payment methods from old orders and new
     * methods from the API, syncing them to the database. Annuities for the
     * payment methods will also be synced to the database.
     *
     * @throws LocalizedException
     * @throws Zend_Db_Statement_Exception
     * @throws Exception
     * @return Method
     */
    public function syncAll(): Method
    {
        // Do not truncate methods table! We need to retain legacy methods
        // removed from Resurs Bank API.
        $this->methodDb->deactivateMethods();
        $this->annuityDb->truncate();

        $collection = $this->apiCredentials->getCollection(true);

        foreach ($collection as $credentials) {
            $methods = $this->apiMethod->collect($credentials['data']);

            foreach ($methods as $method) {
                $this->syncAnnuities(
                    $this->syncMethod(
                        $method,
                        $credentials['data'],
                        $credentials['country']
                    ),
                    $credentials['data']
                );
            }
        }

        $this->syncLegacyMethods();

        $this->methodCache->clean();
        $this->methodCache->cleanConfig();

        return $this;
    }

    /**
     * Syncs all legacy payment methods to the database. A legacy method is a
     * payment method which no longer exists in the Resurs Bank API, but has
     * been used previously to create orders.
     *
     * @return Method
     * @throws Zend_Db_Statement_Exception
     */
    public function syncLegacyMethods(): Method
    {
        foreach ($this->legacyDb->collect() as $method) {
            $this->legacyDb->sync($method);
        }

        return $this;
    }

    /**
     * Sync payment method API data to the database.
     *
     * @param array $method
     * @param Credentials $credentials
     * @param string $country
     * @return MethodModel
     * @throws Exception
     */
    public function syncMethod(
        array $method,
        Credentials $credentials,
        string $country
    ): MethodModel {
        $data = $this->apiMethodDbConverter->convert(
            $method,
            $credentials
        );

        return $this->methodDb->sync($data, $credentials, $country);
    }

    /**
     * Collect annuities for a payment method from the API and sync them to the
     * database.
     *
     * @param MethodModel $method
     * @param Credentials $credentials
     * @return Method
     * @throws Exception
     */
    public function syncAnnuities(
        MethodModel $method,
        Credentials $credentials
    ): Method {
        $annuities = $this->apiAnnuity->collect(
            $method->getIdentifier(),
            $credentials
        );

        /** @var array $annuity */
        foreach ($annuities as $annuity) {
            // Sync payment method annuity to database.
            $this->annuityDb->sync(
                $method->getId(),
                $this->apiAnnuityDbConverter->convert($annuity)
            );
        }

        return $this;
    }
}
