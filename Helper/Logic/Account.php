<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Logic;

use \Exception;
use \Magento\Framework\App\Helper\Context;
use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\ObjectManagerInterface;
use \Resursbank\Checkout\Model\Account as AccountModel;
use \Resursbank\Checkout\Model\Api\Credentials;

/**
 * @package Resursbank\Checkout\Helper\Logic
 */
class Account extends AbstractHelper
{
    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @param ObjectManagerInterface $objectManager
     * @param Context $context
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        Context $context
    ) {
        $this->objectManager = $objectManager;

        parent::__construct($context);
    }

    /**
     * Create account entry in database if it doesn't already exist.
     *
     * @param Credentials $credentials
     * @return $this
     * @throws Exception
     */
    public function createMissingAccount(Credentials $credentials)
    {
        if ($credentials->getUsername() !== '') {
            /** @var AccountModel $account */
            $account = $this->objectManager->create(AccountModel::class);
            $account->loadByCredentials($credentials);

            if (!$account->getId()) {
                $account->setData([
                    'username' => $credentials->getUsername(),
                    'environment' => $credentials->getEnvironment()
                ])->save();
            }
        }

        return $this;
    }
}
