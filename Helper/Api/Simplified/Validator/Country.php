<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Api\Simplified\Validator;

use Magento\Framework\App\Helper\AbstractHelper;
use Resursbank\Checkout\Model\Config\Source\Country as Countries;

/**
 * @package Resursbank\Checkout\Helper\Api\Simplified\Validator
 */
class Country extends AbstractHelper
{
    /**
     * Check if a country is eligible for Simplified Flow.
     *
     * @param string $countryId
     * @return boolean
     */
    public function validate($countryId)
    {
        return $this->isSweden($countryId) ||
            $this->isNorway($countryId) ||
            $this->isFinland($countryId);
    }

    /**
     * Check for Sweden's country ID.
     *
     * @param string $countryId
     * @return boolean
     */
    public function isSweden($countryId)
    {
        return $countryId === Countries::SWEDEN;
    }

    /**
     * Check for Norway's country ID.
     *
     * @param string $countryId
     * @return boolean
     */
    public function isNorway($countryId)
    {
        return $countryId === Countries::NORWAY;
    }

    /**
     * Check for Finland's country ID.
     *
     * @param string $countryId
     * @return boolean
     */
    public function isFinland($countryId)
    {
        return $countryId === Countries::FINLAND;
    }
}
