<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Api\Simplified\Validator;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Resursbank\Checkout\Model\Api;

/**
 * @package Resursbank\Checkout\Helper\Api\Simplified\Validator
 */
class Id extends AbstractHelper
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var Country
     */
    private $country;

    /**
     * @param Context $context
     * @param Country $country
     */
    public function __construct(
        Context $context,
        Country $country
    ) {
        $this->context = $context;
        $this->country = $country;

        parent::__construct($context);
    }

    /**
     * Validates an ssn or organization number. Accepts an optional boolean
     * parameter that states whether an empty id is valid.
     *
     * NOTE: Some countries do not require an SSN.
     *
     * @param string $num
     * @param string $country
     * @param string $customerType
     * @param boolean $allowEmptyId
     * @return boolean
     */
    public function validate(
        $num,
        $country,
        $customerType,
        $allowEmptyId = false
    ) {
        $result = false;

        if ($this->country->validate($country)) {
            if ($customerType === Api::CUSTOMER_TYPE_COMPANY) {
                $result = $this->org($num, $country, $allowEmptyId);
            } elseif ($customerType === Api::CUSTOMER_TYPE_PRIVATE) {
                $result = $this->ssn($num, $country, $allowEmptyId);
            }
        }

        return $result;
    }

    /**
     * Validates an SSN.
     *
     * NOTE: Validates for Sweden, Norway and Finland ONLY.
     *
     * @param string $num
     * @param string $country
     * @param boolean $allowEmptyId
     * @return boolean
     */
    public function ssn($num, $country, $allowEmptyId = false)
    {
        $result = false;

        if ($this->country->isSweden($country)) {
            $result = $this->swedenSsn($num, $allowEmptyId);
        } elseif ($this->country->isNorway($country)) {
            $result = $this->norwaySsn($num, $allowEmptyId);
        } elseif ($this->country->isFinland($country)) {
            $result = $this->finlandSsn($num, $allowEmptyId);
        }

        return $result;
    }

    /**
     * Validates an organisation number.
     *
     * NOTE: Validates for Sweden, Norway and Finland ONLY.
     *
     * @param string $num
     * @param string $country
     * @param boolean $allowEmptyId
     * @return boolean
     */
    public function org($num, $country, $allowEmptyId = false)
    {
        $result = false;

        if ($this->country->isSweden($country)) {
            $result = $this->swedenOrg($num, $allowEmptyId);
        } elseif ($this->country->isNorway($country)) {
            $result = $this->norwayOrg($num, $allowEmptyId);
        } elseif ($this->country->isFinland($country)) {
            $result = $this->finlandOrg($num, $allowEmptyId);
        }

        return $result;
    }

    /**
     * Validates a SSN for Sweden.
     *
     * @param string $num
     * @param boolean [$allowEmptyId]
     * @return boolean
     */
    public function swedenSsn($num, $allowEmptyId = false)
    {
        return $allowEmptyId && $num === '' ?
            true :
            (boolean) preg_match(
                '/^(18\d{2}|19\d{2}|20\d{2}|\d{2})' .
                '(0[1-9]|1[0-2])' .
                '([0][1-9]|[1-2][0-9]|3[0-1])' .
                '(\-|\+)?([\d]{4})$/',
                $num
            );
    }

    /**
     * Validates an organisation number for Sweden.
     *
     * @param string $num
     * @param boolean [$allowEmptyId]
     * @return boolean
     */
    public function swedenOrg($num, $allowEmptyId = false)
    {
        return $allowEmptyId && $num === '' ?
            true :
            (boolean) preg_match(
                '/^(16\d{2}|18\d{2}|19\d{2}|20\d{2}|\d{2})' .
                '(\d{2})(\d{2})(\-|\+)?([\d]{4})$/',
                $num
            );
    }

    /**
     * Validates a SSN for Norway.
     *
     * @param string $num
     * @param boolean [$allowEmptyId]
     * @return boolean
     */
    public function norwaySsn($num, $allowEmptyId = false)
    {
        return $allowEmptyId && $num === '' ?
            true :
            (boolean) preg_match(
                '/^([0][1-9]|[1-2][0-9]|3[0-1])' .
                '(0[1-9]|1[0-2])(\d{2})(\-)?([\d]{5})$/',
                $num
            );
    }

    /**
     * Validates an organisation number for Norway.
     *
     * @param string $num
     * @param boolean [$allowEmptyId]
     * @return boolean
     */
    public function norwayOrg($num, $allowEmptyId = false)
    {
        return $allowEmptyId && $num === '' ?
            true :
            (boolean) preg_match('/^([89]([ |-]?[0-9]){8})$/', $num);
    }

    /**
     * Validates a SSN for Finland.
     *
     * @param string $num
     * @param boolean [$allowEmptyId]
     * @return boolean
     */
    public function finlandSsn($num, $allowEmptyId = false)
    {
        return $allowEmptyId && $num === '' ?
            true :
            (boolean) preg_match(
                '/^([\d]{6})[\+-A]([\d]{3})' .
                '([0123456789ABCDEFHJKLMNPRSTUVWXY])$/',
                $num
            );
    }

    /**
     * Validates an organisation number for Finland.
     *
     * @param string $num
     * @param boolean [$allowEmptyId]
     * @return boolean
     */
    public function finlandOrg($num, $allowEmptyId = false)
    {
        return $allowEmptyId && $num === '' ?
            true :
            (boolean) preg_match('/^((\d{7})(\-)?\d)$/', $num);
    }
}
