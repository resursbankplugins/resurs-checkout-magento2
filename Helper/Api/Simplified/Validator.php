<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Api\Simplified;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Resursbank\Checkout\Helper\Api\Simplified\Validator\Card;
use Resursbank\Checkout\Helper\Api\Simplified\Validator\Country;
use Resursbank\Checkout\Helper\Api\Simplified\Validator\Id;

/**
 * @package Resursbank\Checkout\Helper\Api\Simplified
 */
class Validator extends AbstractHelper
{
    /**
     * @var Id
     */
    private $id;

    /**
     * @var Country
     */
    private $country;

    /**
     * @var Card
     */
    private $card;

    /**
     * @var Context
     */
    private $context;

    /**
     * @param Context $context
     * @param Id $id
     * @param Country $country
     * @param Card $card
     */
    public function __construct(
        Context $context,
        Id $id,
        Country $country,
        Card $card
    ) {
        $this->context = $context;
        $this->id = $id;
        $this->country = $country;
        $this->card = $card;

        parent::__construct($context);
    }

    /**
     * Returns the Id validator.
     *
     * @return Id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the Card validator.
     *
     * @return Card
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * Returns the Country validator.
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }
}
