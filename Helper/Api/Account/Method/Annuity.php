<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Api\Account\Method;

use \Exception;
use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\App\Helper\Context;
use \Resursbank\Checkout\Model\Api\Credentials;
use \Resursbank\Checkout\Helper\Ecom;

/**
 * Class Annuity
 *
 * @package Resursbank\Checkout\Helper\Api\Account\Method
 */
class Annuity extends AbstractHelper
{
    /**
     * @var string
     */
    const DURATION = 'duration';

    /**
     * @var string
     */
    const FACTOR = 'factor';

    /**
     * @var string
     */
    const PAYMENT_PLAN_NAME = 'paymentPlanName';

    /**
     * @var Ecom
     */
    private $ecom;

    /**
     * @param Context $context
     * @param Ecom $ecom
     */
    public function __construct(
        Context $context,
        Ecom $ecom
    ) {
        $this->ecom = $ecom;

        parent::__construct($context);
    }

    /**
     * Collect all available payment methods from the API.
     *
     * @param string $methodIdentifier
     * @param Credentials|null $credentials
     * @return array
     * @throws Exception
     */
    public function collect($methodIdentifier, Credentials $credentials = null)
    {
        $result = $this->ecom
            ->getConnection($credentials)
            ->getAnnuityFactors($methodIdentifier);

        if (is_array($result)) {
            // The raw result from ECom will be an array of stdClass instances,
            // we want a nestled array. To achieve this we can simply
            // json_encode the raw value, and the decode it again.
            $result = json_decode(json_encode($result), true);
        }

        return is_array($result) ? $result : [];
    }
}
