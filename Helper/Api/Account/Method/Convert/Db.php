<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Helper\Api\Account\Method\Convert;

use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Resursbank\Checkout\Helper\Api\Account\Method as MethodApi;
use Resursbank\Checkout\Helper\Logic\Account\Method;
use Resursbank\Checkout\Model\Account\Method as MethodModel;
use Resursbank\Checkout\Model\Api\Credentials;
use Resursbank\Checkout\Model\Payment\Standard;

/**
 * Collect payment method annuities from the database.
 *
 * @package Resursbank\Checkout\Helper\Api\Account\Method\Convert
 */
class Db extends AbstractHelper
{
    /**
     * Convert payment method data from the API to data accepted by the
     * database.
     *
     * @param array $data
     * @param Credentials $credentials
     * @return array
     * @throws Exception
     */
    public function convert(array $data, Credentials $credentials): array
    {
        $result = [];

        if ($this->validate($data)) {
            $result[MethodModel::CODE] = $this->getCode($data, $credentials);
            $result[MethodModel::TITLE] = $this->getTitle($data);
            $result[MethodModel::IDENTIFIER] = $this->getIdentifier($data);

            $result[MethodModel::MIN_ORDER_TOTAL] =
                $this->getMinOrderTotal($data);

            $result[MethodModel::MAX_ORDER_TOTAL] =
                $this->getMaxOrderTotal($data);

            $result[MethodModel::RAW] = json_encode($data);
        }

        return $result;
    }

    /**
     * Check if raw data can be converted into a method.
     *
     * @param mixed $data
     * @return bool
     */
    private function validate(array $data): bool
    {
        return isset($data[MethodApi::IDENTIFIER]);
    }

    /**
     * Get payment method title from raw data.
     *
     * @param array $method
     * @return string
     */
    private function getTitle(array $method): string
    {
        return isset($method[MethodApi::TITLE]) ?
            (string) $method[MethodApi::TITLE] :
            Standard::TITLE;
    }

    /**
     * Get payment method code from raw data.
     *
     * @param array $method
     * @param Credentials $credentials
     * @return string
     * @throws Exception
     */
    private function getCode(array $method, Credentials $credentials): string
    {
        return Method::CODE_PREFIX .
            strtolower($this->getIdentifier($method)) .
            '_' .
            $credentials->getMethodSuffix();
    }

    /**
     * Retrieve 'min_order_total' value from raw data.
     *
     * @param array $method
     * @return float
     */
    private function getMinOrderTotal(array $method): float
    {
        return isset($method[MethodApi::MIN_ORDER_TOTAL]) ?
            (float) $method[MethodApi::MIN_ORDER_TOTAL] :
            Method::DEFAULT_MIN_ORDER_TOTAL;
    }

    /**
     * Retrieve 'max_order_total' value from raw data.
     *
     * @param array $method
     * @return float
     */
    private function getMaxOrderTotal(array $method): float
    {
        return isset($method[MethodApi::MAX_ORDER_TOTAL]) ?
            (float) $method[MethodApi::MAX_ORDER_TOTAL] :
            Method::DEFAULT_MAX_ORDER_TOTAL;
    }

    /**
     * Retrieve 'identifier' value from raw data.
     *
     * @param array $method
     * @return string
     * @throws Exception
     */
    private function getIdentifier(array $method): string
    {
        if (!isset($method[MethodApi::IDENTIFIER]) ||
            !is_string($method[MethodApi::IDENTIFIER])
        ) {
            throw new Exception(
                'No method identifier found in ' . json_encode($method)
            );
        }

        return $method[MethodApi::IDENTIFIER];
    }
}
