<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Api\Account\Method\Annuity\Convert;

use \Magento\Framework\App\Helper\AbstractHelper;
use \Resursbank\Checkout\Model\Account\Method\Annuity as AnnuityModel;
use \Resursbank\Checkout\Helper\Api\Account\Method\Annuity;

/**
 * Collect payment method annuities from the database.
 *
 * @package Resursbank\Checkout\Helper\Api\Account\Method\Annuity\Convert
 */
class Db extends AbstractHelper
{
    /**
     * Convert payment method data from the API to data accepted by the
     * database.
     *
     * @param array $data
     * @return array
     */
    public function convert(array $data)
    {
        $result = [];

        if ($this->validate($data)) {
            $result[AnnuityModel::DURATION] = $this->getDuration($data);
            $result[AnnuityModel::TITLE] = $this->getTitle($data);
            $result[AnnuityModel::FACTOR] = $this->getFactor($data);
        }

        return $result;
    }

    /**
     * Validate raw annuity data.
     *
     * @param array $data
     * @return bool
     */
    public function validate(array $data)
    {
        return (
            isset($data[Annuity::DURATION]) &&
            isset($data[Annuity::FACTOR]) &&
            isset($data[Annuity::PAYMENT_PLAN_NAME])
        );
    }

    /**
     * Get payment method title from raw data.
     *
     * @param array $data
     * @return string
     */
    public function getTitle(array $data)
    {
        return (string) $data[Annuity::PAYMENT_PLAN_NAME];
    }

    /**
     * Get payment method code from raw data.
     *
     * @param array $data
     * @return string
     */
    public function getFactor(array $data)
    {
        return (string) $data[Annuity::FACTOR];
    }

    /**
     * Retrieve 'min_order_total' value from raw data.
     *
     * @param array $data
     * @return float
     */
    public function getDuration(array $data)
    {
        return (string) $data[Annuity::DURATION];
    }
}
