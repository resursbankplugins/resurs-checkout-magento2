<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Api\Account;

use \Exception;
use \Magento\Framework\App\Helper\AbstractHelper;
use \Magento\Framework\App\Helper\Context;
use \Resursbank\Checkout\Model\Api\Credentials;
use \Resursbank\Checkout\Helper\Ecom;

/**
 * Class Method
 *
 * @package Resursbank\Checkout\Helper\Api\Account
 */
class Method extends AbstractHelper
{
    /**
     * @var string
     */
    const TITLE = 'description';

    /**
     * @var string
     */
    const IDENTIFIER = 'id';

    /**
     * @var string
     */
    const MIN_ORDER_TOTAL = 'minLimit';

    /**
     * @var string
     */
    const MAX_ORDER_TOTAL = 'maxLimit';

    /**
     * @var Ecom
     */
    private $ecom;

    /**
     * @param Context $context
     * @param Ecom $ecom
     */
    public function __construct(
        Context $context,
        Ecom $ecom
    ) {
        $this->ecom = $ecom;

        parent::__construct($context);
    }

    /**
     * Collect all available payment methods from the API.
     *
     * @param Credentials|null $credentials
     * @return array
     * @throws Exception
     */
    public function collect(Credentials $credentials = null)
    {
        $result = $this->ecom->getConnection($credentials)->getPaymentMethods();

        if (is_array($result)) {
            // The raw result from ECom will be an array of stdClass instances,
            // we want a nestled array. To achieve this we can simply
            // json_encode the raw value, and the decode it again.
            $result = json_decode(json_encode($result), true);
        }

        return is_array($result) ? $result : [];
    }
}
