<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper\Api;

use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Model\Api\Credentials as ApiCredentials;

/**
 * @package Resursbank\Checkout\Helper\Api
 */
class Credentials extends AbstractHelper
{
    /**
     * @var ApiConfig
     */
    private $config;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param ApiConfig $config
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        ApiConfig $config
    ) {
        $this->storeManager = $storeManager;
        $this->config = $config;

        parent::__construct($context);
    }

    /**
     * Returns all configured API credentials for all store views.
     *
     * @param bool $unique
     * @return array
     * @throws Exception
     */
    public function getCollection($unique = false)
    {
        $list = [];

        foreach ($this->storeManager->getStores() as $store) {
            /** @var ApiCredentials $credentials */
            $credentials = $this->config->getCredentials($store->getCode());

            // Never process the same API account twice.
            if (!$unique || !array_key_exists($credentials->getHash(), $list)) {
                $list[$credentials->getHash()] = [
                    'data' => $credentials,
                    'country' => $this->config->getCountry($store->getCode())
                ];
            }
        }

        return $list;
    }
}
