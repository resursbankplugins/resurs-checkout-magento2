<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Sales\Model\Order\Creditmemo as CreditmemoModel;
use Resursbank\Checkout\Model\Total\Quote;
use Resursbank\Checkout\Helper\Payment\Method;

/**
 * @package Resursbank\Checkout\Helper
 */
class Creditmemo extends AbstractHelper
{
    /**
     * @var Method
     */
    private $method;

    /**
     * @param Context $context
     * @param Method $method
     */
    public function __construct(
        Context $context,
        Method $method
    ) {
        $this->method = $method;

        parent::__construct($context);
    }

    /**
     * Retrieve payment method fee excl. tax from creditmemo.
     *
     * @param CreditmemoModel $model
     * @return float
     */
    public function getPaymentMethodFeeAmount(CreditmemoModel $model): float
    {
        return (float) $model->getData(Quote::CODE . '_amount');
    }

    /**
     * Retrieve payment method fee tax amount from creditmemo.
     *
     * @param CreditmemoModel $model
     * @return float
     */
    public function getPaymentMethodFeeTaxAmount(CreditmemoModel $model): float
    {
        return (float) $model->getData(Quote::CODE . '_tax_amount');
    }

    /**
     * Calculate and return VAT percentage for payment method fee applied on
     * creditmemo.
     *
     * @param CreditmemoModel $model
     * @return float
     */
    public function getPaymentMethodFeeVatPercentage(
        CreditmemoModel $model
    ): float {
        return $this->method->calculateFeeVatPercentage(
            $this->getPaymentMethodFeeAmount($model),
            $this->getPaymentMethodFeeTaxAmount($model)
        );
    }
}
