<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Helper\Checkout;

use Exception;
use Magento\Checkout\Model\Session;
use Magento\Checkout\Model\Type\Onepage as OnepageModel;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;

/**
 * @package Resursbank\Checkout\Helper\Checkout
 */
class Onepage extends AbstractHelper
{
    /**
     * @var OnepageModel
     */
    private $onepage;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @param OnepageModel $onepage
     * @param OrderRepositoryInterface $orderRepository
     * @param Context $context
     */
    public function __construct(
        OnepageModel $onepage,
        OrderRepositoryInterface $orderRepository,
        Context $context
    ) {
        $this->onepage = $onepage;
        $this->orderRepository = $orderRepository;

        parent::__construct($context);
    }

    /**
     * Returns current order.
     *
     * @return Order
     * @throws Exception
     */
    public function getCurrentOrder(): Order
    {
        /** @var Session $session */
        $session = $this->onepage->getCheckout();

        /** @var int $orderId */
        $orderId = (int)$session->getLastOrderId();

        if (!$orderId) {
            throw new Exception(
                'Could not find previous order to rebuild cart ' .
                'contents from.'
            );
        }

        /** @var Order $order */
        $order = $this->orderRepository->get($orderId);

        if (!$order->getId()) {
            throw new Exception(
                'Failed to load order with id ' . $orderId
            );
        }

        return $order;
    }

    /**
     * Checks whether or not an order is in progress.
     *
     * @return bool
     */
    public function hasCurrentOrder(): bool
    {
        $result = true;

        try {
            $this->getCurrentOrder();
        } catch (Exception $e) {
            $result = false;
        }

        return $result;
    }

    /**
     * Get frontend checkout session object
     *
     * @return Session
     */
    public function getSession()
    {
        return $this->onepage->getCheckout();
    }
}
