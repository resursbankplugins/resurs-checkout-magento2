<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Helper;

use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;
use Magento\Store\Model\Store;
use Resursbank\Checkout\Api\Data\PaymentHistoryInterface;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Model\Account as AccountModel;
use Resursbank\Checkout\Model\Api\Credentials;
use Resursbank\RBEcomPHP\RESURS_CALLBACK_TYPES;

/**
 * Callback related methods.
 *
 * @package Resursbank\Checkout\Helper
 */
class Callback extends AbstractHelper
{
    /**
     * Number of characters in randomly generated callback salt.
     *
     * @var int
     */
    const SALT_LENGTH = 64;

    /**
     * @var General
     */
    private $generalHelper;

    /**
     * @var Ecom
     */
    private $ecom;

    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * Callback constructor.
     *
     * @param Context $context
     * @param General $generalHelper
     * @param OrderFactory $orderFactory
     * @param ObjectManagerInterface $objectManager
     * @param ApiConfig $apiConfig
     * @param RequestInterface $request
     */
    public function __construct(
        Context $context,
        General $generalHelper,
        OrderFactory $orderFactory,
        ObjectManagerInterface $objectManager,
        ApiConfig $apiConfig,
        Ecom $ecom,
        RequestInterface $request
    ) {
        $this->generalHelper = $generalHelper;
        $this->orderFactory = $orderFactory;
        $this->objectManager = $objectManager;
        $this->apiConfig = $apiConfig;
        $this->ecom = $ecom;
        $this->request = $request;

        parent::__construct($context);
    }

    /**
     * @param string $paymentId
     * @return Order
     * @throws Exception
     */
    public function getOrderFromRequest(string $paymentId): Order
    {
        if (empty($paymentId)) {
            throw new Exception('No order payment reference supplied.');
        }

        /** @var Order $order */
        $order = $this->orderFactory->create();
        $order->loadByAttribute('increment_id', $paymentId);

        if (!($order instanceof Order) || !$order->getId()) {
            throw new Exception('Failed to locate referenced order.');
        }

        return $order;
    }

    /**
     * Register all callback methods.
     *
     * @return $this
     * @param Store $store
     * @throws Exception
     */
    public function register(Store $store)
    {
        $credentials = $this->apiConfig->getCredentials($store->getCode());

        // Generate new SALT.
        $salt = $this->generateSalt($credentials);

        // Clean up prior callbacks that could conflict with the new setup below.
        $this->ecom->getConnection($credentials)->unregisterEventCallback(
            RESURS_CALLBACK_TYPES::AUTOMATIC_FRAUD_CONTROL +
            RESURS_CALLBACK_TYPES::ANNULMENT +
            RESURS_CALLBACK_TYPES::FINALIZATION,
            true
        );

        // Callback types.
        $types = [
            'unfreeze',
            'booked',
            'update',
            'test'
        ];

        // Register all callbacks.
        foreach ($types as $type) {
            $this->ecom->getConnection($credentials)->setRegisterCallback(
                constant('Resursbank\RBEcomPHP\RESURS_CALLBACK_TYPES::' . strtoupper($type)),
                $this->urlCallbackTemplate($store, $type),
                ['digestSalt' => $salt]
            );
        }

        return $this;
    }

    /**
     * Generate SALT key and associate with provided Credentials instance.
     *
     * @param Credentials $credentials
     * @return string
     */
    public function generateSalt(Credentials $credentials): string
    {
        /** @var string $salt */
        $salt = '';

        /** @var AccountModel $account */
        $account = $this->objectManager->create(AccountModel::class);
        $account->loadByCredentials($credentials);

        if ($account->getId()) {
            $salt = $this->generalHelper->strRand(self::SALT_LENGTH);

            $account->setSalt($salt)->save();
        }

        return $salt;
    }

    /**
     * Retrieve salt key associated with provided Credentials instance.
     *
     * @param Credentials $credentials
     * @return string
     */
    public function getSalt(Credentials $credentials): string
    {
        $salt = '';

        /** @var AccountModel $account */
        $account = $this->objectManager->create(AccountModel::class);
        $account->loadByCredentials($credentials);

        if ($account->getId()) {
            $salt = $account->getSalt();
        }

        return (string) $salt;
    }

    /**
     * Get callback method event identifier.
     *
     * @param string $method
     * @return int
     */
    public function getMethodEvent(string $method): int
    {
        $result = 0;

        switch ($method) {
            case 'unfreeze':
                $result = PaymentHistoryInterface::EVENT_CALLBACK_UNFREEZE;
                break;
            case 'booked':
                $result = PaymentHistoryInterface::EVENT_CALLBACK_BOOKED;
                break;
        }

        return $result;
    }

    /**
     * Get list of all registered callbacks.
     *
     * @param Credentials $credentials
     * @return array
     * @throws Exception
     */
    public function getCallbacks(Credentials $credentials)
    {
        $result = $this->ecom->getConnection($credentials)
            ->getCallBacksByRest(true);

        return is_array($result) ? $result : [];
    }

    /**
     * Retrieve callback URL template.
     *
     * @param string $type
     * @return string
     * @throws Exception
     */
    private function urlCallbackTemplate(Store $store, string $type) : string
    {
        $suffix = $type === 'test' ?
            'param1/Alpha/param2/Bravo/param3/Charlie/param4/Delta/param5/Echo/' :
            'paymentId/{paymentId}/digest/{digest}';

        return (
            $store->getBaseUrl(UrlInterface::URL_TYPE_LINK, $this->request->isSecure()) .
            "rest/V1/resursbank_checkout/order/{$type}/{$suffix}"
        );
    }
}
