<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Helper;

use Exception;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\FileSystemException;
use Magento\Store\Model\ScopeInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Resursbank\Checkout\Helper\Config\Checkout\Debug as DebugConfig;

/**
 * Class Log
 *
 * @package Resursbank\Checkout\Helper
 */
class Log extends AbstractHelper
{
    /**
     * @var Logger
     */
    protected $log;

    /**
     * @var DirectoryList
     */
    protected $directories;

    /**
     * @var DebugConfig
     */
    protected $debugConfig;

    /**
     * Whether or not debugging is enabled.
     *
     * @var bool
     */
    protected $enabled;

    /**
     * Filename where entries are stored.
     *
     * @var string
     */
    protected $file = 'resursbank_general';

    /**
     * @param DirectoryList $directories
     * @param Context $context
     * @param DebugConfig $debugConfig
     * @throws FileSystemException
     * @throws Exception
     */
    public function __construct(
        DirectoryList $directories,
        Context $context,
        DebugConfig $debugConfig
    ) {
        $this->directories = $directories;
        $this->debugConfig = $debugConfig;

        $this->log = new Logger('Resursbank Debug Log');
        $this->log->pushHandler(
            new StreamHandler(
                $this->directories->getPath('var') . "/log/{$this->file}.log",
                Logger::INFO,
                false
            )
        );

        parent::__construct($context);
    }

    /**
     * Log info message.
     *
     * @param array|string|Exception $text
     * @param bool $force
     * @return $this
     */
    public function info($text, $force = false)
    {
        if ($this->isEnabled($force)) {
            $this->log->info($this->prepareMessage($text));
        }

        return $this;
    }

    /**
     * Log error message.
     *
     * @param array|string|Exception $text
     * @param bool $force
     * @return $this
     */
    public function error($text, $force = false)
    {
        if ($this->isEnabled($force)) {
            $this->log->error($this->prepareMessage($text));
        }

        return $this;
    }

    /**
     * Prepare message before adding it to a log file.
     *
     * @param array|string|Exception $text
     * @return string
     */
    public function prepareMessage($text)
    {
        if (is_array($text)) {
            $text = json_encode($text);
        } elseif ($text instanceof Exception) {
            $text = $text->getFile()
                . ' :: '
                . $text->getLine()
                . '   -   '
                . $text->getMessage()
                . '   -   '
                . $text->getTraceAsString();
        }

        return (string) $text;
    }

    /**
     * Check if debugging is enabled.
     *
     * @param bool $force
     * @return bool
     */
    public function isEnabled($force = false)
    {
        if (!$force && $this->enabled === null) {
            $this->enabled = $this->debugConfig->isEnabled();
        }

        return $force || $this->enabled;
    }
}
