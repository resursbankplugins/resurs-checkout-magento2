<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Helper\Payment;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

/**
 * @package Resursbank\Checkout\Helper\Payment
 */
class Method extends AbstractHelper
{
    /**
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Calculate and return VAT percentage for payment method fee.
     *
     * @param float $feeAmount
     * @param float $taxAmount
     * @return float
     */
    public function calculateFeeVatPercentage(
        float $feeAmount,
        float $taxAmount
    ): float {
        return $feeAmount > 0.0 ? ($taxAmount / $feeAmount) * 100 : 0.0;
    }
}
