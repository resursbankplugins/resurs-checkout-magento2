# Resurs Bank - Magento 2 module - Checkout

## Description

A payment and checkout module from Resurs Bank.

---

## Prerequisites

* [Magento 2](https://devdocs.magento.com/guides/v2.3/install-gde/bk-install-guide.html) [Supports Magento 2.2+]
* SOAP for your version of PHP (for example php5-soap or php7.0-soap).

---

## Installing

You will first need to install the composer package.

Navigate to your Magento installation directory:

```
cd /path/to/your/magento/installation
```

Now execute the following commands (note that these instructions assume your PHP
 and Composer installations are included within your PATH):

```
composer require resursbank/checkout
php bin/magento setup:upgrade
```

You will now need to apply some basic configuration for the module.

1. Go into your admin panel, and navigate to **Stores -> Configuration
-> Resurs Bank -> Checkout**.

2. You need to specify your `Username` and `Password` in the `API Settings`
section.

3. Select desired `Flow` in the `API Settings` section. `Checkout` means the
customer will be redirected to Resurs Bank's one page checkout. `Simplified`
means the regular checkout process will be used.

4. Specify `Unit Measure` in the `API Settings` section (it can be anything you
like, but it's required for API calls to function so **please do not leave this
blank**).

5. Select the appropriate `Envrionment` in the `API Settings` section (`Test` or
`Production` depending on your intent). 

6. Please press the `Sync` button in the `Methods` section. This will cause
payment methods to be fetched from the API and stored locally within your
database. Whenever changes are applied to your payment methods at Resurs Bank,
or new methods are added, you will need to sync those changes in Magento.
You can also specify to sync methods automatically at regular intervals
using the `Automatically sync payment methods` setting in the `Methods`
section).

7. Please press the `Update callbacks` button in the `Callbacks` section. This
will register callback URLs at Resurs Bank so the API can report events on
payments to your Magento installation.

8. If you have minified JavaScript/CSS please be sure to reload it after
installing the module to ensure the checkout page will function and appear
properly.

9. If you are using any cache/performance services such as memcache or
Zend_OPcach, please read the related chapters in the Debug section to ensure
that no cache interferes with your installation.

10. If you experience errors during the checkout process, please try
re-compiling your DI before taking other measures:

```
cd /path/to/your/magento/installation
php bin/magento setup:di:compile
```

**_NOTE: You need the SOAP extension for your version of PHP (for example
php5-soap or php7.0-soap). The extension is used by `resursbank/ecomphp` to
communicate with Resurs Bank when handling existing payments._**

**_NOTE: Don’t forget to exclude `index.php` from your base URL, otherwise there
may be issues during the checkout process._**

---

## Checkout Flow

![Checkout](./Images/Frontend/frontend.png)

This is what the frontend checkout page will look like when the module is 
enabled and `Checkout` is the selected `Flow`.

As you can see, the page is divided into three parts. At the top is our cart 
with controls to add/remove/edit individual items; in the middle you can choose 
shipping method and apply a discount code; and at the bottom you fill out your 
address information and select a payment method.

**_NOTE: Actions, such as selecting a shipping method, changing item quantity 
etc. are all performed through AJAX calls, which are queued to prevent race 
conditions._**

### Cart

![Checkout cart](./Images/Frontend/cart.png)

As you change either the quantity of an item or remove an item from the cart, 
the module will update the grand total, shipping methods and any applied 
discount. When you change quantity or remove an item the corresponding row in 
the cart table will temporarily be disabled.

**NOTE: If you change the quantity to 0 (zero) the item will be removed from the 
cart.**

### Shipping methods

![Checkout shipping methods](./Images/Frontend/shipping-methods.png)

As you change shipping methods the module will update the grand total. This will 
temporarily disable interaction with shipping methods.

### Discount code

![Checkout discount code](./Images/Frontend/discount.png)

When you apply a discount code, the module will temporarily disable the discount 
code block. When completed, it will display the outcome below the input field: 
either a red text stating it failed, or a green one stating what amount was 
withdrawn from the grand total.

**_NOTE: The outcome is displayed temporarily in case of failure, but 
permanently when successful._**

### The iframe

**_NOTE: If you encounter any problems with the iframe, first check that you 
exclude `index.php` from your base URL._**

The iframe communicates with the website through the `window.postMessage()`API. 
It enables safe cross-origin communication between websites when properly used. 
We use it to send and receive messages regarding events and changes applied to 
the information within the iframe. Do note that if you're going to use this API 
to extend or modify any existing functionality, **_you MUST know how to use it 
properly! Therefore, here are some useful warnings and advice:_**

**_WARNING: Please be aware that improper use of this API can leave holes in 
your security that attackers can exploit to perform attacks like cross-site 
scripting!_**

**_It is HIGHLY recommended that you thoroughly read the MDN article and get a 
good understanding of the window.postMessage() API before using it: 
https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage._**

### Data syncing

The module uses AJAX to update the quote object in the client session when you:

* Change the quantity of an item.
* Remove an item from the cart.
* Apply a discount code.
* Select a shipping method.
* Enter billing/shipping information.
* Select a payment method.

During these AJAX calls, the module will update any affected parts as a result 
of the change applied. Subsequent requests are queued to prevent race conditions 
and unexpected behaviour.

### Order placement

The order is placed when the client clicks their confirmation button to complete 
their purchase. If this process would for any reason fail, the user's shopping 
cart will be rebuilt and as much information as possible will be automatically 
filled out again to minimize the number of steps required to try placing the 
order again. Remaining required information needs to be provided by the user 
manually.

**_NOTE: The order confirmation email won't be sent after the order has been 
created (more on this in the section about callbacks)._**

---

## Simplified Flow

Use the regular checkout with Resurs Banks methods appearing as any other
payment method.

### Enable simplified flow

1. In the backend, navigate to **Stores -> Configuration -> Resurs Bank ->
Checkout -> API Settings**
2. Set `Flow` to `Simplified`.
3. Set `Country` to the country where your store is located. The country you 
choose will have an impact on how certain elements are displayed and behave 
during the checkout process (further information can be found below). The
country you select here should match the country configured on your API account.
4. Save configuration.
5. Navigate to **Sales -> Tax -> Default Tax Destination Calculation**
6. Set `Default Country` to the one you selected in step 3. If you don't do 
this, certain parts of the module may not be visible or work properly.
7. Save configuration.
8. Navigate to **Stores -> Configuration -> General -> General -> Country
Options**.
9. Set `Default Country` to the one you selected in step 3. If you don't do 
this, certain parts of the module may not be visible or work properly.
10. Navigate to **System -> Cache Management**
11. Select all caches, then select **Refresh** in the drop-down list and press
**Submit**.

## The checkout process

When **Simplified flow** has been enabled the Magento checkout page will have
some additional fields. Let's go through them in order.

### SSN field

In the SSN field the user can specify their social security number (SSN), or 
organization number (Org. number), and are able to fetch their address.

**_NOTE: address fetching is currently only available in Sweden_**

#### Sweden

![Simplified flow - Sweden address field - Fetch](./Images/Frontend/simplified_flow/address_not_fetched.png)

When the country is set to Sweden the widget will display a **Fetch address**
button which allows customers to fetch their address information from the Resurs 
Bank API.

![Simplified flow - Sweden address field - Not you?](./Images/Frontend/simplified_flow/address_not_you.png)

After you've fetched your address the SSN field will be disabled and the button 
will be replaced by a "**Not you?**" button. Clicking this button will clear
your address information as well as the provided SSN.

![Simplified flow - Address fetched](./Images/Frontend/simplified_flow/address_fetched.png)

Address fetched from the API.

#### Norway & Finland

![Simplified flow - Norway / Finland address field](./Images/Frontend/simplified_flow/ssn_norway.png)

When the country is set to either **Norway** or **Finland** the widget to fetch
address is removed. Your customers will instead be required to supply their 
SSN/Org. nr. when they select a payment method (more on this below).

#### Invalid SSN

![Simplified flow - Invalid SSN](./Images/Frontend/simplified_flow/invalid_ssn.png)

When an invalid SSN is provided, a message will be displayed requesting the 
customer to correct the problem.

### Payment methods

Payment methods provided by Resurs Bank may require additional information to be
supplied by the customer. 

#### SSN

The SSN field expects a value that is valid for the country that was previously 
selected. If the customer have already supplied an SSN (for example to fetch
their address) that value is transferred to this field. The customer may change
an already supplied SSN at this point. If the field contains an invalid SSN, an 
error message will be displayed underneath the field.

#### Card Number

The card number is required for **certain** payment methods from Resurs Bank, 
and functions the same as the SSN field.

#### Contact SSN

The contact SSN field is required for company customers. This is where they 
provide their personal SSN as reference for the purchase. The field functions 
the same way as the **SSN** and **card number** fields.

#### Read More

Every payment method from Resurs Bank has a `Read More` link that will display 
additional information about the payment method.

---

## General information

### Zero sum orders

In order for zero (0) sum orders to function you have to enable the native
payment method **Zero Subtotal Checkout** from **Sales -> Payment Methods** in
the configuration.

### Support for multiple store views

This module fully supports multiple store views. You may use different API
credentials for each of your store views. Payment methods, callbacks and all
settings are kept fully separated for each store view.

---

## Admin

### Settings

Overall settings for the module are located under **Stores -> Configuration -> 
Resurs Bank -> Checkout**

#### General Settings

![General Settings](./Images/Backend/admin/settings/general.png)

##### Versions

* **Data | Schema:** These values indicate what changes have been applied to the
**Data** and **Structure** of the database.
* **Composer:** This is the version of the module composer package and should be
considered the actual version of the module.

**_NOTE: When support requests your version information please supply all these
values as well as the version of your Magento installation._**

##### Enabled
Whether the module is activated or not.

##### Redirect Cart Page to Checkout
Redirect clients from the **Shopping Cart** to the **Checkout** page. Only works 
when using `Checkout` flow.

### API Settings

![API Settings](./Images/Backend/admin/settings/api.png)

#### Flow

##### Checkout
Resurs Banks onepage checkout solution relying on an iframe.

##### Simplified
Regular checkout page. Resurs Banks payment methods will be available like any 
other payment methods. A widget will become available for customers to fetch 
their address using their SSN/Org. nr. (this widget only works in **Sweden**).

#### Environment
This setting will define what servers you use for your outgoing API calls. 
`Test` should be used in test/development environments, while `Production` 
should only be used on your production environment (live website).

Both environment options require a separate username and password combination. 
After you've supplied this information it will be retained when switching 
between `Test` and `Production`.

For example, let's say you choose the `Test` environment and put `my-username` 
in the username field and `my-password` in the password field, and then save the 
configuration. When you switch to the `Production` environment the username and 
password fields will be emptied. But when you switch back to the `Test` 
environment they will be updated with the `my-username` and `my-password` 
values.

#### Username
The username for the chosen environment.

**_NOTE: This setting is required for the module to function._**

#### Password
The password for the chosen environment.

**_NOTE: This setting is required for the module to function._**

#### Country
Country should match the country your API account has been assigned to.

#### Unit Measure
Basic unit label for your products. This can be anything you want and should not 
be changed unless you have a clear reason to.

**_NOTE: This setting is required for API calls to work._**

### Payment Methods

![Payment Methods](./Images/Backend/admin/settings/payment_methods.png)

#### Sync
Fetches the available payment methods from Resurs Bank. Fetched payment methods
will be stored in the database. The customer will be able to choose from one of
these payment methods during checkout.

Whenever changes are applied to your payment methods at Resurs Bank (or new
methods are added), you will need to sync your methods again before these
changes apply in Magento.

**_NOTE: If payment methods aren't synced the checkout will still function
 properly, but the payment method will fall back to `resursbank_default` and
  simply state "Resurs Bank" on your order view._**

#### Available Payment Methods
A list of the available payment methods. You must sync your payment methods
before this list will show any entries.

#### Automatically sync payment methods
You can use this setting to automatically sync payment method configuration at
regular intervals. This can be very useful if your methods are regularly updated
or if you sporadically enable/disable methods at Resurs Bank (for campaigns
etc.).

### Part Payment Settings

![Part Payment Settings](./Images/Backend/admin/settings/part_payment.png)

Part payments allow customers to split their payment over a set duration. This
feature will let you display the estimated part payment price for products (
directly on product pages) and purchases (on the checkout page).

#### Enabled
Whether the part payment widgets should be displayed. The widget will be
displayed both on your product pages and on the checkout page (if you are using
`Checkout Flow`).

#### Payment Method
The payment method estimations are calculated against. Selecting a payment
method will update the options for the `Duration` setting. The `Minimum` and
`Maximum` fields will also update to reflect required price intervals.

#### Duration
Select the duration estimations are calculated against. Please note each option
display the rate which will be applied for these calculations.

#### Minimum Price
Minimum price to be eligible for part payment. This field is not editable.

#### Maximum Price
Maximum price to be eligible for part payment. This field is not editable.

### After-shop Settings

![After-shop Settings](./Images/Backend/admin/settings/after_shop.png)

#### Enabled
When this feature is enabled you can **debit**, **cancel** and **refund**
payments directly from the Magento administration panel by
**creating and invoice**, **cancelling an order** and **creating a creditmemo**
respectively.

### Callback Settings

![Callback Settings](./Images/Backend/admin/settings/callback.png)

These settings allow you to register and manage **callback URLs.** These are
used by Resurs Bank to contact your website and notify it of various events in
relation to payments. For example, the module prevents order confirmation emails
from being sent when orders are created. They will instead be sent when Resurs
Bank has registered payment of the order. This process can take anywhere from a
few seconds to a couple of hours, depending on the user's preconditions and
selected method of payment.

Some callbacks will modify the order status. These are callbacks from methods
which mark the payment as confirmed or being under suspicion of fraud. Which
callbacks that trigger these events depend on the selected method of payment.
You can configure what order status will be used for these events.

#### Order Status - Confirmed
Here you can choose the status an order will receive when the payment has been
accepted by Resurs Bank.

#### Order Status - Suspected Fraud
Here you can choose the status an order will receive when it has not passed the 
**automatic fraud control**.

#### Callback Registration
This button will register callbacks at Resurs Bank.

**_NOTE: Since callback URLs are based on your base URL, you should be careful
 when registering callbacks from multiple environments. To avoid issues, please
 never use `Production` anywhere other than on your actual production
 environment._**

#### Registered Callbacks
Here we display a list of your registered callbacks.

### Debug Settings

![Debug Settings](./Images/Backend/admin/settings/debug.png)

#### Enabled
Whether the module should log error messages, information about outgoing API 
calls, incoming callbacks etc. This should not be enabled unless you need to 
debug something, as the log files can grow quite large and eventually cause 
problems (since we do not solely log errors, we log everything that can be
relevant or useful for a debug session).

You will find the log files under **var/log** and they are all prefixed with 
**resursbank_**

### Advanced Settings

![Advanced Settings](./Images/Backend/admin/settings/advanced.png)

#### Hide loading mask
Whether to hide the large fullscreen loading mask that is displayed during 
AJAX requests when using `Checkout` flow.

#### Hide shipping mask
Whether to hide the loading mask that is displayed on top of the shipping block
when you select a shipping method in `Checkout` flow. The loading mask is
displayed until the AJAX request to store the choice within your session has
completed.

#### Carrier Rates Require Address
Whether to require a full address from the customer before displaying a list of
available shipping methods for them to choose from. This feature is only
available when using `Checkout` flow.

#### Round Tax Percentage

Magento tracks most, but not all, tax percentage values calculated during
checkout. At the time of writing this only applies to discounts, and only under
certain conditions. If these conditions apply the tax percentage may naturally
result in a value like 24.9684847869% which Magento later rounds to 25%. Magento
will then re-calculate the tax portion of the discount based on the rounded tax
percentage value.

When creating the payment at Resurs Bank we need to supply a price excl. tax and
a valid tax percentage (24.9684847869% is for example not valid).

Since Magento does not store the actual tax percentage applied in this case, we
need to calculate and round it ourselves in order for the numbers to be accurate
both in Magento and at Resurs Bank.

Therefore this setting should never be disabled, unless you've explicit reason
or instructions to disable it.

#### Reuse Erroneously Created Orders

Orders are created prior to payments, and sometimes an error may occur after the
order has been created, but before the payment has been created. An example of
such an error is when a customer **credit application** is denied.

Sometimes a customer will try several times before giving up, and each time a
new order will be created. This leaves "dangling", cancelled, orders in your 
system.

When this setting is activated the module will re-use the order initially
created in subsequent attempts made by the customer. The module will remove the
previous failed order and create a new one in its stead. The same order number
will still be used, to avoid sequence breaks.

It's recommended to keep this activated, unless you experience problems (such as
orders being removed when they shouldn't be).

---

### Payment Method Settings

Settings for payment methods are located under **Stores -> Configuration -> 
Resurs Bank -> Payment Methods**

**_NOTE: these settings are only available when using `Simplified` flow._**

### Fee Tax Settings

Tax settings for payment method fees.

#### Tax Class

What tax class you wish to apply when calculating taxes for payment method fees.

#### Price Display

How you prefer to display payment method fees in checkout.

---

### Salt-key

The salt-key is used to verify that incoming calls actually originate from
Resurs Bank.

The salt-key is generated when you register your callbacks, and submitted as
part of the callback URLs to Resurs Bank. When incoming calls to the store are
made by Resurs Bank, the callback handler (Model/Callback.php) will evaluate the 
provided `digest` against the content of your local salt-key to confirm that the 
calls do not originate from any other source.

---

## Orders

You will need to activate `After Shop` flow in order to update payments when you
handle your orders (please see the configuration section on 
`After-shop Settings`).

### Charging a payment

Payments are automatically charged when you create an invoice for your order.

![Order view - Invoice button](./Images/Backend/Order/invoice.png)

On a pending order, press the button in the image above. This will take you to
another page.

![Order view - Submit invoice](./Images/Backend/Order/invoice-confirm.png)

At the bottom is the submit button. Press it, and an invoice will be created. At 
the same time the payment will be charged by Resurs Bank.

### Canceling a payment

![Order view - Cancel button](./Images/Backend/Order/cancel.png)

Payments are automatically cancelled along with the order.

![Order view - Confirmation dialouge](./Images/Backend/Order/cancel-confirm.png)

After you click the cancel button, a confirmation window will show up. Just 
click "OK" and the payment at Resurs Bank as well as your order in Magento will 
both be cancelled.

### Refunding a payment

Payments are automatically refunded when credit memos ​are issued.

![Order view - Credit memo button](./Images/Backend/Order/credit.png)

On a completed order, press the button in the image above. This will take you to 
another page.

![Order view - Credit memo Refund offline button](./Images/Backend/Order/credit-confirm.png)

At the bottom is the refund button. Press it to create a credit memo in Magento
and refund the payment at Resurs Bank.

Please note that you can choose to refund specific parts of an order. For more 
complex cases of partial refunds, for example for orders including percentage 
based discounts, it may be preferable to handle it directly from Resurs Bank’s 
administration panel to ensure the numbers are what you intend.

### Payment history

At the very bottom of the order page you will find a button labelled
`View Payment History`. Click this button to open a window displaying all events
that have occurred for a payment.

The history table tracks information about how the payment was initially created
as well as reasons why a payment may not have been successfully concluded. 

It also tracks all incoming callbacks and all outgoing After-Shop calls.

### Order statuses

We add several custom order statuses to helper better describe the state of your
orders. Orders are created prior to payments (if you want more information on
this please contact us directly). The following statuses are appended by our
module:

`Resurs Bank - Credit Denied` the customers credit application was denied,
the payment could therefore not be completed.

`Resurs Bank - Purchase Declined` the payment failed. This could happen if the
customer for example supplies inaccurate information or does not have sufficient
funds to complete the transaction.

`Resurs Bank - Aborted by Customer` the customer didn't complete the payment.
Usually this means the customer reaches the payment gateway and then close their
browser before completing the transaction.

### Automatic cancellation of aborted orders

The module includes a cron job which will automatically cancel orders meeting
the following criteria:

* The order must have existed in your database for at least one hour.
* The grand total must exceed 0.
* Selected payment method most stem from Resurs Bank.
* The payment most not exist at Resurs Bank (an API call is made to confirm 
this).
* The API account information must not have changed between the time the order
was created and the time this cron job executes to cancel the same order (to 
comply with the previous rule, the same API account must be used to ensure we 
can find the payment at all).

### Stores

If you are running a chain of stores you may configure a subsystem to indicate
which store a purchase relates to. You will need to configure your stores at
Resurs Bank first, you can then supply the **id** of the intended store with 
your API requests.

To supply the store id, please utilize the custom event 
**resursbank_checkout_set_store_id** with a payload including the parameter
**storeId** (**int**).

The event will go through our custom observer **Observer/SetStoreId.php** to
apply the provided **storeId** in your session. This value will then be included
with outgoing API requests.

---

## Logs

### How to activate

Logs are activated along with the normal system logs in Magento. We log every 
outgoing and incoming call the module makes/receives. For that reason it’s very 
important that logs are disabled on your production environment (unless required 
for debug/test purposes), otherwise they will grow very rapidly.

### Type of logs

There are six log files where content from various operations will be stored:

* `resursbank_general.log` (Anything without a specified context).
* `resursbank_api.log` (Outgoing API calls to Resurs Bank).
* `resursbank_cache.log` (Cache related information).
* `resursbank_callback.log` (Incoming calls from Resurs Bank).
* `resursbank_cancel_unpaid_orders.log` (Information related to the cron job 
that automatically cancel orders. Please see the section `Automatic 
cancellation of aborted orders` above for more information).
* `resursbank_methods.log` (Information related to payment methods).

---

## Debug

### Compiler issues

There is a fairly common issue which can occur when the compiler is enabled
where PHP will complain a constructor receives an instance of a Context class 
but ObjectManager was expected. This issue is usually remedied by deleting the 
directories `var/cache`, `var/generation` and `var/di` after which you simply 
refresh the cache from your administration panel.

### Memcache

If you are running the **memcached** service, and have the PHP extension for 
**memcache** installed, you will need to flush it after installing the module 
before it will work. The same applies if you should ever run into any problems 
where the module is not loading/behaving correctly.

Normally you can flush memcache by executing the following commands (please note 
that the below is subject to change depending on your server configuration and 
you should not issue any such commands unless you know what you are doing. 
**Resurs Bank is not responsible for any damage caused by improper use of your 
memcached service​).**

```
telnet localhost 11211
flush_all
quit
```

### Zend_OPcache

If you have enabled the Zend_OPcache PHP extension you will need to 
reload/restart PHP after installing the module. If you should experience any 
issue with the module not loading properly at any point please make sure to 
refresh Zend_OPcache by reloading/restarting PHP before further investigation.

### Redis

While Redis has not been reported to cause any issues we recommend you flush it 
after installing the module, or if you should experience any issues, before you 
investigate any further. You can normally flush Redis by executing the following 
commands (please note that the below is subject to change depending on your 
server configuration and you should not issue any such commands unless you know 
what you are doing. **Resurs Bank is not responsible for any damage caused by 
improper use of your redis/redis-cli service​).** 

```
redis-cli flushall
```

### Varnish

If you are running Varnish and you experience problems we currently recommend 
you configure Varnish to avoid the checkout page. To test whether Varnish is the 
cause of the problem you are experiencing you can temporarily disable the 
service to check if the issue disappears. If you need to re-configure Varnish to 
avoid the checkout page we refer you to your developer/system administrator.

### Callbacks

If callbacks are not working please confirm that you have no firewall in place 
blocking incoming calls from Resurs Bank.

---

## Changelog

#### 4.3.2

* Prepared implementation of test callback.
* Structural reformation of code handling data coming from the API, including 
payment methods and their annuity configuration.
* The part payment widget and the cron to cancel unpaid orders have been updated 
to function with the reformation described above.
* Part payment price is now displayed on checkout page.
* Part payment annuity min / max values are no longer configurable from the 
administration panel.
* Synced payment methods are now listed on the configuration page.
* The button to sync payment methods and the new list displaying all synced 
methods have been moved into a new separate section on the configuration page.
* Changed payment method naming convention to 
resursbank_[**method**]\_[**username**]\_[**environment**]
* Module version numbers (**composer**, **database schedule** and 
**database data**) are now displayed in the administration panel and outgoing 
API calls.

**NOTE: After updating from an earlier version of the module you will need to 
sync payment methods, and register callbacks, manually from the administration 
panel because of the changes described above.**

#### 4.3.3

* Added resursbank_callback_received flag column to sales_order table. This is 
an extra insurance for the automatic annulment procedure. To ensure only orders 
without payment are annulled, since the getPayment() check sometimes will result 
in a faulty 500 (meaning the payment is missing).

#### 4.3.4

* Fixed minor problem in advanced configuration reader.

#### 4.3.5

* Fixed race condition between iframe and checkout JS. When the JS on the 
checkout page loaded slower than the iframe communication from the iframe was 
not received.

#### 4.3.8

* Fixed race condition which occurs within the iframe, which sometimes causes 
the websocket to fail communicating with the iframe.
* Fixed minor bug with discounts not updating when items are removed from the 
shopping cart.
* Removed spinners from checkout page.

#### 4.3.9

* Fixed callback handling for Swish payments.

#### 5.0.0

* Added support for Simplified Flow.

#### 6.0.0

* Revamped frontend implementation.

#### 6.0.5

* Fixed test callback registration.
* Fixed issue with cached session data relating to the quote.
* Added missing default values for settings added in 5.0.0.
* Fixed problem with separate shipping address. 

#### 6.8.6

* Separated and organized code to collect prices and calculate taxes in outgoing 
API calls. This fixes issues with tax percentage calculations in combination 
with specific settings applied in Magento.
* Added validation of properties in outgoing API calls.
* Fixed problem with discount coupon code in credit memos.
* Added functionality to re-use orders which are cancelled in the middle of the 
payment process (due to failed credit applications etc.).
* Payments at Resurs Bank can now only be cancelled from Magento when the 
associated order is manually cancelled by an admin.
* Added feature to track and display event history on payments, to clarify why 
an order for example failed due to a rejected credit application or insufficient 
funds.
* Suppressed some unnecessary AJAX requests on frontend when address information 
is updated within the iframe (Checkout Flow).
* Checkout page no longer reloads when credit application is rejected.
* Added feature to automatically sync payment methods at regular intervals.
* Added new order statuses to better indicate payment status.
* Callbacks no longer create order comments (callbacks are tracked through the 
new payment history feature instead).
* Fixed problem with Automatic Fraud Control callback digest resolution.
* Minor improvements and fixes.

#### 7.5.0

* You can now configure payment methods directly from the admin panel (does not
 work with Checkout Flow). You can set payment method title, fee and tax options
 (for the payment fee calculations).
* Billing address on orders in Magento will be overwritten with the billing
 address resolved by Resurs Bank during payment signing. Safety feature to avoid
 fraudulent address information.
* A lot of minor improvements and fixes, mostly relating to Simplified Flow.

#### 8.2.8

* You can no longer set what status orders should attain on incoming callbacks.
* Code relating to callbacks has been centralised.
* Revamped system to apply **order status** and **state** when events occur
during checkout, and when callbacks arrive post-checkout.
* Test callback implemented. It can be triggered from the configuration page
by clicking the button labeled **Test callbacks** under **Callback Settings**.
This will cause Resurs Banks server to submit a test callback to your website.
The timestamp corresponding to its arrival is listed just below the button.
* Implemented support for the **UPDATE** callback.
* Removed the callbacks **AUTOMATIC_FRAUD_CONTROL**, **ANNULMENT** and
 **FINALIZATION**. These will now be handled by **UPDATE** instead.
* Adjusted **Callback Settings** config section so it looks proper even on
 smaller screens.
 * Improved callback listing in config.
* Fixed problem with the payment history modal. Its close button wasn't working.
* All payment method config now includes the parameter **allowspecific** with a
static value of "1".
* All payment method config now includes the parameter **specificcountry** with
a static value matching whatever country you've selected under your
**API Settings**.
* Payment fee now respects your tax settings at checkout.
* Payment fee is no longer displayed at checkout unless its value exceeds "0".
* Revamped system which tracks payment history. The modal still works the same
but there are new entries for it.
* Loosened version restrictions for ECom library.
* Centralised code in plugins.
* Improved checks in various observers and plugins to ensure these only execute
when they are supposed to (for example, whenever you are proceeding through
checkout using a payment method that stems from Resurs Bank).
* Improved error handling and logging in various places.
* Fixed problems relating to virtual and downloadable products.
* Improved phone number validation at checkout. There is now also a setting to
enable/disable this validation in the **Advanced Settings** config section.
* Minor updates to database schema.
* Centralised code in adminhtml controllers.
* Corrected PHP version requirement in composer.json to follow the tech stack
requirements specified for Magento 2.2.x
* Improved multi-store support when using mixed API flows and accounts.
* Fixed rounding issue with payment fee tax percentage.

#### 8.3.1

* Order confirmation email is no longer sent when credit application is denied
in Simplified Flow.

#### 9.0.0

* Added Resurs Bank logotype to part payment widget.
* The module now allows for implementations of Resurs Bank store id (not to be confused with store ids in Magento).
* Part payment modal is now loaded dynamically on product pages to support configurable prices.
* Part payment modal is now loaded dynamically in checkout to decrease page loading time (Simplified Flow).
* Part payment widget implemented in checkout (Checkout Flow).
* Patched AJAX call to update item quantity in checkout (Checkout Flow).
* Updated information displayed in part payment modals to support part payments in Denmark.
* Patched problem with tax calculations of payment method fees under certain conditions.
* Removed cronjob that cleaned expired orders in favor of Magentos internal cronjob which does the same.
* Updated payment history system. Changed what events are tracked when and what order state / status is applied for the corresponding events.
* Default order status when using Resurs Bank payment methods has been changed to *payment_pending*, to support Magentos internal cronjob which clean expired orders.

#### 9.0.2

* Fixed potential null pointer error.
* Removed settings to test callbacks.

#### 9.0.3

* Excluded external JavaScript from minification process.

#### 9.0.9

* Fixed 3 potential null pointers.
* Fixed child item exclusion from Quote, Order and Creditmemo data converters.

#### 9.1.0

* Reversed external oc-shop.js implementation.

#### 9.2.3

* New feature to maintain shopping cart contents when navigating backwards in history from gateway.
* Bundle item layout is now mirrored from order to payment.
* Configurable item layout is now mirrored from order to payment.

#### 9.2.6

* Added CSP XML to whitelist Resurs Bank API URLs.
* Removed child item for configurable products from payment in Resurs Bank.
* Changed order of line types on Resurs Bank payment.

#### 9.2.8

* Payment methods are no longer forced to active in Simplified Flow checkout.
* Inactive payment methods are now filtered in admin.

#### 9.3.0

* Added flag to sales_order table indicating whether order confirmation email has been sent to prevent repeating BOOKED callbacks from submitting additional mails.

---

#### 9.3.5

* Fixed problem with re-rendering Read More modal in Simplified Flow.
* Fixed problem with partial creditmemos not being properly converted.

---

#### 9.4.0

* Added new setting to manually define maximum allowed transaction value when using Swish payment method.

---

#### 9.4.3

* Setting to manually override maximum transaction value for Swish no longer applies to RCO.
* Status management on order success page now mirrors management from callbacks.
* Order cancellation now cancels each item to support item reservation.

---

#### 9.4.5

* Fixed graphical glitch with mini cart sometimes inaccurately displaying cart contents on order success page.
* Added missing logotypes to payment methods at checkout.

---

#### 9.4.6

* BookSignedPayment no longer updates order status.

---

## Contact

