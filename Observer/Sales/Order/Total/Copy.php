<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Observer\Sales\Order\Total;

use Exception;
use Magento\Framework\DataObject\Copy as CopyService;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;
use Resursbank\Checkout\Helper\Config\Checkout\General;

/**
 * @package Resursbank\Checkout\Observer\Sales\Order\Total
 */
class Copy implements ObserverInterface
{
    /**
     * @var CopyService
     */
    private $copyService;

    /**
     * @var General
     */
    private $helperConfigGeneral;

    /**
     * @param CopyService $copyService
     * @param General $helperConfigGeneral
     */
    public function __construct(
        CopyService $copyService,
        General $helperConfigGeneral
    ) {
        $this->copyService = $copyService;
        $this->helperConfigGeneral = $helperConfigGeneral;
    }

    /**
     * This method copies our custom total values from the address object
     * attached to the quote, to our order, right before the order gets created.
     * This is necessary to include the total amount information in the
     * sales_order table.
     *
     * Field mapping is set up here: vendor/resursbank/checkout/etc/fieldset.xml
     *
     * You could map the fields manually using a simple array, but using a
     * fieldset is more accurate.
     *
     * Note: setting the fieldset id in fieldset.xml to
     * "sales_convert_quote_address" may seem more accurate since it would
     * automatically copy our custom total
     * values in vendor/magento/module-quote/Model/Quote/Address/ToOrder.php,
     * making this observer obsolete. However, Magento relies on an interface to
     * cross reference what properties can be copied between the objects, thus
     * preventing us from appending custom values using it.
     *
     * @param Observer $observer
     * @return $this
     * @throws Exception
     */
    public function execute(Observer $observer)
    {
        if ($this->helperConfigGeneral->isEnabled()) {
            /** @var Order $order */
            $order = $observer->getEvent()->getData('order');

            /** @var Quote $quote */
            $quote = $observer->getEvent()->getData('quote');

            if (!($order instanceof Order)) {
                throw new Exception('Missing or invalid order object.');
            }

            if (!($quote instanceof Quote)) {
                throw new Exception('Missing or invalid quote object.');
            }

            // Copy custom total values to order object.
            $this->copyService->copyFieldsetToTarget(
                'resursbank_sales_convert_quote_address',
                'to_order',
                $quote->getBillingAddress(),
                $order
            );
        }

        return $this;
    }
}
