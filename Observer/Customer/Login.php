<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Observer\Customer;

use \Exception;
use \Resursbank\Checkout\Helper\Api;
use \Resursbank\Checkout\Helper\Log;
use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;

/**
 * Clear module specific session values when customer logs in.
 *
 * @package Resursbank\Checkout\Observer\Customer
 */
class Login implements ObserverInterface
{
    /**
     * @var Api
     */
    protected $api;

    /**
     * @var Log
     */
    private $log;

    /**
     * @param Api $api
     * @param Log $log
     */
    public function __construct(
        Api $api,
        Log $log
    ) {
        $this->api = $api;
        $this->log = $log;
    }

    /**
     * Clear payment session when client logs in.
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        try {
            $this->api->clearPaymentSession();
        } catch (Exception $e) {
            $this->log->error($e);
        }
    }
}
