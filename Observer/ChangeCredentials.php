<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Observer;

use \Exception;
use \Resursbank\Checkout\Helper\Logic\Account\Method;
use \Resursbank\Checkout\Helper\Log;
use \Resursbank\Checkout\Helper\Logic\Account;
use \Resursbank\Checkout\Helper\Config\Checkout\Api as Config;
use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Message\ManagerInterface;
use \Magento\Framework\Event\Observer;
use \Magento\Framework\App\RequestInterface;
use \Magento\Framework\ObjectManagerInterface;

/**
 * Actions taken whenever API account credentials changes. For example, when you
 * change your username or environment we want to sync our payment methods and
 * create an entry for the configured values in 'resursbank_checkout_account'.
 *
 * @package Resursbank\Checkout\Observer
 */
class ChangeCredentials implements ObserverInterface
{
    /**
     * @var Log
     */
    private $log;

    /**
     * @var ManagerInterface
     */
    private $messages;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var Method
     */
    private $method;

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var Account
     */
    private $account;

    /**
     * @param Log $log
     * @param ManagerInterface $messages
     * @param Config $config
     * @param RequestInterface $request
     * @param Method $method
     * @param ObjectManagerInterface $objectManager
     * @param Account $account
     */
    public function __construct(
        Log $log,
        ManagerInterface $messages,
        Config $config,
        RequestInterface $request,
        Method $method,
        ObjectManagerInterface $objectManager,
        Account $account
    ) {
        $this->log = $log;
        $this->messages = $messages;
        $this->config = $config;
        $this->request = $request;
        $this->method = $method;
        $this->objectManager = $objectManager;
        $this->account = $account;
    }

    /**
     * Sync payment methods.
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        try {
            if ($this->changedApiCredentials($observer)) {
                $this->addApiAccount($observer)
                    ->syncPaymentMethods();
            }
        } catch (Exception $e) {
            // Add log entry.
            $this->log->error($e);

            // Add error message.
            $this->messages->addErrorMessage(
                'Failed to synchronize information in database.'
            );
        }
    }

    /**
     * Add API account entry to 'resursbank_checkout_account' table.
     *
     * @param Observer $observer
     * @return $this
     */
    private function addApiAccount(Observer $observer)
    {
        try {
            $this->account->createMissingAccount(
                $this->config->getCredentials($this->getStore($observer))
            );
        } catch (Exception $e) {
            // Add log entry.
            $this->log->error($e);

            // Add error message.
            $this->messages->addErrorMessage(
                'Failed to store API account credentials in database.'
            );
        }

        return $this;
    }

    /**
     * This method basically casts an empty string to null because when
     * collecting information from the config scopeCode null gives you the
     * default store view, as do 0, but an empty string gives you nothing. The
     * store value collected from our observer instance is unsafe (at the time
     * of writing this is a numeric value wrapped in a string). Since scopeCode
     * can be string | int | null we cannot make too many assumptions about this
     * not changing in the future and thus we should not typecast it. We should
     * simply ensure it cannot be an empty string.
     *
     * @param Observer $observer
     * @return int|string|null
     */
    private function getStore(Observer $observer)
    {
        $result = $observer->getStore();

        if ($result === '') {
            $result = null;
        }

        return $result;
    }

    /**
     * Synchronize payment methods.
     *
     * @return $this
     */
    private function syncPaymentMethods()
    {
        try {
            $this->method->syncAll();
        } catch (Exception $e) {
            // Add log entry.
            $this->log->error($e);

            // Add error message.
            $this->messages->addNoticeMessage(
                'Failed to synchronize payment methods.'
            );
        }

        return $this;
    }

    /**
     * Check if username / environment changed when saving configuration.
     *
     * @param Observer $observer
     * @return bool
     */
    private function changedApiCredentials(Observer $observer)
    {
        $result = false;

        if (!$observer->hasChangedPaths()) {
            $result = true;
        } else {
            /** @var array $paths */
            $paths = (array) $observer->getChangedPaths();

            foreach ($paths as $path) {
                if ($this->isCredentialsPath($path)) {
                    $result = true;
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * Check if provided path is part of credentials configuration affecting
     * payment methods and callbacks.
     *
     * @param string $path
     * @return false
     */
    private function isCredentialsPath($path)
    {
        return (bool) preg_match(
            '/resursbank_checkout\/api\/(username|environment)(.*)/',
            (string) $path
        );
    }
}
