<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Resursbank\Checkout\Helper\StoreId;

/**
 * Set a store id in your session. This will be submitted as part of the
 * checkout process to indicate intended store (at Resurs Bank, unrelated to
 * stores / store views in Magento).
 *
 * @package Resursbank\Checkout\Observer
 */
class SetStoreId implements ObserverInterface
{
    /**
     * @var StoreId
     */
    private $storeIdHelper;

    /**
     * @param StoreId $storeIdHelper
     */
    public function __construct(StoreId $storeIdHelper)
    {
        $this->storeIdHelper = $storeIdHelper;
    }

    /**
     * Set the store id in session.
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $storeId = $observer->getData('storeId');

        $this->storeIdHelper->setStoreId((int) $storeId);
    }
}
