<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Cron;

use Exception;
use Resursbank\Checkout\Helper\Config\Checkout\General as Config;
use Resursbank\Checkout\Helper\Config\Checkout\PaymentMethods as PaymentMethodsConfig;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Logic\Account\Method;

/**
 * @package Resursbank\Checkout\Cron
 */
class SyncPaymentMethods
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var PaymentMethodsConfig
     */
    private $paymentMethodsConfig;

    /**
     * @var Method
     */
    private $method;

    /**
     * @var Log
     */
    private $log;

    /**
     * @param Config $config
     * @param PaymentMethodsConfig $paymentMethodsConfig
     * @param Method $method
     * @param Log $log
     */
    public function __construct(
        Config $config,
        PaymentMethodsConfig $paymentMethodsConfig,
        Method $method,
        Log $log
    ) {
        $this->config = $config;
        $this->paymentMethodsConfig = $paymentMethodsConfig;
        $this->method = $method;
        $this->log = $log;
    }

    /**
     * Sync the payment methods.
     *
     * @return void
     * @throws Exception
     */
    public function execute()
    {
        if ($this->isEnabled()) {
            try {
                // Synchronize payment methods.
                $this->method->syncAll();
            } catch (Exception $e) {
                // Add log entry.
                $this->log->error($e);
            }
        }
    }

    /**
     * Check if the CRON should be executed.
     * The module needs to be enabled and the setting for
     * automatically sync payment methods needs to be active.
     *
     * @return boolean
     * @throws Exception
     */
    private function isEnabled(): bool
    {
        return (
            $this->config->isEnabled() &&
            $this->paymentMethodsConfig->autoSyncEnabled()
        );
    }
}
