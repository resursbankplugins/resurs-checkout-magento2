<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Block;

use Exception;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Resursbank\Checkout\Helper\Config\Checkout\Advanced as AdvancedConfig;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\Api;

/**
 * Injects JavaScript initialization on the custom checkout page.
 */
class Js extends Template
{
    /**
     * @var Api
     */
    protected $apiModel;

    /**
     * @var FormKey
     */
    protected $formKey;

    /**
     * @var ApiConfig
     */
    protected $apiConfig;

    /**
     * @var AdvancedConfig
     */
    protected $advancedConfig;

    /**
     * @var Log
     */
    private $log;

    /**
     * @param Context $context
     * @param Api $apiModel
     * @param FormKey $formKey
     * @param ApiConfig $apiConfig
     * @param AdvancedConfig $advancedConfig
     * @param Log $log
     * @param array $data
     */
    public function __construct(
        Context $context,
        Api $apiModel,
        FormKey $formKey,
        ApiConfig $apiConfig,
        AdvancedConfig $advancedConfig,
        Log $log,
        array $data = []
    ) {
        $this->apiModel = $apiModel;
        $this->formKey = $formKey;
        $this->apiConfig = $apiConfig;
        $this->advancedConfig = $advancedConfig;
        $this->log = $log;

        parent::__construct($context, $data);
    }

    /**
     * Get form key.
     *
     * @return string
     */
    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }

    /**
     * Retrieve API model.
     *
     * @return Api
     */
    public function getApiModel()
    {
        return $this->apiModel;
    }

    /**
     * Retrieve payment method code suffix.
     *
     * @return string
     */
    public function getMethodSuffix()
    {
        $result = '';

        try {
            $result = $this->apiConfig->getCredentials()->getMethodSuffix();
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Retrieve whether or not to hide loading mask.
     *
     * @return boolean
     */
    public function isLoadingMaskHidden()
    {
        $result = false;

        try {
            $result = $this->advancedConfig->isLoadingMaskHidden();
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Retrieve whether or not to hide loading mask specially used for shipping
     * methods element.
     *
     * @return boolean
     */
    public function isShippingLoadingMaskHidden()
    {
        $result = false;

        try {
            $result = $this->advancedConfig->isShippingLoadingMaskHidden();
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }
}
