<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Block;

use \Resursbank\Checkout\Model\Api;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;

/**
 * Renders the iframe on the custom checkout page.
 */
class Iframe extends Template
{
    /**
     * @var Api
     */
    protected $apiModel;

    /**
     * Iframe constructor.
     * @param Context $context
     * @param Api $apiModel
     * @param array $data
     */
    public function __construct(
        Context $context,
        Api $apiModel,
        array $data = []
    ) {
        $this->apiModel = $apiModel;
        
        parent::__construct($context, $data);
    }

    /**
     * @return Api
     */
    public function getApiModel()
    {
        return $this->apiModel;
    }
}
