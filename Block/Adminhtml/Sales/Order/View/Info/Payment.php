<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Block\Adminhtml\Sales\Order\View\Info;

use Exception;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Checkout\Helper\Data;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order\Invoice;
use Magento\Sales\Model\Order\Shipment;
use Resursbank\Checkout\Helper\Api as HelperApi;
use Resursbank\Checkout\Helper\Config\Checkout\Api as Config;
use Resursbank\Checkout\Helper\Ecom;
use Resursbank\Checkout\Helper\Order as OrderHelper;
use Resursbank\Checkout\Model\Api as ModelApi;
use Resursbank\Checkout\Model\Api\Credentials;
use stdClass;

/**
 * Injects custom HTML containing payment information on order/invoice view.
 */
class Payment extends Template
{
    /**
     * Payment information.
     *
     * @var array
     */
    protected $paymentInfo;

    /**
     * @var ModelApi
     */
    protected $apiModel;

    /**
     * @var HelperApi
     */
    protected $apiHelper;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Data
     */
    protected $checkoutHelper;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Ecom
     */
    protected $ecom;

    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @param Context $context
     * @param ModelApi $apiModel
     * @param HelperApi $apiHelper
     * @param Registry $registry
     * @param Data $checkoutHelper
     * @param Config $config
     * @param Ecom $ecom
     * @param OrderHelper $orderHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        ModelApi $apiModel,
        HelperApi $apiHelper,
        Registry $registry,
        Data $checkoutHelper,
        Config $config,
        Ecom $ecom,
        OrderHelper $orderHelper,
        array $data = []
    ) {
        $this->apiModel = $apiModel;
        $this->apiHelper = $apiHelper;
        $this->registry = $registry;
        $this->checkoutHelper = $checkoutHelper;
        $this->config = $config;
        $this->ecom = $ecom;
        $this->orderHelper = $orderHelper;

        parent::__construct($context, $data);

        $this->setTemplate(
            'Resursbank_Checkout' .
            '::sales/order/view/info/resursbank_checkout.phtml'
        );
    }

    /**
     * Get payment status.
     *
     * @return string
     */
    public function getStatus()
    {
        $result = '';

        $status = $this->getPaymentInformation('status');

        if (is_string($status)) {
            $result = __($status);
        } elseif (is_array($status) && count($status)) {
            foreach ($status as $piece) {
                $result.= !empty($result) ? (' | ' . __($piece)) : __($piece);
            }
        }

        if (empty($result)) {
            $result = __('PENDING');
        }

        return $result;
    }

    /**
     * Retrieve order reference.
     *
     * @return string
     */
    public function getPaymentId()
    {
        return $this->getPaymentInformation('id');
    }

    /**
     * Retrieve payment total.
     *
     * @return float
     */
    public function getPaymentTotal()
    {
        return $this->checkoutHelper->formatPrice(
            (float) $this->getPaymentInformation('totalAmount')
        );
    }

    /**
     * Retrieve payment limit.
     *
     * @return float
     */
    public function getPaymentLimit()
    {
        return $this->checkoutHelper->formatPrice(
            (float) $this->getPaymentInformation('limit')
        );
    }

    /**
     * Check if payment is frozen.
     *
     * @return bool
     */
    public function isFrozen()
    {
        return ($this->getPaymentInformation('frozen') === true);
    }

    /**
     * Check if payment is fraud marked.
     *
     * @return bool
     */
    public function isFraud()
    {
        return ($this->getPaymentInformation('fraud') === true);
    }

    /**
     * Returns the name of the used payment method for the order.
     *
     * @return string
     */
    public function getPaymentMethodName()
    {
        return $this->getPaymentInformation('paymentMethodName');
    }

    /**
     * Retrieve full customer name attached to payment.
     *
     * @return mixed
     */
    public function getCustomerName()
    {
        return $this->getCustomerInformation('fullName', true);
    }

    /**
     * Retrieve full customer address attached to payment.
     *
     * @return mixed
     */
    public function getCustomerAddress()
    {
        $street = $this->getCustomerInformation('addressRow1', true);
        $street2 = $this->getCustomerInformation('addressRow2', true);
        $postal = $this->getCustomerInformation('postalCode', true);
        $city = $this->getCustomerInformation('postalArea', true);
        $country = $this->getCustomerInformation('country', true);

        $result = "{$street}<br />";

        if ($street2) {
            $result.= "{$street2}<br />";
        }

        $result.= "{$city}<br />";
        $result.= "{$country} - {$postal}";

        return $result;
    }

    /**
     * Retrieve customer telephone number attached to payment.
     *
     * @return mixed
     */
    public function getCustomerTelephone()
    {
        return $this->getCustomerInformation('telephone');
    }

    /**
     * Retrieve customer email attached to payment.
     *
     * @return mixed
     */
    public function getCustomerEmail()
    {
        return $this->getCustomerInformation('email');
    }

    /**
     * Retrieve customer information from Resursbank payment.
     *
     * @param string $key
     * @param bool $address
     * @return mixed
     */
    public function getCustomerInformation($key = '', $address = false)
    {
        $result = (array) $this->getPaymentInformation('customer');

        if ($address) {
            $result = (is_array($result) && isset($result['address'])) ?
                (array) $result['address'] :
                null;
        }

        if (!empty($key)) {
            $result = isset($result[$key]) ? $result[$key] : null;
        }

        return $result;
    }

    /**
     * Retrieve payment information from Resurs Bank.
     *
     * @param string $key
     * @return mixed|null|stdClass
     */
    public function getPaymentInformation($key = '')
    {
        $result = null;

        $key = (string) $key;

        if ($this->paymentInfo === null) {
            try {
                $this->paymentInfo = (array) $this->ecom
                    ->getConnection($this->getApiCredentials($this->getOrder()))
                    ->getPayment(
                        $this->orderHelper->getPaymentId($this->getOrder())
                    );
            } catch (Exception $e) {
                // Something went wrong while getting the payment information
                // from Resurs Bank. This should not prevent the order page from
                // rendering though.
                $this->paymentInfo = [];
            }
        }

        if (empty($key)) {
            $result = $this->paymentInfo;
        } elseif (is_array($this->paymentInfo) &&
            isset($this->paymentInfo[$key])
        ) {
            $result = $this->paymentInfo[$key];
        }

        return $result;
    }

    /**
     * Retrieve order model instance. Method of retrieval varies depending on
     * location.
     *
     * @return Order
     * @throws Exception
     */
    public function getOrder()
    {
        $result = $this->registry->registry('current_order');

        if (!$result) {
            $result = $this->getOrderByControllerName();
        }

        if (!($result instanceof Order)) {
            throw new Exception(__(
                'Failed to locate order object. Cannot render Resursbank' .
                ' payment information.'
            ));
        }

        return $result;
    }

    /**
     * @return Order|null
     */
    public function getOrderByControllerName()
    {
        $result = null;

        switch ($this->_request->getControllerName()) {
            case 'order_invoice':
                $result = $this->getOrderByCurrentInvoice();
                break;
            case 'order_creditmemo':
                $result = $this->getOrderByCurrentCreditmemo();
                break;
            case 'order_shipment':
                $result = $this->getOrderByCurrentShipment();
                break;
        }

        return $result;
    }

    /**
     * @return Order|null
     */
    public function getOrderByCurrentInvoice()
    {
        $result = null;

        /** @var Invoice $invoice */
        $invoice = $this->registry->registry('current_invoice');

        if ($invoice) {
            $result = $invoice->getOrder();
        }

        return $result;
    }

    /**
     * @return Order|null
     */
    public function getOrderByCurrentCreditmemo()
    {
        $result = null;

        /** @var Creditmemo $creditmemo */
        $creditmemo = $this->registry->registry('current_creditmemo');

        if ($creditmemo) {
            $result = $creditmemo->getOrder();
        }

        return $result;
    }

    /**
     * @return Order|null
     */
    public function getOrderByCurrentShipment()
    {
        $result = null;

        /** @var Shipment $shipment */
        $shipment = $this->registry->registry('current_shipment');

        if ($shipment) {
            $result = $shipment->getOrder();
        }

        return $result;
    }

    /**
     * Retrieve API credentials relative to the store th order was placed in.
     *
     * @param Order $order
     * @return Credentials
     * @throws Exception
     */
    private function getApiCredentials(Order $order)
    {
        return $this->config->getCredentials(
            $order->getStore()->getCode()
        );
    }
}
