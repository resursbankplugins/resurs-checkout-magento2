<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Block\Adminhtml\System\Config;

use \Exception;
use \Resursbank\Checkout\Helper\Config\Checkout\PartPayment;
use \Resursbank\Checkout\Helper\Logic\Account\Method as MethodHelper;
use \Resursbank\Checkout\Helper\Db\Account\Method as MethodDb;
use \Resursbank\Checkout\Helper\Config\Checkout\Api as Config;
use \Resursbank\Checkout\Helper\Log;
use \Resursbank\Checkout\Model\Account\Method as MethodModel;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Framework\App\RequestInterface;

/**
 * Injects JavaScript on the module configuration page.
 */
class Js extends Template
{
    /**
     * @var PartPayment
     */
    protected $partPayment;

    /**
     * @var MethodDb
     */
    protected $methodDb;

    /**
     * @var Log
     */
    protected $log;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @param Context $context
     * @param PartPayment $partPayment
     * @param MethodDb $methodDb
     * @param Log $log
     * @param Config $config
     * @param array $data
     */
    public function __construct(
        Context $context,
        PartPayment $partPayment,
        MethodDb $methodDb,
        Log $log,
        Config $config,
        array $data = []
    ) {
        $this->partPayment = $partPayment;
        $this->methodDb = $methodDb;
        $this->log = $log;
        $this->request = $context->getRequest();
        $this->config = $config;

        parent::__construct($context, $data);
    }

    /**
     * Returns the min and max limit for the chosen payment method.
     *
     * @return string
     */
    public function getChosenPaymentMethodMinMaxLimits()
    {
        $result = [
            'minLimit' => MethodHelper::DEFAULT_MIN_ORDER_TOTAL,
            'maxLimit' => MethodHelper::DEFAULT_MAX_ORDER_TOTAL
        ];

        try {
            /** @var MethodModel $method */
            $method = $this->methodDb->getModel()->load(
                $this->partPayment->getPaymentMethod()
            );

            if ($method->getId()) {
                $result['minLimit'] = $method->getMinOrderTotal();
                $result['maxLimit'] = $method->getMaxOrderTotal();
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return json_encode($result);
    }

    /**
     * Returns the default min and max limit.
     *
     * @return string
     */
    public function getDefaultMinMaxLimits()
    {
        return json_encode([
            'minLimit' => MethodHelper::DEFAULT_MIN_ORDER_TOTAL,
            'maxLimit' => MethodHelper::DEFAULT_MAX_ORDER_TOTAL
        ]);
    }

    /**
     * Only render this block if the requested config section belongs to our
     * module.
     *
     * @return string
     */
    protected function _toHtml()
    {
        $result = '';

        if ($this->isResursBankCheckoutSection()) {
            $result = parent::_toHtml();
        }

        return $result;
    }

    /**
     * Check whether or not the currently requested config section belongs to
     * our module.
     *
     * @return bool
     */
    private function isResursBankCheckoutSection()
    {
        $section = strtolower((string) $this->request->getParam('section'));

        return $section === 'resursbank_checkout';
    }
}
