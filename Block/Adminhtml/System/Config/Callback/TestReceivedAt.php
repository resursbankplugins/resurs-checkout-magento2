<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Block\Adminhtml\System\Config\Callback;

use Exception;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Resursbank\Checkout\Helper\Config\Checkout\Callback as Config;
use Resursbank\Checkout\Helper\Log;

/**
 * Render time when test callback was last received.
 *
 * @package Resursbank\Checkout\Block\Adminhtml\System\Config\Callback
 */
class TestReceivedAt extends Field
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var Log
     */
    private $log;

    /**
     * @param Context $context
     * @param Config $config
     * @param Log $log
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $config,
        Log $log,
        array $data = []
    ) {
        $this->config = $config;
        $this->log = $log;

        parent::__construct($context, $data);
    }

    /**
     * Returns formatted time when test callback was last received.
     *
     * @return string
     */
    public function getTime(): string
    {
        $text = '';

        try {
            $time = $this->config->getTestReceivedAt();

            $text = ($time !== '') ?
                date('Y-m-d H:i:s', $this->config->getTestReceivedAt()) :
                __(
                    'Test callback has not been received yet. Try triggering ' .
                    'it using the button above.'
                );
        } catch (Exception $e) {
            $this->log->error($e);

            $text = __('Could not retrieve timestamp from database.');
        }

        return (string) $text;
    }

    /**
     * Override element renderer.
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        return $this->getTime();
    }
}
