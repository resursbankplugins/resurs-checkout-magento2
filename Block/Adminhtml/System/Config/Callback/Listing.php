<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Block\Adminhtml\System\Config\Callback;

use Exception;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Resursbank\Checkout\Helper\Callback as CallbackHelper;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;

/**
 * Render all registered callbacks.
 */
class Listing extends Field
{
    /**
     * @var CallbackHelper
     */
    private $callback;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @param Context $context
     * @param CallbackHelper $callback
     * @param ApiConfig $apiConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        CallbackHelper $callback,
        ApiConfig $apiConfig,
        array $data = []
    ) {
        $this->callback = $callback;
        $this->apiConfig = $apiConfig;

        $this->setTemplate('system/config/callback/listing.phtml');

        parent::__construct($context, $data);
    }

    /**
     * Retrieve array of registered callbacks.
     *
     * @return array
     */
    public function getCallbacks(): array
    {
        $callbacks = [];

        try {
            if ($this->apiConfig->hasCredentials()) {
                $callbacks = $this->callback->getCallbacks(
                    $this->apiConfig->getCredentials()
                );
            }
        } catch (Exception $e) {
            // Do nothing, an error will be rendered within the template (we
            // avoid the message back since message otherwise would appear on
            // the next page load). Logging is handled by the API model.
        }

        return $callbacks;
    }

    /**
     * Unset some non-related element parameters.
     *
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element): string
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();

        return parent::render($element);
    }

    /**
     * Get the button and scripts contents
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element): string
    {
        return $this->_toHtml();
    }
}
