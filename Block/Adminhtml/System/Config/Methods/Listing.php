<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Block\Adminhtml\System\Config\Methods;

use Exception;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Resursbank\Checkout\Helper\Config\Checkout\Api;
use Resursbank\Checkout\Helper\Db\Account\Method as MethodDb;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\ResourceModel\Account\Method\Collection;

/**
 * Render list of payment methods.
 *
 * @package Resursbank\Checkout\Block\Adminhtml\System\Config\Methods
 */
class Listing extends Field
{
    /**
     * @var MethodDb
     */
    protected $methodDb;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var Api
     */
    protected $api;

    /**
     * @var Log
     */
    private $log;

    /**
     * @param Context $context
     * @param MethodDb $methodDb
     * @param RequestInterface $request
     * @param Api $api
     * @param Log $log
     * @param array $data
     */
    public function __construct(
        Context $context,
        MethodDb $methodDb,
        RequestInterface $request,
        Api $api,
        Log $log,
        array $data = []
    ) {
        $this->methodDb = $methodDb;
        $this->request = $request;
        $this->api = $api;
        $this->log = $log;

        $this->setTemplate('system/config/methods/listing.phtml');

        parent::__construct($context, $data);
    }

    /**
     * Retrieve collection of payment methods.
     *
     * @return Collection|array
     */
    public function getCollection()
    {
        $result = [];

        try {
            $result = $this->methodDb->getActiveCollection(
                $this->request->getParam('store')
            );
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Unset some non-related element parameters.
     *
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();

        return parent::render($element);
    }

    /**
     * Retrieve HTML content.
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }
}
