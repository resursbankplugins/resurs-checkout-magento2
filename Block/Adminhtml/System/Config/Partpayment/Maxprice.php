<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Block\Adminhtml\System\Config\Partpayment;

use Exception;
use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Resursbank\Checkout\Helper\Config\Checkout\PartPayment;
use Resursbank\Checkout\Helper\Db\Account\Method as MethodDb;
use Resursbank\Checkout\Helper\Log;

/**
 * Render maximum monthly cost of part payment method.
 *
 * @package Resursbank\Checkout\Block\Adminhtml\System\Config\Partpayment
 */
class Maxprice extends Field
{
    /**
     * @var PartPayment
     */
    private $partPayment;

    /**
     * @var MethodDb
     */
    private $methodDb;

    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrencyInterface;

    /**
     * @var Log
     */
    private $log;

    /**
     * @param Context $context
     * @param PartPayment $partPayment
     * @param MethodDb $methodDb
     * @param PriceCurrencyInterface $priceCurrencyInterface
     * @param Log $log
     * @param array $data
     */
    public function __construct(
        Context $context,
        PartPayment $partPayment,
        MethodDb $methodDb,
        PriceCurrencyInterface $priceCurrencyInterface,
        Log $log,
        array $data = []
    ) {
        $this->partPayment = $partPayment;
        $this->methodDb = $methodDb;
        $this->priceCurrencyInterface = $priceCurrencyInterface;
        $this->log = $log;

        $this->setTemplate('system/config/partpayment/max_price.phtml');

        parent::__construct($context, $data);
    }

    /**
     * Returns the maximum price for a part payment method.
     *
     * @return string
     */
    public function getMaxPrice()
    {
        $max = 0.0;

        try {
            $id = $this->partPayment->getPaymentMethod();

            if ($id !== '') {
                $method = $this->methodDb->getModel()->load($id);

                if ($method->getId()) {
                    $max = (float)$method->getMaxOrderTotal();
                }
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $this->priceCurrencyInterface
            ->convertAndFormat(ceil($max), false, 0);
    }

    /**
     * Unset some non-related element parameters.
     *
     * @param AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();

        return parent::render($element);
    }

    /**
     * Override element renderer.
     *
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }
}
