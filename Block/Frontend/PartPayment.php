<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Block\Frontend;

use Exception;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Resursbank\Checkout\Helper\Config\Checkout\PartPayment as PartPaymentConfig;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Logic\Account\Method\Annuity as PartPaymentHelper;

/**
 * @package Resursbank\Checkout\Block\Frontend
 */
class PartPayment extends Template
{
    /**
     * @var PartPaymentHelper
     */
    protected $helper;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrencyInterface;

    /**
     * @var PartPaymentConfig
     */
    protected $config;

    /**
     * @var Log
     */
    protected $log;

    /**
     * PartPayment constructor.
     *
     * @param Context $context
     * @param PartPaymentHelper $helper
     * @param PriceCurrencyInterface $priceCurrencyInterface
     * @param PartPaymentConfig $config
     * @param Log $log
     * @param array $data
     */
    public function __construct(
        Context $context,
        PartPaymentHelper $helper,
        PriceCurrencyInterface $priceCurrencyInterface,
        PartPaymentConfig $config,
        Log $log,
        array $data = []
    ) {
        $this->priceCurrencyInterface = $priceCurrencyInterface;
        $this->helper = $helper;
        $this->config = $config;
        $this->log = $log;

        parent::__construct($context, $data);
    }

    /**
     * Retrieve suggested part payment price based on product price.
     *
     * @param float $price
     * @return float
     */
    public function getSuggestedPartPaymentPrice(float $price): float
    {
        $suggestedPrice = 0.0;

        try {
            $paymentMethod = $this->config->getPaymentMethod();

            if ($price > 0.0) {
                $suggestedPrice = ceil(
                    $this->helper->calculatePartPaymentPrice(
                        $paymentMethod,
                        $price,
                        $this->helper->getAnnuityFactorByDuration(
                            $paymentMethod,
                            $this->getDuration()
                        )
                    )
                );
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $suggestedPrice;
    }

    /**
     * Returns a formatted product price.
     *
     * @param float $amount
     * @return string
     */
    public function formatPrice(float $amount): string
    {
        return $this->priceCurrencyInterface
            ->convertAndFormat($amount, false, 0);
    }

    /**
     * Checks if the current price is falls within the minimum and maximum
     * prices of the selected annuity.
     *
     * @param float $price
     * @return bool
     */
    public function isValidPrice(float $price): bool
    {
        $result = false;

        try {
            $result = $this->helper->isPartPaymentPrice(
                $this->config->getPaymentMethod(),
                $price
            );
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Get enabled status for the part payment settings.
     *
     * @return bool
     */
    public function isEnabled(): bool
    {
        $result = false;

        try {
            $result = $this->config->getEnabled();
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Get the duration for the selected annuity in config.
     *
     * @return int
     */
    public function getDuration(): int
    {
        $result = 0;

        try {
            $result = $this->config->getDuration();
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Get id of the configured payment method in part payment settings.
     *
     * @return int
     */
    public function getConfiguredPaymentMethodId(): int
    {
        $result = 0;

        try {
            $result = $this->config->getPaymentMethod();
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getLogotype(): string
    {
        return $this->getViewFileUrl(
            'Resursbank_Checkout::images/resurs-bank-logo.png'
        );
    }
}
