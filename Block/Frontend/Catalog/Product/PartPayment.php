<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Block\Frontend\Catalog\Product;

use Exception;
use Magento\Catalog\Model\Product;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Resursbank\Checkout\Block\Frontend\PartPayment as PartPaymentBlock;
use Resursbank\Checkout\Helper\Config\Checkout\PartPayment as PartPaymentConfig;
use Resursbank\Checkout\Helper\Ecom;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Logic\Account\Method\Annuity as PartPaymentHelper;
use Resursbank\Checkout\Model\Account\Method;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Magento\Store\Model\StoreManagerInterface;

/**
 * @package Resursbank\Checkout\Block\Catalog\Product
 */
class PartPayment extends PartPaymentBlock
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Ecom
     */
    protected $ecom;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrencyInterface;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Method
     */
    protected $method;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * PartPayment constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param Ecom $ecom
     * @param PartPaymentHelper $helper
     * @param PriceCurrencyInterface $priceCurrencyInterface
     * @param Method $method
     * @param Log $log
     * @param PartPaymentConfig $config
     * @param ApiConfig $apiConfig
     * @param StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Ecom $ecom,
        PartPaymentHelper $helper,
        PriceCurrencyInterface $priceCurrencyInterface,
        Method $method,
        Log $log,
        PartPaymentConfig $config,
        ApiConfig $apiConfig,
        StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->registry = $registry;
        $this->ecom = $ecom;
        $this->priceCurrencyInterface = $priceCurrencyInterface;
        $this->method = $method;
        $this->product = $this->registry->registry('product');
        $this->apiConfig = $apiConfig;
        $this->storeManager = $storeManager;

        parent::__construct(
            $context,
            $helper,
            $priceCurrencyInterface,
            $config,
            $log,
            $data
        );
    }

    /**
     * Returns the price of the product, or null if the product can't be found.
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->product->getPrice() !== null ?
            (float)$this->getFinalPrice() :
            0.0;
    }

    /**
     * Returns product final price.
     *
     * @return null|float
     */
    public function getFinalPrice()
    {
        return $this->product
            ->getPriceInfo()
            ->getPrices()
            ->get('final_price')
            ->getAmount()
            ->getValue();
    }

    /**
     * Returns the part payment cost of the chosen payment method, and the
     * product price, as a string of HTML.
     *
     * @return string
     */
    public function getPurchaseCostHtml()
    {
        $result = '';

        try {
            /** @var Method $method */
            $method = $this->method->load($this->config->getPaymentMethod());
            $country = $this->apiConfig->getCountry($this->storeManager->getStore()->getCode());

            if ($method->getId()) {
                $result = $this->ecom->getConnection()
                    ->getCostOfPriceInformation(
                        !in_array($country, ['DK'])
                            ? $method->getIdentifier() : ($this->ecom->getConnection()->getPaymentMethods()),
                        (int)ceil($this->getPrice()),
                        false,
                        true
                    );
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Retrieve product type.
     *
     * @return string
     */
    public function getProductType()
    {
        // Avoid type casting here since getTypeId may return an array.
        $result = $this->product->getTypeId();

        return !is_string($result) ? '' : $result;
    }
}
