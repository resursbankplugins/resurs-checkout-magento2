<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Block\Frontend\Checkout;

use \Exception;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Framework\Pricing\PriceCurrencyInterface;
use \Resursbank\Checkout\Block\Frontend\PartPayment as PartPaymentBlock;
use \Resursbank\Checkout\Model\Api;
use \Resursbank\Checkout\Helper\Logic\Account\Method\Annuity as PartPaymentHelper;
use \Resursbank\Checkout\Helper\Config\Checkout\PartPayment as PartPaymentConfig;
use \Resursbank\Checkout\Helper\Log;

/**
 * @package Resursbank\Checkout\Block\Checkout
 */
class PartPayment extends PartPaymentBlock
{
    /**
     * @var Api
     */
    private $apiModel;


    /**
     * PartPayment constructor.
     *
     * @param Context $context
     * @param Api $apiModel
     * @param PartPaymentHelper $helper
     * @param PriceCurrencyInterface $priceCurrencyInterface
     * @param PartPaymentConfig $config
     * @param Log $log
     * @param array $data
     */
    public function __construct(
        Context $context,
        Api $apiModel,
        PartPaymentHelper $helper,
        PriceCurrencyInterface $priceCurrencyInterface,
        PartPaymentConfig $config,
        Log $log,
        array $data = []
    ) {
        $this->apiModel = $apiModel;
        $this->log = $log;

        parent::__construct(
            $context,
            $helper,
            $priceCurrencyInterface,
            $config,
            $log,
            $data
        );
    }

    /**
     * Returns the grand total from the quote.
     *
     * @return float
     */
    public function getGrandTotal()
    {
        $result = 0;

        try {
            $result = $this->apiModel->getQuote()->getGrandTotal();
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return (float) $result;
    }
}
