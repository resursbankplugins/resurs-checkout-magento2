<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Block;

use Exception;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Resursbank\Checkout\Helper\Config\Checkout\Api as Config;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\Api;
use Resursbank\Checkout\Model\Api\Adapter\Simplified as Adapter;

/**
 * Injects JavaScript initialization on the custom checkout page.
 *
 * @package Resursbank\Checkout\Block
 */
class Simplified extends Template
{
    /**
     * @var FormKey
     */
    protected $formKey;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Adapter
     */
    protected $adapter;

    /**
     * @var Log
     */
    private $log;

    /**
     * @param Context $context
     * @param FormKey $formKey
     * @param Config $config
     * @param Adapter $adapter
     * @param Log $log
     * @param array $data
     */
    public function __construct(
        Context $context,
        FormKey $formKey,
        Config $config,
        Adapter $adapter,
        Log $log,
        array $data = []
    ) {
        $this->formKey = $formKey;
        $this->config = $config;
        $this->adapter = $adapter;
        $this->log = $log;

        parent::__construct($context, $data);
    }

    /**
     * Get form key.
     *
     * @return string
     */
    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }

    /**
     * Returns the chosen country in the config.
     *
     * @return string
     */
    public function getCountry()
    {
        $result = '';

        try {
            $result = $this->config->getCountry();
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return (string) $result;
    }

    /**
     * Returns the chosen customer type. Defaults to private citizen (NATURAL).
     *
     * @return string
     */
    public function getCustomerType()
    {
        return (bool) $this->adapter->getIsCompany() === true ?
            Api::CUSTOMER_TYPE_COMPANY :
            Api::CUSTOMER_TYPE_PRIVATE;
    }

    /**
     * Returns the ID number from session.
     *
     * @return string
     */
    public function getIdNumber()
    {
        return (string) $this->adapter->getSsn();
    }

    /**
     * Returns the ID number for company contact (reference) from session.
     *
     * @return string
     */
    public function getContactId()
    {
        return (string) $this->adapter->getContactId();
    }

    /**
     * Returns the value of the private citizen customer type (NATURAL).
     *
     * @return string
     */
    public function getPrivateCustomerName()
    {
        return Api::CUSTOMER_TYPE_PRIVATE;
    }

    /**
     * Returns the value of the organization / company customer type (LEGAL).
     *
     * @return string
     */
    public function getCompanyCustomerName()
    {
        return Api::CUSTOMER_TYPE_COMPANY;
    }

    /**
     * Returns the label text for the ID (SSN) input field when selected
     * customer type is organization / company.
     *
     * @return string
     */
    public function getLegalLabel()
    {
        return __('Org. number (XXXXXXXXXX)');
    }

    /**
     * Returns the label text for the ID (SSN) input field when selected
     * customer type is private citizen.
     *
     * @return string
     */
    public function getNaturalLabel()
    {
        return __('SSN (YYYYMMDDXXXX)');
    }
}
