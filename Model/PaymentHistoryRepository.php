<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model;

use Exception;
use Magento\Framework\Api\SearchCriteria\CollectionProcessor\FilterProcessor;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Resursbank\Checkout\Api\Data\PaymentHistoryInterface;
use Resursbank\Checkout\Api\Data\PaymentHistorySearchResultsInterface;
use Resursbank\Checkout\Api\Data\PaymentHistorySearchResultsInterfaceFactory;
use Resursbank\Checkout\Api\PaymentHistoryRepositoryInterface;
use Resursbank\Checkout\Model\PaymentHistoryFactory;
use Resursbank\Checkout\Model\ResourceModel\PaymentHistory as ResourceModel;
use Resursbank\Checkout\Model\ResourceModel\PaymentHistory\CollectionFactory;

/**
 * Repository for payment history event entries.
 *
 * @package Resursbank\Checkout\Model
 */
class PaymentHistoryRepository implements PaymentHistoryRepositoryInterface
{
    /**
     * @var PaymentHistoryFactory
     */
    protected $paymentHistoryFactory;

    /**
     * @var PaymentHistorySearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var ResourceModel
     */
    protected $resourceModel;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var FilterProcessor
     */
    private $filterProcessor;

    /**
     * @param ResourceModel $resourceModel
     * @param PaymentHistoryFactory $paymentHistoryFactory
     * @param PaymentHistorySearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionFactory $collectionFactory
     * @param FilterProcessor $filterProcessor
     */
    public function __construct(
        ResourceModel $resourceModel,
        PaymentHistoryFactory $paymentHistoryFactory,
        PaymentHistorySearchResultsInterfaceFactory $searchResultsFactory,
        CollectionFactory $collectionFactory,
        FilterProcessor $filterProcessor
    ) {
        $this->resourceModel = $resourceModel;
        $this->paymentHistoryFactory = $paymentHistoryFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionFactory = $collectionFactory;
        $this->filterProcessor = $filterProcessor;
    }

    /**
     * @inheritDoc
     * @throws AlreadyExistsException
     * @throws Exception
     */
    public function save(
        PaymentHistoryInterface $entry
    ): PaymentHistoryInterface {
        $this->resourceModel->save($entry);

        return $entry;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function delete(PaymentHistoryInterface $entry): bool
    {
        $this->resourceModel->delete($entry);

        return true;
    }

    /**
     * @inheritDoc
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById(int $Id): bool
    {
        return $this->delete($this->get($Id));
    }

    /**
     * @inheritDoc
     * @throws NoSuchEntityException
     */
    public function get(int $id): PaymentHistoryInterface
    {
        $history = $this->paymentHistoryFactory->create();
        $history->getResource()->load($history, $id);

        if (!$history->getId()) {
            throw new NoSuchEntityException(
                __('Unable to find payment history entry with ID %1', $id)
            );
        }

        return $history;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    ): PaymentHistorySearchResultsInterface {
        $collection = $this->collectionFactory->create();

        $this->filterProcessor->process($searchCriteria, $collection);

        $collection->load();

        return $this->searchResultsFactory->create()
            ->setSearchCriteria($searchCriteria)
            ->setItems($collection->getItems())
            ->setTotalCount($collection->getSize());
    }
}
