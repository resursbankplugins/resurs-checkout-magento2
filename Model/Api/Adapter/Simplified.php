<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model\Api\Adapter;

use Exception;
use Magento\Checkout\Model\Session;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Address as OrderAddress;
use Resursbank\Checkout\Exception\InvalidDataException;
use Resursbank\Checkout\Helper\Api\Simplified\Validator;
use Resursbank\Checkout\Helper\Config\Checkout\Advanced as AdvancedConfig;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Db\Account\Method as MethodHelper;
use Resursbank\Checkout\Helper\Db\PaymentHistory as PaymentHistoryDbHelper;
use Resursbank\Checkout\Helper\Ecom;
use Resursbank\Checkout\Helper\Order as OrderHelper;
use Resursbank\Checkout\Model\Account\Method as MethodModel;
use Resursbank\Checkout\Model\Api\Adapter\Simplified\Address;
use Resursbank\Checkout\Model\Api\Adapter\Simplified\Payment;
use Resursbank\Checkout\Model\Api as ApiModel;
use Resursbank\Checkout\Model\Api\Payment\Converter\OrderConverter;
use Resursbank\Checkout\Model\Config\Source\Country;
use Resursbank\Checkout\Model\PaymentHistory;
use Resursbank\RBEcomPHP\ResursBank;

/**
 * API calls/actions utilizing Simplified Flow.
 *
 * @package Resursbank\Checkout\Api\Adapter
 */
class Simplified
{
    /**
     * Temporary storage location in session for the requested card amount. We
     * will later submit this in the payload of the API call to create the
     * payment.
     *
     * @var string
     */
    const SESSION_KEY_CARD_AMOUNT = 'card_amount';

    /**
     * Temporary storage location in session for company. We need to keep it in
     * memory, without saving it in our database, until the purchase has been
     * completed.
     *
     * @var string
     */
    const SESSION_KEY_IS_COMPANY = 'rbcs_company';

    /**
     * Temporary storage location in session for SSN. We need to keep it in
     * memory, without saving it in our database, until the purchase has been
     * completed.
     *
     * @var string
     */
    const SESSION_KEY_SSN = 'rbcs_ssn';

    /**
     * Temporary storage location in session for contact government id, in case
     * of using LEGAL payment methods. We need to keep it in memory, without
     * saving it in our database, until the purchase has been completed.
     *
     * @var string
     */
    const SESSION_KEY_CONTACT_GOVERNMENT_ID = 'rbcs_contact_government_id';

    /**
     * Temporary storage location in session for contact government id, in case
     * of using LEGAL payment methods. We need to keep it in memory, without
     * saving it in our database, until the purchase has been completed.
     *
     * @var string
     */
    const SESSION_KEY_COMPANY_GOVERNMENT_ID = 'rbcs_company_government_id';

    /**
     * Temporary storage location in session for card number, when payment methods is
     * based on existing Resurs Bank-cards.
     */
    const SESSION_KEY_CARD_NUMBER = 'rbcs_card_number';

    /**
     * Session key where payment id is stored between signing and booking of
     * signed payment.
     *
     * @var string
     */
    const SESSION_PAYMENT_ID = 'rbcs_payment_id';

    /**
     * Session key where signing url is stored between booking and signing.
     *
     * @var string
     */
    const SESSION_KEY_SIGNING_URL = 'rbcs_signing_url';

    /**
     * Registry key for payment status error.
     *
     * @var string
     */
    const REGISTRY_KEY_PAYMENT_STATUS_ERROR = 'resursbank_checkout_payment_status_error';

    /**
     * @var Ecom
     */
    private $ecom;

    /**
     * @var ApiModel
     */
    private $api;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @var AdvancedConfig
    */
    private $advancedConfig;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var MethodHelper
     */
    private $methodHelper;

    /**
     * @var OrderConverter
     */
    private $orderConverter;

    /**
     * @var Validator
     */
    private $validator;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var PaymentHistoryDbHelper
     */
    private $paymentHistoryDbHelper;

    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @param Registry $registry
     * @param ApiModel $api
     * @param ApiConfig $apiConfig
     * @param AdvancedConfig $advancedConfig
     * @param Ecom $ecom
     * @param Session $session
     * @param MethodHelper $methodHelper
     * @param OrderConverter $orderConverter
     * @param Validator $validator
     * @param PaymentHistoryDbHelper $paymentHistoryDbHelper
     * @param OrderHelper $orderHelper
     */
    public function __construct(
        Registry $registry,
        ApiModel $api,
        ApiConfig $apiConfig,
        AdvancedConfig $advancedConfig,
        Ecom $ecom,
        Session $session,
        MethodHelper $methodHelper,
        OrderConverter $orderConverter,
        Validator $validator,
        PaymentHistoryDbHelper $paymentHistoryDbHelper,
        OrderHelper $orderHelper
    ) {
        $this->registry = $registry;
        $this->api = $api;
        $this->apiConfig = $apiConfig;
        $this->advancedConfig = $advancedConfig;
        $this->ecom = $ecom;
        $this->session = $session;
        $this->methodHelper = $methodHelper;
        $this->orderConverter = $orderConverter;
        $this->validator = $validator;
        $this->paymentHistoryDbHelper = $paymentHistoryDbHelper;
        $this->orderHelper = $orderHelper;
    }

    /**
     * Retrieve customer address using SSN.
     *
     * @param string $ssn
     * @param string $customerType
     * @return Address
     * @throws Exception
     * @todo Either the parameter $customerType, or the call to getIsCompany() is unnecessary, I think.
     */
    public function getAddress($ssn, $customerType = 'NATURAL')
    {
        // Retrieve raw address data from the API.
        $address = $this->ecom->getConnection()->getAddress(
            (string)$ssn,
            $customerType
        );

        if (!is_object($address)) {
            throw new Exception('Failed to fetch address.');
        }

        return new Address(
            (isset($address->firstName) ? (string) $address->firstName : ''),
            (isset($address->lastName) ? (string) $address->lastName : ''),
            (isset($address->postalArea) ? (string) $address->postalArea : ''),
            (isset($address->postalCode) ? (string) $address->postalCode : ''),
            (isset($address->country) ? (string) $address->country : ''),
            (
                isset($address->addressRow1) ?
                    (string) $address->addressRow1 :
                    ''
            ),
            (
                isset($address->addressRow2) ?
                    (string) $address->addressRow2 :
                    ''
            ),
            (
                ($this->getIsCompany() && isset($address->fullName)) ?
                    (string) $address->fullName :
                    ''
            )
        );
    }

    /**
     * When fetching the address there could be natural errors, if for example
     * you attempt to fetch a Norwegian address when your API account is
     * configured for Sweden. For that reason we do not consider problems while
     * fetching an address to be an actual problem, we simply return an empty
     * address object.
     *
     * @param string $country
     * @return bool
     * @throws Exception
     */
    public function canFetchAddress($country)
    {
        $configCountry = $this->apiConfig->getCountry();

        return (
            $country === Country::SWEDEN &&
            $configCountry === Country::SWEDEN
        );
    }

    /**
     * Store SSN in session for later use.
     *
     * @param string $value
     * @param string $countryId
     * @param string $customerType
     * @return $this
     * @throws InvalidDataException
     */
    public function setSsn($value, $countryId, $customerType)
    {
        $valid = $this->validator->getId()->validate(
            $value,
            $countryId,
            $customerType,
            true
        );

        if (!$valid) {
            throw new InvalidDataException(
                'The SSN you entered is invalid. Please verify your ' .
                'SSN, and make sure you\'ve selected the right country.'
            );
        }

        $this->session->setData(self::SESSION_KEY_SSN, (string)$value);

        return $this;
    }

    /**
     * Retrieve stored SSN.
     *
     * @return string
     */
    public function getSsn()
    {
        return (string)$this->session->getData(self::SESSION_KEY_SSN);
    }

    /**
     * Removes stored SSN.
     */
    public function unsetSsn()
    {
        $this->session->unsetData(self::SESSION_KEY_SSN);
    }

    /**
     * Stores the flag for if the customer is a company or not in the session
     * for later use.
     *
     * @param bool $value
     * @return $this
     */
    public function setIsCompany($value)
    {
        $this->session->setData(self::SESSION_KEY_IS_COMPANY, (bool) $value);

        return $this;
    }

    /**
     * Retrieves the flag for if the customer is a company or not.
     *
     * @return bool
     */
    public function getIsCompany()
    {
        return (bool) $this->session->getData(self::SESSION_KEY_IS_COMPANY);
    }

    /**
     * Removes the flag for if the customer is a company or not from the
     * session.
     */
    public function unsetIsCompany()
    {
        $this->session->unsetData(self::SESSION_KEY_IS_COMPANY);
    }

    /**
     * Store card number in session for later use.
     *
     * @param string $value
     * @return $this
     * @throws InvalidDataException
     */
    public function setCardNumber($value)
    {
        if (!$this->validator->getCard()->validate($value, true)) {
            throw new InvalidDataException(
                'The card number you\'ve entered is invalid. Please ' .
                'verify the number and try again.'
            );
        }

        $this->session->setData(self::SESSION_KEY_CARD_NUMBER, (string)$value);

        return $this;
    }

    /**
     * Retrieve stored card number.
     *
     * @return string
     */
    public function getCardNumber()
    {
        return (string)$this->session->getData(self::SESSION_KEY_CARD_NUMBER);
    }

    /**
     * Removes stored card number.
     */
    public function unsetCardNumber()
    {
        $this->session->unsetData(self::SESSION_KEY_CARD_NUMBER);
    }

    /**
     * Store a contact id in session for later use.
     *
     * @param string $value
     * @param string $countryId
     * @return $this
     * @throws InvalidDataException
     */
    public function setContactId($value, $countryId)
    {
        if (!$this->validator->getId()->ssn($value, $countryId, true)) {
            throw new InvalidDataException(
                'The SSN you entered is invalid. Please verify your ' .
                'SSN, and make sure you\'ve selected the right country.'
            );
        }

        $this->session->setData(
            self::SESSION_KEY_CONTACT_GOVERNMENT_ID,
            (string)$value
        );

        return $this;
    }

    /**
     * Retrieve stored contact id.
     *
     * @return string
     */
    public function getContactId()
    {
        return (string)$this->session->getData(
            self::SESSION_KEY_CONTACT_GOVERNMENT_ID
        );
    }

    /**
     * Removes stored contact id.
     */
    public function unsetContactId()
    {
        $this->session->unsetData(self::SESSION_KEY_CONTACT_GOVERNMENT_ID);
    }

    /**
     * Stores the requested card amount in session.
     *
     * @param float $value
     * @return $this
     */
    public function setCardAmount($value)
    {
        $this->session->setData(self::SESSION_KEY_CARD_AMOUNT, $value);

        return $this;
    }

    /**
     * Retrieves the stored card amount from session.
     *
     * @return float|null
     */
    public function getCardAmount()
    {
        return $this->session->getData(self::SESSION_KEY_CARD_AMOUNT);
    }

    /**
     * Removes the stored card amount from session.
     */
    public function unsetCardAmount()
    {
        $this->session->unsetData(self::SESSION_KEY_CARD_AMOUNT);
    }

    /**
     * Create payment.
     *
     * @param Order $order
     * @return Payment
     * @throws Exception
     */
    public function createPayment(Order $order)
    {
        $this->paymentHistoryDbHelper->addEntry(
            $this->paymentHistoryDbHelper->createEntry(
                $order->getPayment()->getEntityId(),
                PaymentHistory::EVENT_PAYMENT_CREATING,
                PaymentHistory::USER_RESURS_BANK
            )
        );

        /** @var ResursBank $ecom */
        $ecom = $this->ecom->getConnection();

        // Make preparations.
        $this->preparePayment($order, $ecom);

        /** @var MethodModel $paymentMethod */
        $paymentMethod = $this->methodHelper->loadByCode(
            $order->getPayment()->getMethod()
        );

        // Fill data on payment object.
        $payment = $ecom->createPayment(
            $paymentMethod->getIdentifier()
        );

        /** @var Payment $result */
        $result = new Payment(
            $payment->paymentId,
            $payment->bookPaymentStatus,
            $payment->approvedAmount,
            $payment->signingUrl,
            $payment->customer
        );

        try {
            // Handle payment status (like "DENIED" etc.).
            $this->handlePaymentStatus($result, $order);
        } catch (Exception $e) {
            // For an explanation of this, please see Plugin/Order/AroundPlace.
            $this->registry->register(
                self::REGISTRY_KEY_PAYMENT_STATUS_ERROR,
                $e
            );

            throw $e;
        }

        return $result;
    }

    /**
     * Get the payment information of a registered payment.
     *
     * @param string $paymentId
     * @return array - Returns an empty array if a payment couldn't be found.
     * @throws Exception
     */
    public function getPayment($paymentId)
    {
        $payment = $this->ecom->getConnection()->getPayment($paymentId);

        return $payment !== null ?
            json_decode(json_encode($payment), true) :
            [];
    }

    /**
     * Handle payment status. For example, immediately after creating a payment
     * we want to check for potential problems by examining the status the
     * payment has obtained.
     *
     * @param Payment $result
     * @param Order $order
     * @param array $reject Array of status codes indicating failure. Varies
     * depending on process.
     * @return $this
     * @throws Exception
     * @link https://test.resurs.com/docs/x/I4cW Reference for statuses.
     */
    private function handlePaymentStatus(
        Payment $result,
        Order $order,
        array $reject = ['DENIED'],
        bool $updateStatus = true
    ) {
       $paymentStatus = $result->getBookPaymentStatus();

       if ($paymentStatus === 'DENIED') {
            $this->orderHelper->deny($order);
        } else {
            if ($paymentStatus === 'SIGNING') {
                $this->paymentHistoryDbHelper->addEntry(
                    $this->paymentHistoryDbHelper->createEntry(
                        $order->getPayment()->getEntityId(),
                        PaymentHistory::EVENT_PAYMENT_SIGNING,
                        PaymentHistory::USER_RESURS_BANK
                    )
                );
            }

            if ($updateStatus) {
                $this->orderHelper->pendingPayment($order);
            }
        }

        // Handle reject statuses.
        if (in_array($result->getBookPaymentStatus(), $reject)) {
            throw new Exception(
                __(
                    'Your payment has been rejected, please select a ' .
                    'different payment method and try again. If the problem ' .
                    'persists please contact us for assistance.'
                ),
                400
            );
        }

        return $this;
    }

    /**
     * Book payment after it's been signed by the client.
     *
     * @param string $paymentId
     * @param Order $order
     * @return Payment
     * @throws Exception
     */
    public function bookPayment($paymentId, Order $order)
    {
        // Fill data on payment object.
        $payment = $this->ecom->getConnection()->bookSignedPayment($paymentId);

        /** @var Payment $result */
        $result = new Payment(
            $payment->paymentId,
            $payment->bookPaymentStatus,
            $payment->approvedAmount,
            $payment->signingUrl,
            $payment->customer
        );

        // Handle payment status (like "DENIED" etc.).
        $this->handlePaymentStatus(
            $result,
            $order,
            ['DENIED', 'SIGNING'],
            false
        );

        return $result;
    }

    /**
     * Prepare payment creation.
     *
     * @param Order $order
     * @param ResursBank $ecom
     * @return $this
     * @throws Exception
     */
    public function preparePayment(
        Order $order,
        ResursBank $ecom
    ) {
        // Resolve payment items from order entity (as an array).
        $items = $this->orderConverter->convertItemsToArrays(
            $this->orderConverter->convert($order)
        );

        $shippingAddress = $order->getShippingAddress();
        $billingAddress = $order->getBillingAddress();

        // Set customer data.
        $this->setCustomerData(
            $order,
            $ecom
        )->setCustomerBillingAddress(
            $billingAddress,
            $ecom
        )->setCustomerShippingAddress(
            $shippingAddress !== null ? $shippingAddress : $billingAddress,
            $ecom
        )->addOrderLines(
            $items,
            $ecom
        )->setOrderId(
            $order,
            $ecom
        )->setSigningUrls(
            $ecom
        )->setPaymentData(
            $ecom
        );

        return $this;
    }

    /**
     * Set customer data through ECom.
     *
     * @param Order $order
     * @param ResursBank $ecom
     * @return $this
     * @throws Exception
     */
    public function setCustomerData(
        Order $order,
        ResursBank $ecom
    ) {
        $ecom->setCustomer(
            (string)$this->getSsn(),
            (string)$order->getBillingAddress()->getTelephone(),
            (string)$order->getBillingAddress()->getTelephone(),
            (string)$order->getCustomerEmail(),
            (string)$this->getCustomerType($order),
            (string)$this->session->getData(
                self::SESSION_KEY_CONTACT_GOVERNMENT_ID
            )
        );

        $ecom->setCardData(
            (string) $this->getCardNumber(),
            (float) $this->getCardAmount()
        );

        return $this;
    }

    /**
     * Use order increment_id as payment reference.
     *
     * @param Order $order
     * @param ResursBank $ecom
     * @return $this
     */
    public function setOrderId(
        Order $order,
        ResursBank $ecom
    ) {
        $ecom->setPreferredId($order->getIncrementId());

        return $this;
    }

    /**
     * Set payment signing URLs. This is useful for certain API:s, like
     * Simplified.
     *
     * @param ResursBank $ecom
     * @return $this
     * @throws Exception
     */
    public function setSigningUrls(ResursBank $ecom)
    {
        $ecom->setSigning(
            $this->api->getSuccessCallbackUrl(),
            $this->api->getFailureCallbackUrl()
        );

        return $this;
    }

    /**
     * Set the payment data flags.
     *
     * @param   ResursBank $ecom
     * @return  $this
     */
    public function setPaymentdata(ResursBank $ecom)
    {
        $ecom->setWaitForFraudControl(
            $this->advancedConfig->isWaitingForFraudControl()
        );

        $ecom->setAnnulIfFrozen(
            $this->advancedConfig->isWaitingForFraudControl() ?
                $this->advancedConfig->isAnnulIfFrozen() :
                false
        );

        $ecom->setFinalizeIfBooked(
            $this->advancedConfig->isFinalizeIfBooked()
        );

        return $this;
    }

    /**
     * Set customer billing address information through ECom.
     *
     * @param OrderAddress $address
     * @param ResursBank $ecom
     * @return $this
     * @throws Exception
     */
    public function setCustomerBillingAddress(
        OrderAddress $address,
        ResursBank $ecom
    ) {
        $ecom->setBillingAddress(
            ($address->getFirstname() . ' ' . $address->getLastname()),
            $address->getFirstname(),
            $address->getLastname(),
            $address->getStreetLine(1),
            $address->getStreetLine(2),
            $address->getCity(),
            $address->getPostcode(),
            $address->getCountryId()
        );

        return $this;
    }

    /**
     * Set customer shipping address information through ECom.
     *
     * @param OrderAddress $address
     * @param ResursBank $ecom
     * @return $this
     * @throws Exception
     */
    public function setCustomerShippingAddress(
        OrderAddress $address,
        ResursBank $ecom
    ) {
        $ecom->setDeliveryAddress(
            ($address->getFirstname() . ' ' . $address->getLastname()),
            $address->getFirstname(),
            $address->getLastname(),
            $address->getStreetLine(1),
            $address->getStreetLine(2),
            $address->getCity(),
            $address->getPostcode(),
            $address->getCountryId()
        );

        return $this;
    }

    /**
     * Get customer type.
     *
     * @return string
     */
    public function getCustomerType()
    {
        return $this->getIsCompany() ?
            ApiModel::CUSTOMER_TYPE_COMPANY :
            ApiModel::CUSTOMER_TYPE_PRIVATE;
    }

    /**
     * Add payment line through ECom.
     *
     * @param array $paymentLines
     * @param ResursBank $ecom
     * @return $this
     * @throws Exception
     */
    public function addOrderLines(
        array $paymentLines,
        ResursBank $ecom
    ) {
        foreach ($paymentLines as $paymentLine) {
            $ecom->addOrderLine(
                $paymentLine['artNo'],
                $paymentLine['description'],
                $paymentLine['unitAmountWithoutVat'],
                $paymentLine['vatPct'],
                $paymentLine['unitMeasure'],
                $paymentLine['type'],
                $paymentLine['quantity']
            );
        }

        return $this;
    }
}
