<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model\Api\Adapter\Simplified;

use \stdClass;

/**
 * This class is used to structure payment information returned from ECom after
 * creating a payment. Using this we can always be sure the properties we
 * require exist and have the correct type, even if they do not have a value.
 *
 * @package Resursbank\Checkout\Model\Api\Adapter\Simplified
 */
class Payment
{
    /**
     * @var string
     */
    private $paymentId;

    /**
     * @var string
     */
    private $bookPaymentStatus;

    /**
     * @var string
     */
    private $signingUrl;

    /**
     * @var float
     */
    private $approvedAmount;

    /**
     * @var stdClass
     */
    private $customer;

    /**
     * @param string $paymentId
     * @param string $bookPaymentStatus
     * @param float $approvedAmount
     * @param string $signingUrl
     * @param null|stdClass $customer
     */
    public function __construct(
        $paymentId,
        $bookPaymentStatus,
        $approvedAmount,
        $signingUrl = '',
        $customer = null
    ) {
        $this->setPaymentId($paymentId)
            ->setBookPaymentStatus($bookPaymentStatus)
            ->setApprovedAmount($approvedAmount)
            ->setSigningUrl($signingUrl);

        if ($customer instanceof stdClass) {
            $this->setCustomer($customer);
        }
    }

    /**
     * @param string $val
     * @return $this
     */
    public function setPaymentId($val)
    {
        $this->paymentId = (string) $val;

        return $this;
    }

    /**
     * @return string
     */
    public function getPaymentId()
    {
        return (string) $this->paymentId;
    }

    /**
     * @param string $val
     * @return $this
     */
    public function setBookPaymentStatus($val)
    {
        $this->bookPaymentStatus = (string) $val;

        return $this;
    }

    /**
     * @return string
     */
    public function getBookPaymentStatus()
    {
        return (string) $this->bookPaymentStatus;
    }

    /**
     * @param string $val
     * @return $this
     */
    public function setSigningUrl($val)
    {
        $this->signingUrl = (string) $val;

        return $this;
    }

    /**
     * @return string
     */
    public function getSigningUrl()
    {
        return (string) $this->signingUrl;
    }

    /**
     * @param float $val
     * @return $this
     */
    public function setApprovedAmount($val)
    {
        $this->approvedAmount = (float) $val;

        return $this;
    }

    /**
     * @return float
     */
    public function getApprovedAmount()
    {
        return (float) $this->approvedAmount;
    }

    /**
     * @param stdClass $val
     * @return $this
     */
    public function setCustomer(stdClass $val)
    {
        $this->customer = $val;

        return $this;
    }

    /**
     * @return stdClass
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}
