<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model\Api\Adapter\Simplified;

use Resursbank\Checkout\Exception\InvalidDataException;
use Resursbank\Checkout\Model\Config\Source\Country;

/**
 * This class is used to structure address information fetched through ECom.
 * Using this we can always be sure the properties we require exist and have the
 * correct type.
 *
 * @package Resursbank\Checkout\Model\Api\Adapter\Simplified
 */
class Address
{
    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $postcode;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $street0;

    /**
     * @var string
     */
    private $street1;

    /**
     * @var string
     */
    private $company;

    /**
     * @param string $firstName
     * @param string $lastName
     * @param string $city
     * @param string $postcode
     * @param string $country
     * @param string $street0
     * @param string $street1
     * @param string $company
     * @throws InvalidDataException
     */
    public function __construct(
        $firstName,
        $lastName,
        $city,
        $postcode,
        $country,
        $street0,
        $street1 = '',
        $company = ''
    ) {
        $this->setFirstName($firstName)
            ->setLastName($lastName)
            ->setCity($city)
            ->setPostcode($postcode)
            ->setCountry($country)
            ->setStreet0($street0)
            ->setStreet1($street1)
            ->setCompany($company);
    }

    /**
     * @param string $val
     * @return $this
     */
    public function setFirstName($val)
    {
        $this->firstName = (string) $val;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $val
     * @return $this
     */
    public function setLastName($val)
    {
        $this->lastName = (string) $val;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $val
     * @return $this
     */
    public function setCity($val)
    {
        $this->city = (string) $val;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $val
     * @return $this
     */
    public function setPostcode($val)
    {
        $val = (string) $val;

        // Magento expects postcodes to be formatted as "123 45".
        if (strlen($val) > 3) {
            $this->postcode = substr($val, 0, 3) . ' ' . substr($val, 3);
        } else {
            $this->postcode = $val;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $val
     * @return $this
     * @throws InvalidDataException
     */
    public function setCountry($val)
    {
        if (!in_array($val, Country::getValidCountryCodes())) {
            throw new InvalidDataException(
                __('%1 is not a valid country.', $val)
            );
        }

        $this->country = (string) $val;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $val
     * @return $this
     */
    public function setStreet0($val)
    {
        $this->street0 = (string) $val;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreet0()
    {
        return $this->street0;
    }

    /**
     * @param string $val
     * @return $this
     */
    public function setStreet1($val)
    {
        $this->street1 = (string) $val;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreet1()
    {
        return $this->street1;
    }

    /**
     * @param string $val
     * @return $this
     */
    public function setCompany($val)
    {
        $this->company = (string) $val;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Creates and returns an array of the address data.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'firstname' => $this->getFirstName(),
            'lastname' => $this->getLastName(),
            'city' => $this->getCity(),
            'postcode' => $this->getPostcode(),
            'country' => $this->getCountry(),
            'street0' => $this->getStreet0(),
            'street1' => $this->getStreet1(),
            'company' => $this->getCompany(),
        ];
    }
}
