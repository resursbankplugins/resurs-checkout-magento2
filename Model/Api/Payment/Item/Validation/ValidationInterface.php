<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model\Api\Payment\Item\Validation;

use Exception;
use InvalidArgumentException;

/**
 * Requirements for property validation.
 */
interface ValidationInterface
{
    /**
     * NOTE: all child classes are expected to define values to be validated
     * as optional argument(s) for this method. Since their datatype(s) may
     * vary, arguments cannot be defined within this interface.
     *
     * @link https://test.resurs.com/docs/display/ecom/Hosted+payment+flow+data
     * @throws Exception When something unexpected happens.
     * @throws InvalidArgumentException When validation fails.
     */
    public static function validate();
}
