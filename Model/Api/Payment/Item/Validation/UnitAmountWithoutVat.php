<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model\Api\Payment\Item\Validation;

/**
 * Validation routines for property "unitAmountWithoutVat".
 */
class UnitAmountWithoutVat extends AbstractValidation implements ValidationInterface
{
    /**
     * @var int
     */
    const MIN_INTEGER_LENGTH = 0;

    /**
     * @var int
     */
    const MAX_INTEGER_LENGTH = 15;

    /**
     * @var int
     */
    const MIN_DECIMAL_LENGTH = 0;

    /**
     * @var int
     */
    const MAX_DECIMAL_LENGTH = 5;

    /**
     * NOTE: This is unsigned since payment items of type DISCOUNT expects a
     * negative value while ORDER_LINE and SHIPPING_FEE expects positive values.
     *
     * @inheritDoc
     */
    public static function validate(float $value = 0.0)
    {
        self::hasFloatLength(
            $value,
            [
                'integer' => [
                    'min' => self::MIN_INTEGER_LENGTH,
                    'max' => self::MAX_INTEGER_LENGTH
                ],
                'decimal' => [
                    'min' => self::MIN_DECIMAL_LENGTH,
                    'max' => self::MAX_DECIMAL_LENGTH
                ]
            ]
        );
    }
}
