<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model\Api\Payment\Item\Validation;

/**
 * Validation routines for property "artNo".
 */
class ArtNo extends AbstractValidation implements ValidationInterface
{
    /**
     * Regex defining allowed characters.
     *
     * NOTE: The regex is reversed for simplified use with preg_replace to clean
     * your values. The validating preg_match uses the same regex, again in
     * reverse, to validate the value you've provided.
     *
     * @var string
     */
    const REGEX = "/[^a-z0-9]/";

    /**
     * @var int
     */
    const MIN_LENGTH = 0;

    /**
     * @var int
     */
    const MAX_LENGTH = 100;

    /**
     * @inheritDoc
     */
    public static function validate(string $value = '')
    {
        self::matchesRegex($value, self::REGEX);
        self::hasStringLength($value, [
            'min' => self::MIN_LENGTH,
            'max' => self::MAX_LENGTH
        ]);
    }
}
