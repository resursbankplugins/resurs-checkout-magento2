<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model\Api\Payment;

use Exception;
use InvalidArgumentException;
use Resursbank\Checkout\Model\Api\Payment\Item\Validation\ArtNo;
use Resursbank\Checkout\Model\Api\Payment\Item\Validation\Description;
use Resursbank\Checkout\Model\Api\Payment\Item\Validation\Quantity;
use Resursbank\Checkout\Model\Api\Payment\Item\Validation\Type;
use Resursbank\Checkout\Model\Api\Payment\Item\Validation\UnitAmountWithoutVat;
use Resursbank\Checkout\Model\Api\Payment\Item\Validation\UnitMeasure;
use Resursbank\Checkout\Model\Api\Payment\Item\Validation\VatPct;

/**
 * Information representing a single line in a payment payload.
 *
 * NOTE: all validation routines are separated into individual classes because
 * they all are responsible for their own ruleset and routine. All validation
 * classes are called statically to avoid dependencies in this class.
 */
class Item
{
    /**
     * Data key representing SKU.
     *
     * @var string
     */
    const KEY_ART_NO = 'artNo';

    /**
     * Data key representing description.
     *
     * @var string
     */
    const KEY_DESCRIPTION = 'description';

    /**
     * Data key representing quantity.
     *
     * @var string
     */
    const KEY_QUANTITY = 'quantity';

    /**
     * Data key representing unit measure.
     *
     * @var string
     */
    const KEY_UNIT_MEASURE = 'unitMeasure';

    /**
     * Data key representing price excl. tax.
     *
     * @var string
     */
    const KEY_UNIT_AMOUNT_WITHOUT_VAT = 'unitAmountWithoutVat';

    /**
     * Data key representing tax percentage.
     *
     * @var string
     */
    const KEY_VAT_PCT = 'vatPct';

    /**
     * Data key representing item type.
     *
     * @var string
     */
    const KEY_TYPE = 'type';

    /**
     * Shipping item type identifier.
     *
     * @var string
     */
    const TYPE_SHIPPING = 'SHIPPING_FEE';

    /**
     * General item type identifier.
     *
     * @var string
     */
    const TYPE_PRODUCT = 'ORDER_LINE';

    /**
     * Discount item type identifier.
     *
     * @var string
     */
    const TYPE_DISCOUNT = 'DISCOUNT';

    /**
     * Payment fee item type identifier.
     *
     * @var string
     */
    const TYPE_PAYMENT_FEE = 'ORDER_LINE';

    /**
     * @var string
     */
    private $artNo;

    /**
     * @var string
     */
    private $description;

    /**
     * @var float
     */
    private $quantity;

    /**
     * Unit measurement, for example "kg" or "cup".
     *
     * NOTE: This value is always required, even if the item specification does
     * not rely on a unit measurement value.
     *
     * @var string
     */
    private $unitMeasure;

    /**
     * Unit price without VAT (excl. tax).
     *
     * @var float
     */
    private $unitAmountWithoutVat;

    /**
     * Tax percentage.
     *
     * @var float
     */
    private $vatPct;

    /**
     * Item type specification.
     *
     * ORDER_LINE = general, usually products.
     * SHIPPING_FEE = shipping.
     * DISCOUNT = discount (negative).
     *
     * @var string
     */
    private $type;

    /**
     * @param string $artNo
     * @param string $description
     * @param float $quantity
     * @param string $unitMeasure
     * @param float $unitAmountWithoutVat
     * @param float $vatPct
     * @param string $type
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function __construct(
        string $artNo,
        string $description,
        float $quantity,
        string $unitMeasure,
        float $unitAmountWithoutVat,
        float $vatPct,
        string $type
    ) {
        $this->setArtNo($artNo)
            ->setDescription($description)
            ->setQuantity($quantity)
            ->setUnitMeasure($unitMeasure)
            ->setUnitAmountWithoutVat($unitAmountWithoutVat)
            ->setVatPct($vatPct)
            ->setType($type);
    }

    /**
     * @param string $value
     * @return Item
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function setArtNo(string $value): Item
    {
        $this->artNo = $value;

        ArtNo::validate($value);

        return $this;
    }

    /**
     * @return string
     */
    public function getArtNo(): string
    {
        return $this->artNo;
    }

    /**
     * @param string $value
     * @return Item
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function setDescription(string $value): Item
    {
        $this->description = $value;

        Description::validate($value);

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param float $value
     * @return Item
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function setQuantity(float $value): Item
    {
        $this->quantity = $value;

        Quantity::validate($value);

        return $this;
    }

    /**
     * @return float
     */
    public function getQuantity(): float
    {
        return $this->quantity;
    }

    /**
     * @param string $value
     * @return Item
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function setUnitMeasure(string $value): Item
    {
        $this->unitMeasure = $value;

        UnitMeasure::validate($value);

        return $this;
    }

    /**
     * @return string
     */
    public function getUnitMeasure(): string
    {
        return $this->unitMeasure;
    }

    /**
     * @param float $value
     * @return Item
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function setUnitAmountWithoutVat(float $value): Item
    {
        $this->unitAmountWithoutVat = $value;

        UnitAmountWithoutVat::validate($value);

        return $this;
    }

    /**
     * @return float
     */
    public function getUnitAmountWithoutVat(): float
    {
        return $this->unitAmountWithoutVat;
    }

    /**
     * @param float $value
     * @return Item
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function setVatPct(float $value): Item
    {
        $this->vatPct = $value;

        VatPct::validate($value);

        return $this;
    }

    /**
     * @return float
     */
    public function getVatPct(): float
    {
        return $this->vatPct;
    }

    /**
     * @param string $value
     * @return Item
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function setType(string $value): Item
    {
        $this->type = $value;

        Type::validate($value);

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Retrieve object data converted to array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
