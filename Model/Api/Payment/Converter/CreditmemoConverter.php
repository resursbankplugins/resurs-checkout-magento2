<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model\Api\Payment\Converter;

use Exception;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order\Creditmemo\Item;
use Magento\Sales\Model\ResourceModel\Order\Tax\ItemFactory as TaxItemResourceFactory;
use Resursbank\Checkout\Exception\InvalidDataException;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Creditmemo as CreditmemoHelper;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\Creditmemo\ProductItem;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\Creditmemo\ProductItemFactory;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\DiscountItemFactory;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\PaymentFeeItemFactory;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\ShippingItemFactory;
use Resursbank\Checkout\Model\Api\Payment\Item as PaymentItem;
use Resursbank\Checkout\Model\Api\Payment\ItemFactory;
use Resursbank\RBEcomPHP\ResursBank;

/**
 * Creditmemo entity conversion for payment payloads.
 */
class CreditmemoConverter extends AbstractConverter implements ConverterInterface
{
    /**
     * @var ProductItemFactory
     */
    private $productItemFactory;

    /**
     * @var CreditmemoHelper
     */
    private $creditMemoHelper;

    /**
     * @var ItemFactory
     */
    private ItemFactory $itemFactory;

    /**
     * @param Log $log
     * @param CreditmemoHelper $creditMemoHelper
     * @param TaxItemResourceFactory $taxItemResourceFactory
     * @param ShippingItemFactory $shippingItemFactory
     * @param DiscountItemFactory $discountItemFactory
     * @param ProductItemFactory $productItemFactory
     * @param PaymentFeeItemFactory $paymentFeeItemFactory
     * @param ItemFactory $itemFactory
     */
    public function __construct(
        Log $log,
        CreditmemoHelper $creditMemoHelper,
        TaxItemResourceFactory $taxItemResourceFactory,
        ShippingItemFactory $shippingItemFactory,
        DiscountItemFactory $discountItemFactory,
        ProductItemFactory $productItemFactory,
        PaymentFeeItemFactory $paymentFeeItemFactory,
        ItemFactory $itemFactory
    ) {
        $this->productItemFactory = $productItemFactory;
        $this->creditMemoHelper = $creditMemoHelper;
        $this->itemFactory = $itemFactory;

        parent::__construct(
            $log,
            $taxItemResourceFactory,
            $shippingItemFactory,
            $discountItemFactory,
            $paymentFeeItemFactory
        );
    }

    /**
     * Convert supplied entity to a collection of PaymentItem instances. These
     * objects can later be mutated into a simple array the API can interpret.
     *
     * @param ResursBank $ecom
     * @param Creditmemo $entity
     * @throws Exception
     */
    public function convert(
        ResursBank $ecom,
        Creditmemo $entity
    ) {
        if ((float) $entity->getGrandTotal() < 0.0) {
            throw new LocalizedException(
                __('Resurs Bank does not support negative credit memos.')
            );
        }

        $data = array_merge(
            $this->getProductData($entity),
            array_merge(
                $this->getDiscountData(
                    (string) $entity->getOrder()->getCouponCode(),
                    (float) $entity->getDiscountAmount(),
                    (float) $entity->getDiscountTaxCompensationAmount()
                ),
                $this->getPaymentFeeData(
                    $entity->getOrder()->getPayment()->getMethod(),
                    $this->creditMemoHelper->getPaymentMethodFeeAmount(
                        $entity
                    ),
                    $this->creditMemoHelper->getPaymentMethodFeeVatPercentage(
                        $entity
                    )
                ),
                $this->getShippingData(
                    (string) $entity->getOrder()->getShippingMethod(),
                    (string) $entity->getOrder()->getShippingDescription(),
                    (float) $entity->getShippingInclTax(),
                    $this->getTaxPercentage(
                        (int) $entity->getOrderId(),
                        'shipping'
                    )
                ),
                $this->getAdjustmentFee($entity)
            )
        );

        if (!count($data)) {
            throw new InvalidDataException(
                'Unexpected empty payload. This would affect the entire payment. Aborting execution.'
            );
        }

        foreach ($data as $line) {
            $ecom->addOrderLine(
                $line->getArtNo(),
                $line->getDescription(),
                $line->getUnitAmountWithoutVat(),
                $line->getVatPct(),
                $line->getUnitMeasure(),
                $line->getType(),
                $line->getQuantity()
            );
        }
    }

    /**
     * Extract adjustment fee information from Creditmemo entity.
     *
     * NOTE: Since there is no corresponding type at Resurs Bank we report this
     * as a product.
     *
     * @param Creditmemo $entity
     * @return PaymentItem[]
     * @throws Exception
     */
    protected function getAdjustmentFee(
        Creditmemo $entity
    ): array {
        $result = [];
        $fee = (float) $entity->getAdjustment();

        if ($fee !== 0.0) {
            $result[] = $this->itemFactory->create([
                PaymentItem::KEY_ART_NO => $this->getAdjustmentArtNo(),
                PaymentItem::KEY_DESCRIPTION => (string) __('Adjustment'),
                PaymentItem::KEY_QUANTITY => 1,
                PaymentItem::KEY_UNIT_MEASURE => 'st',
                PaymentItem::KEY_UNIT_AMOUNT_WITHOUT_VAT => $fee,
                PaymentItem::KEY_VAT_PCT => 0,
                PaymentItem::KEY_TYPE => PaymentItem::TYPE_PRODUCT
            ]);
        }

        return $result;
    }

    /**
     * This article number must be unique at the time of
     * writing to avoid EComPHP filtering the order lines from
     * the API payload.
     *
     * @return string
     */
    private function getAdjustmentArtNo(): string
    {
        return 'adjustment' . time();
    }

    /**
     * Extract product information from Creditmemo entity.
     *
     * @param Creditmemo $entity
     * @return PaymentItem[]
     * @throws Exception
     */
    protected function getProductData(Creditmemo $entity): array
    {
        $result = [];

        if ($this->includeProductData($entity)) {
            /** @var Item $product */
            foreach ($entity->getAllItems() as $product) {
                if ($product->getQty() > 0 &&
                    !$this->hasConfigurableParent($product)
                ) {
                    /** @var ProductItem $item */
                    $item = $this->productItemFactory->create([
                        'product' => $product
                    ]);

                    $result[] = $item->getItem();
                }
            }
        }

        return $result;
    }

    /**
     * Whether or not to include product data in payment payload.
     *
     * @param Creditmemo $entity
     * @return bool
     */
    public function includeProductData(Creditmemo $entity): bool
    {
        $items = $entity->getAllItems();

        return !empty($items);
    }

    /**
     * Whether a product have a configurable product as a parent.
     *
     * @param Item $product
     * @return bool
     */
    private function hasConfigurableParent(Item $product): bool
    {
        $orderItem = $product->getOrderItem();

        return $orderItem->getParentItem() instanceof OrderItemInterface &&
            $orderItem->getParentItem()->getProductType() === 'configurable';
    }
}
