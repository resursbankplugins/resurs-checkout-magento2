<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model\Api\Payment\Converter;

use Exception;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address;
use Magento\Quote\Model\Quote\Item;
use Magento\Sales\Model\ResourceModel\Order\Tax\ItemFactory as TaxItemResourceFactory;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\DiscountItemFactory;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\PaymentFeeItemFactory;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\Quote\ProductItem;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\Quote\ProductItemFactory;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\ShippingItemFactory;
use Resursbank\Checkout\Model\Api\Payment\Item as PaymentItem;

/**
 * Quote entity conversion for payment payloads.
 */
class QuoteConverter extends AbstractConverter implements ConverterInterface
{
    /**
     * @var ProductItemFactory
     */
    private $productItemFactory;

    /**
     * @param Log $log
     * @param TaxItemResourceFactory $taxItemResourceFactory
     * @param ShippingItemFactory $shippingItemFactory
     * @param DiscountItemFactory $discountItemFactory
     * @param ProductItemFactory $productItemFactory
     * @param PaymentFeeItemFactory $paymentFeeItemFactory
     */
    public function __construct(
        Log $log,
        TaxItemResourceFactory $taxItemResourceFactory,
        ShippingItemFactory $shippingItemFactory,
        DiscountItemFactory $discountItemFactory,
        ProductItemFactory $productItemFactory,
        PaymentFeeItemFactory $paymentFeeItemFactory
    ) {
        $this->productItemFactory = $productItemFactory;

        parent::__construct(
            $log,
            $taxItemResourceFactory,
            $shippingItemFactory,
            $discountItemFactory,
            $paymentFeeItemFactory
        );
    }

    /**
     * Convert supplied entity to a collection of PaymentItem instances. These
     * objects can later be mutated into a simple array the API can interpret.
     *
     * @param Quote $entity
     * @return PaymentItem[]
     * @throws Exception
     */
    public function convert(
        Quote $entity
    ): array {
        /** @var Address $address */
        $address = $entity->getShippingAddress();

        return array_merge(
            $this->getProductData($entity),
            array_merge(
                $this->getDiscountData(
                    (string) $entity->getCouponCode(),
                    (float) $address->getDiscountAmount(),
                    (float) $address->getDiscountTaxCompensationAmount()
                ),
                $this->getShippingData(
                    (string) $address->getShippingMethod(),
                    (string) $address->getShippingDescription(),
                    (float) $address->getShippingInclTax(),
                    $this->getShippingVatPct($address)
                )
            )
        );
    }

    /**
     * Extract product information from Quote entity.
     *
     * @param Quote $entity
     * @return PaymentItem[]
     * @throws Exception
     */
    protected function getProductData(Quote $entity): array
    {
        $result = [];

        if ($this->includeProductData($entity)) {
            /** @var Item $product */
            foreach ($entity->getAllItems() as $product) {
                if ($product->getQty() > 0 &&
                    !$this->hasConfigurableParent($product)
                ) {
                    /** @var ProductItem $item */
                    $item = $this->productItemFactory->create([
                        'product' => $product
                    ]);

                    $result[] = $item->getItem();
                }
            }
        }

        return $result;
    }

    /**
     * Whether or not to include product data in payment payload.
     *
     * @param Quote $entity
     * @return bool
     */
    public function includeProductData(Quote $entity): bool
    {
        $items = $entity->getAllItems();

        return !empty($items);
    }

    /**
     * Retrieve VAT (tax percentage) of applied shipping method.
     *
     * @param Address $address
     * @return float
     */
    private function getShippingVatPct(Address $address): float
    {
        $result = 0.0;

        $taxes = $address->getData('items_applied_taxes');

        if (is_array($taxes) && isset($taxes['shipping'][0]['percent'])) {
            $result = (float) $taxes['shipping'][0]['percent'];
        }

        return $result;
    }

    /**
     * Assemble total value of converted quote. This lets us check the actual
     * data we are submitting to Resurs Bank.
     *
     * @param PaymentItem[] $items
     * @return float
     */
    public function getCollectedTotal(array $items): float
    {
        $result = 0;

        /** @var PaymentItem $item */
        foreach ($items as $item) {
            if ($item instanceof PaymentItem) {
                $result += (
                        $item->getUnitAmountWithoutVat() * $item->getQuantity()
                    ) * (1 + $item->getVatPct() / 100);
            }
        }

        return (float) $result;
    }

    /**
     * Whether a product have a configurable product as a parent.
     *
     * @param Item $product
     * @return bool
     */
    private function hasConfigurableParent(Item $product): bool
    {
        return $product->getParentItem() instanceof Item &&
            $product->getParentItem()->getProductType() === 'configurable';
    }
}
