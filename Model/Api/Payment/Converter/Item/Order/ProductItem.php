<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model\Api\Payment\Converter\Item\Order;

use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Model\Order\Item as OrderItem;
use Magento\Framework\Serialize\Serializer\Json;
use Resursbank\Checkout\Helper\Config\Checkout\Advanced as AdvancedConfig;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\ItemInterface;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\AbstractItem;
use Resursbank\Checkout\Model\Api\Payment\Item;
use Resursbank\Checkout\Model\Api\Payment\ItemFactory;

/**
 * Product data converter.
 */
class ProductItem extends AbstractItem implements ItemInterface
{
    /**
     * @var OrderItem
     */
    protected $product;

    /**
     * @var Json
     */
    protected $json;

    /**
     * @param ApiConfig $apiConfig
     * @param AdvancedConfig $advancedConfig
     * @param ItemFactory $itemFactory
     * @param Log $log
     * @param OrderItem $product
     * @param Json $json
     */
    public function __construct(
        ApiConfig $apiConfig,
        AdvancedConfig $advancedConfig,
        ItemFactory $itemFactory,
        Log $log,
        OrderItem $product,
        Json $json
    ) {
        $this->product = $product;
        $this->json = $json;

        parent::__construct(
            $apiConfig,
            $advancedConfig,
            $itemFactory,
            $log
        );
    }

    /**
     * @inheritDoc
     */
    public function getArtNo(): string
    {
        return $this->sanitizeArtNo((string) $this->product->getSku());
    }

    /**
     * Creates a description for a payment entry. Usually it will be the
     * product's name, but special rules apply to mirror orders in Magento as
     * closely as possible.
     *
     * For example, children of a bundle with fixed price calculation will have
     * their price appended to the end of their description.
     *
     * @return string
     */
    public function getDescription(): string
    {
        $result = (string) $this->product->getName();
        $options = $this->product->getProductOptions();

        if ($this->hasFixedPrice() &&
            $options &&
            isset($options['bundle_selection_attributes'])
        ) {
            $price = $this->json->unserialize(
                $options['bundle_selection_attributes']
            )['price'];

            $result .= " " .
                $this->product
                    ->getOrder()
                    ->getOrderCurrency()
                    ->formatPrecision(
                        (float) $price,
                        2,
                        false,
                        false
                    );
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getQuantity(): float
    {
        return (float) $this->product->getQtyOrdered();
    }

    /**
     * Retrieves the price without tax from a product, and returns it. The type
     * of the product, whether it has a parent or children, and how the price
     * is calculated determines the returned value. The point is that a payment
     * entry should mirror the order in Magento as closely as possible, which
     * is what the following examples will try to illustrate.
     *
     * A: If the product is a bundle, and the price calculation is set to
     * fixed, then the price of the bundle is determined by the fixed price of
     * the bundle plus the overriding prices of the children as dictated by
     * the bundle. 0.0 will be returned for every child product.
     *
     * B: If the product is a bundle, and the price calculation is set to
     * dynamic, then the price of the bundle is determined by the original
     * prices of its children, and the bundle will have a price of 0.0
     * returned. The children on the other hand will have their original prices
     * returned which will make up the entire cost of the bundle.
     *
     * C: If the product is a configurable, its children will have 0.0
     * returned, while the configurable parent will have the entire price
     * returned.
     *
     * @return float
     */
    public function getUnitAmountWithoutVat(): float
    {
        $result = 0.0;
        $parent = $this->product->getParentItem();

        if ($this->product->getProductType() === 'bundle') {
            $result = $this->hasFixedPrice() ?
                (float) $this->product->getPrice() :
                0.0;
        } else if ($parent instanceof OrderItemInterface) {
            if ($parent->getProductType() === 'bundle' &&
                $this->hasDynamicPrice()
            ) {
                $result = (float) $this->product->getPrice();
            }
        } else {
            $result = (float) $this->product->getPrice();
        }

        return $this->sanitizeUnitAmountWithoutVat($result);
    }

    /**
     * Retrieve the VAT percentage of a product. The type of the product,
     * whether it has a parent or children, and how the price is calculated
     * determines the returned value. The point is that a payment entry should
     * mirror the order in Magento as closely as possible, which is what the
     * following examples will try to illustrate.
     *
     * A: Children of a bundled product with fixed price calculation does not
     * have VAT percentage values. The value is supplied directly by the bundle.
     *
     * B: Children of a configurable product does not have VAT percentage
     * values. The value is supplied directly by the configurable product.
     *
     * C: All other product types have their own VAT percentages values.
     *
     * @return float
     */
    public function getVatPct(): float
    {
        $result = 0.0;
        $parent = $this->product->getParentItem();

        if ($this->product->getProductType() === 'bundle' &&
            $this->hasFixedPrice()
        ) {
            $result = ((float) (
                $this->product->getTaxAmount() /
                $this->product->getPrice()
            ) * 100) / $this->getQuantity();
        } else if ($this->product->getProductType() === 'configurable') {
            $result = (float) $this->product->getTaxPercent();
        } else if (!($parent instanceof OrderItemInterface) ||
            $parent->getProductType() !== 'configurable'
        ) {
            $result = (float) $this->product->getTaxPercent();
        }

        return $this->sanitizeVatPct($result);
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return Item::TYPE_PRODUCT;
    }

    /**
     * Checks if the the product has dynamic pricing by its parent's product
     * options. If a parent can't be found the product itself will be checked.
     *
     * @return bool
     */
    public function hasDynamicPrice(): bool
    {
        return $this->product->isChildrenCalculated();
    }

    /**
     * Checks if the the product has fixed pricing by its parent's product
     * options. If a parent can't be found the product itself will be checked.
     *
     * @return bool
     */
    public function hasFixedPrice(): bool
    {
        return !$this->product->isChildrenCalculated();
    }
}
