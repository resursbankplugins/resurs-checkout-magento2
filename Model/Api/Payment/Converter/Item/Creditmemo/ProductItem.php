<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model\Api\Payment\Converter\Item\Creditmemo;

use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Model\Order\Creditmemo\Item as CreditmemoItem;
use Resursbank\Checkout\Helper\Config\Checkout\Advanced as AdvancedConfig;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\ItemInterface;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\AbstractItem;
use Resursbank\Checkout\Model\Api\Payment\Item;
use Resursbank\Checkout\Model\Api\Payment\ItemFactory;

/**
 * Product data converter.
 */
class ProductItem extends AbstractItem implements ItemInterface
{
    /**
     * @var CreditmemoItem
     */
    protected $product;

    /**
     * @param ApiConfig $apiConfig
     * @param AdvancedConfig $advancedConfig
     * @param ItemFactory $itemFactory
     * @param Log $log
     * @param CreditmemoItem $product
     */
    public function __construct(
        ApiConfig $apiConfig,
        AdvancedConfig $advancedConfig,
        ItemFactory $itemFactory,
        Log $log,
        CreditmemoItem $product
    ) {
        $this->product = $product;

        parent::__construct(
            $apiConfig,
            $advancedConfig,
            $itemFactory,
            $log
        );
    }

    /**
     * @inheritDoc
     */
    public function getArtNo(): string
    {
        return $this->sanitizeArtNo((string) $this->product->getSku());
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): string
    {
        return (string) $this->product->getName();
    }

    /**
     * @inheritDoc
     */
    public function getQuantity(): float
    {
        return (float) $this->product->getQty();
    }

    /**
     * @inheritDoc
     */
    public function getUnitAmountWithoutVat(): float
    {
        $result = 0.0;
        $product = $this->product->getOrderItem();
        $parent = $product->getParentId();

        if ($product->getProductType() === 'bundle') {
            $result = $this->hasFixedPrice() ?
                (float) $product->getPrice() :
                0.0;
        } else if ($parent instanceof OrderItemInterface) {
            if ($parent->getProductType() === 'bundle' &&
                $this->hasDynamicPrice()
            ) {
                $result = (float) $product->getPrice();
            }
        } else {
            $result = (float) $product->getPrice();
        }

        return $this->sanitizeUnitAmountWithoutVat($result);
    }

    /**
     * @inheritDoc
     */
    public function getVatPct(): float
    {
        $result = 0.0;
        $product = $this->product->getOrderItem();
        $parent = $product->getParentItem();

        if ($product->getProductType() === 'bundle' &&
            $this->hasFixedPrice()
        ) {
            $result = ((float)(
                    $product->getTaxAmount() /
                    $product->getPrice()
                ) * 100) / $this->getQuantity();
        } else if ($product->getProductType() === 'configurable') {
            $result = (float) $product->getTaxPercent();
        } else if (!($parent instanceof OrderItemInterface) ||
            $parent->getProductType() !== 'configurable'
        ) {
            $result = (float) $product->getTaxPercent();
        }

        return $this->sanitizeVatPct($result);
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return Item::TYPE_PRODUCT;
    }

    /**
     * Checks if the the product has dynamic pricing by its parent's product
     * options. If a parent can't be found the product itself will be checked.
     *
     * @return bool
     */
    public function hasDynamicPrice(): bool
    {
        return $this->product->getOrderItem()->isChildrenCalculated();
    }

    /**
     * Checks if the the product has fixed pricing by its parent's product
     * options. If a parent can't be found the product itself will be checked.
     *
     * @return bool
     */
    public function hasFixedPrice(): bool
    {
        return !$this->product->getOrderItem()->isChildrenCalculated();
    }
}
