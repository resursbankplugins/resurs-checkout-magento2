<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model\Api\Payment\Converter\Item;

use Resursbank\Checkout\Model\Api\Payment\Item;

/**
 * Extract data from an entity (like an Order Item or Creditmemo Item) and
 * prepare it to be used in an API payment payload.
 *
 * NOTE: excl. tax prices are re-calculated using the incl. tax price and tax
 * percentage to provide Resurs Bank with accurate values. Magento rounds the
 * excl. / incl. tax prices. Depending on the utilised API flow, we only provide
 * Resurs Bank with the excl. tax price (unitAmountWithoutVat) and tax
 * percentage (vatPct) values. Resurs Bank then calculate the incl. tax price
 * using these values. This means we cannot submit rounded values to Resurs Bank
 * since this can incur a slight price difference (depending on how you've
 * configured tax settings and prices in Magento).
 */
interface ItemInterface
{
    /**
     * @return Item
     */
    public function getItem(): Item;

    /**
     * @return string
     */
    public function getArtNo(): string;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @return float
     */
    public function getQuantity(): float;

    /**
     * @return string
     */
    public function getUnitMeasure(): string;

    /**
     * @return float
     */
    public function getUnitAmountWithoutVat(): float;

    /**
     * @return float
     */
    public function getVatPct(): float;

    /**
     * @return string
     */
    public function getType(): string;
}
