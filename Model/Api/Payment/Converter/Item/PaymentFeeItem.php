<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model\Api\Payment\Converter\Item;

use Exception;
use Resursbank\Checkout\Helper\Config\Checkout\Advanced as AdvancedConfig;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\Api\Payment\Item;
use Resursbank\Checkout\Model\Api\Payment\ItemFactory;
use Resursbank\Checkout\Model\Total\Quote;

/**
 * Payment fee data converter.
 */
class PaymentFeeItem extends AbstractItem implements ItemInterface
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var float
     */
    private $vatPct;

    /**
     * @param ApiConfig $apiConfig
     * @param AdvancedConfig $advancedConfig
     * @param ItemFactory $itemFactory
     * @param Log $log
     * @param string $name
     * @param float $amount
     * @param float $vatPct
     */
    public function __construct(
        ApiConfig $apiConfig,
        AdvancedConfig $advancedConfig,
        ItemFactory $itemFactory,
        Log $log,
        string $name,
        float $amount,
        float $vatPct
    ) {
        $this->name = $name;
        $this->amount = $amount;
        $this->vatPct = $vatPct;

        parent::__construct(
            $apiConfig,
            $advancedConfig,
            $itemFactory,
            $log
        );
    }

    /**
     * @inheritDoc
     */
    public function getArtNo(): string
    {
        return $this->sanitizeArtNo(Quote::CODE);
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getQuantity(): float
    {
        return 1.0;
    }

    /**
     * @inheritDoc
     */
    public function getUnitAmountWithoutVat(): float
    {
        return $this->sanitizeUnitAmountWithoutVat($this->amount);
    }

    /**
     * NOTE: the tax percentage value is being rounded here because it should
     * always be an integer. The tax percentage is calculated internally by
     * Magento during checkout (we only resolve and provide the correct tax
     * class here; Model/Total/Quote.php). It's possible this could be improved
     * so we can resolve and store the tax percentage during checkout, thus
     * avoid rounding the value here.
     *
     * @inheritDoc
     * @throws Exception
     */
    public function getVatPct(): float
    {
        $result = $this->vatPct;

        // VAT percentage should always be an int, unless explicitly configured.
        if ($this->roundTaxPercentage()) {
            $result = round($result);
        }

        return $this->sanitizeVatPct($result);
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return Item::TYPE_PAYMENT_FEE;
    }
}
