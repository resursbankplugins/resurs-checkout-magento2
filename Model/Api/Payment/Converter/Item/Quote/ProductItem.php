<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model\Api\Payment\Converter\Item\Quote;

use Magento\Quote\Model\Quote\Item as QuoteItem;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\ItemInterface;
use Resursbank\Checkout\Model\Api\Payment\Item;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\AbstractItem;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Config\Checkout\Advanced as AdvancedConfig;
use Resursbank\Checkout\Model\Api\Payment\ItemFactory;
use Resursbank\Checkout\Helper\Log;

/**
 * Product data converter.
 */
class ProductItem extends AbstractItem implements ItemInterface
{
    /**
     * @var QuoteItem
     */
    protected $product;

    /**
     * @param ApiConfig $apiConfig
     * @param AdvancedConfig $advancedConfig
     * @param ItemFactory $itemFactory
     * @param Log $log
     * @param QuoteItem $product
     */
    public function __construct(
        ApiConfig $apiConfig,
        AdvancedConfig $advancedConfig,
        ItemFactory $itemFactory,
        Log $log,
        QuoteItem $product
    ) {
        $this->product = $product;

        parent::__construct(
            $apiConfig,
            $advancedConfig,
            $itemFactory,
            $log
        );
    }

    /**
     * @inheritDoc
     */
    public function getArtNo(): string
    {
        return $this->sanitizeArtNo((string) $this->product->getSku());
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): string
    {
        return (string) $this->product->getName();
    }

    /**
     * @inheritDoc
     */
    public function getQuantity(): float
    {
        return (float) $this->product->getQty();
    }

    /**
     * @inheritDoc
     */
    public function getUnitAmountWithoutVat(): float
    {
        return $this->sanitizeUnitAmountWithoutVat(
            (float) $this->product->getConvertedPrice()
        );
    }

    /**
     * @inheritDoc
     */
    public function getVatPct(): float
    {
        return $this->sanitizeVatPct(
            (float) $this->product->getTaxPercent()
        );
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return Item::TYPE_PRODUCT;
    }
}
