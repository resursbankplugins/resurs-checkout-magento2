<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model\Api\Payment\Converter\Item;

use Resursbank\Checkout\Helper\Config\Checkout\Advanced as AdvancedConfig;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\Api\Payment\Item;
use Resursbank\Checkout\Model\Api\Payment\ItemFactory;

/**
 * Shipping data converter.
 */
class ShippingItem extends AbstractItem implements ItemInterface
{
    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $description;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var float
     */
    private $vatPct;

    /**
     * @param ApiConfig $apiConfig
     * @param AdvancedConfig $advancedConfig
     * @param ItemFactory $itemFactory
     * @param Log $log
     * @param string $method Shipping method code.
     * @param string $description Shipping method title.
     * @param float $amount Amount incl. tax.
     * @param float $vatPct Tax percentage.
     */
    public function __construct(
        ApiConfig $apiConfig,
        AdvancedConfig $advancedConfig,
        ItemFactory $itemFactory,
        Log $log,
        string $method,
        string $description,
        float $amount,
        float $vatPct
    ) {
        $this->method = $method;
        $this->description = $description;
        $this->amount = $amount;
        $this->vatPct = $vatPct;

        parent::__construct(
            $apiConfig,
            $advancedConfig,
            $itemFactory,
            $log
        );
    }

    /**
     * @inheritDoc
     */
    public function getArtNo(): string
    {
        return $this->sanitizeArtNo($this->method);
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @inheritDoc
     */
    public function getQuantity(): float
    {
        return 1.0;
    }

    /**
     * @inheritDoc
     */
    public function getUnitAmountWithoutVat(): float
    {
        $inclTax = $this->amount;
        $vatPct = $this->getVatPct();

        $result = ($inclTax > 0 && $vatPct > 0) ?
            $inclTax / (1 + ($vatPct / 100)) :
            $inclTax;

        return $this->sanitizeUnitAmountWithoutVat($result);
    }

    /**
     * @inheritDoc
     */
    public function getVatPct(): float
    {
        return $this->sanitizeVatPct(
            $this->vatPct
        );
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return Item::TYPE_SHIPPING;
    }
}
