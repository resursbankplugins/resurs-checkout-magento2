<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model\Api\Payment\Converter\Item;

use Exception;
use Resursbank\Checkout\Helper\Config\Checkout\Advanced as AdvancedConfig;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\Api\Payment\Item;
use Resursbank\Checkout\Model\Api\Payment\ItemFactory;

/**
 * Discount data converter.
 */
class DiscountItem extends AbstractItem implements ItemInterface
{
    /**
     * @var string
     */
    private $couponCode;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var float
     */
    private $taxAmount;

    /**
     * @param ApiConfig $apiConfig
     * @param AdvancedConfig $advancedConfig
     * @param ItemFactory $itemFactory
     * @param Log $log
     * @param string $couponCode
     * @param float $amount Amount incl. tax.
     * @param float $taxAmount Tax amount.
     */
    public function __construct(
        ApiConfig $apiConfig,
        AdvancedConfig $advancedConfig,
        ItemFactory $itemFactory,
        Log $log,
        string $couponCode,
        float $amount,
        float $taxAmount
    ) {
        $this->couponCode = $couponCode;
        $this->amount = $amount;
        $this->taxAmount = $taxAmount;

        parent::__construct(
            $apiConfig,
            $advancedConfig,
            $itemFactory,
            $log
        );
    }

    /**
     * @inheritDoc
     */
    public function getArtNo(): string
    {
        $result = 'discount';

        if ($this->couponCode !== '') {
            $result .= $this->couponCode;
        }

        $result .= time();

        return $this->sanitizeArtNo($result);
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): string
    {
        $result = 'Discount';

        if ($this->couponCode !== '') {
            $result .= " ({$this->couponCode})";
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getQuantity(): float
    {
        return 1.0;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function getUnitAmountWithoutVat(): float
    {
        $vatPct = $this->getVatPct();

        $result = ($this->amount < 0 && $vatPct > 0) ?
            ($this->amount / (1 + ($vatPct / 100))) :
            $this->amount;

        return $this->sanitizeUnitAmountWithoutVat($result);
    }

    /**
     * NOTE: the tax percentage value is being rounded here because it should
     * always be an integer. Shipping and product tax percentage are stored with
     * the order entity, so we can re-calculate their accurate excl. tax
     * price (for more on this, please refer to the docblock of
     * ConverterItemInterface). Since we cannot safely obtain the applied tax
     * percentage value for discounts we will need to calculate it using the
     * excl. / incl. tax values of the discount. However this will leave us with
     * a value like '24.966934532%' since Magento rounds the excl. / incl. tax
     * discount prices. So, we round of the tax percentage value, and we will
     * later use our correct tax percentage to re-calculate the accurate excl.
     * tax price. This ensures the prices will be the same both in Magento and
     * at Resurs Bank.
     *
     * @inheritDoc
     * @throws Exception
     */
    public function getVatPct(): float
    {
        $exclTax = abs((float) $this->amount) - $this->taxAmount;

        $result = ($exclTax > 0 && $this->taxAmount > 0) ?
            (($this->taxAmount / $exclTax) * 100) :
            0.0;

        // VAT percentage should always be an int, unless explicitly configured.
        if ($this->roundTaxPercentage()) {
            $result = round($result);
        }

        return $this->sanitizeVatPct($result);
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return Item::TYPE_DISCOUNT;
    }
}
