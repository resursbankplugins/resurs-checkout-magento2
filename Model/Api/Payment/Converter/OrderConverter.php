<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model\Api\Payment\Converter;

use Exception;
use Magento\Sales\Api\Data\OrderItemInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Item;
use Magento\Sales\Model\ResourceModel\Order\Tax\ItemFactory as TaxItemResourceFactory;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Order as OrderHelper;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\DiscountItemFactory;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\Order\ProductItem;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\Order\ProductItemFactory;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\PaymentFeeItemFactory;
use Resursbank\Checkout\Model\Api\Payment\Converter\Item\ShippingItemFactory;
use Resursbank\Checkout\Model\Api\Payment\Item as PaymentItem;

/**
 * Order entity conversion for payment payloads.
 */
class OrderConverter extends AbstractConverter implements ConverterInterface
{
    /**
     * @var ProductItemFactory
     */
    private $productItemFactory;

    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @param Log $log
     * @param OrderHelper $orderHelper
     * @param TaxItemResourceFactory $taxItemResourceFactory
     * @param ShippingItemFactory $shippingItemFactory
     * @param DiscountItemFactory $discountItemFactory
     * @param ProductItemFactory $productItemFactory
     * @param PaymentFeeItemFactory $paymentFeeItemFactory
     */
    public function __construct(
        Log $log,
        OrderHelper $orderHelper,
        TaxItemResourceFactory $taxItemResourceFactory,
        ShippingItemFactory $shippingItemFactory,
        DiscountItemFactory $discountItemFactory,
        ProductItemFactory $productItemFactory,
        PaymentFeeItemFactory $paymentFeeItemFactory
    ) {
        $this->productItemFactory = $productItemFactory;
        $this->orderHelper = $orderHelper;

        parent::__construct(
            $log,
            $taxItemResourceFactory,
            $shippingItemFactory,
            $discountItemFactory,
            $paymentFeeItemFactory
        );
    }

    /**
     * Convert supplied entity to a collection of PaymentItem instances. These
     * objects can later be mutated into a simple array the API can interpret.
     *
     * @param Order $entity
     * @return PaymentItem[]
     * @throws Exception
     */
    public function convert(Order $entity): array
    {
        return array_merge(
            $this->getProductData($entity),
            array_merge(
                $this->getDiscountData(
                    (string) $entity->getCouponCode(),
                    (float) $entity->getDiscountAmount(),
                    (float) $entity->getDiscountTaxCompensationAmount()
                ),
                $this->getPaymentFeeData(
                    $entity->getPayment()->getMethod(),
                    $this->orderHelper->getPaymentMethodFeeAmount($entity),
                    $this->orderHelper->getPaymentMethodFeeVatPercentage(
                        $entity
                    )
                ),
                $this->getShippingData(
                    (string) $entity->getShippingMethod(),
                    (string) $entity->getShippingDescription(),
                    (float) $entity->getShippingInclTax(),
                    $this->getTaxPercentage(
                        (int) $entity->getId(),
                        'shipping'
                    )
                )
            )
        );
    }

    /**
     * Extract product information from Order entity.
     *
     * @param Order $entity
     * @return PaymentItem[]
     * @throws Exception
     */
    protected function getProductData(Order $entity): array
    {
        $result = [];

        if ($this->includeProductData($entity)) {
            /** @var Item $product */
            foreach ($entity->getAllItems() as $product) {
                if ($product->getQtyOrdered() > 0 &&
                    !$this->hasConfigurableParent($product)
                ) {
                    /** @var ProductItem $item */
                    $item = $this->productItemFactory->create([
                        'product' => $product
                    ]);

                    $result[] = $item->getItem();
                }
            }
        }

        return $result;
    }

    /**
     * Whether or not to include product data in payment payload.
     *
     * @param Order $entity
     * @return bool
     */
    public function includeProductData(Order $entity): bool
    {
        $items = $entity->getAllItems();

        return !empty($items);
    }

    /**
     * Whether a product have a configurable product as a parent.
     *
     * @param OrderItemInterface $product
     * @return bool
     */
    private function hasConfigurableParent(OrderItemInterface $product): bool
    {
        return $product->getParentItem() instanceof OrderItemInterface &&
            $product->getParentItem()->getProductType() === 'configurable';
    }
}
