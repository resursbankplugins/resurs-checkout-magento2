<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model;

use Exception;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Webapi\Exception as WebapiException;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderRepository;
use Resursbank\Checkout\Api\CallbackInterface;
use Resursbank\Checkout\Api\Data\PaymentHistoryInterface;
use Resursbank\Checkout\Exception\CallbackValidationException;
use Resursbank\Checkout\Helper\Callback as Helper;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Config\Checkout\Callback as Config;
use Resursbank\Checkout\Helper\Db\PaymentHistory;
use Resursbank\Checkout\Helper\Log\Callback as Log;
use Resursbank\Checkout\Helper\Order as OrderHelper;
use Magento\Framework\App\Cache\TypeListInterface;
use Resursbank\Checkout\Helper\Ecom;
use Resursbank\RBEcomPHP\RESURS_PAYMENT_STATUS_RETURNCODES;

/**
 * @package Resursbank\Checkout\Model
 */
class Callback implements CallbackInterface
{
    /**
     * @var Helper
     */
    private $helper;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Ecom
     */
    private $ecom;

    /**
     * @var PaymentHistory
     */
    private $paymentHistory;

    /**
     * @var TypeListInterface
     */
    private $cacheTypeList;

    /**
     * @param Helper $helper
     * @param Log $log
     * @param OrderHelper $orderHelper
     * @param OrderRepository $orderRepository
     * @param ApiConfig $apiConfig
     * @param Config $config
     * @param PaymentHistory $paymentHistory
     * @param TypeListInterface $cacheTypeList
     * @param Ecom $ecom
     */
    public function __construct(
        Helper $helper,
        Log $log,
        OrderHelper $orderHelper,
        OrderRepository $orderRepository,
        ApiConfig $apiConfig,
        Config $config,
        PaymentHistory $paymentHistory,
        TypeListInterface $cacheTypeList,
        Ecom $ecom
    ) {
        $this->helper = $helper;
        $this->log = $log;
        $this->orderHelper = $orderHelper;
        $this->orderRepository = $orderRepository;
        $this->apiConfig = $apiConfig;
        $this->config = $config;
        $this->paymentHistory = $paymentHistory;
        $this->cacheTypeList = $cacheTypeList;
        $this->ecom = $ecom;
    }

    /**
     * @inheritdoc
     * @throws WebapiException
     */
    public function unfreeze(string $paymentId, string $digest)
    {
        try {
            $this->execute('unfreeze', $paymentId, $digest);
        } catch (Exception $e) {
            $this->handleError($e);
        }
    }

    /**
     * @inheritdoc
     * @throws WebapiException
     */
    public function booked(string $paymentId, string $digest)
    {
        try {
            /** @var Order $order */
            $order = $this->execute('booked', $paymentId, $digest);

            // Send order confirmation email.
            $this->orderHelper->sendConfirmationEmail($order);
        } catch (Exception $e) {
            $this->handleError($e);
        }
    }

    /**
     * @inheritdoc
     * @throws WebapiException
     */
    public function update(string $paymentId, string $digest)
    {
        try {
            $this->execute('update', $paymentId, $digest);
        } catch (Exception $e) {
            $this->handleError($e);
        }
    }

    /**
     * @inheritdoc
     * @throws WebapiException
     */
    public function test(
        string $param1,
        string $param2,
        string $param3,
        string $param4,
        string $param5,
        string $digest
    ) {
        try {
            // Mark time we received the test callback.
            $this->config->setTestReceivedAt(time());
            // Clear the config cache so this value show up.
            $this->cacheTypeList->cleanType('config');
        } catch (Exception $e) {
            $this->handleError($e);
        }
    }

    /**
     * General callback instructions.
     *
     * @param string $method
     * @param string $paymentId
     * @param string $digest
     * @param array [$params]
     * @return Order
     * @throws Exception
     * @throws AlreadyExistsException
     * @throws LocalizedException
     */
    private function execute(
        string $method,
        string $paymentId,
        string $digest,
        array $params = []
    ): Order {
        // Validate incoming request.
        $this->validate([$paymentId], $digest);

        // Log incoming call. Request validation occurs before this to avoid
        // flooding the logs.
        $this->addLogEntry($paymentId, $method, $params);

        /** @var Order $order */
        $order = $this->orderHelper->getOrderFromIncrementId($paymentId);

        if (!$order->getId()) {
            throw new Exception('Failed to locate order ' . $paymentId);
        }

        // Create payment history entry.
        $this->logPaymentHistoryEvent(
            $order,
            $this->helper->getMethodEvent($method)
        );

        // Ask Resurs Bank for what status/state this order should be set to
        // and make an entry in paymentHistory
        $this->syncStatusFromGateway($order);

        return $order;
    }

    /**
     * @param Exception $e
     * @return void
     * @throws WebapiException
     */
    private function handleError(Exception $e)
    {
        $this->log->error($e);

        if ($e instanceof CallbackValidationException) {
            throw new WebapiException(
                __($e->getMessage()),
                0,
                WebapiException::HTTP_NOT_ACCEPTABLE
            );
        }
    }

    /**
     * Validate incoming callback using provided digest value.
     *
     * NOTE: Digest calculation should include the same parameters as defined
     * when registering the callback. For more info please refer to
     * Resursbank\Checkout\Model\Api :: registerCallback() and
     * Resursbank\Checkout\Model\Api :: getCallbackDigestParameters()
     *
     * Throws an Exception if validation fails.
     *
     * @param array $params
     * @param string $digest
     * @return void
     * @throws Exception
     */
    private function validate(
        array $params,
        string $digest
    ) {
        $data = (string) array_reduce($params, function ($result, $param) {
            return $result . (string) $param;
        });

        // Calculate local digest.
        $ourDigest = strtoupper(sha1($data . $this->getSalt()));

        if ($ourDigest !== $digest) {
            throw new CallbackValidationException('Invalid callback digest.');
        }
    }

    /**
     * Retrieve digest salt for current API account.
     *
     * @return string
     * @throws Exception
     */
    private function getSalt(): string
    {
        $result = $this->helper->getSalt($this->apiConfig->getCredentials());

        if ($result === '') {
            throw new Exception(
                'No digest salt available for requested API account.'
            );
        }

        return $result;
    }

    /**
     * Resolve the status and state for the order by asking Resurs Bank.
     * Enter the state/status change event into payment history.
     *
     * @param Order $order
     * @return void
     * @throws AlreadyExistsException
     * @throws LocalizedException
     * @throws Exception
     */
    public function syncStatusFromGateway(Order $order)
    {
        $credentials = $this->apiConfig->getCredentials(
            $order->getStore()->getCode()
        );

        $status = $this->ecom->getConnection($credentials)
            ->getOrderStatusByPayment($order->getIncrementId());

        switch ($status) {
            case RESURS_PAYMENT_STATUS_RETURNCODES::PAYMENT_PENDING:
                $orderStatus = PaymentHistoryInterface::ORDER_STATUS_PAYMENT_REVIEW;
                $orderState = Order::STATE_PAYMENT_REVIEW;
                $historyEntry = PaymentHistoryInterface::EVENT_PAYMENT_PENDING;
                break;
            case RESURS_PAYMENT_STATUS_RETURNCODES::PAYMENT_PROCESSING:
                $orderStatus = PaymentHistoryInterface::ORDER_STATUS_CONFIRMED;
                $orderState = Order::STATE_PENDING_PAYMENT;
                $historyEntry = PaymentHistoryInterface::EVENT_PAYMENT_PROCESSING;
                break;
            case RESURS_PAYMENT_STATUS_RETURNCODES::PAYMENT_COMPLETED:
                $orderStatus = PaymentHistoryInterface::ORDER_STATUS_FINALIZED;
                $orderState = Order::STATE_PROCESSING;
                $historyEntry = PaymentHistoryInterface::EVENT_PAYMENT_DEBITED;
                break;
            case RESURS_PAYMENT_STATUS_RETURNCODES::PAYMENT_ANNULLED:
                $orderStatus = PaymentHistoryInterface::ORDER_STATUS_CANCELLED;
                $orderState = Order::STATE_CANCELED;
                $historyEntry = PaymentHistoryInterface::EVENT_PAYMENT_ANNULLED;
                $this->orderHelper->cancel($order);
                break;
            case RESURS_PAYMENT_STATUS_RETURNCODES::PAYMENT_CREDITED:
                $orderStatus = Order::STATE_CLOSED;
                $orderState = Order::STATE_CLOSED;
                $historyEntry = PaymentHistoryInterface::EVENT_PAYMENT_CREDITED;
                break;
            default:
                throw new Exception(
                    sprintf(
                        'Failed to resolve order status (%s) from Resurs Bank.',
                        $status
                    )
                );
                break;
        }

        $originalState = $order->getState();
        $originalStatus = $order->getStatus();
        $order->setStatus($orderStatus);
        $order->setState($orderState);
        $this->orderRepository->save($order);

        $this->paymentHistory->addEntry(
            $this->paymentHistory->createEntry(
                (int) $order->getPayment()->getEntityId(),
                $historyEntry,
                PaymentHistoryInterface::USER_RESURS_BANK,
                '',
                $originalState,
                $orderState,
                $originalStatus,
                $orderStatus
            )
        );
    }

    /**
     * Log that we received a callback.
     *
     * @param string $paymentId
     * @param string $method
     * @param array [$params]
     * @return void
     */
    private function addLogEntry(
        string $paymentId,
        string $method,
        array $params = []
    ) {
        $text = "Received callback {$method} for {$paymentId}";

        if (!empty($params)) {
            $text .= ' ' . implode(' , ', $params);
        }

        $this->log->info("{$text}.");
    }

    /**
     * @param Order $order
     * @param int $event
     * @return void
     * @throws AlreadyExistsException
     * @throws LocalizedException
     */
    private function logPaymentHistoryEvent(
        Order $order,
        int $event
    ) {
        $this->paymentHistory->addEntry(
            $this->paymentHistory->createEntry(
                (int) $order->getPayment()->getEntityId(),
                $event,
                PaymentHistoryInterface::USER_RESURS_BANK
            )
        );
    }
}
