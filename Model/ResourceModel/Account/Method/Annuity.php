<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model\ResourceModel\Account\Method;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use \Magento\Framework\DB\Select;

/**
 * Payment method resource.
 *
 * @package Resursbank\Checkout\Model\ResourceModel\Account
 */
class Annuity extends AbstractDb
{
    /**
     * Initialize resource model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'resursbank_checkout_account_method_annuity',
            'annuity_id'
        );
    }

    /**
     * Modify select statement to include API credentials associated with
     * method.
     *
     * @inheritdoc
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        /** @var string $methodTable */
        $methodTable = $this->getTable('resursbank_checkout_account_method');

        /** @var Select $select */
        $select = parent::_getLoadSelect($field, $value, $object);

        // Left join is important to allow for null value 'account_id'. Methods
        // collected from the order table aren't associated with any API acc.
        $select->joinLeft(
            $methodTable,
            $this->getMainTable() . ".method_id = {$methodTable}.method_id"
        );

        return $select;
    }
}
