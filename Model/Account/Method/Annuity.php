<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model\Account\Method;

use \Magento\Framework\Model\AbstractModel;
use \Resursbank\Checkout\Model\ResourceModel\Account\Method\Annuity as Resource;

/**
 * @package Resursbank\Checkout\Model\Account\Method
 */
class Annuity extends AbstractModel
{
    /**
     * @var string
     */
    const DURATION = 'duration';

    /**
     * @var string
     */
    const FACTOR = 'factor';

    /**
     * @var string
     */
    const TITLE = 'title';

    /**
     * @var string
     */
    const METHOD_ID = 'method_id';

    /**
     * Initialize model.
     */
    protected function _construct()
    {
        $this->_init(Resource::class);
    }
}
