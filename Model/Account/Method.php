<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model\Account;

use \Magento\Framework\App\ObjectManager;
use \Magento\Framework\Model\AbstractModel;
use \Resursbank\Checkout\Model\ResourceModel\Account\Method as Resource;
use \Resursbank\Checkout\Model\Api\Credentials;

/**
 * @package Resursbank\Checkout\Model\Account
 */
class Method extends AbstractModel
{
    /**
     * @var string
     */
    const CODE = 'code';

    /**
     * @var string
     */
    const TITLE = 'title';

    /**
     * @var string
     */
    const ID = 'id';

    /**
     * @var string
     */
    const ACCOUNT_ID = 'account_id';

    /**
     * @var string
     */
    const IDENTIFIER = 'identifier';

    /**
     * @var string
     */
    const MIN_ORDER_TOTAL = 'min_order_total';

    /**
     * @var string
     */
    const MAX_ORDER_TOTAL = 'max_order_total';

    /**
     * @var string
     */
    const ACTIVE = 'active';

    /**
     * @var string
     */
    const RAW = 'raw';

    /**
     * Initialize model.
     */
    protected function _construct()
    {
        $this->_init(Resource::class);
    }

    /**
     * Check if payment method is active.
     *
     * @return bool
     */
    public function isActive()
    {
        return ((int) $this->_getData(self::ACTIVE)) === 1;
    }

    /**
     * Retrieve key from raw data.
     *
     * @param string $key
     * @return mixed|null
     */
    public function getRawValue($key)
    {
        $result = null;
        $key = (string) $key;
        $raw = $this->getRaw();

        if ($raw !== '') {
            $raw = @json_decode($raw, true);

            if (is_array($raw) && isset($raw[$key])) {
                $result = $raw[$key];
            }
        }

        return $result;
    }
}
