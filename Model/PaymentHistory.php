<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model;

use Magento\Framework\Model\AbstractModel;
use Resursbank\Checkout\Api\Data\PaymentHistoryInterface;
use Resursbank\Checkout\Model\ResourceModel\PaymentHistory as ResourceModel;

/**
 * @package Resursbank\Checkout\Model
 */
class PaymentHistory extends AbstractModel implements PaymentHistoryInterface
{
    /**
     * Constructor.
     */
    protected function _construct()
    {
        $this->_init(ResourceModel::class);
    }

    /**
     * @inheritDoc
     */
    public function getId(int $default = null)
    {
        $data = $this->_getData(self::ID);

        return $data === null ? $default : (int)$data;
    }

    /**
     * @inheritDoc
     */
    public function setId($id): PaymentHistoryInterface
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function getPaymentId(int $default = null)
    {
        $data = $this->_getData(self::PAYMENT_ID);

        return $data === null ? $default : (int)$data;
    }

    /**
     * @inheritDoc
     */
    public function setPaymentId(int $id): PaymentHistoryInterface
    {
        return $this->setData(self::PAYMENT_ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function getEvent(int $default = null)
    {
        $data = $this->_getData(self::EVENT);

        return $data === null ? $default : (int)$data;
    }

    /**
     * @inheritDoc
     */
    public function setEvent(int $event): PaymentHistoryInterface
    {
        return $this->setData(self::EVENT, $event);
    }

    /**
     * @inheritDoc
     */
    public function getUser(int $default = null)
    {
        $data = $this->_getData(self::USER);

        return $data === null ? $default : (int)$data;
    }

    /**
     * @inheritDoc
     */
    public function setUser(int $user): PaymentHistoryInterface
    {
        return $this->setData(self::USER, $user);
    }

    /**
     * @inheritDoc
     */
    public function getExtra(string $default = null)
    {
        return $this->_getData(self::EXTRA) ?? $default;
    }

    /**
     * @inheritDoc
     */
    public function setExtra(string $extra): PaymentHistoryInterface
    {
        return $this->setData(self::EXTRA, $extra);
    }

    /**
     * @inheritDoc
     */
    public function setStateFrom(string $state): PaymentHistoryInterface
    {
        return $this->setData(self::STATE_FROM, $state);
    }

    /**
     * @inheritDoc
     */
    public function getStateFrom(string $default = null)
    {
        return $this->_getData(self::STATE_FROM) ?? $default;
    }

    /**
     * @inheritDoc
     */
    public function setStateTo(string $state): PaymentHistoryInterface
    {
        return $this->setData(self::STATE_TO, $state);
    }

    /**
     * @inheritDoc
     */
    public function getStateTo(string $default = null)
    {
        return $this->_getData(self::STATE_TO) ?? $default;
    }

    /**
     * @inheritDoc
     */
    public function setStatusFrom(string $status): PaymentHistoryInterface
    {
        return $this->setData(self::STATUS_FROM, $status);
    }

    /**
     * @inheritDoc
     */
    public function getStatusFrom(string $default = null)
    {
        return $this->_getData(self::STATUS_FROM) ?? $default;
    }

    /**
     * @inheritDoc
     */
    public function setstatusTo(string $status): PaymentHistoryInterface
    {
        return $this->setData(self::STATUS_TO, $status);
    }

    /**
     * @inheritDoc
     */
    public function getStatusTo(string $default = null)
    {
        return $this->_getData(self::STATUS_TO) ?? $default;
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(string $default = null)
    {
        return $this->_getData(self::CREATED_AT) ?? $default;
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt(string $createdAt): PaymentHistoryInterface
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * @inheritDoc
     */
    public function getUserLabel(int $user, bool $translate = true): string
    {
        switch ($user) {
            case self::USER_CUSTOMER:
                $label = self::USER_CUSTOMER_LABEL;
                break;

            case self::USER_RESURS_BANK:
                $label = self::USER_RESURS_BANK_LABEL;
                break;

            case self::USER_CLIENT:
                $label = self::USER_CLIENT_LABEL;
                break;

            default:
                $label = '';
        }

        return $translate ? (string) __($label) : $label;
    }

    /**
     * @inheritDoc
     */
    public function getEventLabel(int $event, bool $translate = true): string
    {
        switch ($event) {
            case self::EVENT_CREDIT_DENIED:
                $label = self::EVENT_CREDIT_DENIED_LABEL;
                break;

            case self::EVENT_PURCHASE_DECLINED:
                $label = self::EVENT_PURCHASE_DECLINED_LABEL;
                break;

            case self::EVENT_REDIRECTED_GATEWAY:
                $label = self::EVENT_REDIRECTED_GATEWAY_LABEL;
                break;

            case self::EVENT_PAYMENT_COMPLETED:
                $label = self::EVENT_PAYMENT_COMPLETED_LABEL;
                break;

            case self::EVENT_ABORTED_BY_CUSTOMER:
                $label = self::EVENT_ABORTED_BY_CUSTOMER_LABEL;
                break;

            case self::EVENT_PAYMENT_ANNULLED:
                $label = self::EVENT_PAYMENT_ANNULLED_LABEL;
                break;

            case self::EVENT_PAYMENT_CREDITED:
                $label = self::EVENT_PAYMENT_CREDITED_LABEL;
                break;

            case self::EVENT_PAYMENT_DEBITED:
                $label = self::EVENT_PAYMENT_DEBITED_LABEL;
                break;

            case self::EVENT_CALLBACK_UNFREEZE:
                $label = self::EVENT_CALLBACK_UNFREEZE_LABEL;
                break;

            case self::EVENT_CALLBACK_BOOKED:
                $label = self::EVENT_CALLBACK_BOOKED_LABEL;
                break;

            case self::EVENT_CALLBACK_UPDATE:
                $label = self::EVENT_CALLBACK_UPDATE_LABEL;
                break;

            case self::EVENT_PAYMENT_CREATING:
                $label = self::EVENT_PAYMENT_CREATING_LABEL;
                break;

            case self::EVENT_PAYMENT_PROCESSING:
                $label = self::EVENT_PAYMENT_PROCESSING_LABEL;
                break;

            case self::EVENT_PAYMENT_PENDING:
                $label = self::EVENT_PAYMENT_PENDING_LABEL;
                break;

            case self::EVENT_PAYMENT_SIGNING:
                $label = self::EVENT_PAYMENT_SIGNING_LABEL;
                break;

            default:
                $label = '';
        }

        return $translate ? (string) __($label) : $label;
    }
}
