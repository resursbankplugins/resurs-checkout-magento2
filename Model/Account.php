<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model;

use \Resursbank\Checkout\Model\Api\Credentials;
use \Resursbank\Checkout\Model\ResourceModel\Account as Resource;
use \Magento\Framework\Model\AbstractModel;

/**
 * API account.
 *
 * @package Resursbank\Checkout\Model
 */
class Account extends AbstractModel
{
    /**
     * Initialize model.
     */
    protected function _construct()
    {
        $this->_init(Resource::class);
    }

    /**
     * Load by Credentials instance.
     *
     * @param Credentials $credentials
     * @return $this
     */
    public function loadByCredentials(Credentials $credentials)
    {
        $collection = $this->getCollection()
            ->addFieldToFilter('username', $credentials->getUsername())
            ->addFieldToFilter('environment', $credentials->getEnvironment());

        if (count($collection) > 0) {
            $this->load($collection->getLastItem()->getId());
        }

        return $this;
    }
}
