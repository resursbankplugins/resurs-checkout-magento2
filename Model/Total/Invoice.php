<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model\Total;

use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Invoice as MagentoInvoice;
use Magento\Sales\Model\Order\Total\AbstractTotal;
use Resursbank\Checkout\Model\Total\Quote as QuoteTotal;

/**
 * Collect and append custom payment method fee to invoice totals.
 *
 * @package Resursbank\Checkout\Model\Total
 */
class Invoice extends AbstractTotal
{
    /**
     * @param MagentoInvoice $invoice
     * @return $this
     */
    public function collect(
        MagentoInvoice $invoice
    ) {
        /** @var Order $order */
        $order = $invoice->getOrder();

        /** @var string $code */
        $code = QuoteTotal::CODE;

        $fee = (float) $order->getData($code . '_amount');
        $baseFee = (float) $order->getData('base_' . $code . '_amount');
        $tax = (float) $order->getData($code . '_tax_amount');
        $baseTax = (float) $order->getData('base_' . $code . '_tax_amount');

        $invoice->setGrandTotal($invoice->getGrandTotal() + $fee);
        $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $baseFee);

        $invoice->setData($code . '_amount', $fee);
        $invoice->setData('base_' . $code . '_amount', $baseFee);
        $invoice->setData($code . '_tax_amount', $tax);
        $invoice->setData('base_' . $code . '_tax_amount', $baseTax);

        return $this;
    }
}
