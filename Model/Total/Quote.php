<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model\Total;

use Exception;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote\Address\Total;
use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;
use Magento\Quote\Model\Quote as MagentoQuote;
use Magento\Tax\Model\Sales\Total\Quote\Tax as QuoteTax;
use Magento\Tax\Model\TaxDetails\ItemDetails;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Config\Methods as Config;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Logic\Account\Method\Tax;

/**
 * Calculate and append custom payment fee total on quote.
 *
 * @package Resursbank\Checkout\Model\Total
 */
class Quote extends AbstractTotal
{
    /**
     * Code identifying our custom payment fee.
     */
    const CODE = 'resursbank_payment_fee';

    /**
     * @var Config
     */
    private $config;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * @var Tax
     */
    private $tax;

    /**
     * @var Log
     */
    private $log;

    /**
     * @param Config $config
     * @param ApiConfig $apiConfig
     * @param PriceCurrencyInterface $priceCurrency
     * @param Tax $tax
     * @param Log $log
     */
    public function __construct(
        Config $config,
        ApiConfig $apiConfig,
        PriceCurrencyInterface $priceCurrency,
        Tax $tax,
        Log $log
    ) {
        $this->config = $config;
        $this->apiConfig = $apiConfig;
        $this->priceCurrency = $priceCurrency;
        $this->tax = $tax;
        $this->log = $log;

        $this->setCode(self::CODE);
    }

    /**
     * Collect totals.
     *
     * @param MagentoQuote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Total $total
     * @return $this
     * @throws Exception
     */
    public function collect(
        MagentoQuote $quote,
        ShippingAssignmentInterface $shippingAssignment,
        Total $total
    ): Quote {
        parent::collect($quote, $shippingAssignment, $total);

        $this->clearTotals($total, $quote);

        if ($this->isEnabled() && $this->hasItems($shippingAssignment)) {
            // Fee in base currency.
            $baseFee = $this->getPaymentMethodFee($quote);

            // Fee in store currency.
            $fee = $this->priceCurrency->convert($baseFee, $quote->getStore());

            // Add our custom total value.
            $total->setTotalAmount($this->getCode(), $fee);
            $total->setBaseTotalAmount($this->getCode(), $baseFee);

            // Add tax totals.
            if ($baseFee > 0) {
                try {
                    $this->appendAssociatedTaxable(
                        $quote,
                        $baseFee,
                        $fee
                    );

                    $this->addTaxDetailsToAddress(
                        $quote,
                        $baseFee
                    );
                } catch (Exception $e) {
                    $this->log->error($e);

                    throw $e;
                }
            }
        }

        return $this;
    }

    /**
     * @param Total $total
     * @param MagentoQuote $quote
     */
    public function clearTotals(Total $total, MagentoQuote $quote)
    {
        // Reset total values.
        $total->setTotalAmount($this->getCode(), 0)
            ->setBaseTotalAmount($this->getCode(), 0);

        // Reset tax values on billing address.
        $quote->getBillingAddress()
            ->unsetData("{$this->getCode()}_tax_amount")
            ->unsetData("base_{$this->getCode()}_tax_amount");

        // Reset tax values on shipping address.
        $quote->getShippingAddress()
            ->unsetData("{$this->getCode()}_tax_amount")
            ->unsetData("base_{$this->getCode()}_tax_amount");
    }

    /**
     * Fetch price details for JS configuration on frontend.
     *
     * @param MagentoQuote $quote
     * @param Total $total
     * @return array
     * @throws Exception
     */
    public function fetch(
        MagentoQuote $quote,
        Total $total
    ): array {
        return [
            'code' => $this->getCode(),
            'title' => __('Payment Fee'),
            'value' => $this->getPaymentMethodFee($quote)
        ];
    }

    /**
     * Append custom taxable item to shipping address. This lets us make use of
     * Magento's internal methods to calculate additional tax information.
     *
     * @param MagentoQuote $quote
     * @param float $baseFee
     * @param float $fee
     * @throws Exception
     */
    private function appendAssociatedTaxable(
        MagentoQuote $quote,
        float $baseFee,
        float $fee
    ) {
        // Get associated tax data.
        $tax = $this->getAssociatedTaxData($quote, $baseFee, $fee);

        /** @var array $associatedTaxables */
        $associatedTaxables = $quote->getShippingAddress()
            ->getAssociatedTaxables();

        // Add our custom tax data to associated taxables.
        if (!is_array($associatedTaxables)) {
            $associatedTaxables = [$tax];
        } else {
            // Totals are collected multiple times throughout the checkout
            // process, we do not want to add our tax data more than once.
            $taxApplied = false;

            foreach ($associatedTaxables as $taxable) {
                $code = $taxable[QuoteTax::KEY_ASSOCIATED_TAXABLE_CODE];

                if ($code === $this->getCode()) {
                    $taxApplied = true;
                    break;
                }
            }

            if (!$taxApplied) {
                $associatedTaxables = array_merge(
                    $associatedTaxables,
                    $tax
                );
            }
        }

        $quote->getShippingAddress()
            ->setAssociatedTaxables($associatedTaxables);
    }

    /**
     * Get associated tax data.
     *
     * @param MagentoQuote $quote
     * @param float $baseFee
     * @param float $fee
     * @return array
     * @throws Exception
     */
    private function getAssociatedTaxData(
        MagentoQuote $quote,
        float $baseFee,
        float $fee
    ) {
        $taxClass = $this->config->getTaxClass($quote->getStoreId());

        return [
            QuoteTax::KEY_ASSOCIATED_TAXABLE_CODE => $this->getCode(),
            QuoteTax::KEY_ASSOCIATED_TAXABLE_TYPE => $this->getCode(),
            QuoteTax::KEY_ASSOCIATED_TAXABLE_QUANTITY => 1,
            QuoteTax::KEY_ASSOCIATED_TAXABLE_TAX_CLASS_ID => $taxClass,
            QuoteTax::KEY_ASSOCIATED_TAXABLE_UNIT_PRICE => $fee,
            QuoteTax::KEY_ASSOCIATED_TAXABLE_BASE_UNIT_PRICE => $baseFee,
            QuoteTax::KEY_ASSOCIATED_TAXABLE_PRICE_INCLUDES_TAX => false,
            QuoteTax::KEY_ASSOCIATED_TAXABLE_ASSOCIATION_ITEM_CODE => null
        ];
    }

    /**
     * Append tax details to billing address. This helps us keep track of the
     * tax totals, which are later copied from the quote_address to the order
     * (see vendor/resursbank/checkout/etc/events.xml ::
     * sales_model_service_quote_submit_before and
     * vendor/resursbank/checkout/Observer/Sales/Order/Total/Copy.php
     * for more information).
     *
     * @param MagentoQuote $quote
     * @param float $baseFee
     * @throws Exception
     */
    private function addTaxDetailsToAddress(
        MagentoQuote $quote,
        float $baseFee
    ) {
        /** @var ItemDetails $taxDetails */
        $taxDetails = $this->tax->getTaxPrice($baseFee, $quote);

        if ($taxDetails === null) {
            throw new Exception('Failed to obtain tax details.');
        }

        // Add information to billing address.
        $quote->getBillingAddress()->setData(
            $this->getCode() . '_tax_amount',
            $taxDetails->getRowTax()
        );

        $quote->getBillingAddress()->setData(
            'base_' . $this->getCode() . '_tax_amount',
            $taxDetails->getRowTax()
        );

        // Add information to shipping address.
        $quote->getShippingAddress()->setData(
            $this->getCode() . '_tax_amount',
            $taxDetails->getRowTax()
        );

        $quote->getShippingAddress()->setData(
            'base_' . $this->getCode() . '_tax_amount',
            $taxDetails->getRowTax()
        );
    }

    /**
     * Retrieve payment method fee from configuration.
     *
     * @param MagentoQuote $quote
     * @return float
     */
    private function getPaymentMethodFee(MagentoQuote $quote): float
    {
        $result = 0.0;

        try {
            $method = $quote->getPayment()->getMethod();

            if ($method !== null) {
                $result = $this->config->getFee(
                    $method,
                    $quote->getStore()->getCode()
                );
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Whether or not this total segment is enabled.
     *
     * @return bool
     * @throws Exception
     */
    private function isEnabled()
    {
        return !$this->apiConfig->isCheckoutFlow();
    }

    /**
     * Check if the address has any products.
     *
     * @param ShippingAssignmentInterface $shippingAssignment
     * @return bool
     */
    private function hasItems(
        ShippingAssignmentInterface $shippingAssignment
    ): bool {
        $items = $shippingAssignment->getItems();

        return !empty($items);
    }
}
