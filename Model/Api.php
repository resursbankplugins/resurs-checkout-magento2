<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model;

use Exception;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Address;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Quote\Model\Quote;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Checkout\Helper\Api as HelperApi;
use Resursbank\Checkout\Helper\Callback as HelperCallback;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ConfigHelper;
use Resursbank\Checkout\Helper\Ecom;
use Resursbank\Checkout\Helper\General as GeneralHelper;
use Resursbank\Checkout\Helper\Log\Api as HelperLogApi;
use Resursbank\Checkout\Helper\Version;
use Resursbank\Checkout\Model\Api\Payment\Converter\QuoteConverter;
use stdClass;
use Zend\Http\Client;
use Zend\Http\Header\GenericHeader;
use Zend\Http\Header\HeaderInterface;
use Zend\Http\Headers;
use Zend\Http\Response;

/**
 * Class Api
 *
 * @package Resursbank\Checkout\Model
 */
class Api extends DataObject
{
    /**
     * Test API URL.
     *
     * @var string
     */
    const TEST_ENVIRONMENT_URL = 'https://omnitest.resurs.com/';

    /**
     * Production API URL.
     *
     * @var string
     */
    const PRODUCTION_ENVIRONMENT_URL = 'https://checkout.resurs.com/';

    /**
     * Key in checkout/session where we store the payment session id provided by the API.
     *
     * @var string
     */
    const PAYMENT_SESSION_ID_KEY = 'resursbank_checkout_payment_session_id';

    /**
     * Key in checkout/session where we store the payment reference.
     *
     * @var string
     */
    const PAYMENT_REFERENCE = 'resursbank_checkout_payment_reference';

    /**
     * Key in checkout/session where we store the resulting iframe provided by the API when we initialize a new session.
     *
     * @var string
     */
    const PAYMENT_SESSION_IFRAME_KEY = 'resursbank_checkout_payment_session_iframe';

    /**
     * Key in checkout/session where we store the resizer/communications script for the iframe, provided by the API.
     *
     * @var string
     */
    const PAYMENT_SESSION_IFRAME_SCRIPT_KEY = 'resursbank_checkout_payment_session_iframe_script';

    /**
     * Key in checkout/session where we store a potential origin of the iframe.
     *
     * @var string
     */
    const PAYMENT_SESSION_IFRAME_ORIGIN_KEY = 'resursbank_checkout_payment_session_iframe_origin';

    /**
     * Customer type for company.
     *
     * @var string
     */
    const CUSTOMER_TYPE_COMPANY = 'LEGAL';

    /**
     * Customer type for private citizens.
     *
     * @var string
     */
    const CUSTOMER_TYPE_PRIVATE = 'NATURAL';

    /**
     * @var HelperApi
     */
    private $helper;

    /**
     * @var HelperCallback
     */
    private $callbackHelper;

    /**
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @var HelperLogApi
     */
    private $log;

    /**
     * @var ManagerInterface
     */
    private $messages;

    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var ConfigHelper
     */
    private $configHelper;

    /**
     * @var QuoteConverter
     */
    private $quoteConverter;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Version
     */
    private $version;

    /**
     * @var GeneralHelper
     */
    private $generalHelper;

    /**
     * @var ProductMetadataInterface
     */
    private $productMetaData;

    /**
     * @var Ecom
     */
    private $ecom;

    /**
     * Constructor.
     *
     * @param CustomerSession $customerSession
     * @param HelperApi $helper
     * @param HelperCallback $callbackHelper
     * @param ScopeConfigInterface $scopeConfig
     * @param HelperLogApi $log
     * @param UrlInterface $url
     * @param ManagerInterface $messages
     * @param CheckoutSession $checkoutSession
     * @param ConfigHelper $configHelper
     * @param QuoteConverter $quoteConverter
     * @param RequestInterface $request
     * @param StoreManagerInterface $storeManager
     * @param Version $version
     * @param GeneralHelper $generalHelper
     * @param ProductMetadataInterface $productMetadata
     * @param ECom $ecom
     * @param array $data
     */
    public function __construct(
        CustomerSession $customerSession,
        HelperApi $helper,
        HelperCallback $callbackHelper,
        ScopeConfigInterface $scopeConfig,
        HelperLogApi $log,
        UrlInterface $url,
        ManagerInterface $messages,
        CheckoutSession $checkoutSession,
        ConfigHelper $configHelper,
        QuoteConverter $quoteConverter,
        RequestInterface $request,
        StoreManagerInterface $storeManager,
        Version $version,
        GeneralHelper $generalHelper,
        ProductMetadataInterface $productMetadata,
        Ecom $ecom,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->callbackHelper = $callbackHelper;
        $this->customerSession = $customerSession;
        $this->checkoutSession = $checkoutSession;
        $this->scopeConfig = $scopeConfig;
        $this->url = $url;
        $this->log = $log;
        $this->messages = $messages;
        $this->configHelper = $configHelper;
        $this->quoteConverter = $quoteConverter;
        $this->request = $request;
        $this->storeManager = $storeManager;
        $this->version = $version;
        $this->generalHelper = $generalHelper;
        $this->productMetaData = $productMetadata;
        $this->ecom = $ecom;

        parent::__construct($data);
    }

    /**
     * Initialize payment session.
     *
     * @return stdClass
     * @throws Exception
     */
    public function initPaymentSession()
    {
        // Collect data submitted in the API request.
        $data = [
            'orderLines' => $this->getPaymentLinesFromQuote($this->getQuote()),
            'successUrl' => $this->getSuccessCallbackUrl(),
            'backUrl' => $this->getFailureCallbackUrl(),
            'shopUrl' => $this->getShopUrl(),
            'customer' => $this->getCustomerInformation()
        ];

        // Perform API request.
        $result = $this->call(
            "checkout/payments/{$this->getPaymentReference(true)}",
            'post',
            $data
        );

        // Handle API response.
        $result = @json_decode($result);

        if (!is_object($result) ||
            !isset($result->paymentSessionId) ||
            !isset($result->iframe)
        ) {
            throw new Exception(
                __('Failed to create payment session, unexpected return value.')
            );
        }

        $this->checkoutSession->setData(
            self::PAYMENT_SESSION_ID_KEY,
            $result->paymentSessionId
        );

        $this->checkoutSession->setData(
            self::PAYMENT_SESSION_IFRAME_KEY,
            $result->iframe
        );

        $ocScriptSource = preg_replace('/(.*?)src="(.*?)"(.*)/', '$2', $result->script);
        $this->checkoutSession->setData(
            self::PAYMENT_SESSION_IFRAME_SCRIPT_KEY,
            $ocScriptSource
        );

        $this->checkoutSession->setData(
            self::PAYMENT_SESSION_IFRAME_ORIGIN_KEY,
            $this->ecom->getConnection()->getIframeOrigin($ocScriptSource, true)
        );

        return $result;
    }

    /**
     * Update existing payment session.
     *
     * @return string
     * @throws Exception
     */
    public function updatePaymentSession()
    {
        if (!$this->paymentSessionInitialized()) {
            throw new Exception(
                'Please initialize your payment session ' .
                'before you updating it.'
            );
        }

        $data = [
            'orderLines' => $this->getPaymentLinesFromQuote($this->getQuote())
        ];

        // Perform API request.
        return $this->call(
            "checkout/payments/{$this->getPaymentReference()}",
            'put',
            $data
        );
    }

    /**
     * Retrieve completed payment by quote id.
     *
     * @param string $paymentId
     * @return stdClass
     * @throws Exception
     */
    public function getPayment($paymentId)
    {
        $result = $this->call("checkout/payments/{$paymentId}", 'get');

        if (!empty($result)) {
            $result = @json_decode($result);
        }

        return $result;
    }

    /**
     * Delete active payment session.
     *
     * @return string
     * @throws Exception
     */
    public function deletePaymentSession()
    {
        return $this->call(
            "checkout/payments/{$this->getPaymentReference()}",
            'delete'
        );
    }

    /**
     * Update reference (id) of existing payment.
     *
     * @param string $current
     * @param string $new
     * @throws Exception
     */
    public function updatePaymentReference($current, $new)
    {
        if (!is_string($current) || !is_string($new)) {
            throw new Exception('Payment reference must always be a string.');
        }

        if ($current !== $new) {
            $this->call(
                "checkout/payments/{$current}/updatePaymentReference",
                'put',
                [
                    'paymentReference' => $new
                ]
            );

            $this->setPaymentReference($new);
        }
    }

    /**
     * Perform API call.
     *
     * If it's ever necessary to urlencode data sent to the API please refer to
     * the urlencodeArray() method in Helper\Data.php
     *
     * @param string $action
     * @param string $method (post|put|delete|get)
     * @param string|array $data
     * @param null|string $scopeCode
     * @return string
     * @throws Exception
     */
    public function call($action, $method, $data = null, $scopeCode = null)
    {
        $result = '';

        // Validate requested method.
        $this->validateCallMethod($method);

        try {
            // Perform API call.
            $response = $this->handleCall(
                $method,
                "/{$action}",
                $data,
                $scopeCode
            );
        } catch (Exception $e) {
            $match = preg_match(
                '/invalid chunk size \\"\\" unable to read chunked body/i',
                $e->getMessage()
            );

            if (!$match) {
                // Log error.
                $this->log->error($e);

                // Clear the payment session, ensuring that a new session will
                // be started once the API is reachable again.
                $this->helper->clearPaymentSession();

                // Throw the error forward.
                throw $e;
            }
        }

        // Handle none-empty response.
        if (isset($response) && ($response instanceof Response)) {
            // Handle potential errors.
            $this->handleErrors($response);

            // Set result to response body.
            $result = $response->getBody();
        }

        return $result;
    }

    /**
     * This method should never be called directly, only through call().
     *
     * @param string $method
     * @param string $path
     * @param string|array $data
     * @param null|string $scopeCode
     * @return null|Response
     * @throws Exception
     */
    private function handleCall($method, $path, $data = null, $scopeCode = null)
    {
        $data = json_encode($data);

        if ($this->httpClient === null) {
            $this->prepareHttpClient($scopeCode);
        }

        $this->httpClient->getUri()->setPath($path);

        // Log the call we are performing.
        $this->log->info(
            "Performing call to URL {$this->httpClient->getUri()}," .
            " path {$path}, method {$method}. Data submitted: {$data}"
        );

        $result = $this->httpClient->setEncType('application/json')
            ->setRawBody($data)
            ->setMethod($method)
            ->send();

        // Log result from performed call.
        $this->log->info("API responded with {$result->toString()}");

        return $result;
    }

    /**
     * Prepare API client.
     *
     * @param null|string $scopeCode
     * @throws Exception
     */
    public function prepareHttpClient($scopeCode = null)
    {
        $this->httpClient = new Client($this->getApiUrl('', $scopeCode));
        $this->httpClient
            ->setAuth(
                $this->configHelper->getUsername($scopeCode),
                $this->configHelper->getPassword($scopeCode)
            )
            ->setOptions([
                'timeout' => 180
            ]);

        $this->addUserAgentHeader($this->httpClient);
    }

    /**
     * Add module version information to user agent header.
     *
     * @param Client $client
     */
    private function addUserAgentHeader(Client &$client)
    {
        /** @var Headers $headers */
        $headers = $client->getRequest()->getHeaders();

        /** @var array $data */
        $data = $this->version->getAll();
        $data['magento2'] = $this->productMetaData->getVersion();

        /** @var HeaderInterface $header */
        $header = new GenericHeader(
            'user-agent',
            json_encode($data)
        );

        $headers->addHeader($header);
        $client->setHeaders($headers);
    }

    /**
     * Validate API request method.
     *
     * @param string $method
     * @return $this
     * @throws Exception
     */
    public function validateCallMethod($method)
    {
        if ($method !== 'get' &&
            $method !== 'put' &&
            $method !== 'post' &&
            $method !== 'delete'
        ) {
            throw new Exception(__('Invalid API method requested.'));
        }

        return $this;
    }

    /**
     * Retrieve iframe of current payment session from checkout/session.
     *
     * @return string
     */
    public function getSessionIframeHtml()
    {
        return (string) $this->checkoutSession->getData(
            self::PAYMENT_SESSION_IFRAME_KEY
        );
    }

    /**
     * Retrieve iframe script (oc/resizer) of current payment session.
     *
     * @return string
     */
    public function getSessionIframeScript() {
        return (string) $this->checkoutSession->getData(
            self::PAYMENT_SESSION_IFRAME_SCRIPT_KEY
        );
    }

    /**
     * Retreive iframe origin of current payment session.
     *
     * @return string
     */
    public function getSessionIframeOrigin() {
        return (string)$this->checkoutSession->getData(
            self::PAYMENT_SESSION_IFRAME_ORIGIN_KEY
        );
    }

    /**
     * Handle errors on response object.
     *
     * @param Response $response
     * @return $this
     * @throws Exception
     */
    public function handleErrors(Response $response)
    {
        if (($response->isClientError() || $response->isServerError())) {
            // Log the error.
            $this->log->error($response->toString());

            // Get readable error message.
            $error = (string) __(
                'We apologize, an error occurred while communicating with ' .
                'the payment gateway. Please contact us as soon as possible ' .
                'so we can review this problem.'
            );

            // Stop script.
            throw new Exception($error);
        }

        return $this;
    }

    /**
     * Retrieve payment session id.
     *
     * @return string
     * @throws Exception
     */
    public function getPaymentSessionId()
    {
        return (string) $this->checkoutSession->getData(
            self::PAYMENT_SESSION_ID_KEY
        );
    }

    /**
     * Retrieve payment reference from active quote.
     *
     * @param bool $refresh
     * @return string
     */
    public function getPaymentReference($refresh = false)
    {
        $result = (string) $this->checkoutSession->getData(
            self::PAYMENT_REFERENCE
        );

        if ($refresh) {
            $result = $this->generalHelper->strRand(32);

            $this->setPaymentReference($result);
        }

        return (string) $result;
    }

    /**
     * Set payment reference in session.
     *
     * @param string $reference
     * @return $this
     */
    public function setPaymentReference($reference)
    {
        $this->checkoutSession->setData(self::PAYMENT_REFERENCE, $reference);

        return $this;
    }

    /**
     * Shorthand to get current quote object.
     *
     * @return Quote
     * @throws NoSuchEntityException
     */
    public function getQuote()
    {
        return $this->helper->getQuote();
    }

    /**
     * Check if a payment session has been initialized.
     *
     * @return bool
     * @throws Exception
     */
    public function paymentSessionInitialized()
    {
        return (bool) $this->getPaymentSessionId();
    }

    /**
     * Retrieve API URL.
     *
     * @param string $action
     * @param null $scopeCode
     * @return string
     * @throws Exception
     */
    public function getApiUrl($action = '', $scopeCode = null)
    {
        return $this->configHelper->isInTestMode($scopeCode) ?
            (self::TEST_ENVIRONMENT_URL . $action) :
            (self::PRODUCTION_ENVIRONMENT_URL . $action);
    }

    /**
     * Retrieve payment lines for API call from Quote object.
     *
     * @param Quote $quote
     * @return array
     * @throws Exception
     */
    public function getPaymentLinesFromQuote(Quote $quote)
    {
        return $this->quoteConverter->convertItemsToArrays(
            $this->quoteConverter->convert($quote)
        );
    }

    /**
     * Retrieve customer information used when initializing a payment session.
     *
     * @return array
     */
    public function getCustomerInformation()
    {
        $billingAddress = $this->getCustomer()->getPrimaryBillingAddress();

        $phone = ($billingAddress instanceof Address) ?
            $billingAddress->getData('telephone')
            : null;

        return [
            'mobile' => $phone,
            'email' => $this->getCustomer()->getData('email')
        ];
    }

    /**
     * Get the currently logged in customer.
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customerSession->getCustomer();
    }

    /**
     * Retrieve shop url (used for iframe communication, the return value will
     * be the target origin). Basically this is your Magento websites
     * protocol:domain without any trailing slashes. For example
     * http://www.testing.com
     *
     * @return string
     */
    public function getShopUrl()
    {
        return rtrim($this->url->getBaseUrl(), '/');
    }

    /**
     * Retrieve iframe protocol:domain. This is used for iframe communication
     * (from JavaScript). This follows the same rules as getShopUrl() above,
     * so no trailing slashes (e.g https://resursbank.com)
     *
     * @return string
     * @throws Exception
     */
    public function getIframeUrl()
    {
        return rtrim($this->getApiUrl(), '/');
    }

    /**
     * Retrieve URL for order success callback from API.
     *
     * @return string
     */
    public function getSuccessCallbackUrl()
    {
        return $this->url->getUrl('checkout/onepage/success');
    }

    /**
     * Retrieve URL for order failure callback from API.
     *
     * @return string
     */
    public function getFailureCallbackUrl()
    {
        return $this->url->getUrl('checkout/onepage/failure');
    }

    /**
     * Check if a provided payment has a certain status applied.
     *
     * @param stdClass $payment
     * @param string $status
     * @return bool
     */
    public function paymentHasStatus(stdClass $payment, $status)
    {
        $status = strtoupper((string) $status);

        return (isset($payment->status) &&
            (
                (
                    is_array($payment->status) &&
                    in_array($status, $payment->status)
                ) ||
                (
                    is_string($payment->status) &&
                    strtoupper($payment->status) === $status
                )
            )
        );
    }

    /**
     * Attempt to retrieve current store.
     *
     * @return StoreInterface|null
     */
    private function getStore()
    {
        $store = null;

        try {
            $storeId = (int)$this->request->getParam('store');

            if ($storeId > 0) {
                $store = $this->storeManager->getStore($storeId);
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $store;
    }
}
