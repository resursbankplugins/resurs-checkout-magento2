<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model;

use Magento\Checkout\Api\Data\PaymentDetailsInterface;
use Magento\Checkout\Model\GuestPaymentInformationManagement;
use Magento\Checkout\Model\Session;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Quote\Model\Quote;
use Resursbank\Checkout\Api\GuestPaymentManagementInterface;

/**
 * Apply payment method on quote and collect new totals to reflect configured
 * fee for the payment method.
 *
 * NOTE: at first glance it may appear as though this class is unnecessary since
 * it's virtually a copy of PaymentManagement. The difference is
 * PaymentManagement expects the provided cartId in collectTotals to be an int
 * and GuestPaymentManagement expects this to be a string, a masked value the
 * actual quote gets resolved from. Because of this we need both classes.
 *
 * @package Resursbank\Checkout\Model
 */
class GuestPaymentManagement implements GuestPaymentManagementInterface
{
    /**
     * @var CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var GuestPaymentInformationManagement
     */
    private $paymentInformationManagement;

    /**
     * @param CartRepositoryInterface $quoteRepository
     * @param Session $session
     * @param GuestPaymentInformationManagement $paymentInformationManagement
     */
    public function __construct(
        CartRepositoryInterface $quoteRepository,
        Session $session,
        GuestPaymentInformationManagement $paymentInformationManagement
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->session = $session;
        $this->paymentInformationManagement = $paymentInformationManagement;
    }

    /**
     * @param int $cartId
     * @param PaymentInterface $paymentMethod
     * @return PaymentDetailsInterface|void
     */
    public function collectTotals(
        $cartId,
        PaymentInterface $paymentMethod
    ) {
        /** @var Quote $quote */
        $quote = $this->session->getQuote();

        // Assign payment method on quote.
        $quote->getPayment()->setMethod($paymentMethod->getMethod());

        // Collect totals and update quote.
        $this->quoteRepository->save($quote->collectTotals());

        // Assemble response containing all updated total values.
        return $this->paymentInformationManagement
            ->getPaymentInformation($cartId);
    }
}
