<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model\Config\Source\PartPayment;

use Exception;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Option\ArrayInterface;
use Resursbank\Checkout\Helper\Cache\Account as AccountCache;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ConfigApi;
use Resursbank\Checkout\Helper\Db\Account as AccountDb;
use Resursbank\Checkout\Helper\Db\Account\Method as MethodDb;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Logic\Account\Method as Helper;
use Resursbank\Checkout\Model\Account\Method as MethodModel;

/**
 * Class Methods
 *
 * Assembles list of payment methods eligible for part payments (to be displayed
 * on the configuration page).
 *
 * @package Resursbank\Checkout\Model\Config\Source\PartPayment
 */
class Methods implements ArrayInterface
{
    /**
     * @var Helper
     */
    private $helper;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var MethodDb
     */
    private $methodDb;

    /**
     * @var ConfigApi
     */
    private $configApi;

    /**
     * @var AccountDb
     */
    private $accountDb;

    /**
     * @var AccountCache
     */
    private $accountCache;

    /**
     * @var string
     */
    const CACHE_KEY = 'resursbank_checkout_account_methods';

    /**
     * Methods constructor.
     *
     * @param Helper $helper
     * @param Log $log
     * @param RequestInterface $request
     * @param MethodDb $methodDb
     * @param ConfigApi $configApi
     * @param AccountDb $accountDb
     * @param AccountCache $accountCache
     */
    public function __construct(
        Helper $helper,
        Log $log,
        RequestInterface $request,
        MethodDb $methodDb,
        ConfigApi $configApi,
        AccountDb $accountDb,
        AccountCache $accountCache
    ) {
        $this->helper = $helper;
        $this->log = $log;
        $this->request = $request;
        $this->methodDb = $methodDb;
        $this->configApi = $configApi;
        $this->accountDb = $accountDb;
        $this->accountCache = $accountCache;
    }

    /**
     * Options getter.
     *
     * @return array
     * @throws Exception
     */
    public function toOptionArray()
    {
        $result = [
            [
                'value' => '',
                'label' => __('Please select')
            ]
        ];

        foreach ($this->toArray() as $code => $title) {
            $result[] = [
                'value' => $code,
                'label' => $title
            ];
        }

        return $result;
    }

    /**
     * Get options in "key-value" format. Returns an associative array with the
     * payment methods id as key and its description as value.
     *
     * NOTE: Only methods with specificType == PART_PAYMENT or REVOLVING_CREDIT
     * will be included in the return value.
     *
     * @return string[]
     * @throws Exception
     */
    public function toArray()
    {
        $result = [];

        try {
            $accountId = $this->accountDb->getIdByCredentials(
                $this->configApi->getCredentials($this->getStoreId())
            );

            $result = $this->accountCache->loadByAccountId($accountId);

            if (empty($result)) {
                $collection = $this->methodDb->getCollection()
                    ->addFieldToFilter(MethodModel::ACCOUNT_ID, $accountId)
                    ->addFieldToFilter(MethodModel::ACTIVE, '1');

                foreach ($collection as $model) {
                    // Check if the payment method can be used for part payments.
                    if ($this->isEligible($model)) {
                        $result[$model->getId()] = $model->getTitle();
                    }
                }

                $this->accountCache->saveByAccountId($result, $accountId);
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return is_array($result) ? $result : [];
    }

    /**
     * Check whether or not a payment method is eligible for part payments.
     *
     * @param MethodModel $method
     * @return bool
     */
    public function isEligible(MethodModel $method)
    {
        $raw = json_decode($method->getData('raw'), true);

        return (
            is_array($raw) &&
            array_key_exists('specificType', $raw) &&
            (
                $raw['specificType'] === 'PART_PAYMENT' ||
                $raw['specificType'] === 'REVOLVING_CREDIT' ||
                $raw['specificType'] === 'CARD'
            )
        );
    }

    /**
     * Returns the store id.
     *
     * @return null|string
     */
    private function getStoreId()
    {
        $storeId = $this->request->getParam('store');

        return $storeId === '' ? null : $storeId;
    }
}
