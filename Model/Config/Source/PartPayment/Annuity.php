<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model\Config\Source\PartPayment;

use Exception;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Option\ArrayInterface;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiHelper;
use Resursbank\Checkout\Helper\Config\Checkout\PartPayment;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Logic\Account\Method\Annuity as AnnuityHelper;

/**
 * @package Resursbank\Checkout\Model\Config\Source\PartPayment
 */
class Annuity implements ArrayInterface
{
    /**
     * @var ApiHelper
     */
    private $apiHelper;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var PartPayment
     */
    private $config;

    /**
     * @var AnnuityHelper
     */
    private $annuityHelper;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * Duration constructor.
     *
     * @param ApiHelper $apiHelper
     * @param Log $log
     * @param PartPayment $config
     * @param AnnuityHelper $annuityHelper
     * @param RequestInterface $request
     */
    public function __construct(
        ApiHelper $apiHelper,
        Log $log,
        PartPayment $config,
        AnnuityHelper $annuityHelper,
        RequestInterface $request
    ) {
        $this->apiHelper = $apiHelper;
        $this->log = $log;
        $this->config = $config;
        $this->annuityHelper = $annuityHelper;
        $this->request = $request;
    }

    /**
     * Options getter.
     *
     * @param int|null methodId
     * @return array
     */
    public function toOptionArray($methodId = null)
    {
        $result = [
            [
                'value' => '',
                'label' => __('Please select')
            ]
        ];

        foreach ($this->toArray($methodId) as $duration => $title) {
            $result[] = [
                'value' => $duration,
                'label' => $title
            ];
        }

        return is_array($result) ? $result : [];
    }

    /**
     * Get options in "key-value" format. Returns an associative array with the
     * payment method annuities.
     *
     * @param int|null methodId
     * @return array
     */
    public function toArray($methodId = null)
    {
        $result = [];

        try {
            // This is a temporary solution. We should no longer require it
            // after upgrading the ECom library to version 1.3.x.
            if (!$this->apiHelper->hasCredentials()) {
                throw new Exception('No API credentials provided.');
            }

            if (!is_int($methodId)) {
                $methodId = $this->config->getPaymentMethod($this->getStoreId());
            }

            $annuities = $this->annuityHelper->getAnnuities($methodId);

            foreach ($annuities as $annuity) {
                $result[$annuity['duration']] = $annuity['title'];
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Returns the store id.
     *
     * @return null|string
     */
    private function getStoreId()
    {
        $storeId = $this->request->getParam('store');

        return $storeId === '' ? null : $storeId;
    }
}
