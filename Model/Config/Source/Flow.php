<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Options for flow selection on configuration page.
 *
 * @package Resursbank\Checkout\Model\Config\Source
 */
class Flow implements ArrayInterface
{
    /**
     * Value for the "Simplified" option.
     *
     * @type string
     */
    const SIMPLIFIED = 'simplified';

    /**
     * Value for the "Checkout" option.
     *
     * @type string
     */
    const CHECKOUT = 'checkout';

    /**
     * Options getter.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::CHECKOUT,
                'label' => __('Onepage iFrame based Resurs Checkout')
            ],
            [
                'value' => self::SIMPLIFIED,
                'label' => __('Two step Magento Checkout with Resurs payment methods')
            ]
        ];
    }

    /**
     * Get options in "key-value" format.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            self::CHECKOUT => __('Onepage iFrame based Resurs Checkout'),
            self::SIMPLIFIED => __('Two step Magento Checkout with Resurs payment methods')
        ];
    }
}
