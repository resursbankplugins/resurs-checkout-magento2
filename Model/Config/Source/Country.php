<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Options for country selection on configuration page.
 *
 * @package Resursbank\Checkout\Model\Config\Source
 */
class Country implements ArrayInterface
{
    /**
     * @var string
     */
    const SWEDEN = 'SE';

    /**
     * @var string
     */
    const NORWAY = 'NO';

    /**
     * @var string
     */
    const FINLAND = 'FI';

    /**
     * Options getter.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::SWEDEN,
                'label' => __('Sweden')
            ],
            [
                'value' => self::NORWAY,
                'label' => __('Norway')
            ],
            [
                'value' => self::FINLAND,
                'label' => __('Finland')
            ]
        ];
    }

    /**
     * Get options in "key-value" format.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            self::SWEDEN => __('Sweden'),
            self::NORWAY => __('Norway'),
            self::FINLAND => __('Finland')
        ];
    }

    /**
     * Returns a list of country codes used by validators.
     *
     * @return array
     */
    public static function getValidCountryCodes()
    {
        return [
            self::SWEDEN,
            self::NORWAY,
            self::FINLAND
        ];
    }
}
