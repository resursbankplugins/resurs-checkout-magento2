<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model\Cache\Type;

use \Magento\Framework\Cache\Frontend\Decorator\TagScope;
use \Magento\Framework\Config\CacheInterface;

class General extends TagScope implements CacheInterface
{
    /**
     * Unique cache identifier.
     *
     * @var string
     */
    const TYPE_IDENTIFIER = 'resursbank_checkout';

    /**
     * Tag which identifies all cached data processed using this class.
     *
     * @var string
     */
    const CACHE_TAG = 'RESURSBANK_CHECKOUT';

    /**
     * @var \Magento\Framework\App\Cache\Type\FrontendPool
     */
    private $cacheFrontendPool;

    /**
     * @param \Magento\Framework\App\Cache\Type\FrontendPool $cacheFrontendPool
     */
    public function __construct(\Magento\Framework\App\Cache\Type\FrontendPool $cacheFrontendPool)
    {
        $this->cacheFrontendPool = $cacheFrontendPool;
    }

    /**
     * Retrieve cache frontend instance being decorated.
     *
     * @return \Magento\Framework\Cache\FrontendInterface
     */
    protected function _getFrontend()
    {
        $frontend = parent::_getFrontend();
        if (!$frontend) {
            $frontend = $this->cacheFrontendPool->get(self::TYPE_IDENTIFIER);
            $this->setFrontend($frontend);
        }
        return $frontend;
    }

    /**
     * Retrieve cache tag name.
     *
     * @return string
     */
    public function getTag()
    {
        return self::CACHE_TAG;
    }

    /**
     * {@inheritdoc}
     */
    public function clean(
        $mode = \Zend_Cache::CLEANING_MODE_ALL,
        array $tags = [self::CACHE_TAG]
    ) {
        return parent::clean($mode, $tags);
    }
}
