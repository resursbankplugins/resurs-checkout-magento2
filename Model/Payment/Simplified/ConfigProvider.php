<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model\Payment\Simplified;

use Exception;
use Magento\Checkout\Helper\Data;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Checkout\Helper\Api as ApiHelper;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Config\Methods as Config;
use Resursbank\Checkout\Helper\Db\Account\Method as MethodDb;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Logic\Account\Method\Tax;
use Resursbank\Checkout\Model\Account\Method;
use Resursbank\Checkout\Model\ResourceModel\Account\Method\Collection;

/**
 * Provides configuration information for checkout when using Simplified Flow.
 *
 * @package Resursbank\Checkout\Model\Payment\Simplified
 */
class ConfigProvider implements ConfigProviderInterface
{
    /**
     * @var Log
     */
    private $log;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var MethodDb
     */
    private $methodDb;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @var ApiHelper
     */
    private $apiHelper;

    /**
     * @var Tax
     */
    private $tax;

    /**
     * @var Data
     */
    private $data;

    /**
     * @param Log $log
     * @param Config $config
     * @param MethodDb $methodDb
     * @param StoreManagerInterface $storeManager
     * @param ApiConfig $apiConfig
     * @param ApiHelper $apiHelper
     * @param Tax $tax
     * @param Data $data
     */
    public function __construct(
        Log $log,
        Config $config,
        MethodDb $methodDb,
        StoreManagerInterface $storeManager,
        ApiConfig $apiConfig,
        ApiHelper $apiHelper,
        Tax $tax,
        Data $data
    ) {
        $this->log = $log;
        $this->config = $config;
        $this->methodDb = $methodDb;
        $this->storeManager = $storeManager;
        $this->apiConfig = $apiConfig;
        $this->apiHelper = $apiHelper;
        $this->tax = $tax;
        $this->data = $data;
    }

    /**
     * {@inheritdoc}
     * @throws Exception
     */
    public function getConfig()
    {
        return [
            'payment' => [
                'resursbank' => [
                    'methods' => $this->getMethods(),
                    'apiConfig' => [
                        'country' => $this->apiConfig->getCountry()
                    ],
                    'fee' => [
                        'tax' => [
                            'display' => $this->config->getTaxDisplay()
                        ]
                    ],
                    'selectedMethod' => $this->apiHelper->getQuote()
                        ->getPayment()->getMethod(),
                    'isBillingOnPaymentMethod' => $this->data
                        ->isDisplayBillingOnPaymentMethodAvailable()
                ]
            ]
        ];
    }

    /**
     * Retrieve list of available payment methods from Resurs Bank.
     *
     * @return array
     */
    protected function getMethods()
    {
        $result = [];

        try {
            /** @var Collection $result */
            $collection = $this->methodDb->getActiveCollection(
                $this->storeManager->getStore()->getCode()
            );

            foreach ($collection as $method) {
                $fee = $this->getMethodFee($method);

                $result[] = [
                    'code' => $method->getCode(),
                    'title' => $this->getMethodTitle($method),
                    'type' => $method->getRawValue('type'),
                    'fee' => $fee,
                    'feeTax' => $this->getMethodFeeTax($fee),
                    'maxOrderTotal' => $method->getMaxOrderTotal(),
                    'specificType' => $method->getRawValue('specificType'),
                    'disableInput' => $this->getDisableInput($method),
                ];
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Get configured method title (fall back to title provided by the API).
     *
     * @param Method $method
     * @return string
     * @throws LocalizedException
     * @throws Exception
     */
    private function getMethodTitle(Method $method)
    {
        $result = (string) $this->config->getSetting(
            'title',
            $method->getCode(),
            $this->storeManager->getStore()->getCode()
        );

        if ($result === '') {
            $result = $method->getTitle();
        }

        return $result;
    }

    /**
     * Get configured method payment fee.
     *
     * @param Method $method
     * @return float
     * @throws LocalizedException
     * @throws Exception
     */
    private function getMethodFee(Method $method)
    {
        return $this->config->getFee(
            $method->getCode(),
            $this->storeManager->getStore()->getCode()
        );
    }

    /**
     * Retrieve method fee tax.
     *
     * @param float $fee
     * @return float
     * @throws NoSuchEntityException
     * @throws Exception
     */
    private function getMethodFeeTax($fee)
    {
        $fee = (float) $fee;
        $result = 0.0;

        if ($fee > 0) {
            $taxDetails = $this->tax->getTaxPrice(
                $fee,
                $this->apiHelper->getQuote()
            );

            if ($taxDetails === null) {
                throw new Exception(
                    'Failed to obtain tax details for payment fee.'
                );
            }

            $result = (float) $taxDetails->getRowTax();
        }

        return $result;
    }

    /**
     * Get whether or not inputs are disabled for requested field.
     *
     * @param Method $method
     * @return boolean
     * @throws NoSuchEntityException
     * @throws Exception
     */
    private function getDisableInput(Method $method)
    {
        return $this->config->getFlag(
            'disable_input',
            $method->getCode(),
            $this->storeManager->getStore()->getCode()
        );
    }
}
