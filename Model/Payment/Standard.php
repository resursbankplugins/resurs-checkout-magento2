<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Model\Payment;

use Exception;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\InfoInterface;
use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Payment\Model\Method\Logger;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;
use Resursbank\Checkout\Helper\Admin;
use Resursbank\Checkout\Helper\Api;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Config\Checkout\Ecom as EcomConfig;
use Resursbank\Checkout\Helper\Config\Checkout\General as GeneralConfig;
use Resursbank\Checkout\Helper\Config\Methods as MethodsConfig;
use Resursbank\Checkout\Helper\Db\Account\Method as MethodDb;
use Resursbank\Checkout\Helper\Db\PaymentHistory as PaymentHistoryDbHelper;
use Resursbank\Checkout\Helper\Ecom;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Order as OrderHelper;
use Resursbank\Checkout\Helper\Payment as PaymentHelper;
use Resursbank\Checkout\Model\Account\Method;
use Resursbank\Checkout\Model\Api\Adapter\Simplified;
use Resursbank\Checkout\Model\Api\Credentials;
use Resursbank\Checkout\Model\PaymentHistory;
use Resursbank\RBEcomPHP\ResursBank;

/**
 * This describes the standard (fallback) payment method, as well as include
 * generic functionality shared between all payment methods provided by Resurs
 * Bank (such as capturing payments).
 */
class Standard extends AbstractMethod
{
    /**
     * Resurs Bank payment method code prefix.
     *
     * @var string
     */
    const CODE_PREFIX = 'resursbank_';

    /**
     * The code of the standard payment method (we call it "resursbank_default")
     * for legacy reasons. In the original Magento 1 module it was called that,
     * and to ensure upgrades function properly it's code remain the same here.
     * The reason this file is named "Standard" is because "Default" is a
     * keyword and thus we cannot use it.
     *
     * @var string
     */
    const CODE = (
        self::CODE_PREFIX . 'default'
    );

    /**
     * Default title.
     *
     * @var string
     */
    const TITLE = 'Resurs Bank';

    /**
     * Default min limit value (minimum grand total to use method).
     *
     * @var float
     */
    const MIN_LIMIT = 150.0;

    /**
     * Default max limit value (maximum grand total to use method).
     *
     * @var float
     */
    const MAX_LIMIT = 0.0;

    /**
     * Payment method code.
     *
     * @var string
     */
    protected $_code = self::CODE;

    /**
     * Can capture payment.
     *
     * @var bool
     */
    protected $_canCapture = true;

    /**
     * Can be refunded.
     *
     * @var bool
     */
    protected $_canRefund = true;

    /**
     * Can refund partial invoice amount.
     *
     * @var bool
     */
    protected $_canRefundInvoicePartial = true;

    /**
     * Can be used during checkout.
     *
     * @var bool
     */
    protected $_canUseCheckout = true;

    /**
     * Cannot be used from the administration panel.
     *
     * NOTE: this is set to false because of an old work-around to get our
     * payment methods working properly in 2.1.x. The old fix read the code for
     * the method from the info instance object attached to the instance of this
     * model. This caused problems with the code of other payment methods being
     * overwritten with the code of the currently selected payment method in the
     * admin panel, and as later discovered in Simplified Flow. We initially
     * did not know this, and thus disabled our payment methods for the admin
     * panel to avoid the issue. Technically it should be safe to enable them
     * now in the admin panel again but it's unnecessary since they cannot be
     * used there right now due to the lack of other properties required for
     * the methods (such as customer SSN).
     *
     * @var bool
     */
    protected $_canUseInternal = false;

    /**
     * @var BuilderInterface
     */
    private $transactionBuilder;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var Ecom
     */
    private $ecom;

    /**
     * @var Api
     */
    private $apiHelper;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var PaymentHelper
     */
    private $paymentHelper;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @var EcomConfig
     */
    private $ecomConfig;

    /**
     * @var GeneralConfig
     */
    private $generalConfig;

    /**
     * @var MethodsConfig
     */
    private $methodsConfig;

    /**
     * @var Simplified
     */
    private $simplified;

    /**
     * @var MethodDb
     */
    private $methodDb;

    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @var PaymentHistoryDbHelper
     */
    private $paymentHistoryDbHelper;

    /**
     * @var Admin
     */
    private $admin;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param Data $paymentData
     * @param ScopeConfigInterface $scopeConfig
     * @param Logger $logger
     * @param BuilderInterface $transactionBuilder
     * @param Ecom $ecom
     * @param Api $apiHelper
     * @param Log $log
     * @param ApiConfig $apiConfig
     * @param EcomConfig $ecomConfig
     * @param GeneralConfig $generalConfig
     * @param MethodsConfig $methodsConfig
     * @param Simplified $simplified
     * @param MethodDb $methodDb
     * @param OrderHelper $orderHelper
     * @param PaymentHelper $paymentHelper
     * @param ManagerInterface $messageManager
     * @param PaymentHistoryDbHelper $paymentHistoryDbHelper
     * @param Admin $admin
     * @param AbstractResource $resource
     * @param AbstractDb $resourceCollection
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        Data $paymentData,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        BuilderInterface $transactionBuilder,
        Ecom $ecom,
        Api $apiHelper,
        Log $log,
        ApiConfig $apiConfig,
        EcomConfig $ecomConfig,
        GeneralConfig $generalConfig,
        MethodsConfig $methodsConfig,
        Simplified $simplified,
        MethodDb $methodDb,
        OrderHelper $orderHelper,
        PaymentHelper $paymentHelper,
        ManagerInterface $messageManager,
        PaymentHistoryDbHelper $paymentHistoryDbHelper,
        Admin $admin,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->transactionBuilder = $transactionBuilder;
        $this->messageManager = $messageManager;
        $this->ecom = $ecom;
        $this->apiHelper = $apiHelper;
        $this->log = $log;
        $this->paymentHelper = $paymentHelper;
        $this->apiConfig = $apiConfig;
        $this->ecomConfig = $ecomConfig;
        $this->generalConfig = $generalConfig;
        $this->methodsConfig = $methodsConfig;
        $this->simplified = $simplified;
        $this->methodDb = $methodDb;
        $this->orderHelper = $orderHelper;
        $this->paymentHistoryDbHelper = $paymentHistoryDbHelper;
        $this->admin = $admin;

        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Check whether or not the method is available.
     *
     * If we are using Simplified Flow a couple of additional rules dictates
     * whether or not a payment method is available:
     *
     * 1) The default payment method is never available since it's a fallback
     * for Checkout Flow.
     *
     * 2) If the country defined in the customers billing address doesn't match
     * the country configured for our module no payment methods will work at
     * all.
     *
     * @param CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(CartInterface $quote = null)
    {
        $result = false;

        try {
            $result = $this->generalConfig->isEnabled() ?
                parent::isAvailable($quote) :
                false;

            if ($result && $this->apiConfig->isSimplifiedFlow()) {
                $result = $this->isAvailableInSimplifiedFlow($quote);
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Retrieve Resurs Bank method model instance, to retrieve values from the
     * database.
     *
     * @return Method
     * @throws LocalizedException
     */
    public function getMethodModel()
    {
        return $this->methodDb->loadByCode($this->getCode());
    }

    /**
     * Collect method title from info instance attached to this model.
     *
     * @return string
     */
    public function getTitle()
    {
        $result = '';

        try {
            if ($this->getInfoInstance()->hasMethod()) {
                $result = (string)$this->getInfoInstance()
                    ->getAdditionalInformation('method_title');
            }
        } catch (Exception $e) {
            // Do nothing.
        }

        return $result !== '' ? $result : parent::getTitle();
    }

    /**
     * Retrieve payment method config value.
     *
     * We need this method in order for native payment method values, like
     * "sort_order", to function while using Simplified Flow.
     *
     * @param string $field
     * @param null $storeId
     * @return int|mixed|null
     * @throws LocalizedException
     * @throws Exception
     */
    public function getConfigData($field, $storeId = null)
    {
        $result = null;

        if ($storeId === null) {
            $storeId = $this->getStore();
        }

        if ($field === 'sort_order') {
            $result = $this->methodsConfig->getSetting(
                $field,
                $this->getCode(),
                $storeId
            );
        } else if ($field === 'max_order_total') {
            $result = $this->getMaxOrderTotal($this->getMethodModel());
        } else {
            $result = parent::getConfigData($field, $storeId);
        }

        return $result;
    }

    /**
     * Set code on this model instance to provided value.
     *
     * @param string $code
     */
    public function setCode($code)
    {
        $this->_code = (string)$code;
    }

    /**
     * @param InfoInterface $payment
     * @param float $amount
     * @return $this
     * @throws Exception
     */
    public function capture(InfoInterface $payment, $amount)
    {
        parent::capture($payment, $amount);

        try {
            /** @var Order $order */
            $order = $payment->getOrder();
            if ($this->isEnabled($order)) {
                /** @var string $reference */
                $reference = $this->orderHelper->getPaymentId(
                    $payment->getOrder()
                );

                // Log capturing event.
                $this->log->info(
                    "Capturing payment of order {$reference} with amount " .
                    "{$amount}, using method {$this->_code}."
                );

                // Finalize the payment.
                $this->finalizePayment($payment, $reference);

                // Set order increment_id as transaction identifier.
                $payment->setTransactionId($reference);

                // Log capturing event.
                $this->log->info(
                    "Successfully captured payment of order {$reference}."
                );

                // Close transaction to complete process.
                $payment->setIsTransactionClosed(true);

                // Add payment history entry.
                $this->logPaymentDebitedEvent($order);
            }
        } catch (Exception $e) {
            // Display generic error.
            $this->messageManager->addErrorMessage(
                sprintf(__('Failed to debit Resurs Bank payment: %s.'), $e->getMessage())
            );

            // Log actual error.
            $this->log->error($e);

            throw $e;
        }

        return $this;
    }

    /**
     * Finalize Resursbank payment.
     *
     * @param InfoInterface $payment
     * @param string $reference
     * @return $this
     * @throws Exception
     */
    public function finalizePayment(InfoInterface $payment, $reference)
    {
        // Retrieve API credentials.
        $credentials = $this->getApiCredentials($payment->getOrder());

        $reference = (string)$reference;
        $paymentSession = $this->ecom->getConnection($credentials)
            ->getPayment($reference);

        // Finalize Resursbank payment.
        if ($this->canFinalize($paymentSession)) {
            if ($paymentSession->frozen) {
                throw new Exception(
                    "Resursbank payment {$reference} is still frozen."
                );
            }

            // Check if payment is debitable.
            if (!$this->paymentHelper->isDebitable($paymentSession)) {
                throw new Exception(
                    "Resursbank payment {$reference} cannot be debited yet."
                );
            }

            /** @var ResursBank $ecom */
            $ecom = $this->ecom->getConnection($credentials);

            // Set platform / user reference.
            $ecom->setRealClientName('Magento2');
            $ecom->setLoggedInUser($this->admin->getUserName());

            // Without this ECom will lose the payment reference.
            $ecom->setPreferredId($reference);

            // Finalize payment session.
            $finalize = $ecom->finalizePayment($reference);

            if (!$finalize) {
                throw new Exception(
                    "Failed to finalize Resursbank payment {$reference}."
                );
            }
        }

        return $this;
    }

    /**
     * Retrieve API credentials relative to the store the order was placed in.
     *
     * @param Order $order
     * @return Credentials
     * @throws Exception
     */
    private function getApiCredentials(Order $order)
    {
        return $this->apiConfig->getCredentials($order->getStore()->getCode());
    }

    /**
     * Payment can only be captured if the payment at Resurs Bank is not marked
     * as "finalized" and the payment is not fully or partially debited.
     *
     * @param mixed $payment
     * @return bool
     */
    private function canFinalize($payment)
    {
        return (
            !$this->paymentHelper->isFinalized($payment) &&
            !$this->ecom->paymentSessionHasStatus($payment, 'IS_DEBITED')
        );
    }

    /**
     * Check whether or not we can capture payment.
     *
     * @param Order $order
     * @return bool
     * @throws Exception
     */
    private function isEnabled(Order $order)
    {
        return (
            $this->ecomConfig->isAfterShopEnabled(
                $order->getStore()->getCode()
            ) &&
            $this->paymentHelper->validate($order)
        );
    }

    /**
     * Check if this method is available while using Simplified flow.
     *
     * 1) Cannot be resursbank_default method.
     * 2) Billing address country must match configured country for the module.
     * 3) Selected customer type must be applicable.
     *
     * @param CartInterface|null $quote
     * @return bool
     * @throws LocalizedException
     */
    private function isAvailableInSimplifiedFlow(CartInterface $quote = null)
    {
        return (
            $this->getCode() !== self::CODE &&
            $this->validateSimplifiedFlowCustomerType()
        );
    }

    /**
     * Check if selected customer type stored in our session matches the
     * customerType raw data value for this payment method.
     *
     * NOTE: This is really only relevant for Simplified flow.
     *
     * @return bool
     * @throws LocalizedException
     */
    private function validateSimplifiedFlowCustomerType()
    {
        return in_array(
            $this->simplified->getCustomerType(),
            (array)$this->getMethodModel()->getRawValue('customerType')
        );
    }

    /**
     * Log payment history event when payment has been debited.
     *
     * @param Order $order
     * @return void
     * @throws LocalizedException
     * @throws AlreadyExistsException
     */
    private function logPaymentDebitedEvent(Order $order)
    {
        $this->paymentHistoryDbHelper->addEntry(
            $this->paymentHistoryDbHelper->createEntry(
                (int)$order->getPayment()->getEntityId(),
                PaymentHistory::EVENT_PAYMENT_DEBITED,
                PaymentHistory::USER_CLIENT,
                'Performed by user: ' . $this->admin->getUserName()
            )
        );
    }

    /**
     * @param Method $method
     * @return float
     * @throws Exception
     */
    public function getMaxOrderTotal(
        Method $method
    ) {
        $type = $method->getRawValue('type');
        $specificType = $method->getRawValue('specificType');

        $result = $method->getMaxOrderTotal();

        if (
            $type === 'PAYMENT_PROVIDER' &&
            $specificType === 'SWISH' &&
            $this->apiConfig->isSimplifiedFlow()
        ) {
            $configured = $this->methodsConfig->getSwishMaxOrderTotal();

            if ($configured !== '' && is_numeric($configured)) {
                $result = (float) $configured;
            }
        }

        return $result;
    }
}
