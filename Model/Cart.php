<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Model;

use Exception;
use Magento\Sales\Model\Order;
use Resursbank\Checkout\Api\CartInterface;
use Resursbank\Checkout\Helper\Api;
use Resursbank\Checkout\Helper\Cart as CartHelper;
use Resursbank\Checkout\Helper\Checkout\Onepage;
use Resursbank\Checkout\Helper\Db\PaymentHistory as History;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Order as OrderHelper;
use Resursbank\Checkout\Model\PaymentHistory;

/**
 * Methods related to shopping cart.
 *
 * @package Resursbank\Checkout\Model
 */
class Cart implements CartInterface
{
    /**
     * @var Onepage
     */
    private $onepage;

    /**
     * @var CartHelper
     */
    private $cartHelper;

    /**
     * @var History
     */
    private $history;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @param Onepage $onepage
     * @param CartHelper $cartHelper
     * @param History $history
     * @param Log $log
     */
    public function __construct(
        Onepage $onepage,
        CartHelper $cartHelper,
        History $history,
        Log $log,
        OrderHelper $orderHelper
    ) {
        $this->onepage = $onepage;
        $this->cartHelper = $cartHelper;
        $this->history = $history;
        $this->log = $log;
        $this->orderHelper = $orderHelper;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function rebuild()
    {
        // Rebuild cart from previous quote.
        $this->cartHelper->rebuildCart($this->getOrder());
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function deny()
    {
        // Deny the order
        $this->orderHelper->deny($this->getOrder());
    }

    /**
     * @inheritdoc
     */
    public function logRedirectToGatewayEvent()
    {
        try {
            $paymentId = $this->getPaymentId();

            if ($paymentId !== 0) {
                $this->orderHelper->pendingPayment(
                    $this->getOrder(),
                    PaymentHistory::USER_CUSTOMER
                );
            }
        } catch (Exception $e) {
            $this->log->error($e->getMessage());
        }
    }

    /**
     * Retrieve id of payment entity associated with current order.
     *
     * @return int
     * @throws Exception
     */
    private function getPaymentId(): int
    {
        return (int) $this->getOrder()
            ->getPayment()
            ->getEntityId();
    }

    /**
     * Retrieve the current order.
     *
     * @return Order
     */
    private function getOrder(): Order
    {
        return $this->onepage->getCurrentOrder();
    }
}
