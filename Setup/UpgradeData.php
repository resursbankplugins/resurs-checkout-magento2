<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Setup;

use Exception;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Sales\Model\Order\StatusFactory;
use Magento\Sales\Model\ResourceModel\Order\Status as StatusResourceModel;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Checkout\Helper\Config\Checkout\Api as Config;
use Resursbank\Checkout\Helper\Db\Account\Method\Annuity as AnnuityDb;
use Resursbank\Checkout\Helper\Db\Account\Method as MethodDb;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Logic\Account as AccountHelper;
use Resursbank\Checkout\Helper\Logic\Account\Method as MethodHelper;
use Resursbank\Checkout\Model\Account\Method;
use Resursbank\Checkout\Model\Api\Credentials;
use Resursbank\Checkout\Model\PaymentHistory;

/**
 * Data manipulation during module upgrades.
 *
 * @package Resursbank\Checkout\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * XML (config) path to username for test environment.
     *
     * @var string
     */
    const XML_PATH_TEST_UN = 'resursbank_checkout/api/username_test';

    /**
     * XML (config) path to password for test environment.
     *
     * @var string
     */
    const XML_PATH_TEST_PW = 'resursbank_checkout/api/password_test';

    /**
     * XML (config) path to username for production environment.
     *
     * @var string
     */
    const XML_PATH_PROD_UN = 'resursbank_checkout/api/username_production';

    /**
     * XML (config) path to password for production environment.
     *
     * @var string
     */
    const XML_PATH_PROD_PW = 'resursbank_checkout/api/password_production';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var WriterInterface
     */
    private $writerInterface;

    /**
     * @var Method
     */
    private $method;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var AccountHelper
     */
    private $accountHelper;

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var MethodHelper
     */
    private $methodHelper;

    /**
     * @var MethodDb
     */
    private $methodDb;

    /**
     * @var AnnuityDb
     */
    private $annuityDb;

    /**
     * @var StatusResourceModel
     */
    private $statusResourceModel;

    /**
     * @var StatusFactory
     */
    private $statusFactory;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param WriterInterface $writerInterface
     * @param Method $method
     * @param StoreManagerInterface $storeManager
     * @param Config $config
     * @param Log $log
     * @param AccountHelper $accountHelper
     * @param ObjectManagerInterface $objectManager
     * @param MethodHelper $methodHelper
     * @param MethodDb $methodDb
     * @param AnnuityDb $annuityDb
     * @param StatusResourceModel $statusResourceModel
     * @param StatusFactory $statusFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        WriterInterface $writerInterface,
        Method $method,
        StoreManagerInterface $storeManager,
        Config $config,
        Log $log,
        AccountHelper $accountHelper,
        ObjectManagerInterface $objectManager,
        MethodHelper $methodHelper,
        MethodDb $methodDb,
        AnnuityDb $annuityDb,
        StatusResourceModel $statusResourceModel,
        StatusFactory $statusFactory
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->writerInterface = $writerInterface;
        $this->method = $method;
        $this->storeManager = $storeManager;
        $this->config = $config;
        $this->log = $log;
        $this->accountHelper = $accountHelper;
        $this->objectManager = $objectManager;
        $this->methodHelper = $methodHelper;
        $this->methodDb = $methodDb;
        $this->annuityDb = $annuityDb;
        $this->statusResourceModel = $statusResourceModel;
        $this->statusFactory = $statusFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        try {
            // In version 2.6.0 we implement a new way to handle API credentials
            // based on selected environment.
            if (version_compare($context->getVersion(), '2.6.0', '<')) {
                $this->separateCredentials();
            }

            // In version 3.8.0 we implement a new way to tack payment methods.
            if (version_compare($context->getVersion(), '3.8.0', '<')) {
                // Fill method table with payment methods collected from orders.
                $this->methodHelper->syncLegacyMethods();

                // Fill account table with data collected from config.
                $this->fillAccountTable();
            }

            // In version 4.1.1 we revamp method naming convention.
            if (version_compare($context->getVersion(), '4.1.1', '<')) {
                // NOTE: Annuities needs to be truncated first because of
                // foreign keys.
                $this->annuityDb->truncate();
                $this->methodDb->truncate();
            }

            // In version 6.3.0 we add new order statuses.
            if (version_compare($context->getVersion(), '6.3.0', '<')) {
                $this->addOrderStatuses();
            }

            // In version 6.3.0 we add new order statuses.
            if (version_compare($context->getVersion(), '7.6.0', '<')) {
                $this->addOrderStatuses2();
            }

            if (version_compare($context->getVersion(), '7.6.2', '<')) {
                $this->addOrderStatuses3();
            }

            if (version_compare($context->getVersion(), '8.3.3', '<')) {
                $this->addOrderStatuses4();
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        $setup->endSetup();
    }

    /**
     * Duplicate credentials for test / production settings.
     *
     * @return $this
     */
    private function separateCredentials()
    {
        /** @var string $username */
        $username = $this->scopeConfig->getValue(
            'resursbank_checkout/api/username'
        );

        /** @var string $password */
        $password = $this->scopeConfig->getValue(
            'resursbank_checkout/api/password'
        );

        // Copy previously used API username value to new username settings.
        if (!empty($username)) {
            if (!$this->scopeConfig->getValue(self::XML_PATH_TEST_UN)) {
                $this->writerInterface->save(self::XML_PATH_TEST_UN, $username);
            }

            if (!$this->scopeConfig->getValue(self::XML_PATH_PROD_UN)) {
                $this->writerInterface->save(self::XML_PATH_PROD_UN, $username);
            }
        }

        // Copy previously used API password value to new password settings.
        if (!empty($password)) {
            if (!$this->scopeConfig->getValue(self::XML_PATH_TEST_PW)) {
                $this->writerInterface->save(self::XML_PATH_TEST_PW, $password);
            }

            if (!$this->scopeConfig->getValue(self::XML_PATH_PROD_PW)) {
                $this->writerInterface->save(self::XML_PATH_PROD_PW, $password);
            }
        }

        return $this;
    }

    /**
     * Fill up the newly created account table with information from the config.
     *
     * @return void
     * @throws Exception
     */
    private function fillAccountTable()
    {
        foreach ($this->storeManager->getStores() as $store) {
            /** @var Credentials $credentials */
            $credentials = $this->config->getCredentials($store->getCode());

            $this->accountHelper->createMissingAccount($credentials);
        }
    }

    /**
     * Add new custom order statuses based on actions applied on a Resurs Bank
     * payment instance.
     *
     * @throws AlreadyExistsException
     */
    private function addOrderStatuses()
    {
        $this->addOrderStatus(
            PaymentHistory::ORDER_STATUS_CREDIT_DENIED,
            'Resurs Bank - Credit Denied'
        )->addOrderStatus(
            PaymentHistory::ORDER_STATUS_PURCHASE_DECLINED,
            'Resurs Bank - Purchase Declined'
        )->addOrderStatus(
            PaymentHistory::ORDER_STATUS_ABORTED_BY_CUSTOMER,
            'Resurs Bank - Aborted by Customer'
        );
    }

    /**
     * Add new custom order statuses based on actions applied on a Resurs Bank
     * payment instance.
     *
     * @throws AlreadyExistsException
     */
    private function addOrderStatuses2()
    {
        $this->addOrderStatus(
            PaymentHistory::ORDER_STATUS_PAYMENT_REVIEW,
            'Resurs Bank - Payment Review'
        )->addOrderStatus(
            PaymentHistory::ORDER_STATUS_FINALIZED,
            'Resurs Bank - Payment Finalized'
        )->addOrderStatus(
            PaymentHistory::ORDER_STATUS_BOOKED,
            'Resurs Bank - Payment Created'
        )->addOrderStatus(
            PaymentHistory::ORDER_STATUS_PURCHASE_FAILED,
            'Resurs Bank - Purchase Failed'
        )->addOrderStatus(
            PaymentHistory::ORDER_STATUS_CANCELLED,
            'Resurs Bank - Purchase Cancelled'
        );
    }

    /**
     * Add new custom order statuses based on actions applied on a Resurs Bank
     * payment instance.
     *
     * @throws AlreadyExistsException
     */
    private function addOrderStatuses3()
    {
        $this->addOrderStatus(
            PaymentHistory::ORDER_STATUS_CONFIRMED,
            'Resurs Bank - Confirmed'
        );
    }

    /**
     * The status key of order status canceled has been changed.
     *
     * @return void
     */
    private function addOrderStatuses4()
    {
        $this->addOrderStatus(
            PaymentHistory::ORDER_STATUS_CANCELLED,
            'Resurs Bank - Purchase Cancelled'
        );
    }

    /**
     * Add a new order status.
     *
     * @param string $status
     * @param string $label
     * @return $this
     * @throws AlreadyExistsException
     */
    private function addOrderStatus($status, $label)
    {
        $this->statusResourceModel->save(
            $this->statusFactory->create()
                ->setData('status', $status)
                ->setData('label', $label)
        );

        return $this;
    }
}
