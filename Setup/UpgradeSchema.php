<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Resursbank\Checkout\Helper\Api;
use Resursbank\Checkout\Helper\Logic\Account\Method;
use Resursbank\Checkout\Helper\Order;
use Resursbank\Checkout\Model\Total\Quote as Total;
use Zend_Db_Exception;

/**
 * Schema modification instructions during module upgrade.
 *
 * @package Resursbank\Checkout\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @inheritDoc
     * @throws Zend_Db_Exception
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        // In version 3.8.0 we add a new way to track payment methods.
        if (version_compare($context->getVersion(), '3.8.0', '<')) {
            // Create table containing API credentials.
            $this->createAccountTable($setup);

            // Create table containing payment methods.
            $this->createMethodTable($setup);
        }

        // In version 4.1.0 we add a table for method annuity.
        if (version_compare($context->getVersion(), '4.1.0', '<')) {
            $this->createAnnuityTable($setup);
        }

        // In version 4.3.3 we add the callback indication flag to order table.
        if (version_compare($context->getVersion(), '4.3.3', '<')) {
            $this->addCallbackReceivedFlag($setup);
        }

        // Drop resursbank_token column.
        if (version_compare($context->getVersion(), '6.0.2', '<')) {
            $this->dropTokenColumn($setup);
        }

        // In version 6.3.0 we add tables to record actions applied on payment
        // instances at Resurs Bank.
        if (version_compare($context->getVersion(), '6.3.0', '<')) {
            $this->createPaymentHistoryTable($setup);
        }

        // In version 7.1.0 we add custom payment method fees.
        if (version_compare($context->getVersion(), '7.1.0', '<')) {
            $this->addPaymentFeeColumns($setup);
        }

        if (version_compare($context->getVersion(), '7.5.5', '<')) {
            $this->addEnvironmentToOrder($setup);
        }

        if (version_compare($context->getVersion(), '7.6.1', '<')) {
            $this->addSpecificCountryToMethodTable($setup);
        }

        if (version_compare($context->getVersion(), '7.6.3', '<')) {
            $this->addStateAndStatusChangesToPaymentHistory($setup);
        }

        if (version_compare($context->getVersion(), '7.6.4', '<')) {
            $this->alterFactorColumnPrecision($setup);
        }

        if (version_compare($context->getVersion(), '8.3.2', '<')) {
            $this->dropCallbackReceivedFlag($setup);
        }

        if (version_compare($context->getVersion(), '9.3.0', '<')) {
            $this->addEmailRequestedFlag($setup);
        }
    }

    /**
     * Create table 'resursbank_checkout_payment_history'
     *
     * @param SchemaSetupInterface $setup
     * @throws Zend_Db_Exception
     */
    private function createPaymentHistoryTable(SchemaSetupInterface $setup)
    {
        $setup->startSetup();

        $table = $setup->getConnection()->newTable(
            $setup->getTable('resursbank_checkout_payment_history')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            10,
            [
                'unsigned' => true,
                'identity' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Prim Key'
        )->addColumn(
            'payment_id',
            Table::TYPE_INTEGER,
            10,
            [
                'unsigned' => true,
                'nullable' => false
            ],
            'Order payment reference.'
        )->addColumn(
            'event',
            Table::TYPE_SMALLINT,
            3,
            ['nullable' => false],
            'Event that was triggered.'
        )->addColumn(
            'user',
            Table::TYPE_SMALLINT,
            1,
            ['nullable' => false],
            'User that triggered the event.'
        )->addColumn(
            'created_at',
            Table::TYPE_TIMESTAMP,
            null,
            [
                'nullable' => false,
                'default' => Table::TIMESTAMP_INIT
            ],
            'Creation time.'
        )->addColumn(
            'extra',
            Table::TYPE_TEXT,
            0,
            ['nullable' => true],
            'Additional information about what happened.'
        )->addIndex(
            $setup->getIdxName(
                'resursbank_checkout_payment_history',
                ['payment_id']
            ),
            ['payment_id']
        )->addForeignKey(
            $setup->getFkName(
                'resursbank_checkout_payment_history',
                'payment_id',
                'sales_order_payment',
                'entity_id'
            ),
            'payment_id',
            $setup->getTable('sales_order_payment'),
            'entity_id',
            Table::ACTION_CASCADE
        )->setComment(
            'Resurs Bank payment events.'
        );

        $setup->getConnection()->createTable($table);
        $setup->endSetup();
    }

    /**
     * Create table 'resursbank_checkout_account'
     *
     * @param SchemaSetupInterface $setup
     * @throws Zend_Db_Exception
     */
    private function createAccountTable(SchemaSetupInterface $setup)
    {
        $setup->startSetup();

        $table = $setup->getConnection()->newTable(
            $setup->getTable('resursbank_checkout_account')
        )->addColumn(
            'account_id',
            Table::TYPE_SMALLINT,
            null,
            [
                'unsigned' => true,
                'identity' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Prim Key'
        )->addColumn(
            'username',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'API Account Username'
        )->addColumn(
            'environment',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'API Account Environment'
        )->addColumn(
            'salt',
            Table::TYPE_TEXT,
            255,
            ['nullable' => true],
            'Callback SALT'
        )->addColumn(
            'created_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
            'Created At'
        )->addColumn(
            'updated_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
            'Updated At'
        )->addIndex(
            $setup->getIdxName(
                'resursbank_checkout_account',
                ['username', 'environment'],
                AdapterInterface::INDEX_TYPE_UNIQUE
            ),
            ['username', 'environment'],
            ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
        )->setComment(
            'Resurs Bank API Account Table'
        );

        $setup->getConnection()->createTable($table);
        $setup->endSetup();
    }

    /**
     * Create table 'resursbank_checkout_account_method'
     *
     * @param SchemaSetupInterface $setup
     * @throws Zend_Db_Exception
     */
    private function createMethodTable(SchemaSetupInterface $setup)
    {
        $setup->startSetup();

        $table = $setup->getConnection()->newTable(
            $setup->getTable('resursbank_checkout_account_method')
        )->addColumn(
            'method_id',
            Table::TYPE_SMALLINT,
            null,
            [
                'unsigned' => true,
                'identity' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Prim Key'
        )->addColumn(
            'account_id',
            Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => true],
            'Account ID'
        )->addColumn(
            'identifier',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Raw Method ID'
        )->addColumn(
            'code',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Method Code'
        )->addColumn(
            'active',
            Table::TYPE_BOOLEAN,
            null,
            ['nullable' => false, 'default' => 0],
            'Active'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Method Title'
        )->addColumn(
            'min_order_total',
            Table::TYPE_DECIMAL,
            '15,5',
            ['unsigned' => true, 'nullable' => false, 'default' => '0.00000'],
            'Minimum Order Total'
        )->addColumn(
            'max_order_total',
            Table::TYPE_DECIMAL,
            '15,5',
            ['unsigned' => true, 'nullable' => false, 'default' => '0.00000'],
            'Maximum Order Total'
        )->addColumn(
            'order_status',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false, 'default' => Method::DEFAULT_ORDER_STATUS],
            'Order Status'
        )->addColumn(
            'raw',
            Table::TYPE_TEXT,
            0,
            ['nullable' => true],
            'Raw API Data'
        )->addColumn(
            'created_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
            'Created At'
        )->addColumn(
            'updated_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
            'Updated At'
        )->addIndex(
            $setup->getIdxName(
                'resursbank_checkout_account_method',
                ['code'],
                AdapterInterface::INDEX_TYPE_UNIQUE
            ),
            ['code'],
            ['type' => AdapterInterface::INDEX_TYPE_UNIQUE]
        )->addIndex(
            $setup->getIdxName(
                'resursbank_checkout_account_method',
                ['account_id']
            ),
            ['account_id']
        )->addForeignKey(
            $setup->getFkName(
                'resursbank_checkout_account_method',
                'account_id',
                'resursbank_checkout_account',
                'account_id'
            ),
            'account_id',
            $setup->getTable('resursbank_checkout_account'),
            'account_id',
            Table::ACTION_CASCADE
        )->setComment(
            'Resurs Bank Method Table'
        );

        $setup->getConnection()->createTable($table);
        $setup->endSetup();
    }

    /**
     * Create table 'resursbank_checkout_account_duration'
     *
     * @param SchemaSetupInterface $setup
     * @throws Zend_Db_Exception
     */
    private function createAnnuityTable(SchemaSetupInterface $setup)
    {
        $setup->startSetup();

        $table = $setup->getConnection()->newTable(
            $setup->getTable('resursbank_checkout_account_method_annuity')
        )->addColumn(
            'annuity_id',
            Table::TYPE_SMALLINT,
            null,
            [
                'unsigned' => true,
                'identity' => true,
                'nullable' => false,
                'primary' => true
            ],
            'Prim Key'
        )->addColumn(
            'method_id',
            Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => true],
            'Method ID'
        )->addColumn(
            'title',
            Table::TYPE_TEXT,
            255,
            ['nullable' => false],
            'Annuity Title'
        )->addColumn(
            'raw',
            Table::TYPE_TEXT,
            0,
            ['nullable' => true],
            'Raw API Data'
        )->addColumn(
            'duration',
            Table::TYPE_SMALLINT,
            null,
            ['unsigned' => true, 'nullable' => true],
            'Annuity Duration'
        )->addColumn(
            'factor',
            Table::TYPE_FLOAT,
            null,
            ['unsigned' => true, 'nullable' => true],
            'Annuity Factor'
        )->addColumn(
            'created_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
            'Created At'
        )->addColumn(
            'updated_at',
            Table::TYPE_TIMESTAMP,
            null,
            ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
            'Updated At'
        )->addIndex(
            $setup->getIdxName(
                'resursbank_checkout_account_method_annuity',
                ['method_id']
            ),
            ['method_id']
        )->addForeignKey(
            $setup->getFkName(
                'resursbank_checkout_account_method_annuity',
                'method_id',
                'resursbank_checkout_account_method',
                'method_id'
            ),
            'method_id',
            $setup->getTable('resursbank_checkout_account_method'),
            'method_id',
            Table::ACTION_CASCADE
        )->setComment(
            'Resurs Bank Annuity Table'
        );

        $setup->getConnection()->createTable($table);
        $setup->endSetup();
    }

    /**
     * Add "resursbank_callback_received" flag to order table.
     *
     * @param SchemaSetupInterface $setup
     */
    private function addCallbackReceivedFlag(SchemaSetupInterface $setup)
    {
        $setup->startSetup();

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            Order::CALLBACK_RECEIVED_COLUMN,
            [
                'type' => Table::TYPE_BOOLEAN,
                'length' => null,
                'nullable' => false,
                'default' => 0,
                'comment' => 'Whether or not callbacks have been received.'
            ]
        );

        $setup->endSetup();
    }

    /**
     * Drop resursbank_column from quote table. We no longer require this column
     * since we handle the payment reference within the checkout session now.
     *
     * @param SchemaSetupInterface $setup
     */
    private function dropTokenColumn(SchemaSetupInterface $setup)
    {
        $setup->startSetup();

        $setup->getConnection()->dropColumn(
            $setup->getTable('quote'),
            Api::QUOTE_TOKEN_FIELD
        );

        $setup->endSetup();
    }

    /**
     * Add payment method fee columns to all required tables.
     *
     * @param SchemaSetupInterface $setup
     */
    private function addPaymentFeeColumns(SchemaSetupInterface $setup)
    {
        $setup->startSetup();

        $this->addPaymentFeeToQuoteAddress($setup)
            ->addPaymentFeeToOrder($setup)
            ->addPaymentFeeToInvoice($setup)
            ->addPaymentFeeToCreditmemo($setup);

        $setup->endSetup();
    }

    /**
     * Add payment fee columns to quote_address table. This is required for our
     * tax calculations to function properly.
     *
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    private function addPaymentFeeToQuoteAddress(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('quote_address'),
            Total::CODE . '_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => true,
                'comment' => 'Currency converted Resurs Bank payment method fee.'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote_address'),
            'base_' . Total::CODE . '_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => true,
                'comment' => 'Resurs Bank payment method fee.'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote_address'),
            Total::CODE . '_tax_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => true,
                'comment' => 'Currency converted Resurs Bank payment method fee tax.'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote_address'),
            'base_' . Total::CODE . '_tax_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => true,
                'comment' => 'Resurs Bank payment method fee tax.'
            ]
        );

        return $this;
    }

    /**
     * Add payment method fee columns to order table.
     *
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    private function addPaymentFeeToOrder(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            Total::CODE . '_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => true,
                'comment' => 'Currency converted Resurs Bank payment method fee.'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'base_' . Total::CODE . '_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => true,
                'comment' => 'Resurs Bank payment method fee.'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            Total::CODE . '_tax_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => true,
                'comment' => 'Currency converted Resurs Bank payment method fee tax.'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'base_' . Total::CODE . '_tax_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => true,
                'comment' => 'Resurs Bank payment method fee tax.'
            ]
        );

        return $this;
    }

    /**
     * Add payment method fee columns to invoice table.
     *
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    private function addPaymentFeeToInvoice(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_invoice'),
            Total::CODE . '_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => true,
                'comment' => 'Currency converted Resurs Bank payment method fee.'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_invoice'),
            'base_' . Total::CODE . '_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => true,
                'comment' => 'Resurs Bank payment method fee.'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_invoice'),
            Total::CODE . '_tax_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => true,
                'comment' => 'Currency converted Resurs Bank payment method fee tax.'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_invoice'),
            'base_' . Total::CODE . '_tax_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => true,
                'comment' => 'Resurs Bank payment method fee tax.'
            ]
        );

        return $this;
    }

    /**
     * Add payment method fee columns to creditmemo table.
     *
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    private function addPaymentFeeToCreditmemo(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_creditmemo'),
            Total::CODE . '_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => true,
                'comment' => 'Currency converted Resurs Bank payment method fee.'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_creditmemo'),
            'base_' . Total::CODE . '_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => true,
                'comment' => 'Resurs Bank payment method fee.'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_creditmemo'),
            Total::CODE . '_tax_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => true,
                'comment' => 'Currency converted Resurs Bank payment method fee tax.'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_creditmemo'),
            'base_' . Total::CODE . '_tax_amount',
            [
                'type' => Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => true,
                'comment' => 'Resurs Bank payment method fee tax.'
            ]
        );

        return $this;
    }

    /**
     * Add environment column to order table.
     *
     * @param SchemaSetupInterface $setup
     * @return $this
     */
    private function addEnvironmentToOrder(SchemaSetupInterface $setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'resursbank_environment',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'nullable' => false,
                'comment' => 'The selected environment in Resurs Bank config.'
            ]
        );

        return $this;
    }

    /**
     * Create table 'resursbank_checkout_account_method'
     *
     * @param SchemaSetupInterface $setup
     */
    private function addSpecificCountryToMethodTable(
        SchemaSetupInterface $setup
    ) {
        $setup->getConnection()->addColumn(
            $setup->getTable('resursbank_checkout_account_method'),
            'specificcountry',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'nullable' => false,
                'comment' => 'Which country the method can be used with'
            ]
        );
    }

    /**
     * Add columns to our payment history table. Columns that will hold
     * state and status changes.
     *
     * @param   SchemaSetupInterface $setup
     * @return  void
     */
    private function addStateAndStatusChangesToPaymentHistory(
        SchemaSetupInterface $setup
    ) {
        $setup->getConnection()->addColumn(
            $setup->getTable('resursbank_checkout_payment_history'),
            'state_from',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'nullable' => true,
                'comment' => 'What state this history entry went from.'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('resursbank_checkout_payment_history'),
            'state_to',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'nullable' => true,
                'comment' => 'What state this history entry went to.'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('resursbank_checkout_payment_history'),
            'status_from',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'nullable' => true,
                'comment' => 'What status this history entry went from.'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('resursbank_checkout_payment_history'),
            'status_to',
            [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'nullable' => true,
                'comment' => 'What status this history entry went to.'
            ]
        );
    }

    /**
     * Change the precision on the factor float to be able to
     * hold the full decimal value of the factor.
     *
     * @param   SchemaSetupInterface $setup
     * @return  void
     */
    private function alterFactorColumnPrecision(
        SchemaSetupInterface $setup
    ) {
        $setup->getConnection()->changeColumn(
            $setup->getTable('resursbank_checkout_account_method_annuity'),
            'factor',
            'factor',
            [
                'type' => Table::TYPE_DECIMAL,
                'scale' => 10,
                'unsigned' => true,
                'nullable' => true,
                'comment' => 'Annuity Factor'
            ]
        );
    }

    /**
     * Drop "resursbank_callback_received" flag from order table.
     *
     * @param SchemaSetupInterface $setup
     */
    private function dropCallbackReceivedFlag(SchemaSetupInterface $setup)
    {
        $setup->startSetup();

        $setup->getConnection()->dropColumn(
            $setup->getTable('sales_order'),
            Order::CALLBACK_RECEIVED_COLUMN
        );

        $setup->endSetup();
    }

    /**
     * Add "resursbank_email_requested" flag to order table.
     *
     * @param SchemaSetupInterface $setup
     */
    private function addEmailRequestedFlag(SchemaSetupInterface $setup)
    {
        $setup->startSetup();

        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            Order::EMAIL_REQUESTED_COLUMN,
            [
                'type' => Table::TYPE_BOOLEAN,
                'length' => null,
                'nullable' => false,
                'default' => 0,
                'comment' => 'Whether or not we have requested Magento submits the order confirmation email.'
            ]
        );

        $setup->endSetup();
    }
}
