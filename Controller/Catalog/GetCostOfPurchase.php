<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Controller\Catalog;

use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Checkout\Exception\InvalidDataException;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Db\Account\Method as MethodDb;
use Resursbank\Checkout\Helper\Ecom;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\Account\Method as MethodModel;

/**
 * Fetch part payment information from Resurs Bank.
 *
 * @package Resursbank\Checkout\Controller\Catalog
 */
class GetCostOfPurchase extends Action
{
    /**
     * @var Log
     */
    private $log;

    /**
     * @var ResultFactory
     */
    protected $resultFactory;

    /**
     * @var MethodDb
     */
    private $methodDb;

    /**
     * @var Ecom
     */
    private $ecom;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param Context $context
     * @param Log $log
     * @param ResultFactory $resultFactory
     * @param MethodDb $methodDb
     * @param Ecom $ecom
     * @param ApiConfig $apiConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        Log $log,
        ResultFactory $resultFactory,
        MethodDb $methodDb,
        Ecom $ecom,
        ApiConfig $apiConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->log = $log;
        $this->resultFactory = $resultFactory;
        $this->methodDb = $methodDb;
        $this->ecom = $ecom;
        $this->apiConfig = $apiConfig;
        $this->storeManager = $storeManager;

        parent::__construct($context);
    }

    /**
     * Fetches a table of part payment prices for a given price and payment
     * method id, and returns it as an HTML string.
     *
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var ResultInterface $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        try {
            /** @var MethodModel $method */
            $method = $this->getMethod();

            if ($method->isActive()) {
                $result->setData([
                    'html' => $this->getHtml($method)
                ]);
            }
        } catch (Exception $e) {
            $result->setData([
                'message' => [
                    'error' => $this->getExceptionMessage($e)
                ]
            ]);

            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Retrieve error messages based on Exception type.
     *
     * @param Exception $e
     * @return string
     */
    private function getExceptionMessage(Exception $e): string
    {
        return $e instanceof InvalidDataException ?
            $e->getMessage() :
            (string) __(
                'Something went wrong while fetching the information. Please ' .
                'reload the page and try again.'
            );
    }

    /**
     * @return MethodModel
     */
    private function getMethod(): MethodModel
    {
        return $this->methodDb->getModel()->load(
            (int) $this->getRequest()->getParam('methodId')
        );
    }

    /**
     * @return float
     */
    private function getPrice(): float
    {
        return (float) $this->getRequest()->getParam('price');
    }

    /**
     * @param MethodModel $method
     * @return string
     * @throws Exception
     */
    private function getHtml(MethodModel $method): string
    {
        $country = $this->apiConfig->getCountry($this->storeManager->getStore()->getCode());

        $html = $this->ecom->getConnection()
            ->getCostOfPriceInformation(
                !in_array($country, ['DK'])
                    ? $method->getIdentifier() : ($this->ecom->getConnection()->getPaymentMethods()),
                ceil($this->getPrice()),
                false,
                true
            );

        if ($html === '') {
            $html = (string) __('No part payment information exists for this product.');
        }

        return $html;
    }
}
