<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Controller\Adminhtml\Partpayment;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Resursbank\Checkout\Helper\Config\Checkout\Api as Config;
use Resursbank\Checkout\Helper\Db\Account\Method\Annuity as AnnuityDb;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Logic\Account\Method;
use Resursbank\Checkout\Helper\Logic\Account\Method\Annuity as PartPaymentHelper;
use Resursbank\Checkout\Model\Account\Method as MethodModel;
use Resursbank\Checkout\Model\Config\Source\PartPayment\Annuity;

/**
 * Retrieve part payment information.
 *
 * @package Resursbank\Checkout\Controller\Adminhtml
 */
class Index extends Action
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var Method
     */
    private $method;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var PartPaymentHelper
     */
    private $partPaymentHelper;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var AnnuityDb
     */
    private $db;

    /**
     * @var Annuity
     */
    private $annuity;

    /**
     * @param Context $context
     * @param Method $method
     * @param Log $log
     * @param PartPaymentHelper $partPaymentHelper
     * @param AnnuityDb $db
     * @param Config $config
     * @param Annuity $annuity
     */
    public function __construct(
        Context $context,
        Method $method,
        Log $log,
        PartPaymentHelper $partPaymentHelper,
        AnnuityDb $db,
        Config $config,
        Annuity $annuity
    ) {
        $this->context = $context;
        $this->method = $method;
        $this->partPaymentHelper = $partPaymentHelper;
        $this->log = $log;
        $this->db = $db;
        $this->config = $config;
        $this->annuity = $annuity;

        parent::__construct($context);
    }

    /**
     * Takes a payment method id and returns data about that payment method if
     * it's eligible for part payment. This data contains, for instance, a
     * method's factors, and its min and max limits.
     *
     * NOTE: This method is meant to be used with AJAX requests. It expects a
     * "paymentMethodId" parameter.
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $result = $this->context->getResultFactory()->create(
            ResultFactory::TYPE_JSON
        );

        try {
            // Get request payment method id.
            $paymentMethodId = (int)$this->getRequest()
                ->getParam('paymentMethodId');

            /** @var MethodModel $method */
            $method = ObjectManager::getInstance()->create(MethodModel::class);
            $method->load($paymentMethodId);

            if ($method->getId()) {
                $result->setData([
                    'methodExists' => true,
                    'factors' => $this->annuity->toOptionArray(
                        $paymentMethodId
                    ),
                    'minLimit' => $method->getMinOrderTotal(),
                    'maxLimit' => $method->getMaxOrderTotal()
                ]);
            } else {
                $result->setData([
                    'methodExists' => false,
                    'factors' => $this->annuity->toOptionArray($paymentMethodId)
                ]);
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        // Respond.
        return $result;
    }
}
