<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Controller\Adminhtml\Methods;

use Exception;
use Resursbank\Checkout\Helper\Config;
use Resursbank\Checkout\Helper\Logic\Account\Method;
use Resursbank\Checkout\Helper\Log;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

/**
 * Controller to sync payment methods from old orders and the API to the db.
 *
 * @package Resursbank\Checkout\Controller\Adminhtml\Method
 */
class Sync extends Action
{
    /**
     * @var Method
     */
    private $method;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param Context $context
     * @param Method $method
     * @param Log $log
     * @param Config $config
     */
    public function __construct(
        Context $context,
        Method $method,
        Log $log,
        Config $config
    ) {
        $this->method = $method;
        $this->log = $log;
        $this->config = $config;

        parent::__construct($context);
    }

    /**
     * Synchronize payment methods.
     */
    public function execute()
    {
        try {
            // Synchronize payment methods.
            $this->method->syncAll();

            // Add success message.
            $this->getMessageManager()->addSuccessMessage(
                'Successfully synchronized payment methods.'
            );
        } catch (Exception $e) {
            // Add log entry.
            $this->log->error($e);

            // Add error message.
            $this->getMessageManager()->addErrorMessage(
                'Failed to synchronize payment methods.'
            );
        }

        // Redirect back to the config section.
        $this->_redirect($this->config->buildUrl(
            'adminhtml/system_config/edit/section/resursbank_checkout'
        ));
    }
}
