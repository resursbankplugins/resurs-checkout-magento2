<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Controller\Adminhtml\Callback;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Resursbank\Checkout\Helper\Config;
use Resursbank\Checkout\Helper\Ecom;
use Resursbank\Checkout\Helper\Log;

/**
 * Trigger test callback.
 *
 * @package Resursbank\Checkout\Controller\Adminhtml\Callback
 */
class Test extends Action
{
    /**
     * @var Ecom
     */
    private $ecom;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param Context $context
     * @param Ecom $ecom
     * @param Log $log
     * @param Config $config
     */
    public function __construct(
        Context $context,
        Ecom $ecom,
        Log $log,
        Config $config
    ) {
        $this->ecom = $ecom;
        $this->log = $log;
        $this->config = $config;

        parent::__construct($context);
    }

    /**
     * Trigger test callback.
     */
    public function execute()
    {
        try {
            $this->ecom->getConnection()->triggerCallback();
        } catch (Exception $e) {
            // Log error.
            $this->log->error($e);

            // Add error message.
            $this->getMessageManager()->addErrorMessage(
                __('Test callback could not be triggered.')
            );
        }

        // Redirect back to the config section.
        $this->_redirect($this->config->buildUrl(
            'adminhtml/system_config/edit/section/resursbank_checkout'
        ));
    }
}
