<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Controller\Adminhtml\Callback;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Checkout\Helper\Callback;
use Resursbank\Checkout\Helper\Config;
use Resursbank\Checkout\Helper\Log;

/**
 * Register callback URLs.
 *
 * @package Resursbank\Checkout\Controller\Adminhtml\Callback
 */
class Registration extends Action
{

    /**
     * @var Log
     */
    private $log;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Callback
     */
    private $callbackHelper;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param Context $context
     * @param Log $log
     * @param Config $config
     * @param Callback $callbackHelper
     */
    public function __construct(
        Context $context,
        Log $log,
        Config $config,
        Callback $callbackHelper,
        RequestInterface $request,
        StoreManagerInterface $storeManager
    ) {
        $this->log = $log;
        $this->config = $config;
        $this->callbackHelper = $callbackHelper;
        $this->request = $request;
        $this->storeManager = $storeManager;

        parent::__construct($context);
    }

    /**
     * Register callback URLs.
     */
    public function execute()
    {
        try {
            // Register callback URLs.
            $this->callbackHelper->register(
                $this->getStore()
            );

            // Add success message.
            $this->getMessageManager()->addSuccessMessage(
                'Callback URLs were successfully registered.'
            );
        } catch (Exception $e) {
            // Log error.
            $this->log->error($e);

            // Add error message.
            $this->getMessageManager()->addErrorMessage(
                __('Callback URLs failed to register.')
            );
        }

        // Redirect back to the config section.
        $this->_redirect($this->config->buildUrl(
            'adminhtml/system_config/edit/section/resursbank_checkout'
        ));
    }

    /**
     * Get the current store.
     *
     * @return \Magento\Store\Model\Store
     */
    private function getStore()
    {
        try {
            $storeId = (int) $this->request->getParam('store');

            if ($storeId > 0) {
                $store = $this->storeManager->getStore($storeId);
            } else {
                $store = $this->storeManager->getStore();
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $store;
    }
}
