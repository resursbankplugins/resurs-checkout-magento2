<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Controller\Simplified;

use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultFactory;
use Resursbank\Checkout\Exception\InvalidDataException;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\Api\Adapter\Simplified;

/**
 * Store requested card amount for Resurs Bank New Card payment method. The
 * value is stored in the client session and later submitted to Resurs Bank as
 * part of the API call that creates the payment.
 *
 * NOTE: The property handled by this controller and subsequent processes is
 * referred to as "card amount" by the Resurs Bank API. We define it as
 * "credit limit" in client messages etc. since it's more descriptive.
 *
 * @package Resursbank\Checkout\Controller\Simplified
 */
class SetCardAmount extends Action
{
    /**
     * @var Simplified
     */
    private $simplified;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var ResultFactory
     */
    protected $resultFactory;

    /**
     * @param Context $context
     * @param Simplified $simplified
     * @param Log $log
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        Context $context,
        Simplified $simplified,
        Log $log,
        ResultFactory $resultFactory
    ) {
        $this->simplified = $simplified;
        $this->log = $log;
        $this->resultFactory = $resultFactory;

        parent::__construct($context);
    }

    /**
     * Store requested card amount in session.
     *
     * @return Json
     */
    public function execute()
    {
        /** @var Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $response = [];

        try {
            $this->simplified->setCardAmount(
                (float) $this->getRequest()->getParam('card_amount')
            );
        } catch (Exception $e) {
            $this->log->error($e);

            if ($e instanceof InvalidDataException) {
                $response['message']['error'] = __($e->getMessage());
            } else {
                $response['message']['error'] = __(
                    'Something went wrong while storing your credit limit.'
                );
            }
        }

        $result->setData($response);

        // Respond.
        return $result;
    }
}
