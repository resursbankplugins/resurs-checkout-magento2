<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Controller\Simplified;

use Exception;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultFactory;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Checkout\Exception\InvalidDataException;
use Resursbank\Checkout\Helper\Api;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Db\Account\Method;
use Resursbank\Checkout\Helper\Ecom;
use Resursbank\Checkout\Helper\Log;

/**
 * Retrieve "Cost of Purchase" HTML from Resurs Bank for intended payment (based
 * on selected method and requested amount). This HTML will contain information
 * about part payment fees etc.
 *
 * @package Resursbank\Checkout\Controller\Simplified
 */
class GetCostOfPurchase extends Action
{
    /**
     * @var Ecom
     */
    private $ecom;

    /**
     * @var Method
     */
    private $method;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var ResultFactory
     */
    protected $resultFactory;

    /**
     * @var Api
     */
    private $api;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param Context $context
     * @param Ecom $ecom
     * @param Method $method
     * @param Session $session
     * @param Log $log
     * @param ResultFactory $resultFactory
     * @param Api $api
     * @param ApiConfig $apiConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        Ecom $ecom,
        Method $method,
        Session $session,
        Log $log,
        ResultFactory $resultFactory,
        Api $api,
        ApiConfig $apiConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->ecom = $ecom;
        $this->method = $method;
        $this->session = $session;
        $this->log = $log;
        $this->resultFactory = $resultFactory;
        $this->api = $api;
        $this->apiConfig = $apiConfig;
        $this->storeManager = $storeManager;

        parent::__construct($context);
    }

    /**
     * @return Json
     */
    public function execute(): Json
    {
        /** @var Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $response = [];

        try {
            $method = $this->method->loadByCode(
                $this->getRequest()->getParam('code')
            );

            if ($method->isActive()) {
                $country = $this->apiConfig->getCountry($this->storeManager->getStore()->getCode());

                $html = $this->ecom->getConnection()
                    ->getCostOfPriceInformation(
                        !in_array($country, ['DK'])
                            ? $method->getRawValue('id') : ($this->ecom->getConnection()->getPaymentMethods()),
                        $this->api->getQuote()->getGrandTotal(),
                        false,
                        true
                    );

                if ($html === '') {
                    $html = __(
                        'No additional information exists for this ' .
                        'payment method.'
                    );
                }

                $response['html'] = $html;
            }
        } catch (Exception $e) {
            $response['message'] = [
                'error' => $this->getExceptionMessage($e)
            ];

            $this->log->error($e);
        }

        $result->setData($response);

        // Respond.
        return $result;
    }

    /**
     * Retrieve error messages based on Exception type.
     *
     * @param Exception $e
     * @return string
     */
    private function getExceptionMessage(Exception $e): string
    {
        return $e instanceof InvalidDataException ?
            $e->getMessage() :
            (string) __(
                'Something went wrong while fetching the information. Please ' .
                'reload the page and try again.'
            );
    }
}
