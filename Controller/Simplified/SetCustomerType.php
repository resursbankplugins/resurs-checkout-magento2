<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Controller\Simplified;

use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultFactory;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\Api;
use Resursbank\Checkout\Model\Api\Adapter\Simplified;

/**
 * Store selected customer type (private citizen / organization) in session for
 * later use.
 *
 * @package Resursbank\Checkout\Controller\Simplified
 */
class SetCustomerType extends Action
{
    /**
     * @var Simplified
     */
    private $simplified;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var ResultFactory
     */
    protected $resultFactory;

    /**
     * @param Context $context
     * @param Simplified $simplified
     * @param Log $log
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        Context $context,
        Simplified $simplified,
        Log $log,
        ResultFactory $resultFactory
    ) {
        $this->simplified = $simplified;
        $this->log = $log;
        $this->resultFactory = $resultFactory;

        parent::__construct($context);
    }

    /**
     * @return Json
     */
    public function execute()
    {
        /** @var Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $response = [];

        try {
            $customerType = (string) $this->getRequest()->getParam('type');

            if ($customerType === Api::CUSTOMER_TYPE_COMPANY) {
                $this->simplified->setIsCompany(true);
                $this->simplified->unsetSsn();
            } elseif ($customerType === Api::CUSTOMER_TYPE_PRIVATE) {
                $this->simplified->unsetIsCompany();
            }
        } catch (Exception $e) {
            $response['message']['error'] =__(
                'Something went wrong while updating the customer type ' .
                'for the SSN field. Please fill out the form manually to ' .
                'continue.'
            );

            $this->log->error($e);
        }

        $result->setData($response);

        return $result;
    }
}
