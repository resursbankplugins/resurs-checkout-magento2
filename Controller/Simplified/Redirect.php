<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Controller\Simplified;

use \Exception;
use \Resursbank\Checkout\Helper\Log;
use \Resursbank\Checkout\Model\Api\Adapter\Simplified;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\App\Action\Action;
use \Magento\Framework\Controller\Result\RedirectFactory;
use \Magento\Checkout\Model\Session;
use \Magento\Framework\Controller\Result\Redirect as RedirectResult;

/**
 * Redirect to signing page at Resurs Bank.
 *
 * @package Resursbank\Checkout\Controller\Simplified
 */
class Redirect extends Action
{
    /**
     * @var RedirectFactory
     */
    private $redirectFactory;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var Log
     */
    private $log;

    /**
     * @param Context $context
     * @param RedirectFactory $redirectFactory
     * @param Log $log
     * @param Session $session
     */
    public function __construct(
        Context $context,
        RedirectFactory $redirectFactory,
        Session $session,
        Log $log
    ) {
        $this->redirectFactory = $redirectFactory;
        $this->session = $session;
        $this->log = $log;

        parent::__construct($context);
    }

    /**
     * Redirect to signing URL. If there is none, redirect straight to success
     * page.
     *
     * @return RedirectResult
     * @throws Exception
     */
    public function execute()
    {
        /** @var RedirectResult $redirect */
        $redirect = $this->redirectFactory->create();

        try {
            // Get signing URL stored in session during payment creation. See
            // Plugin/Simplified/Order/Create.php
            $url = (string)$this->session->getData(
                Simplified::SESSION_KEY_SIGNING_URL
            );

            if ($url !== '') {
                // Redirect to Resurs Bank signing page.
                $redirect->setUrl($url);
            } else {
                // Redirect to success page.
                $redirect->setPath('checkout/onepage/success');
            }
        } catch (Exception $e) {
            $this->log->error($e);

            throw $e;
        }

        return $redirect;
    }
}
