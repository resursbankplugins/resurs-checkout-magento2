<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Controller\Simplified;

use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultFactory;
use Resursbank\Checkout\Exception\InvalidDataException;
use Resursbank\Checkout\Model\Api\Adapter\Simplified;

/**
 * This controller fetches an address from Resurs Bank through ECom using an
 * identification number ('ssn' for private citizens, 'organisation number' for
 * organisations).
 *
 * @package Resursbank\Checkout\Controller\Simplified
 */
class FetchAddress extends Action
{
    /**
     * @var Simplified
     */
    private $simplified;

    /**
     * @var ResultFactory
     */
    protected $resultFactory;

    /**
     * @param Context $context
     * @param Simplified $simplified
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        Context $context,
        Simplified $simplified,
        ResultFactory $resultFactory
    ) {
        $this->simplified = $simplified;
        $this->resultFactory = $resultFactory;

        parent::__construct($context);
    }

    /**
     * @return Json
     */
    public function execute()
    {
        /** @var Json $result */
        $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $response = [];

        try {
            $country = $this->getRequest()->getParam('country');
            $idNum = $this->getRequest()->getParam('id_num');
            $customerType = $this->getRequest()->getParam('customer_type');

            if ($this->simplified->canFetchAddress($country)) {
                $address = $this->simplified->getAddress($idNum, $customerType);
                $response['address'] = $address->toArray();
                $this->simplified->setSsn($idNum, $country, $customerType);
            }
        } catch (Exception $e) {
            if ($e instanceof InvalidDataException) {
                $response['message']['error'] = __($e->getMessage());
            } else {
                $response['message']['error'] = __(
                    'Something went wrong while fetching your address. ' .
                    'Please fill out the form manually to continue.'
                );
            }
        }

        $result->setData($response);

        return $result;
    }
}
