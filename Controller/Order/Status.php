<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Controller\Order;

use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Resursbank\Checkout\Helper\Api as ApiHelper;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\Api\Payment\Converter\QuoteConverter;

/**
 * Class Status
 *
 * Retrieve ambiguous order information.
 *
 * @package Resursbank\Checkout\Controller\Order
 */
class Status extends Action
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var ApiHelper
     */
    private $apiHelper;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var QuoteConverter
     */
    private $quoteConverter;

    /**
     * Constructor.
     *
     * @param Context $context
     * @param ApiHelper $apiHelper
     * @param Log $log
     * @param QuoteConverter $quoteConverter
     */
    public function __construct(
        Context $context,
        ApiHelper $apiHelper,
        Log $log,
        QuoteConverter $quoteConverter
    ) {
        $this->context = $context;
        $this->apiHelper = $apiHelper;
        $this->log = $log;
        $this->quoteConverter = $quoteConverter;

        parent::__construct($context);
    }

    /**
     * Retrieve order information.
     *
     * @return string JSON
     * @throws Exception
     */
    public function execute()
    {
        $result = '';

        try {
            $result = $this->context->getResultFactory()->create(
                ResultFactory::TYPE_JSON
            );

            if (!$this->apiHelper->validateSessionData()) {
                $error = __(
                    'We apologize, your session expired and the page needs ' .
                    'to be reloaded. Your order has not yet been placed. ' .
                    'Please fill out your information below after reloading ' .
                    'to proceed with placing your order.'
                );

                $result->setData([
                    'validSession' => false,
                    'invalidSessionError' => $error
                ]);
            } else {
                // We specifically want the assembled value submitted to Resurs
                // Bank, and not the quote grand total. If the payment session
                // at Resurs Bank is at "0" we emit an event to the iframe.
                // Which means we need to base this around the value submitted
                // to Resurs Bank, not what we have in our Quote (albeit they
                // should be the same unless something is wrong).
                $result->setData([
                    'grandTotal' => $this->quoteConverter->getCollectedTotal(
                        $this->quoteConverter->convert(
                            $this->apiHelper->getQuote()
                        )
                    ),
                    'validSession' => true
                ]);
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }
}
