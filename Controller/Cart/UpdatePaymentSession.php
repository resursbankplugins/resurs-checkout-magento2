<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Controller\Cart;

use Exception;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Resursbank\Checkout\Model\Api;
use Resursbank\Checkout\Helper\Log\Api as Log;

/**
 * @package Resursbank\Checkout\Controller\Cart
 */
class UpdatePaymentSession extends Action
{
    /**
     * @var Api
     */
    private $apiModel;

    /**
     * @var Context
     */
    private $context;

    /**
     * @var Log
     */
    private $log;

    /**
     * @param Context $context
     * @param Api $apiModel
     * @param Log $log
     */
    public function __construct(
        Context $context,
        Api $apiModel,
        Log $log
    ) {
        $this->context = $context;
        $this->apiModel = $apiModel;
        $this->log = $log;

        parent::__construct($context);
    }

    /**
     * Update payment session. This ensures the payment session and iframe are
     * properly synced.
     *
     * @throws Exception
     */
    public function execute()
    {
        try {
            $this->apiModel->updatePaymentSession();
        } catch (Exception $e) {
            $this->log->error($e);

            throw $e;
        }

        return;
    }
}
