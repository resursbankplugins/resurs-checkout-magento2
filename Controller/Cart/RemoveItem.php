<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Controller\Cart;

use Exception;
use Magento\Checkout\Controller\Cart\Delete;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Checkout\Helper\Api;

/**
 * Class RemoveItem
 * @package Resursbank\Checkout\Controller\Cart
 */
class RemoveItem extends Delete
{
    /**
     * @var Api
     */
    private $apiHelper;

    /**
     * @var Context
     */
    private $context;

    /**
     * @param Context $context
     * @param Api $apiHelper
     * @param ScopeConfigInterface $scopeConfig
     * @param Session $checkoutSession
     * @param StoreManagerInterface $storeManager
     * @param Validator $formKeyValidator
     * @param Cart $cart
     */
    public function __construct(
        Context $context,
        Api $apiHelper,
        ScopeConfigInterface $scopeConfig,
        Session $checkoutSession,
        StoreManagerInterface $storeManager,
        Validator $formKeyValidator,
        Cart $cart
    ) {
        $this->context = $context;
        $this->apiHelper = $apiHelper;

        parent::__construct(
            $context,
            $scopeConfig,
            $checkoutSession,
            $storeManager,
            $formKeyValidator,
            $cart
        );
    }

    /**
     * Initializes payment session at Resurs Bank and renders the checkout page.
     *
     * @return string JSON
     * @throws Exception
     */
    public function execute()
    {
        $result = $this->context->getResultFactory()->create(
            ResultFactory::TYPE_JSON
        );

        try {
            parent::execute();

            // Build response object.
            $result->setData([
                'cart_qty' => $this->apiHelper->getQuote()->getItemsQty()
            ]);
        } catch (Exception $e) {
            $result->setData([
                'message' => [
                    'error' => $e->getMessage()
                ]
            ]);
        }

        // Respond.
        return $result;
    }
}
