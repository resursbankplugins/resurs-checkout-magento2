<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Controller\Cart;

use Exception;
use Magento\Checkout\Helper\Data;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Quote\Model\Quote\Item;
use Resursbank\Checkout\Helper\Api;
use Resursbank\Checkout\Helper\Log;

/**
 * Collects price information from quote.
 *
 * @package Resursbank\Checkout\Controller\Cart
 */
class CollectItemPrices extends Action
{
    /**
     * @var Api
     */
    private $apiHelper;

    /**
     * @var Data
     */
    private $checkoutHelper;

    /**
     * @var Context
     */
    private $context;

    /**
     * @var Log
     */
    private $log;

    /**
     * @param Context $context
     * @param Data $checkoutHelper
     * @param Api $apiHelper
     * @param Log $log
     */
    public function __construct(
        Context $context,
        Data $checkoutHelper,
        Api $apiHelper,
        Log $log
    ) {
        $this->context = $context;
        $this->apiHelper = $apiHelper;
        $this->checkoutHelper = $checkoutHelper;
        $this->log = $log;

        parent::__construct($context);
    }

    /**
     * Collect price information from all items on quote.
     *
     * @return ResultInterface
     * @throws Exception
     */
    public function execute(): ResultInterface
    {
        // Build response object.
        $result = $this->context->getResultFactory()->create(
            ResultFactory::TYPE_JSON
        );

        try {
            // Get request parameters.
            $rows = [];

            /** @var Item $item */
            foreach ($this->apiHelper->getQuote()->getAllItems() as $item) {
                $rows[] = [
                    'item_id' => $item->getId(),
                    'item_total' => $this->checkoutHelper->formatPrice(
                        $item->getRowTotalInclTax()
                    ),
                    'item_total_excl_tax' => $this->checkoutHelper->formatPrice(
                        $item->getRowTotal()
                    ),
                    'item_price' => $this->checkoutHelper->formatPrice(
                        $item->getPriceInclTax()
                    ),
                    'item_price_excl_tax' => $this->checkoutHelper->formatPrice(
                        $item->getPrice()
                    )
                ];
            }

            $result->setData($rows);
        } catch (Exception $e) {
            $this->log->error($e);

            $result->setData([
                'message' => [
                    'error' => __(
                        'We encountered a problem when trying to collect ' .
                        'the price information from your cart. This could be ' .
                        'because your session has expired. Please refresh the ' .
                        'page.'
                    )
                ]
            ]);
        }

        // Respond.
        return $result;
    }
}
