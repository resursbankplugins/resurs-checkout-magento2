<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Controller\Cart;

use Exception;
use InvalidArgumentException;
use Magento\Checkout\Helper\Data;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\Quote\Item;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteRepository;
use Resursbank\Checkout\Helper\Api;

/**
 * Updates quantity on quote item.
 *
 * @package Resursbank\Checkout\Controller\Cart
 */
class SetItemQty extends Action
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var Api
     */
    private $apiHelper;

    /**
     * @var Data
     */
    private $checkoutHelper;

    /**
     * @var QuoteRepository
     */
    private $quoteRepository;

    /**
     * @param Context $context
     * @param Api $apiHelper
     * @param Data $checkoutHelper
     * @param QuoteRepository $quoteRepository
     */
    public function __construct(
        Context $context,
        Api $apiHelper,
        Data $checkoutHelper,
        QuoteRepository $quoteRepository
    ) {
        $this->context = $context;
        $this->apiHelper = $apiHelper;
        $this->checkoutHelper = $checkoutHelper;
        $this->quoteRepository = $quoteRepository;

        parent::__construct($context);
    }

    /**
     * @return ResultInterface
     * @throws InvalidArgumentException
     */
    public function execute(): ResultInterface
    {
        /** @var ResultInterface $result */
        $result = $this->context->getResultFactory()->create(
            ResultFactory::TYPE_JSON
        );

        try {
            /** @var Item $item */
            $item = $this->getItem();

            // Update item qty.
            $item->setQty($this->getQty());
            $this->quoteRepository->save($this->getQuote());

            // Recalculate row totals.
            $item->calcRowTotal();

            // Assemble price information in response.
            $result->setData([
                'price' => $this->formatPrice(
                    (float) $item->getPrice()
                ),
                'price_incl_tax' => $this->formatPrice(
                    (float) $item->getPriceInclTax()
                ),
                'row_total' => $this->formatPrice(
                    (float) $item->getRowTotal()
                ),
                'row_total_incl_tax' => $this->formatPrice(
                    (float) $item->getRowTotalInclTax()
                )
            ]);
        } catch (Exception $e) {
            $result->setData([
                'message' => [
                    'error' => $e->getMessage()
                ]
            ]);
        }

        return $result;
    }

    /**
     * @return float
     */
    private function getQty(): float
    {
        return (float) $this->getRequest()->getParam('qty');
    }

    /**
     * @return int
     */
    private function getId(): int
    {
        return (int) $this->getRequest()->getParam('id');
    }

    /**
     * @param float $price
     * @return string
     */
    private function formatPrice(float $price): string
    {
        return $this->checkoutHelper->formatPrice($price);
    }

    /**
     * @return Quote
     * @throws NoSuchEntityException
     */
    private function getQuote(): Quote
    {
        return $this->apiHelper->getQuote();
    }

    /**
     * @return Item
     * @throws NoSuchEntityException
     * @throws Exception
     */
    private function getItem(): Item
    {
        // Resolve item id.
        $id = $this->getId();

        if ($id === 0) {
            throw new Exception('Please provide a valid item id.');
        }

        /** @var Item|false $item */
        $item = $this->getQuote()->getItemById($id);

        if (!$item instanceof Item) {
            throw new Exception('Unable to find item with id ' . $id);
        }

        return $item;
    }
}
