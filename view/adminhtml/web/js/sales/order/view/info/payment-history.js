/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'ko',
        'jquery',
        'mage/translate',
        'uiElement',
        'Magento_Ui/js/modal/modal'
    ],
    function(ko, $, $t, Component, Modal) {
        'use strict';

        /**
         * @type {object} Modal widget.
         */
        var modal;

        /**
         * @type {object} JQuery element object of the modal content.
         */
        var modalContentEl;

        return Component.extend({
            defaults: {
                template: 'Resursbank_Checkout/sales/order/view/info/payment-history'
            },

            subtitle: 'This log shows what happened after the customer was ' +
                'redirected to Resurs Bank\'s payment gateway.',

            /**
             * Opens the modal.
             */
            openModal: function() {
                if (modalContentEl.length === 1) {
                    modalContentEl.modal('openModal');
                }
            },

            /**
             * Initializes the JQuery Modal widget. Can only be executed once.
             */
            initModal: function() {
                if (typeof modalContentEl === 'undefined') {
                    modalContentEl = $(
                        '#resursbank-checkout-payment-history-modal-content'
                    );

                    if (modalContentEl.length === 1) {
                        modal = Modal({
                            autoOpen: false,
                            type: 'popup',
                            responsive: true,
                            innerScroll: true,
                            title: $.mage.__(
                                'Resurs Bank - Order Payment History'
                            ),
                            modalClass: 'custom-modal',
                            buttons: []
                        }, modalContentEl);
                    }
                }
            }
        });
    }
);
