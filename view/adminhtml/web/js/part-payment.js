/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery'
    ],
    function ($) {
        /**
         * Public object.
         *
         * @type {object}
         */
        var me = {};

        /**
         * The chosen payment method cache. We store the min and max limit here.
         *
         * @type {null|object}
         */
        var paymentMethodCache = null;

        /**
         * Default min and max limits for the payment methods.
         *
         * @type {{minLimit: number, maxLimit: number}}
         */
        var defaultLimits = {
            minLimit: 0,
            maxLimit: 0
        };

        /**
         * States whether initialization has happened.
         *
         * @private
         * @type {boolean}
         */
        var initialized = false;

        /**
         * @private
         * @type {string}
         */
        var baseUrl = '';

        /**
         * The element where you choose part payment duration for the selected payment method.
         *
         * @private
         * @type {HTMLElement | null}
         */
        var durationBox = document.getElementById('resursbank_checkout_part_payment_duration');

        /**
         * The element where you specify the minimum price of the products that part payment should
         * apply for.
         *
         * @private
         * @type {HTMLElement | null}
         */
        var minPriceField = document.getElementById('resursbank_checkout_part_payment_min_price');

        /**
         * The element where you specify the maximum price of the products that part payment should
         * apply for.
         *
         * @private
         * @type {HTMLElement | null}
         */
        var maxPriceField = document.getElementById('resursbank_checkout_part_payment_max_price');

        /**
         * The element where you choose payment method.
         *
         * @private
         * @type {HTMLElement | null}
         */
        var paymentMethodBox = document.getElementById(
            'resursbank_checkout_part_payment_payment_method'
        );

        /**
         * Checks whether a value is an object.
         *
         * @param {*} value
         * @returns {boolean}
         */
        var isObject = function (value) {
            return Object.prototype.toString.call(value) === '[object Object]';
        };

        /**
         * Sets the payment method cache.
         *
         * @param {number} minLimit
         * @param {number} maxLimit
         */
        var setPaymentMethodCache = function (minLimit, maxLimit) {
            minLimit = parseFloat(minLimit);
            maxLimit = parseFloat(maxLimit);

            paymentMethodCache = {
                minLimit: isNaN(minLimit) ? defaultLimits.minLimit : minLimit,
                maxLimit: isNaN(maxLimit) ? defaultLimits.maxLimit : maxLimit
            };
        };

        /**
         * Sets the default min and max limit.
         *
         * @param {number} minLimit
         * @param {number} maxLimit
         */
        var setDefaultLimits = function (minLimit, maxLimit) {
            var defaultMinLimit = parseFloat(minLimit);
            var defaultMaxLimit = parseFloat(maxLimit);

            defaultLimits.minLimit = isNaN(defaultMinLimit) ? 0 : defaultMinLimit;
            defaultLimits.maxLimit = isNaN(defaultMaxLimit) ? 0 : defaultMaxLimit;
        };

        /**
         * Event handler for when the payment method changes.
         *
         * @private
         * @return
         */
        var onPaymentMethodChange = function () {
            paymentMethodBox.disabled = true;
            durationBox.disabled = true;

            return me.fetchDurationOptions(me.getPaymentMethodId())
                .done(function (response) {
                    try {
                        updateDurationOptions(
                            response.factors.map(function (factor) {
                                return new Option(factor.label, factor.value);
                            })
                        );

                        if (response.methodExists) {
                            setPaymentMethodCache(response.minLimit, response.maxLimit);
                            setMinPrice(response.minLimit);
                            setMaxPrice(response.maxLimit);
                        } else {
                            setPaymentMethodCache(0, 0);
                            setMinPrice(null);
                            setMaxPrice(null);
                        }
                    } catch (error) {
                        console.error(
                            'An error occured when updating the duration options!\n\n',
                            error
                        );
                    }
                })
                .always(function () {
                    durationBox.disabled = false;
                    paymentMethodBox.disabled = false;
                });
        };

        /**
         * Takes an array of option elements and replaces the current list of duration options.
         *
         * @private
         * @param factors
         * @return {jQuery}
         */
        var updateDurationOptions = function (factors) {
            return $(durationBox)
                .empty()
                .append(factors);
        };

        /**
         * Updates the value of the minimum price field.
         *
         * @private
         * @param price
         */
        var setMinPrice = function (price) {
            var value = parseFloat(price);

            if (price === null) {
                value = '';
            } else if (paymentMethodCache !== null) {
                if (isNaN(value) || price < paymentMethodCache.minLimit) {
                    value = paymentMethodCache.minLimit;
                } else if (price > paymentMethodCache.maxLimit) {
                    value = paymentMethodCache.maxLimit
                }
            }

            minPriceField.textContent = value;
        };

        /**
         * Updates the value of the maximum price field.
         *
         * @private
         * @param price
         */
        var setMaxPrice = function (price) {
            var value = parseFloat(price);

            if (price === null || isNaN(value)) {
                value = '';
            }

            maxPriceField.textContent = value;
        };

        /**
         * Returns the current value of the payment method box.
         *
         * @return {string}
         */
        me.getPaymentMethodId = function () {
            return paymentMethodBox.value;
        };

        /**
         * Takes a payment method code and calls the PartPayment controllers. The controller will
         * return a list of duration options for the given code, and then we will replace our
         * current list of duration options with the new one.
         *
         * @param {string} id - A payment method id.
         * @return {jQuery} The ajax request.
         */
        me.fetchDurationOptions = function (id) {
            return $.ajax(baseUrl, {
                data: {
                    paymentMethodId: id
                },

                error: function (response) {
                    console.error(
                        'An error occured serverside when fetching duration options.\n\n',
                        response
                    );
                }
            });
        };

        /**
         * Initialization function. Can only be run once.
         *
         * @param {object} config
         * @param {string} config.baseUrl
         * @param {object|null} config.chosenMethodMinMaxLimit
         * @param {number} config.chosenMethodMinMaxLimit.minLimit
         * @param {number} config.chosenMethodMinMaxLimit.maxLimit
         * @param {object|undefined} config.defaultLimits
         * @param {number} config.defaultLimits.minLimit
         * @param {number} config.defaultLimits.maxLimit
         *
         */
        me.init = function (config) {
            if (initialized === false) {
                $(paymentMethodBox).on('change', onPaymentMethodChange);

                if (config.hasOwnProperty('defaultLimits') && isObject(config.defaultLimits)) {
                    setDefaultLimits(config.defaultLimits.minLimit, config.defaultLimits.maxLimit);
                }

                if (config.hasOwnProperty('chosenMethodMinMaxLimit')
                    && isObject(config.chosenMethodMinMaxLimit)
                ) {
                    setPaymentMethodCache(
                        config.chosenMethodMinMaxLimit.minLimit,
                        config.chosenMethodMinMaxLimit.maxLimit
                    );
                }

                if (paymentMethodBox.value === '') {
                    setMinPrice(null);
                    setMaxPrice(null);
                }

                baseUrl = typeof config.baseUrl === 'string' ? config.baseUrl : '';
                initialized = true;
            }

            return me;
        };

        return me;
    }
);
