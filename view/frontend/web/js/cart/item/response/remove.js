/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [],
    function() {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/cart/item/response/remove
         * @type {function}
         */
        var EXPORT;

        /**
         * @param {object} config
         * @param {number} config.id
         * @param {boolean} config.reloadShipping
         * @param {boolean} config.reloadDiscount
         * @param {Success|Flawed} config.response
         * @property {number} id
         * @property {number} cartQty
         * @property {Success|Flawed} response
         * @property {boolean} reloadShipping
         * @property {boolean} reloadShipping
         * @constructor
         */
        function Remove(config) {
            this.id = config.id;
            this.cartQty = config.cartQty;
            this.response = config.response;

            this.reloadShipping = typeof config.reloadShipping === 'boolean' ?
                config.reloadShipping :
                true;

            this.reloadDiscount = typeof config.reloadDiscount === 'boolean' ?
                config.reloadDiscount :
                true;

            Object.freeze(this);
        }

        EXPORT = Remove;

        return Object.freeze(EXPORT);
    }
);
