/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery',
        'Resursbank_Checkout/js/lib/channel',
        'Resursbank_Checkout/js/cart/dom',
        'Resursbank_Checkout/js/cart/item/prices'
    ],
    function(
        // Magento
        $,

        // Resurs Bank
        Channel,
        CartDom,
        Prices
    ) {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/cart/item/component
         * @type {function}
         */
        var EXPORT;

        /**
         * @param {object} config
         * @param {object} config.id
         * @param {object} config.quantity
         * @property {number} id
         * @property {Channel} channel
         * @property {function} setQuantity
         * @property {function} getPreviousQuantity
         * @property {function} setPreviousQuantity
         * @property {function} setPrices
         * @property {function} disable
         * @property {function} enable
         * @property {function} destroy
         * @constructor
         */
        function ItemComponent(config) {
            /**
             * @type {ItemComponent}
             */
            var me = this;

            /**
             * @type {HTMLInputElement}
             */
            var QTY_INPUT;

            /**
             * @type {number}
             */
            var PREVIOUS_QTY;

            /**
             * @type {HTMLButtonElement}
             */
            var DEL_BTN;

            /**
             * @type {HTMLTableSectionElement}
             */
            var EL;

            /**
             * An event channel.
             * 
             * @type {Channel}
             */
            var CHANNEL;

            /**
             * Whether the item controls are disabled.
             *
             * @type {boolean}
             */
            var DISABLED = false;

            /**
             * Event handler for when quantity changes.
             */
            function onChange() {
                if (!DISABLED) {
                    disable();

                    CHANNEL.emit('change', {
                        id: me.id,
                        quantity: parseFloat(QTY_INPUT.value)
                    });
                }
            }

            /**
             * Event handler for when the item is removed.
             */
            function onRemove(event) {
                event.stopPropagation();
                event.preventDefault();

                if (!DISABLED) {
                    disable();
                    CHANNEL.emit('remove', me.id);
                }
            }

            /**
             * Set the value of the quantity input.
             *
             * @param qty
             * @returns {ItemComponent}
             */
            function setQuantity(qty) {
                if (typeof qty === 'number' && !Number.isNaN(qty)) {
                    QTY_INPUT.value = qty;
                }

                return me;
            }

            /**
             * Returns the previously selected quantity.
             *
             * @returns {number}
             */
            function getPreviousQuantity() {
                return PREVIOUS_QTY;
            }

            /**
             * Sets the previously selected quantity.
             *
             * @param {number} qty
             * @returns {ItemComponent}
             */
            function setPreviousQuantity(qty) {
                if (typeof qty === 'number' && !Number.isNaN(qty)) {
                    PREVIOUS_QTY = qty;
                }

                return me;
            }

            /**
             * Takes an object with price elements and replaces the current price elements.
             *
             * @public
             * @param {Prices} prices
             * @return {ItemComponent} The instance.
             */
            function setPrices(prices) {
                if (prices instanceof Prices) {
                    if (typeof prices.subtotal !== 'undefined') {
                        updateSubtotal(prices.subtotal);
                    }

                    if (typeof prices.subtotalExcl !== 'undefined') {
                        updateSubtotalExcl(prices.subtotalExcl);
                    }

                    if (typeof prices.price !== 'undefined') {
                        updatePrice(prices.price);
                    }

                    if (typeof prices.priceExcl !== 'undefined') {
                        updatePriceExcl(prices.priceExcl);
                    }
                }

                return me;
            }

            /**
             * Replaces the subtotal element.
             *
             * @private
             * @param {HTMLElement} priceEl
             * @return {ItemComponent} The instance.
             */
            function updateSubtotal(priceEl) {
                $(CartDom.getSubtotalElement(EL)).replaceWith(priceEl);

                return me;
            }

            /**
             * Replaces the subtotal excluding tax element.
             *
             * @private
             * @param {HTMLElement} priceEl
             * @return {ItemComponent} The instance.
             */
            function updateSubtotalExcl(priceEl) {
                $(CartDom.getSubtotalExclElement(EL)).replaceWith(priceEl);

                return me;
            }

            /**
             * Replaces the price element.
             *
             * @private
             * @param {HTMLElement} priceEl
             * @return {ItemComponent} The instance.
             */
            function updatePrice(priceEl) {
                $(CartDom.getPriceElement(EL)).replaceWith(priceEl);

                return me;
            }

            /**
             * Replaces the price excluding tax element.
             *
             * @private
             * @param {HTMLElement} priceEl
             * @return {ItemComponent} The instance.
             */
            function updatePriceExcl(priceEl) {
                $(CartDom.getPriceExclElement(EL)).replaceWith(priceEl);

                return me;
            }

            /**
             * Disables the item by preventing removal, and quantity change of
             * the item.
             */
            function disable() {
                $(EL).addClass('resursbank-checkout-disable-item');
                $(QTY_INPUT).prop('disabled', true);
                $(DEL_BTN).prop('disabled', true);

                DISABLED = true;
            }

            /**
             * Enables the item by allowing removal, and quantity change of
             * the item.
             */
            function enable() {
                $(EL).removeClass('resursbank-checkout-disable-item');
                $(QTY_INPUT).prop('disabled', false);
                $(DEL_BTN).prop('disabled', false);

                DISABLED = false;
            }

            /**
             * Destroys the instance, removes the element from the DOM.
             */
            function destroy() {
                $(QTY_INPUT).off('change', onChange);
                $(DEL_BTN).off('click', onRemove);
                $(EL).remove();

                CHANNEL.destroy();
                me = null;
            }

            /**
             * Initialization function.
             *
             * @returns {ItemComponent}
             */
            function init() {
                EL = CartDom.getProductRow(config.id);
                QTY_INPUT = CartDom.getQuantityInput(config.id);
                DEL_BTN = CartDom.getDeleteButton(config.id);
                PREVIOUS_QTY = parseFloat(QTY_INPUT.value);
                CHANNEL = new Channel();

                $(QTY_INPUT).on('change', onChange);
                $(DEL_BTN).on('click', onRemove);

                me.id = config.id;
                me.channel = CHANNEL;
                me.setQuantity = setQuantity;
                me.getPreviousQuantity = getPreviousQuantity;
                me.setPreviousQuantity = setPreviousQuantity;
                me.setPrices = setPrices;
                me.disable = disable;
                me.enable = enable;
                me.destroy = destroy;

                return me;
            }

            Object.freeze(init())
        }

        EXPORT = ItemComponent;

        return Object.freeze(EXPORT);
    }
);
