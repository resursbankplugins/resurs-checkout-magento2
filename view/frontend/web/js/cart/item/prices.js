/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [],
    function() {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/cart/item/prices
         * @type {function}
         */
        var EXPORT;

        /**
         * @param {object} config
         * @param {number} config.id
         * @param {number} [config.subtotal]
         * @param {number} [config.subtotalExcl]
         * @param {number} [config.price]
         * @param {number} [config.priceExcl]
         * @property {number} id
         * @property {number} subtotal
         * @property {number} subtotalExcl
         * @property {number} price
         * @property {number} priceExcl
         * @constructor
         */
        function Prices(config) {
            this.id = config.id;
            this.subtotal = config.subtotal;
            this.subtotalExcl = config.subtotalExcl;
            this.price = config.price;
            this.priceExcl = config.priceExcl;

            Object.freeze(this);
        }

        EXPORT = Prices;

        return Object.freeze(EXPORT);
    }
);
