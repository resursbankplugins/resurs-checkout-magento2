/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery'
    ], 
    function($) {
        'use strict';

        /**
         * The module singleton.
         *
         * @exports Resursbank_Checkout/js/cart/dom
         * @constant {object}
         */
        var EXPORT = {};
    
        /**
         * Get the product row element.
         * 
         * @param {number} id
         * @returns {HTMLTableSectionElement | undefined}
         */
        function getProductRow(id) {
            return $(getQuantityInput(id)).closest('tbody')[0]
        }

        /**
         * Returns the quantity input element based on the product id.
         *
         * @param {number} id
         * @returns {HTMLInputElement | undefined}
         */
        function getQuantityInput(id) {
            return getQuantityInputs().filter(function (el) {
                return getIdFromQuantityInput(el) === id;
            })[0];
        }

        /**
         * Returns the delete element based on the product id.
         *
         * @param {number} id
         * @returns {HTMLLinkElement}
         */
        function getDeleteButton(id) {
            return getDeleteButtons().filter(function (el) {
                return getIdFromDeleteButton(el) === id;
            })[0];
        }

        /**
         * Returns the id from a remove button of a product.
         *
         * @param {HTMLLinkElement} button
         * @returns {number}
         */
        function getIdFromDeleteButton(button) {
            return parseInt(
                JSON.parse(button.getAttribute('data-post')).data.id,
                10
            );
        }

        /**
         * Returns the id from a quantity input of a product.
         *
         * @param {HTMLInputElement} input
         * @returns {number}
         */
        function getIdFromQuantityInput(input) {
            return parseInt(input.name.match(/cart\[(\d+)\]/)[1], 10);
        }

        /**
         * Returns an empty array or an array of the quantity inputs for every
         * product in the cart.
         *
         * @returns {HTMLInputElement[]}
         */
        function getQuantityInputs() {
            var form = getCartForm();
            var inputs;

            if (form instanceof HTMLFormElement) {
                inputs = $(form).find('input[data-role="cart-item-qty"]');
            }

            return typeof inputs !== 'undefined' &&
            inputs.length > 0 ?
                $.makeArray(inputs) :
                [];
        }
        
        /**
         * Returns an empty array or an array of the delete buttons for every
         * product in the cart.
         *
         * @returns {HTMLLinkElement[]}
         */
        function getDeleteButtons() {
            var form = getCartForm();
            var buttons;

            if (form instanceof HTMLFormElement) {
                buttons = $(form).find('[data-post]');
            }

            return typeof buttons !== 'undefined' &&
                buttons.length > 0 ?
                $.makeArray(buttons.filter(function (i, btn) {
                    var postData = JSON.parse(
                        btn.getAttribute('data-post')
                    );

                    return postData.hasOwnProperty('action') &&
                        postData.action.search('checkout/cart/delete') !== -1;
                })) :
                [];
        }
        
        /**
         * Returns the cart element.
         *
         * @returns {HTMLTableElement | undefined}
         */
        function getCartForm() {
            return $('form[action*="checkout/cart"]')[0];
        }
        
        /**
         * Returns all the product IDs in the cart.
         *
         * @returns {number[]}
         */
        function getProductIds() {
            return getQuantityInputs().map(getIdFromQuantityInput);
        }

        /**
         * Returns the subtotal element of the item.
         *
         * @param {HTMLTableSectionElement} itemEl
         * @returns {HTMLElement | undefined}
         */
        function getSubtotalElement(itemEl) {
            return $(itemEl).find(
                'td.subtotal span.price-including-tax span.price'
            )[0];
        }

        /**
         * Returns the subtotal excluding tax element of the item.
         *
         * @param {HTMLTableSectionElement} itemEl
         * @returns {HTMLElement | undefined}
         */
        function getSubtotalExclElement(itemEl) {
            return $(itemEl).find(
                'td.subtotal span.price-excluding-tax span.price'
            )[0];
        }

        /**
         * Returns the price element of the item.
         *
         * @param {HTMLTableSectionElement} itemEl
         * @returns {HTMLElement | undefined}
         */
        function getPriceElement(itemEl) {
            return $(itemEl).find(
                'td.price span.price-including-tax span.price'
            )[0];
        }

        /**
         * Returns the price excluding tax element of the item.
         *
         * @param {HTMLTableSectionElement} itemEl
         * @returns {HTMLElement | undefined}
         */
        function getPriceExclElement(itemEl) {
            return $(itemEl).find(
                'td.price span.price-excluding-tax span.price'
            )[0];
        }

        EXPORT.getProductRow = getProductRow;
        EXPORT.getQuantityInput = getQuantityInput;
        EXPORT.getQuantityInputs = getQuantityInputs;
        EXPORT.getDeleteButton = getDeleteButton;
        EXPORT.getIdFromDeleteButton = getIdFromDeleteButton;
        EXPORT.getIdFromQuantityInput = getIdFromQuantityInput;
        EXPORT.getProductIds = getProductIds;
        EXPORT.getSubtotalElement = getSubtotalElement;
        EXPORT.getSubtotalExclElement = getSubtotalExclElement;
        EXPORT.getPriceElement = getPriceElement;
        EXPORT.getPriceExclElement = getPriceExclElement;

        return EXPORT;
    }
);
