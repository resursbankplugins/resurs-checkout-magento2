/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery',
        'ko',
        'Magento_Customer/js/customer-data',
        'Resursbank_Checkout/js/cart/item/item',
        'Resursbank_Checkout/js/cart/item/prices',
        'Resursbank_Checkout/js/cart/item/response/change',
        'Resursbank_Checkout/js/cart/item/response/remove',
        'Resursbank_Checkout/js/lib/ajax',
        'Resursbank_Checkout/js/lib/ajax/response',
        'Resursbank_Checkout/js/lib/server'
    ], 
    function(
        // Magento
        $,
        ko,
        CustomerData,

        // Resurs Bank
        Item, 
        Prices, 
        ItemChangeResponse,
        ItemRemoveResponse,
        Ajax,
        Response,
        Server
    ) {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/cart/model
         * @constant {object}
         */
        var EXPORT = {};

        /**
         * @type {Chain}
         */
        var AJAX_CHAIN = new Ajax.Chain({
            name: 'Cart model'
        });

        /**
         * A collection of chains for all registered items.
         *
         * @type {object}
         */
        var ITEM_CHAINS = {};

        /**
         * Base URL for the cart.
         *
         * @constant {string}
         */
        var BASE_URL = 'cart/';

        /**
         * @type {function} ko.observable - array.
         */
        var ITEMS = ko.observableArray();

        /**
         * Returns an AJAX chain of the item with the given id.
         *
         * @param {number} id
         * @returns {Chain|undefined}
         */
        function getItemChain(id) {
            return ITEM_CHAINS[id];
        }

        /**
         * Says whether an item with the given id exists in the model.
         *
         * @param {number} id
         * @returns {boolean}
         */
        function hasItem(id) {
            var item = getItem(id);

            return typeof item !== 'undefined' && item instanceof Item;
        }

        /**
         * Attempts to retrieve an item with the given id from the model and 
         * then return that item, or undefined if none was found.
         *
         * @param {number} id
         * @returns {Item|undefined}
         */
        function getItem(id) {
            var index = getItemIndex(id);
            var result;

            if (index !== -1) {
                result = ITEMS()[index];
            }

            return result;
        }

        /**
         * Returns the index of the item with the given id, or -1 if it
         * doesn't exist.
         *
         * @param {number} id
         * @returns {number}
         */
        function getItemIndex(id) {
            var result = -1;

            ITEMS.some(function(item, i) {
                if (item.id === id) {
                    result = i;
                }

                return item.id === id;
            });

            return result;
        }

        /**
         * Adds an item to the model. Each item will have it's own Chain
         * associated with it.
         *
         * @param {Item} item
         * @returns {object} The module.
         */
        function addItem(item) {
            if (item instanceof Item && !hasItem(item.id)) {
                ITEMS.push(new Item({
                    id: item.id,
                    quantity: item.quantity
                }));
                
                ITEM_CHAINS[item.id] = new Ajax.Chain({
                    limit: 1,
                    name: item.id
                });

                AJAX_CHAIN.addSubordinate(item.id, ITEM_CHAINS[item.id]);
            }

            return EXPORT;
        }

        /**
         * Removes the item with the given id.
         *
         * @param {number} id
         * @returns {Request}
         */
        function removeItem(id) {
            var item = getItem(id);
            var result;
            var call;
            var ajaxChain;
            var deferred;

            if (item instanceof Item) {
                deferred = $.Deferred();
                ajaxChain = getItemChain(id);
                call = getRemoveItemCall(id);

                call.onDone(function (response) {
                    var status = Ajax.getStatus(response);
                    var itemResponse;

                    if (status instanceof Response.Success) {
                        itemResponse = new ItemRemoveResponse({
                            id: id,
                            cartQty: response.cart_qty,
                            response: status,
                            reloadShipping: response.reloadShipping,
                            reloadDiscount: response.reloadDiscount
                        });

                        ITEMS.splice(getItemIndex(id), 1);
                        AJAX_CHAIN.removeSubordinate(id);

                        CustomerData.reload(['cart', 'messages'], true);
                        deferred.resolve(itemResponse);
                    } else {
                        deferred.resolve(status);
                    }
                });

                call.onFail(function(response) {
                    deferred.reject(response);
                });

                ajaxChain.add(call).run();

                result = new Ajax.Request({
                    call: call,
                    deferred: deferred
                });
            }

            return result;
        }

        /**
         * Pushes a new quantity for an item with the given id to the server.
         *
         * @param {number} id
         * @param {number} quantity
         * @returns {Request|undefined}
         */
        function setItemQuantity(id, quantity) {
            var item = getItem(id);
            var result;
            var call;
            var ajaxChain;
            var deferred;

            if (item instanceof Item) {
                deferred = $.Deferred();
                ajaxChain = getItemChain(id);
                call = getItemQuantityCall(id, quantity);
                call.onDone(function(response) {
                    var status = Ajax.getStatus(response);
                    var itemResponse;

                    if (status instanceof Response.Success) {
                        itemResponse = new ItemChangeResponse({
                            id: id,
                            quantity: quantity,
                            previousQuantity: item.quantity,
                            reloadShipping: true,
                            reloadDiscount: true,
                            response: status,
                            prices: new Prices({
                                id: id,
                                subtotal: response.row_total_incl_tax,
                                subtotalExcl: response.row_total,
                                price: response.price_incl_tax,
                                priceExcl: response.price
                            })
                        });

                        item.quantity = quantity;

                        ITEMS.valueHasMutated();
                        CustomerData.reload(['cart', 'messages'], true);
                        deferred.resolve(itemResponse);
                    } else {
                        deferred.resolve(status);
                    }
                });

                call.onFail(function(response) {
                    deferred.reject(response);
                });

                ajaxChain.add(call).run();

                result = new Ajax.Request({
                    call: call,
                    deferred: deferred
                });
            }

            return result;
        }

        /**
         * Fetches prices for all items in the cart. Expected response is an
         * array of objects, one object per item, containing price information.
         * This array will be converted into an array of Prices objects as the
         * $.Deferred() response.
         *
         * NOTE: Prices are HTML formatted strings.
         *
         * @returns {Request} An object with the call object, and a $.Deferred()
         * object that will receive a modified response when resolved.
         */
        function collectItemPrices() {
            var deferred = $.Deferred();
            var call = getCollectItemPricesCall();
            var result = new Ajax.Request({
                call: call,
                deferred: deferred
            });

            call.onDone(function(response) {
                var status = Ajax.getStatus(response);
                var itemResponse;

                if (status instanceof Response.Success) {
                    itemResponse = response.map(function(item) {
                        return new Prices({
                            id: item.item_id,
                            subtotal: item.item_total,
                            subtotalExcl: item.item_total_excl_tax,
                            price: item.item_price,
                            priceExcl: item.item_price_excl_tax
                        });
                    });

                    deferred.resolve(itemResponse);
                } else {
                    deferred.resolve(status);
                }
            });

            AJAX_CHAIN.add(call).run();

            return result;
        }

        /**
         * Creates a call to set the quantity of an item.
         *
         * @param {number} id
         * @param {number} quantity
         * @returns {Call}
         */
        function getItemQuantityCall(id, quantity) {
            return new Ajax.Call({
                options: {
                    method: 'POST',
                    url: createServerUrl('setItemQty'),
                    data: {
                        id: id,
                        qty: quantity,
                        form_key: Server.getFormKey()
                    }
                }
            });
        }

        /**
         * Creates a call to remove the item.
         *
         * @param {number} id
         * @returns {Call}
         */
        function getRemoveItemCall(id) {
            return new Ajax.Call({
                options: {
                    method: 'POST',
                    url: createServerUrl('removeItem'),
                    data: {
                        id: id,
                        form_key: Server.getFormKey()
                    }
                }
            });
        }

        /**
         * Creates AJAX call to collect all item prices from Quote.
         *
         * @returns {Call}
         */
        function getCollectItemPricesCall() {
            return new Ajax.Call({
                options: {
                    method: 'POST',
                    url: createServerUrl('collectItemPrices'),
                    data: {
                        form_key: Server.getFormKey()
                    }
                }
            });
        }

        /**
         * Creates a URL for a server call.
         *
         * @param {string} path
         * @returns {string}
         */
        function createServerUrl(path) {
            return Server.createUrl(BASE_URL + path);
        }

        EXPORT.items = ITEMS;
        EXPORT.ajaxChain = AJAX_CHAIN;
        EXPORT.addItem = addItem;
        EXPORT.getItem = getItem;
        EXPORT.hasItem = hasItem;
        EXPORT.getItemChain = getItemChain;
        EXPORT.setItemQuantity = setItemQuantity;
        EXPORT.removeItem = removeItem;
        EXPORT.collectItemPrices = collectItemPrices;

        return EXPORT;
    }
);
