/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'Magento_Checkout/js/model/quote',
        'mage/translate',
        'Resursbank_Checkout/js/lib/channel',
        'Resursbank_Checkout/js/lib/ajax',
        'Resursbank_Checkout/js/cart/dom',
        'Resursbank_Checkout/js/cart/model',
        'Resursbank_Checkout/js/cart/item/component',
        'Resursbank_Checkout/js/cart/item/item',
        'Resursbank_Checkout/js/cart/item/response/change',
        'Resursbank_Checkout/js/cart/item/response/remove'
    ],
    function(
        Quote,
        $t,
        Channel,
        Ajax,
        CartDom,
        Model,
        ItemComponent,
        Item,
        ItemChangeResponse,
        ItemRemoveResponse
    ) {
        'use strict';
        
        /**
         * @exports Resursbank_Checkout/js/cart/component
         * @constant {object}
         */
        var EXPORT = {};

        /**
         * An event channel.
         * 
         * @type {Channel}
         */
        var CHANNEL = new Channel();

        /**
         * Whether the module has been inititalized.
         * 
         * @type {boolean}
         * @readonly
         */
        var INITIALIZED = false;

        /**
         * Registered item components.
         * 
         * @type {object}
         */
        var ITEMS = {};

        /**
         * Says whether the module has been initialized.
         */
        function isInitialized() {
            return INITIALIZED;
        }

        /**
         * Says whether the cart has a registered item component with the given 
         * id.
         *
         * @param {number} id
         */
        function hasItem(id) {
            return ITEMS.hasOwnProperty(id);
        }

        /**
         * Returns an item component with the given id or undefined if it
         * doesn't exist.
         *
         * @param {number} id
         * @returns {ItemComponent|undefined}
         */
        function getItem(id) {
            return ITEMS[id];
        }

        /**
         * Adds an item component to the cart.
         *
         * @param {Item} item
         */
        function addItem(item) {
            if (item instanceof Item &&
                Model.hasItem(item.id) &&
                !hasItem(item.id)
            ) {
                var comp = new ItemComponent({
                    id: item.id,
                    quantity: item.quantity
                });

                comp.channel.on('change', onItemChange);
                comp.channel.on('remove', onItemRemove);

                ITEMS[item.id] = comp;
            }
        }

        /**
         * Removes an item component with the given ID stored in the
         * collection of item components.
         *
         * @param {number} id
         * @return {object} The model.
         */
        function removeItem(id) {
            var item = getItem(id);

            if (item instanceof ItemComponent) {
                item.destroy();
                delete ITEMS[id];
            }

            return EXPORT;
        }

        /**
         * Event handler for when an item component changes quantity.
         *
         * @param {object} data
         * @param {number} data.id
         * @param {number} data.quantity
         */
        function onItemChange(data) {
            var item = getItem(data.id);

            if (item instanceof ItemComponent) {
                var request = Model.setItemQuantity(data.id, data.quantity);

                if (request instanceof Ajax.Request) {
                    request.deferred.done(function(response) {
                        if (response instanceof ItemChangeResponse) {
                            item.setPrices(response.prices);
                            item.setPreviousQuantity(response.quantity);
                            CHANNEL.emit('quantity-changed', response);
                        } else if (response instanceof Ajax.Response.Flawed) {
                            if (response.error !== null) {
                                window.alert(
                                    response.error + '\n\n' +
                                    $t(
                                        'The previous quantity will be ' +
                                        'used.'
                                    )
                                );
                            }

                            item.setQuantity(item.getPreviousQuantity());
                        }

                        item.enable();
                    });
                }
            }
        }

        /**
         * Event handler for when an item component should be removed.
         *
         * @param {number} id
         */
        function onItemRemove(id) {
            var request;
            var item = getItem(id);

            if (item instanceof ItemComponent) {
                request = Model.removeItem(id);

                if (request instanceof Ajax.Request) {
                    request.deferred.done(function(response) {
                        if (response instanceof ItemRemoveResponse) {
                            CHANNEL.emit('item-removed', response);
                        } else if (response instanceof Ajax.Response.Flawed) {
                            window.alert(response.error);
                        }
                    });
                }
            }
        }

        /**
         * Handler for when the billing address changes.
         */
        function onBillingAddressChange() {
            updateItemPrices();
        }

        /**
         * Fetches and updates all prices of all items in the cart.
         */
        function updateItemPrices() {
            var request = Model.collectItemPrices();

            if (request instanceof Ajax.Request) {
                request.deferred.done(function(response) {
                    if (Array.isArray(response)) {
                        response.forEach(function(itemPrices) {
                            var item = getItem(itemPrices.id);

                            if (item instanceof ItemComponent) {
                                item.setPrices(itemPrices);
                            }
                        });
                    } else if (response instanceof Ajax.Response.Flawed) {
                        window.alert(response.error);
                    }
                });
            }
        }

        /**
         * Initialization function.
         * 
         * @returns {object} The module.
         */
        function init() {
            if (!INITIALIZED) {
                // Inititalize model data.
                CartDom.getQuantityInputs().forEach(function(input) {
                    var item = new Item({
                        id: CartDom.getIdFromQuantityInput(input),
                        quantity: Number(input.value)
                    });

                    Model.addItem(item);
                    addItem(item);
                });

                Model.items.subscribe(
                    function(changes) {
                        changes.forEach(function(change) {
                            var item = getItem(change.value.id);

                            if (change.status === 'deleted' &&
                                item instanceof ItemComponent
                            ) {
                                removeItem(change.value.id);
                            }
                        });
                    },
                    null,
                    'arrayChange'
                );

                Quote.billingAddress.subscribe(onBillingAddressChange);

                INITIALIZED = true;
            }

            return EXPORT;
        }

        EXPORT.init = init;
        EXPORT.isInitialized = isInitialized;
        EXPORT.channel = CHANNEL;

        return Object.freeze(EXPORT);
    }
);
