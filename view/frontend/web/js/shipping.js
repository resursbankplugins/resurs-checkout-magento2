/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery',
        'mage/storage',
        'Magento_Checkout/js/model/resource-url-manager',
        'Magento_Checkout/js/model/shipping-rate-registry',
        'Magento_Checkout/js/model/shipping-service',
        'Magento_Checkout/js/model/quote',
        'Resursbank_Checkout/js/lib/ajax'
    ],
    function(
        // Magento
        $,
        Storage,
        ResourceUrlManager,
        RateRegistry,
        ShippingService,
        Quote,

        // Resurs Bank
        Ajax
    ) {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/shipping
         * @constant {object}
         */
        var EXPORT = {};

        /**
         * @type {Chain}
         */
        var AJAX_CHAIN = new Ajax.Chain({
            limit: 1,
            name: 'Shipping'
        });

        /**
         * Updates the shipping methods with an AJAX call.
         *
         * @return {Object} The module.
         */
        function reload() {
            var shippingAddress = Quote.shippingAddress();
            var payload = JSON.stringify({
                address: {
                    'street': shippingAddress.street,
                    'city': shippingAddress.city,
                    'region_id': shippingAddress.regionId,
                    'region': shippingAddress.region,
                    'country_id': shippingAddress.countryId,
                    'postcode': shippingAddress.postcode,
                    'email': shippingAddress.email,
                    'customer_id': shippingAddress.customerId,
                    'firstname': shippingAddress.firstname,
                    'lastname': shippingAddress.lastname,
                    'middlename': shippingAddress.middlename,
                    'prefix': shippingAddress.prefix,
                    'suffix': shippingAddress.suffix,
                    'vat_id': shippingAddress.vatId,
                    'company': shippingAddress.company,
                    'telephone': shippingAddress.telephone,
                    'fax': shippingAddress.fax,
                    'custom_attributes': shippingAddress.customAttributes,
                    'save_in_address_book': shippingAddress.saveInAddressBook
                }
            });

            var dummy = AJAX_CHAIN.addDummy();

            AJAX_CHAIN.run();

            Storage.post(
                ResourceUrlManager.getUrlForEstimationShippingMethodsForNewAddress(Quote),
                payload,
                false
            ).done(
                function (result) {
                    RateRegistry.set(shippingAddress.getKey(), result);
                    ShippingService.setShippingRates(result);
                }
            ).fail(
                function (response) {
                    ShippingService.setShippingRates([]);
                }
            ).always(
                function () {
                    dummy.getCall().resolve();
                    ShippingService.isLoading(false);
                }
            );

            return EXPORT;
        }

        /**
         * Says whether the customer has chosen a shipping method.
         *
         * @returns {boolean}
         */
        function hasChosenMethod() {
            return Quote.shippingMethod() !== null;
        }

        EXPORT.ajaxChain = AJAX_CHAIN;
        EXPORT.reload = reload;
        EXPORT.hasChosenMethod = hasChosenMethod;

        return Object.freeze(EXPORT);
    }
);
