/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [],
    function () {
        /**
         * @exports Resursbank_Checkout/js/lib/channel
         * @type {function}
         */
        var EXPORT;

        /**
         * @constructor
         * @property {Function} hasEvent
         * @property {Function} getList
         * @property {Function} emit
         * @property {Function} on
         * @property {Function} onAll
         * @property {Function} ignore
         * @property {Function} destroy
         */
        function Channel() {
            /**
             * Placeholder for [this].
             *
             * @private
             * @readonly
             * @type {Channel}
             */
            var me = this;

            /**
             * A collection of callback functions. The callbacks will be stored
             * in arrays, each array will be stored as a property.
             *
             * @private
             * @type {object}
             */
            var LISTENERS = {};

            /**
             * A callback function that will be fired for any emitted event.
             *
             * @private
             * @type {function}
             */
            var GLOBAL_LISTENER = null;

            /**
             * Checks if a list of listeners has been created for the given
             * name.
             *
             * @public
             * @param {string} name
             * @return {boolean}
             */
            function hasEvent(name) {
                return LISTENERS.hasOwnProperty(name);
            }

            /**
             * Returns the list of events for the given name. If a list can't
             * be found, it returns an empty array instead.
             *
             * @private
             * @param {string} evName
             * @return {array}
             */
            function getList(evName) {
                return hasEvent(evName) ? LISTENERS[evName] : [];
            }

            /**
             * Fires all callbacks registered to an event and passes in the
             * optional data to each callback. If there is a global listener,
             * it will be fired first.
             *
             * @public
             * @param {string} evName
             * @param {*} [data] - Optional.
             * @return {Channel} Returns itself.
             */
            function emit(evName, data) {
                if (typeof GLOBAL_LISTENER === 'function') {
                    GLOBAL_LISTENER(evName, data);
                }

                getList(evName).forEach(function (listenerFn) {
                    listenerFn(data);
                });

                return me;
            }

            /**
             * Registers a listener callback for the given event. The callback
             * function can take one argument which is some optional data that
             * can be sent to it when the event is later emitted.
             *
             * @public
             * @param {string} evName
             * @param {function} fn
             * @return {Channel} Returns itself.
             */
            function on(evName, fn) {
                if (typeof evName === 'string' && hasEvent(evName) === false) {
                    LISTENERS[evName] = [];
                }

                LISTENERS[evName].push(fn);

                return me;
            }

            /**
             * Registers a global listener. The listener will be fired for
             * every emitted event. It takes the same argument(s) as a normal
             * listener.
             *
             * @public
             * @param {function} fn
             * @return {Channel} Returns itself.
             */
            function onAll(fn) {
                if (typeof fn === 'function') {
                    GLOBAL_LISTENER = fn;
                }

                return me;
            }

            /**
             * Removes listeners. If you pass in a name and a function, it will
             * try to remove that specific listener. If you pass in only an
             * event, it will remove all listeners for that event.
             *
             * @public
             * @param {string} evName
             * @param {function} [fn] - Optional.
             * @return {Channel} Returns itself.
             */
            function ignore(evName, fn) {
                var fnIndex;
                var list;

                if (typeof evName === 'string' && hasEvent(evName) === true) {
                    if (typeof fn === 'function') {
                        list = getList(evName);
                        fnIndex = list.indexOf(fn);

                        if (fnIndex !== -1) {
                            list.splice(fnIndex, 1);
                        }
                    } else {
                        LISTENERS[evName] = [];
                    }
                }

                return me;
            }

            /**
             * Destroys the instance.
             *
             * @public
             */
            function destroy() {
                LISTENERS = null;
                GLOBAL_LISTENER = null;
                me = null;
            }

            /**
             * Initialization function.
             *
             * @returns {Channel}
             */
            function init() {
                me.on = on;
                me.emit = emit;
                me.ignore = ignore;
                me.onAll = onAll;
                me.hasEvent = hasEvent;
                me.destroy = destroy;

                return me;
            }

            Object.freeze(init());
        }

        EXPORT = Channel;

        return Object.freeze(EXPORT);
    }
);
