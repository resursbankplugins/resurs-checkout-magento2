/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [],
    function () {
        'use strict';

        /**
         * The module singleton.
         *
         * @exports Resursbank_Checkout/js/lib/server
         * @constant {object}
         */
        var EXPORT = {};

        /**
         * The beginning the website's URL.
         *
         * @readonly
         * @type {string}
         */
        var BASE_URL = '';

        /**
         * The module's URL namespace.
         *
         * @readonly
         * @type {string}
         */
        var MODULE_URL = '';

        /**
         * The form key.
         *
         * @readonly
         * @type {string}
         */
        var FORM_KEY = '';

        /**
         * URL to send the customer to when a purchase failed.
         *
         * @readonly
         * @type {string}
         */
        var FAILURE_URL = '';

        /**
         * URL to send the customer to when a purchase was successful.
         *
         * @readonly
         * @type {string}
         */
        var SUCCESS_URL = '';

        /**
         * States whether the module has been initialized.
         *
         * @type {boolean}
         */
        var INITIALIZED = false;

        /**
         * Says whether the module has been initialized.
         *
         * @returns {boolean}
         */
        function isInitialized() {
            return INITIALIZED;
        }

        /**
         * Creates a url with the baseUrl as a starting point. The "path"
         * argument should not start with a "/".
         *
         * @param {string} path
         * @returns {string}
         */
        function createUrl(path) {
            return BASE_URL + (MODULE_URL === '' ? '' : MODULE_URL + '/') + path;
        }

        /**
         * Returns the form key.
         *
         * @returns {string}
         */
        function getFormKey() {
            return FORM_KEY;
        }

        /**
         * Returns the failure URL.
         *
         * @returns {string}
         */
        function getFailureUrl() {
            return FAILURE_URL;
        }

        /**
         * Returns the success URL.
         *
         * @returns {string}
         */
        function getSuccessUrl() {
            return SUCCESS_URL;
        }

        /**
         * Returns the base URL.
         *
         * @returns {string}
         */
        function getBaseUrl() {
            return BASE_URL;
        }

        /**
         * Initializer function.
         *
         * @param {object} config
         * @param {string} config.baseUrl
         * @param {string} config.formKey
         * @param {string} config.failureUrl
         * @param {string} config.successUrl
         * @param {string} [config.moduleUrl]
         * @returns {object} The module singleton.
         */
        function init(config) {
            if (INITIALIZED === false) {
                BASE_URL = config.baseUrl;
                FORM_KEY = config.formKey;
                FAILURE_URL = config.failureUrl;
                SUCCESS_URL = config.successUrl;
                MODULE_URL = typeof config.moduleUrl === 'string'
                    ? config.moduleUrl
                    : '';

                INITIALIZED = true;
            }

            return EXPORT;
        }

        EXPORT.init = init;
        EXPORT.isInitialized = isInitialized;
        EXPORT.createUrl = createUrl;
        EXPORT.getFormKey = getFormKey;
        EXPORT.getFailureUrl = getFailureUrl;
        EXPORT.getSuccessUrl = getSuccessUrl;
        EXPORT.getBaseUrl = getBaseUrl;

        return Object.freeze(EXPORT);
    }
);
