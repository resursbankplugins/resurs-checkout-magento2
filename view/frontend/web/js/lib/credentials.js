/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'Magento_Checkout/js/model/quote',
    ],
    function (Quote) {
        'use strict';

        /**
         * The module singleton.
         *
         * @exports Resursbank_Checkout/js/lib/credentials
         * @constant {object}
         * @readonly
         */
        var EXPORT = {};

        /**
         * Check if address fetching is allowed based on provided country.
         *
         * @param {string} country
         * @returns {boolean}
         */
        function isCountryAllowed(country) {
            return isSweden(country) ||
                isNorway(country) ||
                isFinland(country);
        }

        /**
         * Check for the Swedish country id.
         *
         * @param {string} country
         * @returns {boolean}
         */
        function isSweden(country) {
            return country === 'SE';
        }

        /**
         * Check for the Norwegian country id.
         *
         * @param {string} country
         * @returns {boolean}
         */
        function isNorway(country) {
            return country === 'NO';
        }

        /**
         * Check for the Finnish country id.
         *
         * @param {string} country
         * @returns {boolean}
         */
        function isFinland(country) {
            return country === 'FI';
        }

        /**
         * Validates an ssn or organization number.
         *
         * NOTE: The if-else is as intended to support the nestled business
         * logic.
         *
         * NOTE: Some countries do not require an SSN.
         *
         * @param {string} idNum
         * @param {string} country
         * @param {string} customerType
         * @returns {boolean}
         */
        function validate(idNum, country, customerType) {
            var result = false;

            if (idNum !== '') {
                if (isCountryAllowed(country)) {
                    if (customerType === 'LEGAL') {
                        result = validateOrg(idNum, country);
                    } else if (customerType === 'NATURAL') {
                        result = validateSsn(idNum, country);
                    }
                }
            } else {
                result = true;
            }

            return result;
        }

        /**
         * Validates an SSN.
         *
         * NOTE: Validates for Sweden, Norway and Finland ONLY.
         *
         * @param {string} ssn
         * @param {string} country
         * @returns {boolean}
         */
        function validateSsn(ssn, country) {
            var result = false;
            var norway = /^([0][1-9]|[1-2][0-9]|3[0-1])(0[1-9]|1[0-2])(\d{2})(\-)?([\d]{5})$/;
            var finland = /^([\d]{6})[\+-A]([\d]{3})([0123456789ABCDEFHJKLMNPRSTUVWXY])$/;
            var sweden = /^(18\d{2}|19\d{2}|20\d{2}|\d{2})(0[1-9]|1[0-2])([0][1-9]|[1-2][0-9]|3[0-1])(\-|\+)?([\d]{4})$/;

            if (isSweden(country)) {
                result = sweden.test(ssn);
            } else if (isNorway(country)) {
                result = norway.test(ssn);
            } else if (isFinland(country)) {
                result = finland.test(ssn);
            }

            return result;
        }

        /**
         * Validates an organisation number.
         *
         * NOTE: Validates for Sweden, Norway and Finland ONLY.
         *
         * @param {string} org
         * @param {string} country
         * @returns {boolean}
         */
        function validateOrg(org, country) {
            var result = false;
            var finland = /^((\d{7})(\-)?\d)$/;
            var sweden = /^(16\d{2}|18\d{2}|19\d{2}|20\d{2}|\d{2})(\d{2})(\d{2})(\-|\+)?([\d]{4})$/;
            var norway = /^([89]([ |-]?[0-9]){8})$/;

            if (isSweden(country)) {
                result = sweden.test(org);
            } else if (isNorway(country)) {
                result = norway.test(org);
            } else if (isFinland(country)) {
                result = finland.test(org);
            }

            return result;
        }

        /**
         * Validates a card number.
         *
         * @param {string} num
         * @returns {boolean}
         */
        function validateCard(num) {
            return num === '' ||
                /^([1-9][0-9]{3}[ ]{0,1}[0-9]{4}[ ]{0,1}[0-9]{4}[ ]{0,1}[0-9]{4})$/.test(num);
        }

        /**
         * Validates a phone number based on a country.
         *
         * @param {string} num
         * @param {string} country
         * @returns {boolean}
         */
        function validatePhone(num, country) {
            return (isSweden(country) && validatePhoneSweden(num)) ||
                (isNorway(country) && validatePhoneNorway(num)) ||
                (isFinland(country) && validatePhoneFinland(num));
        }

        /**
         * Validates a Swedish phone number.
         *
         * @param {string} num
         * @returns {boolean}
         */
        function validatePhoneSweden(num) {
            return /^(0|\+46|0046)[ |-]?(200|20|70|73|76|74|[1-9][0-9]{0,2})([ |-]?[0-9]){5,8}$/.test(num);
        }

        /**
         * Validates a Norwegian phone number.
         *
         * @param {string} num
         * @returns {boolean}
         */
        function validatePhoneNorway(num) {
            return /^(\+47|0047|)?[ |-]?[2-9]([ |-]?[0-9]){7,7}$/.test(num);
        }

        /**
         * Validates a Finnish phone number.
         *
         * @param {string} num
         * @returns {boolean}
         */
        function validatePhoneFinland(num) {
            return /^((\+358|00358|0)[-| ]?(1[1-9]|[2-9]|[1][0][1-9]|201|2021|[2][0][2][4-9]|[2][0][3-8]|29|[3][0][1-9]|71|73|[7][5][0][0][3-9]|[7][5][3][0][3-9]|[7][5][3][2][3-9]|[7][5][7][5][3-9]|[7][5][9][8][3-9]|[5][0][0-9]{0,2}|[4][0-9]{1,3})([-| ]?[0-9]){3,10})?$/.test(num);
        }

        EXPORT.isCountryAllowed = isCountryAllowed;
        EXPORT.isSweden = isSweden;
        EXPORT.isNorway = isNorway;
        EXPORT.isFinland = isFinland;
        EXPORT.validate = validate;
        EXPORT.validateSsn = validateSsn;
        EXPORT.validateOrg = validateOrg;
        EXPORT.validateCard = validateCard;
        EXPORT.validatePhone = validatePhone;
        EXPORT.validatePhoneSweden = validatePhoneSweden;
        EXPORT.validatePhoneNorway = validatePhoneNorway;
        EXPORT.validatePhoneFinland = validatePhoneFinland;

        return Object.freeze(EXPORT);
    }
);
