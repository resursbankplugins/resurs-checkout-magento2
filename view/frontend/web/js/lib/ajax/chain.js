/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery',
        'Resursbank_Checkout/js/lib/ajax/call',
        'Resursbank_Checkout/js/lib/ajax/dummy'
    ],
    function ($, Call, Dummy) {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/lib/ajax/chain
         * @type {function}
         */
        var EXPORT;

        /**
         * Chain constructor.
         *
         * @constructor
         * @property {Function} run
         * @property {Function} add
         * @property {Function} addDummy
         * @property {Function} stop
         * @property {Function} remove
         * @property {Function} getRunning
         * @property {Function} getLimit
         * @property {Function} atLimit
         * @property {Function} block
         * @property {Function} unblock
         * @property {Function} isEmpty
         * @property {Function} isQueueEmpty
         * @property {Function} areSubordinatesEmpty
         * @property {Function} addSubordinate
         * @property {Function} onEmpty
         * @property {Function} onSubsEmpty
         * @property {Function} getQueue
         * @property {Function} removeSubordinate
         * @property {Function} destroy
         * @param {object} config
         * @param {number} [config.limit]
         */
        function Chain(config) {
            /**
             * Placeholder for [this].
             *
             * @private
             * @readonly
             * @type {Chain}
             */
            var me = this;

            /**
             * The number of calls that are allowed to be queued.
             *
             * @private
             * @readonly
             * @type {number}
             */
            var LIMIT;

            /**
             * Name of the chain. Can be used when debugging.
             *
             * @type {string}
             */
            var NAME = '';

            /**
             * States whether the chain is firing AJAX requests.
             *
             * @private
             * @type {boolean}
             */
            var RUNNING = false;

            /**
             * List of AJAX requests.
             *
             * @private
             * @type {Call[]|Dummy[]}
             */
            var QUEUE = [];

            /**
             * Subordinate chains.
             *
             * @private
             * @type {object}
             */
            var SUB_CHAINS = {};

            /**
             * Callbacks that will fire when the queue and all subordinates are
             * empty of calls.
             *
             * @private
             * @type {Function[]}
             */
            var ON_EMPTY_CBS = [];

            /**
             * Currently processed request.
             *
             * @type {null|Call|Dummy}
             */
            var PROCESSING = null;

            /**
             * Whether the chain can add calls or not.
             *
             * @type {boolean}
             */
            var BLOCKED = false;

            /**
             * Fires the callbacks meant for when the queue and all subordinates
             * are empty of calls.
             */
            function fireOnEmptyCallbacks() {
                ON_EMPTY_CBS.forEach(function(cb) {
                    cb();
                });
            }

            /**
             * Takes a callback function that will run when the chain is empty
             * of calls.
             *
             * @public
             * @param {Function} callback
             * @returns {Chain} The instance.
             */
            function onEmpty(callback) {
                ON_EMPTY_CBS.push(callback);

                getSubordinatesArray().forEach(function(chain) {
                    chain.onEmpty(function() {
                        if (isEmpty() === true) {
                            fireOnEmptyCallbacks();
                        }
                    });
                });

                return me;
            }

            /**
             * Checks if there are any calls left in the queue, and if so,
             * fires the next call. If there aren't any left, it will mark the
             * chain as stopped.
             *
             * @private
             */
            function fireNext() {
                PROCESSING = null;

                if (RUNNING === true) {
                    if (QUEUE.length > 0 ) {
                        fire(QUEUE.shift());
                    } else {
                        stop();

                        if (isEmpty() === true) {
                            fireOnEmptyCallbacks();
                        }
                    }
                }
            }

            /**
             * Takes a call and creates an AJAX request from it.
             *
             * @private
             * @param {Call | Dummy} call
             * @returns {object} The AJAX request.
             */
            function fire(call) {
                var jqXHR;

                if (call instanceof Call) {
                    PROCESSING = call;
                    jqXHR = call.fire();
                    jqXHR.always(fireNext);
                } else if (call instanceof Dummy) {
                    PROCESSING = call;
                    call.fire();
                    call.getCall().always(fireNext);
                }

                return jqXHR;
            }

            /**
             * Puts the chain in the "running" state, where it will fire all
             * calls in the queue until it is empty.
             *
             * @public
             * @returns {Chain}
             */
            function run() {
                if (RUNNING === false && QUEUE.length > 0) {
                    RUNNING = true;
                    fire(QUEUE.shift());
                }

                return me;
            }

            /**
             * Stops the chain from firing calls if the chain is running.
             *
             * @private
             * @returns {Chain}
             */
            function stop() {
                RUNNING = false;

                return me;
            }

            /**
             * Adds a call to the queue.
             *
             * @public
             * @param {Call} call
             * @returns {Chain}
             */
            function add(call) {
                if (BLOCKED === false && call instanceof Call) {
                    if (LIMIT > 0 && QUEUE.length === LIMIT) {
                        QUEUE.pop().destroy();
                    }

                    QUEUE.push(call);
                }

                return me;
            }

            /**
             * Creates and adds a dummy call to the queue, and then returns the
             * dummy call back to the user.
             *
             * Dummy calls are deferred objects that won't create a request when
             * fired, and so the queue can only advance once the deferred object
             * has been resolved or rejected. Check JQuery's documentation for
             * more information about deferred objects if you're unfamiliar with
             * them.
             *
             * @public
             * @param {Dummy} [call] - Optional. If left out, a new dummy will
             * be created and returned.
             * @returns {Dummy}
             */
            function addDummy(call) {
                var dummy = call instanceof Dummy ? call : new Dummy();

                if (BLOCKED === false) {
                    if (LIMIT > 0 && QUEUE.length === LIMIT) {
                        QUEUE.pop().destroy();
                    }

                    QUEUE.push(dummy);
                }

                return call instanceof Dummy ? call : dummy;
            }

            /**
             * Adds a chain as a subordinate. Subordinates stay in the chain
             * and can be locked (prevented from adding new calls) and unlocked
             * (allowed to add calls again.)
             *
             * @public
             * @param {string} name
             * @param {Chain} chain
             * @returns {Chain}
             */
            function addSubordinate(name, chain) {
                if (SUB_CHAINS.hasOwnProperty(name) === false) {
                    SUB_CHAINS[name] = chain;

                    if (BLOCKED === true) {
                        SUB_CHAINS[name].block();
                    }
                } else {
                    throw new Error('Subordinate ' + name + ' already exists.');
                }

                return me;
            }

            /**
             * Removes a specific call from the queue, and returns that call
             * or undefined if it can't be found.
             *
             * @public
             * @param {Call} call
             * @returns {Call | undefined}
             */
            function remove(call) {
                var index;
                var retValue;

                if (call instanceof Call) {
                    index = QUEUE.indexOf(call);

                    if (index !== -1) {
                        retValue = QUEUE.splice(index, 1);
                    }
                }

                return retValue;
            }

            /**
             * Removes a subordinate.
             *
             * @public
             * @param {string} id
             */
            function removeSubordinate(id) {
                if (SUB_CHAINS.hasOwnProperty(id)) {
                    SUB_CHAINS[id] = null;
                    delete SUB_CHAINS[id];
                }
            }

            /**
             * States whether the chain is running.
             *
             * @public
             * @returns {boolean}
             */
            function getRunning() {
                return RUNNING;
            }

            /**
             * Returns the queue limit.
             *
             * @public
             * @returns {number}
             */
            function getLimit() {
                return LIMIT;
            }

            /**
             * States whether the queue is empty.
             *
             * @public
             * @returns {boolean}
             */
            function isEmpty() {
                return PROCESSING === null && isQueueEmpty() && areSubordinatesEmpty();
            }

            /**
             * Says whether the queue is empty or not.
             *
             * @public
             * @returns {boolean}
             */
            function isQueueEmpty() {
                return QUEUE.length === 0;
            }

            /**
             * Says whether the subordinates are empty of calls. Every
             * subordinate is checked, and their subordinates as well.
             *
             * @public
             * @returns {boolean}
             */
            function areSubordinatesEmpty() {
                return getSubordinatesArray().every(function(chain) {
                    return chain.isEmpty();
                });
            }

            /**
             * Returns the subordinate list as an array.
             *
             * @private
             * @returns {Chain[]}
             */
            function getSubordinatesArray() {
                return Object.keys(SUB_CHAINS).map(function(key) {
                    return SUB_CHAINS[key];
                });
            }

            /**
             * Checks if the queue has reached its limit.
             *
             * @public
             * @returns {boolean}
             */
            function atLimit() {
                return QUEUE.length === LIMIT;
            }

            /**
             * Blocks the chain from adding more calls.
             *
             * @returns {Chain}
             */
            function block() {
                BLOCKED = true;

                getSubordinatesArray().forEach(function(chain) {
                    chain.block();
                });

                return me;
            }

            /**
             * Unblocks the chain, allowing it to add more calls.
             *
             * @returns {Chain}
             */
            function unblock() {
                BLOCKED = false;

                getSubordinatesArray().forEach(function(chain) {
                    chain.unblock();
                });

                return me;
            }

            /**
             * Destroys the instance.
             */
            function destroy() {
                LIMIT = null;
                RUNNING = null;
                QUEUE = null;
                me = null;
            }

            /**
             * Returns an array with objects, where each object represents a
             * chain and will contain its queue and might contain its name,
             * alias or both.
             *
             * @param {boolean} includeSubChains - Include the queues of all
             * sub-chains in the returned result.
             *
             * @param {string} alias - Substitutes a chains name for another.
             * Useful if a chain was constructed without a name. Will be put
             * in a "alias" property on the object.
             *
             * @returns {array}
             */
            function getQueue(includeSubChains, alias) {
                var qs = [];
                var obj = {queue: QUEUE};

                if (typeof alias === 'string' && alias !== '') {
                    obj.alias = alias;
                }

                if (NAME !== '') {
                    obj.name = NAME;
                }

                qs.push(obj);

                Object.keys(SUB_CHAINS).forEach(function(key) {
                    qs.push(SUB_CHAINS[key].getQueue(includeSubChains, key));
                });

                return qs;
            }

            /**
             * Initialization function.
             *
             * @returns {Chain}
             */
            function init() {
                if (config.hasOwnProperty('limit') &&
                    typeof config.limit === 'number'
                ) {
                    LIMIT = config.limit;
                } else {
                    LIMIT = 0;
                }

                if (config.hasOwnProperty('name') &&
                    typeof config.name === 'string'
                ) {
                    NAME = config.name;
                }

                me.run = run;
                me.add = add;
                me.addDummy = addDummy;
                me.stop = stop;
                me.remove = remove;
                me.getRunning = getRunning;
                me.getLimit = getLimit;
                me.isEmpty = isEmpty;
                me.isQueueEmpty = isQueueEmpty;
                me.areSubordinatesEmpty = areSubordinatesEmpty;
                me.addSubordinate = addSubordinate;
                me.removeSubordinate = removeSubordinate;
                me.getQueue = getQueue;
                me.onEmpty = onEmpty;
                me.atLimit = atLimit;
                me.block = block;
                me.unblock = unblock;
                me.destroy = destroy;

                return me;
            }

            Object.freeze(init());
        }

        EXPORT = Chain;

        return Object.freeze(EXPORT);
    }
);
