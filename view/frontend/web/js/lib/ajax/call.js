/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery'
    ],
    function ($) {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/lib/ajax/call
         * @type {function}
         */
        var EXPORT;

        /**
         * Call constructor. 
         *
         * @class
         * @property {function} fire
         * @property {function} getStatus
         * @property {function} onFired
         * @property {function} onDone
         * @property {function} onFail
         * @property {function} onAlways
         * @property {function} getRequest
         * @property {function} destroy
         * @param {object} config
         * @param {object} config.options
         */
        function Call(config) {
            /**
             * Placeholder for [this].
             *
             * @private
             * @readonly
             * @type {Call}
             */
            var me = this;

            /**
             * The object that will be passed to the AJAX function. That would
             * be the $.ajax() function in the case of JQuery.
             *
             * @private
             * @readonly
             * @type {object}
             */
            var OPTIONS;

            /**
             * If the call has been fired.
             *
             * @private
             * @type {boolean}
             */
            var FIRED = false;

            /**
             * An array of callbacks that will run when the AJAX request has
             * succeeded.
             *
             * @private
             * @type {Function[]}
             */
            var ON_DONE_CBS = [];

            /**
             * An array of callbacks that will run when the AJAX request has
             * failed.
             *
             * @private
             * @type {Function[]}
             */
            var ON_FAIL_CBS = [];

            /**
             * An array of callbacks that will run when the AJAX request has
             * completed.
             *
             * @private
             * @type {Function[]}
             */
            var ON_ALWAYS_CBS = [];

            /**
             * A callback that will run when the call has been fired.
             *
             * @private 
             * @type {Function}
             */
            var ON_FIRED_CB;

            /**
             * The AJAX request object.
             *
             * @private
             * @type {object}
             */
            var JQ_XHR;

            /**
             * Takes a list of callbacks and returns a function that will loop
             * through the list and call each of them.
             * 
             * @param {Function[]} cbs
             * @returns {Function}
             */
            function fireCallbacks(cbs) {
                return function() {
                    var args = arguments;

                    cbs.forEach(function(cb) {
                        cb.apply(null, args);
                    });
                }
            }

            /**
             * Fires the call.
             *
             * @returns {object} The AJAX request object.
             */
            function fire() {
                if (FIRED === false) {
                    JQ_XHR = $.ajax(OPTIONS)
                        .done(fireCallbacks(ON_DONE_CBS))
                        .fail(fireCallbacks(ON_FAIL_CBS))
                        .always(fireCallbacks(ON_ALWAYS_CBS));

                    FIRED = true;

                    if (typeof ON_FIRED_CB === 'function') {
                        ON_FIRED_CB();
                    }
                }

                return JQ_XHR;
            }

            /**
             * States whether the call has been fired.
             *
             * @public
             * @returns {boolean}
             */
            function getStatus() {
                return FIRED;
            }

            /**
             * Takes a callback function that will run when the call has been
             * fired.
             *
             * @public
             * @param {Function} callback
             * @returns {Call} The instance.
             */
            function onFired(callback) {
                if (typeof callback === 'function') {
                    ON_FIRED_CB = callback;
                }

                return me;
            }

            /**
             * Takes a callback function that will run when the call has
             * succeeded.
             *
             * @public
             * @param {Function} callback
             * @returns {Call} The instance.
             */
            function onDone (callback) {
                if (typeof JQ_XHR === 'undefined') {
                    ON_DONE_CBS.push(callback);
                } else {
                    JQ_XHR.done(callback);
                }

                return me;
            }

            /**
             * Takes a callback function that will run when the call has
             * failed.
             *
             * @public
             * @param {Function} callback
             * @returns {Call} The instance.
             */
            function onFail (callback) {
                if (typeof JQ_XHR === 'undefined') {
                    ON_FAIL_CBS.push(callback);
                } else if (typeof JQ_XHR !== 'undefined') {
                    JQ_XHR.fail(callback);
                }

                return me;
            }

            /**
             * Takes a callback function that will run when the call has
             * completed.
             *
             * @public
             * @param {Function} callback
             * @returns {Call} The instance.
             */
            function onAlways (callback) {
                if (typeof JQ_XHR === 'undefined') {
                    ON_ALWAYS_CBS.push(callback);
                } else if (typeof JQ_XHR !== 'undefined') {
                    JQ_XHR.always(callback);
                }

                return me;
            }

            /**
             * Returns the AJAX request, or undefined if the call hasn't been
             * fired.
             *
             * @public
             * @returns {object | undefined}
             */
            function getRequest() {
                return JQ_XHR;
            }

            /**
             * Destroys the instance.
             *
             * @public
             */
            function destroy() {
                OPTIONS = null;
                FIRED = null;
                ON_DONE_CBS = null;
                ON_FAIL_CBS = null;
                ON_ALWAYS_CBS = null;
                ON_FIRED_CB = null;
                JQ_XHR = null;
                me = null;
            }

            /**
             * Initialization function.
             *
             * @private
             * @returns {Call} The instance.
             */
            function init() {
                OPTIONS = config.options;

                me.fire = fire;
                me.getStatus = getStatus;
                me.getRequest = getRequest;
                me.onDone = onDone;
                me.onFail = onFail;
                me.onAlways = onAlways;
                me.onFired = onFired;
                me.destroy = destroy;

                return me;
            }

            Object.freeze(init());
        }

        EXPORT = Call;

        return Object.freeze(EXPORT);
    }
);
