/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery'
    ],
    function ($) {
        /**
         * @exports Resursbank_Checkout/js/lib/ajax/dummy
         * @type {function}
         */
        var EXPORT;

        /**
         * Dummy constructor.
         *
         * @constructor
         * @property {Function} getCall
         * @property {Function} onFire
         * @property {Function} fire
         * @property {Function} hasOnFireCallbacks
         * @property {Function} destroy
         */
        function Dummy() {
            /**
             * Placeholder for [this].
             *
             * @private
             * @readonly
             * @type {Dummy}
             */
            var me = this;

            /**
             * The deferred object.
             *
             * @private
             * @readonly
             * @type {object}
             */
            var CALL;

            /**
             * Whether the dummy has been fired.
             *
             * @private
             * @type {boolean}
             */
            var FIRED = false;

            /**
             * Callbacks that will run when the dummy has been fired.
             *
             * @private
             * @type {Array}
             */
            var ON_FIRED_CBS = [];

            /**
             * Returns the deferred object.
             *
             * @public
             * @returns {object}
             */
            function getCall() {
                return CALL;
            }

            /**
             * Adds a callback that will run when the dummy has been fired.
             *
             * @public
             * @param {Function} fn
             * @returns {Dummy} The instance.
             */
            function onFire(fn) {
                if (typeof fn === 'function') {
                    ON_FIRED_CBS.push(fn);
                }

                return me;
            }

            /**
             * Puts the dummy is the "fired" state.
             *
             * NOTE: This does not resolve() or reject() the dummy!
             *
             * @public
             * @returns {Dummy}
             */
            function fire() {
                if (FIRED === false) {
                    if (hasOnFireCallbacks()) {
                        ON_FIRED_CBS.forEach(function (fn) {
                            fn(CALL, me);
                        });
                    }

                    FIRED = true;
                }

                return me;
            }

            /**
             * Says whether the dummy has any callbacks that will run when it's
             * fired.
             *
             * @public
             * @returns {boolean}
             */
            function hasOnFireCallbacks() {
                return ON_FIRED_CBS.length > 0;
            }

            /**
             * Destroys the instance.
             *
             * @public
             */
            function destroy() {
                CALL = null;
                ON_FIRED_CBS = null;
                me = null;
            }

            /**
             * Initialization function.
             *
             * @returns {Dummy}
             */
            function init() {
                CALL = $.Deferred();

                me.getCall = getCall;
                me.onFire = onFire;
                me.fire = fire;
                me.hasOnFireCallbacks = hasOnFireCallbacks;
                me.destroy = destroy;

                return me;
            }

            Object.freeze(init());
        }

        EXPORT = Dummy;

        return Object.freeze(EXPORT);
    }
);
