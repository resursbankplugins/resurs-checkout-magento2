/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @namespace AjaxQ
 */

/**
 * @typedef {object} AjaxQ
 * @property {Call} Call
 * @property {Chain} Chain
 * @property {Dummy} Dummy
 */

define(
    [
        'Resursbank_Checkout/js/lib/ajax/call',
        'Resursbank_Checkout/js/lib/ajax/chain',
        'Resursbank_Checkout/js/lib/ajax/dummy',
        'Resursbank_Checkout/js/lib/ajax/response',
        'Resursbank_Checkout/js/lib/ajax/request'
    ],
    function (
        Call,
        Chain,
        Dummy,
        Response,
        Request
    ) {
        /**
         * @constant {object}
         * @exports Resursbank_Checkout/js/lib/ajax
         */
        var EXPORT = {};

        /**
         * Takes an AJAX response object, analyses it, and returns the proper
         * response instance based on its data.
         *
         * @param {object} response - AJAX response object.
         * @returns {Success|Flawed}
         */
        function getStatus(response) {
            var result;

            if (response.hasOwnProperty('message') &&
                response.message.hasOwnProperty('error') &&
                typeof response.message.error === 'string'
            ) {
                result = new Response.Flawed(response);
            } else {
                result = new Response.Success(response);
            }

            return result;
        }

        EXPORT.Chain = Chain;
        EXPORT.Call = Call;
        EXPORT.Dummy = Dummy;
        EXPORT.Response = Response;
        EXPORT.Request = Request;
        EXPORT.getStatus = getStatus;

        return Object.freeze(EXPORT);
    }
);
