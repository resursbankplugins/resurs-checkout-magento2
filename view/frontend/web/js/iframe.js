/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'require',
        'jquery',
        'Resursbank_Checkout/js/lib/channel',
        'Resursbank_Checkout/js/oc-shop'
    ],
    function(
        // Magento
        require,
        $,

        // Resurs Bank
        Channel,
        iFrameResizer
    ) {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/payment-method
         * @constant {object}
         */
        var EXPORT = {};

        /**
         * An event channel.
         *
         * @type {Channel}
         */
        var CHANNEL = new Channel();

        /**
         * Whether the module has been inititalized.
         *
         * @type {boolean}
         * @readonly
         */
        var INITIALIZED = false;

        /**
         * Iframe element.
         *
         * @type {HTMLIFrameElement}
         * @readonly
         */
        var IFRAME;

        /**
         * The iframe's URL.
         *
         * @type {string}
         * @readonly
         */
        var URL;

        /**
         * Says whether the module has been initialized.
         *
         * @returns {boolean}
         */
        function isInitialized() {
            return INITIALIZED;
        }

        /**
         * Receives and delegates messages from the ResursbankCheckout iframe
         * when it uses window.postMessage(). This function expects event.data
         * to be a string which is an JSON encoded object. The string is then
         * parsed with JSON and will then check for an data.eventType property,
         * which is also a string and tells the delegator how to handle the
         * message. If this data.eventType does not exists, the message will
         * be ignored.
         *
         * NOTE: The event argument is an object with information about the
         * message, where it came from and such. The data that was passed along
         * with the message will be under a event.data property.
         *
         * @param event
         */
        function postMessageDelegator(event) {
            var data;
            var origin = event.origin || event.originalEvent.origin;

            if (origin !== URL ||
                typeof event.data !== 'string' ||
                event.data === '[iFrameResizerChild]Ready'
            ) {
                return;
            }

            // Calls made by the iFrameResizer are picked up by this function
            // and needs to be parsed. Calls made by ResursbankCheckout however
            // does not require parsing.
            var jsonTest = {};

            try {
                jsonTest = JSON.parse(event.data);
            } catch (e) {

            }

            data = jsonTest;

            if (data.hasOwnProperty('eventType') &&
                typeof data.eventType === 'string'
            ) {
                switch (data.eventType) {
                    case 'checkout:user-info-change':
                        CHANNEL.emit('user-info-change', data);
                        break;

                    case 'checkout:payment-method-change':
                        CHANNEL.emit('payment-method-change', data);
                        break;

                    case 'checkout:puchase-button-clicked':
                        CHANNEL.emit('book-order', data);
                        break;

                    case 'checkout:loaded':
                        CHANNEL.emit('loaded');
                        break;

                    case 'checkout:purchase-denied':
                        CHANNEL.emit('purchase-denied');
                        break;

                    case 'checkout:purchase-failed':
                        CHANNEL.emit('purchase-failed');
                        break;
                }
            }
        }

        /**
         * Posts a message to the iframe window with postMessage(). The data
         * argument will be sent to the iframe window and it should have an
         * eventType property set. The eventType property is a string and is
         * used by the receiver to determine how the message and its data
         * should be handled.
         *
         * @param {object} data - Information to be passed to the iframe.
         * @param {string} data.eventType - The event that the receiving end
         * should handle.
         */
        function postMessage(data) {
            var iframeWindow;

            if (IFRAME instanceof HTMLIFrameElement &&
                typeof URL === 'string' &&
                URL !== ''
            ) {
                iframeWindow = IFRAME.contentWindow || IFRAME.contentDocument;
                iframeWindow.postMessage(JSON.stringify(data), URL);
            }
        }

        /**
         * Adds the iframe to the checkout page.
         */
        function addIframe() {
            $('#resursbank-checkout-iframe-container').append(IFRAME);
        }

        /**
         * Initialization function.
         *
         * @param {object} config
         * @param {HTMLIFrameElement} config.iframe
         * @param {string} config.origin
         * @param {string} [config.script] - Optional. External IFrameResizer
         * script path.
         * @returns {object} The module.
         */
        function init(config) {
            if (!INITIALIZED) {
                IFRAME = config.iframe;
                // URL = config.origin;
                URL = config.url;

                // Add message listener.
                window.addEventListener('message', postMessageDelegator, false);

                if (IFRAME instanceof HTMLIFrameElement) {
                    addIframe();

                    // Initialize iframe resizer. It will pick up any iframes
                    // on the page.
                    iFrameResizer(
                        {
                            log: false
                        },
                        IFRAME
                    );
                }

                // If the customer is using an old API version, they will not
                // get the new IFrameResizer, but an old version instead. In
                // that scenario, we need to initialize the resizer. The new
                // version does this automatically and does not give us a
                // function to call when we require the script.
                // if (typeof config.script === 'string') {
                //     require([config.script], function (script) {
                //         if (typeof script === 'function') {
                //             script({log: false}, IFRAME);
                //         }
                //     });
                // }

                INITIALIZED = true;
            }

            return EXPORT;
        }

        EXPORT.channel = CHANNEL;
        EXPORT.init = init;
        EXPORT.isInitialized = isInitialized;
        EXPORT.postMessage = postMessage;
        EXPORT.postMessageDelegator = postMessageDelegator;

        return Object.freeze(EXPORT);
    }
);
