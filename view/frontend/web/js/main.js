/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery',
        'mage/translate',
        'mage/storage',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/url-builder',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/full-screen-loader',
        'Resursbank_Checkout/js/lib/ajax',
        'Resursbank_Checkout/js/lib/channel',
        'Resursbank_Checkout/js/lib/server',
        'Resursbank_Checkout/js/cart/component',
        'Resursbank_Checkout/js/cart/model',
        'Resursbank_Checkout/js/cart/item/response/remove',
        'Resursbank_Checkout/js/cart/item/response/change',
        'Resursbank_Checkout/js/shipping',
        'Resursbank_Checkout/js/payment-method',
        'Resursbank_Checkout/js/iframe',
        'Resursbank_Checkout/js/order',
        'Resursbank_Checkout/js/address/component',
        'Resursbank_Checkout/js/address/model',
        'Resursbank_Checkout/js/discount/model'
    ],
    function(
        // Magento
        $,
        $t,
        Storage,
        Quote,
        UrlBuilder,
        Customer,
        FullscreenLoader,

        // Resurs Bank
        Ajax,
        Channel,
        Server,
        CartComponent,
        CartModel,
        ItemRemoveResponse,
        ItemChangeResponse,
        Shipping,
        PaymentMethod,
        Iframe,
        Order,
        AddressComponent,
        AddressModel,
        DiscountModel
    ) {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/main
         * @constant {object}
         */
        var EXPORT = {};

        /**
         * @constant {Chain}
         */
        var AJAX_CHAIN = new Ajax.Chain({
            limit: 1,
            name: 'Main'
        });

        /**
         * Whether the module has been inititalized.
         *
         * @type {boolean}
         * @readonly
         */
        var INITIALIZED = false;

        /**
         * Whether the iframe has fully loaded.
         *
         * @type {boolean}
         * @readonly
         */
        var IFRAME_FINALIZED = false;

        /**
         * Whether the order can be booked.
         *
         * @type {boolean}
         */
        var BOOK_ORDER = false;

        /**
         * Says whether the module has been initialized.
         *
         * @returns {boolean}
         */
        function isInitialized() {
            return INITIALIZED;
        }

        /**
         * Redirects users to home page.
         */
        function redirectToHome() {
            window.location = Server.getBaseUrl();
        }

        /**
         * When a purchae gets denied (for example when not creditworthy),
         * try to rebuild the cart and cancel the previous booking.
         */
        function purchaseDenied() {
            console.log('pruchase denied')
            var url = UrlBuilder.createUrl(
                '/resursbank_checkout/cart/deny',
                {}
            );

            Storage.post(url)
                .fail(function () {
                    window.alert($.mage.__(
                        'Failed to rebuild shopping cart. The page will ' +
                        'now reload. Please confirm your cart contents and ' +
                        'supplied address information before proceeding.'
                    ));

                    // window.location = Server.getFailureUrl();
                })
                .done(function () {
                    cancelBooking();
                    Order.stopFullscreenLoader();
                });
        }

        /**
         * When the purchase fails (for example because signing doesn't work),
         * try to rebuild the cart and cancel the booking. If something should
         * fail during this process, reload the checkout page.
         */
        function purchaseFailed() {
            var url = UrlBuilder.createUrl(
                '/resursbank_checkout/cart/rebuild',
                {}
            );

            Storage.post(url)
                .fail(function () {
                    window.alert($.mage.__(
                        'Failed to rebuild shopping cart. The page will ' +
                        'now reload. Please confirm your cart contents and ' +
                        'supplied address information before proceeding.'
                    ));

                    window.location = Server.getFailureUrl();
                })
                .done(function () {
                    cancelBooking();
                    Order.stopFullscreenLoader();
                });
        }

        /**
         * Redirects the user to the order success page.
         *
         * @returns {boolean}
         */
        var redirectToOrderSuccess = function() {
            window.location.replace(Server.getSuccessUrl());
            return false;
        };

        /**
         * Event handler for when an item is removed from the cart.
         *
         * @param {ItemRemoveResponse} response
         */
        function onCartItemRemoved(response) {
            if (response instanceof ItemRemoveResponse) {
                if (response.cartQty === 0) {
                    redirectToHome();
                } else {
                    if (response.reloadShipping) {
                        Shipping.reload();
                    }

                    if (response.reloadDiscount) {
                        if (DiscountModel.isApplied()) {
                            DiscountModel.apply(
                                DiscountModel.discountCode()
                            );
                        }
                    }
                }
            }
        }

        /**
         * Event handler for when item quantity changes.
         *
         * @param {ItemRemoveResponse} response
         */
        function onCartQuantityChanged(response) {
            if (response instanceof ItemChangeResponse) {
                if (response.reloadShipping) {
                    Shipping.reload();
                }

                if (response.reloadDiscount) {
                    if (DiscountModel.isApplied()) {
                        DiscountModel.apply(DiscountModel.discountCode());
                    }
                }
            }
        }

        /**
         * Event handler for order status updates.
         *
         * @param {object} data
         * @param {boolean} data.orderReady
         */
        function onOrderStatusUpdate(data) {
            if (typeof data.orderReady === 'boolean') {
                if (data.orderReady === false) {
                    alert('An error occurred. Please make sure you have entered all your information and try again.');
                    cancelBooking();
                    Order.stopFullscreenLoader();
                } else {
                    finalizeBooking();
                }
            }
        }

        /**
         * Cancels the booking or an order.
         */
        function cancelBooking() {
            Order.cancelBooking();

            BOOK_ORDER = false;
            AJAX_CHAIN.unblock();
        }

        /**
         * Finalizes the booking or an order.
         */
        function finalizeBooking() {
            window.addEventListener('beforeunload', recordGatewayRedirect);
            Order.finalizeBooking();

            BOOK_ORDER = true;
        }

        /**
         * Fire an AJAX call to record payment history event when user is
         * redirected to payment gateway.
         */
        function recordGatewayRedirect() {
            var url = UrlBuilder.createUrl(
                '/resursbank_checkout/cart/log_gateway_redirect',
                {}
            );

            Storage.post(url);
        }

        /**
         * When the iframe is done loading, this method will do some final work
         * in order to make the checkout page and the iframe communicate and
         * work together as intended.
         *
         * NOTE: Should only be called once when the iframe has fully loaded.
         * That is, when it has done all of its AJAX requests and other
         * preparations.
         */
        function finalizeIframeSetup() {
            if (!IFRAME_FINALIZED) {
                // Post the booking rule to ResursbankCheckout. This post
                // basically says that when a user presses the button to
                // finalize the order, check with the server if the order is
                // ready to be booked.
                Iframe.postMessage({
                    eventType: 'checkout:set-purchase-button-interceptor',
                    checkOrderBeforeBooking: true
                });

                // Update the payment session to avoid race conditions where
                // the price is updated before the session is initialized.
                AJAX_CHAIN.add(new Ajax.Call({
                    options: {
                        url: Server.getBaseUrl() + 'resursbank_checkout/cart/updatePaymentSession',
                        method: 'GET'
                    }
                })).run();

                IFRAME_FINALIZED = true;
            }
        }

        /**
         * Event handler for when the iframe wants to book the order.
         */
        function onBookOrder() {
            if (!Quote.isVirtual() && !Shipping.hasChosenMethod()) {
                alert($.mage.__(
                    'Please choose a shipping method before placing order.'
                ));

                cancelBooking();
            } else {
                BOOK_ORDER = true;
                if (AJAX_CHAIN.isEmpty()) {
                    bookOrder();
                }
            }
        }

        /**
         * Create order.
         */
        function bookOrder() {
            AJAX_CHAIN.block();
            Order.bookOrder();
        }

        /**
         * Initialization function.
         *
         * @param {object} config
         * @param {string} config.iframe
         * @param {string} config.origin
         * @param {string} [config.script]
         * @param {string} config.baseUrl
         * @param {string} config.formKey
         * @param {string} config.iframeUrl
         * @param {string} config.failureUrl
         * @param {string} config.successUrl
         * @param {string} config.moduleUrl
         * @param {string} config.methodSuffix
         * @returns {object} The module.
         */
        function init(config) {
            if (!INITIALIZED) {
                Server.init({
                    baseUrl: config.baseUrl,
                    failureUrl: config.failureUrl,
                    successUrl: config.successUrl,
                    moduleUrl: config.moduleUrl,
                    formKey: config.formKey
                });

                CartComponent.init();
                CartComponent.channel.on('item-removed', onCartItemRemoved);
                CartComponent.channel.on(
                    'quantity-changed',
                    onCartQuantityChanged
                );

                Iframe.init({
                    iframe: $(config.iframe).get(0),
                    origin: config.origin,
                    url: config.iframeUrl,
                    script: config.script
                });

                Iframe.channel.on('loaded', finalizeIframeSetup);
                Iframe.channel.on('purchase-denied', purchaseDenied);
                Iframe.channel.on('purchase-failed', purchaseFailed);
                Iframe.channel.on('book-order', onBookOrder);

                AddressComponent.init();

                PaymentMethod.init({
                    methodSuffix: config.methodSuffix
                });

                Order.channel.on('grand-total-zero', redirectToOrderSuccess);
                Order.channel.on('order-status-update', onOrderStatusUpdate);

                AJAX_CHAIN.addSubordinate('cart', CartModel.ajaxChain);
                AJAX_CHAIN.addSubordinate('discount', DiscountModel.ajaxChain);
                AJAX_CHAIN.addSubordinate('shipping', Shipping.ajaxChain);
                AJAX_CHAIN.addSubordinate(
                    'address',
                    AddressComponent.ajaxChain
                );

                AJAX_CHAIN.onEmpty(function() {
                    if (BOOK_ORDER) {
                        AJAX_CHAIN.block();
                        Order.bookOrder();
                    }
                });

                INITIALIZED = true;
            }

            return EXPORT;
        }

        EXPORT.init = init;
        EXPORT.isInitialized = isInitialized;
        EXPORT.ajaxChain = AJAX_CHAIN;

        return Object.freeze(EXPORT);
    }
);
