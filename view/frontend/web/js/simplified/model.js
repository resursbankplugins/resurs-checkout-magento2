/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery',
        'ko',
        'Magento_Checkout/js/model/quote',
        'Resursbank_Checkout/js/lib/ajax',
        'Resursbank_Checkout/js/lib/credentials',
        'Resursbank_Checkout/js/simplified/server',
        'Magento_Checkout/js/model/url-builder',
        'Magento_Customer/js/model/customer',
        'mage/storage'
    ],
    function (
        $,
        ko,
        quote,
        Ajax,
        Credentials,
        Server,
        urlBuilder,
        customer,
        storage
    ) {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/simplified/checkout
         * @type {object}
         * @readonly
         */
        var $this = {};

        /**
         * The name of the customer type for private citizen.
         *
         * @type {string}
         * @readonly
         */
        var CUSTOMER_PRIVATE = '';

        /**
         * The name of the customer type for companies.
         *
         * @type {string}
         * @readonly
         */
        var CUSTOMER_COMPANY = '';

        /**
         * @constant {Chain}
         */
        var AJAX_CHAIN = new Ajax.Chain({
            name: 'simplified-model',
            limit: 1
        });

        /**
         * Chain that is reserved for requests that handle the totals.
         *
         * @constant {Chain}
         */
        var AJAX_CHAIN_TOTALS = new Ajax.Chain({
            name: 'payment-fee',
            limit: 1
        });

        /**
         * Whether the module has been initialized.
         *
         * @type {boolean}
         */
        var INITIALIZED = false;

        /**
         * The country ISO code, e.g. the ISO for Sweden would be SE.
         *
         * @readonly
         * @type {string}
         */
        var CONFIG_COUNTRY_ISO = '';

        /**
         * The customer's ID-number.
         *
         * @type {function} ko.observable() - string.
         */
        var ID_NUMBER = ko.observable('');

        /**
         * The type of customer.
         *
         * @type {function} ko.observable() - string.
         */
        var CUSTOMER_TYPE = ko.observable('');

        /**
         * The resurs bank logo.
         *
         * @type {function} ko.observable() - string.
         */
        var LOGO = ko.observable('');

        /**
         * The contact ID for company customers.
         *
         * @type {function} ko.observable() - string.
         */
        var CONTACT_ID = ko.observable('');

        /**
         * Resurs Bank card number (if any).
         *
         * @type {function} ko.observable() - string.
         */
        var CARD_NUMBER = ko.observable('');

        /**
         * The previous ID-number.
         *
         * @type {function} ko.observable() - string.
         */
        var previousIdNumber = ko.observable('');

        /**
         * Whether we are pushing an ID-number to the server.
         *
         * @type {function} ko.observable() - boolean.
         */
        var PUSHING_ID_NUMBER = ko.observable(false);

        /**
         * Whether we are pushing a customer type to the server.
         *
         * @type {function} ko.observable() - boolean.
         */
        var PUSHING_CUSTOMER_TYPE = ko.observable(false);

        /**
         * Whether we are pushing a contact ID-number to the server.
         *
         * @type {function} ko.observable() - boolean.
         */
        var PUSHING_CONTACT_ID = ko.observable(false);

        /**
         * Whether we are pushing a Resurs Bank card number to the server.
         *
         * @type {function} ko.observable() - boolean.
         */
        var PUSHING_CARD_NUMBER = ko.observable(false);

        /**
         * Whether we are pushing a new payment method to the server.
         *
         * @type {function} ko.observable() - boolean.
         */
        var PUSHING_PAYMENT_METHOD = ko.observable(false);

        /**
         * Whether we are pushing card amount to the server.
         *
         * @type {function} ko.observable() - boolean.
         */
        var PUSHING_CARD_AMOUNT = ko.observable(false);

        /**
         * Whether we are fetching an address from the server.
         *
         * @type {function} ko.observable() - boolean.
         */
        var FETCHING_ADDRESS = ko.observable(false);

        /**
         * Whether we are fetching data that should be displayed in the "Read
         * more" modal.
         *
         * @type {function} ko.observable() - boolean.
         */
        var FETCHING_READ_MORE_DATA = ko.observable(false);

        /**
         * Whether we are performing a request to the server.
         *
         * @type {function} ko.observable() - string.
         */
        var PERFORMING_REQUEST = ko.computed(function() {
            return PUSHING_ID_NUMBER() ||
                PUSHING_CUSTOMER_TYPE() ||
                PUSHING_CONTACT_ID() ||
                PUSHING_CARD_NUMBER() ||
                PUSHING_PAYMENT_METHOD() ||
                PUSHING_CARD_AMOUNT() ||
                FETCHING_ADDRESS() ||
                FETCHING_READ_MORE_DATA();
        });

        /**
         * The interval for each card amount option.
         *
         * @type {number}
         */
        var CARD_AMOUNT_INTERVAL = 5000;

        /**
         * Sets the ID number. The value will have to pass a validation test.
         *
         * @param {string} id
         * @returns {object} Returns a JQuery.Deferred() object, regardless if
         * it could validate the ID or not.
         */
        function setIdNumber(id) {
            var result = $.Deferred();

            if (Credentials.validate(id, CONFIG_COUNTRY_ISO, CUSTOMER_TYPE())) {
                result = pushIdNumber(id, CONFIG_COUNTRY_ISO, CUSTOMER_TYPE());
            }

            return result;
        }

        /**
         * Sets the customer type. The value will have to pass a validation
         * test.
         *
         * @param {string} value
         * @returns {object} Returns a JQuery.Deferred() object, regardless if
         * it could validate the customer type or not.
         */
        function setCustomerType(value) {
            var result = $.Deferred();

            if (value === CUSTOMER_PRIVATE || value === CUSTOMER_COMPANY) {
                result = pushCustomerType(value);
            }

            return result;
        }

        /**
         * Sets the company contact ID number. The value will have to pass a
         * validation test.
         *
         * @param {string} id
         * @returns {object} Returns a JQuery.Deferred() object, regardless if
         * it could validate the ID or not.
         */
        function setContactId(id) {
            var result = $.Deferred();

            if (Credentials.validate(id, CONFIG_COUNTRY_ISO, CUSTOMER_PRIVATE)) {
                result = pushContactId(id, CONFIG_COUNTRY_ISO);
            }

            return result;
        }

        /**
         * Sets the Resurs Bank card number.
         *
         * @param {string} num
         * @returns {object} Returns a JQuery.Deferred() object.
         */
        function setCardNumber(num) {
            var result = $.Deferred();

            if (Credentials.validateCard(num)) {
                result = pushCardNumber(num);
            }

            return result;
        }

        /**
         * Set payment method.
         *
         * @param {string} code
         * @returns {object} Returns a JQuery.Deferred() object.
         */
        function setPaymentMethod(code) {
            // Update currently selected payment method (see getPaymentMethod()
            // docblock for a detailed explanation).
            window.checkoutConfig.payment.resursbank.selectedMethod = code;

            // Push selected payment method to server.
            return AJAX_CHAIN_TOTALS.addDummy(
                createPushPaymentMethodCall(code)
            );
        }

        /**
         * Retrieve code of currently selected payment method.
         *
         * NOTE: We avoid using quote.paymentMethod because it won't be
         * initialized when checkout/summary/fee is rendered. This means that
         * when you reload the checkout page the payment method specified on the
         * quote will be null, and thus our tax value cannot be collected from
         * window.checkoutConfig.payment.resursbank (so this really only is
         * apparent when you select a payment method and then reload the page).
         *
         * @returns {string}
         */
        function getPaymentMethod() {
            return window.checkoutConfig.payment.resursbank.selectedMethod;
        }

        /**
         * Retrieve config value for payment method. Values are provided by
         * Resursbank\Checkout\Model\Payment\Simplified\ConfigProvider
         *
         * @param {string} code
         * @param {string} key
         * @returns {*}
         */
        function getMethodData(code, key) {
            var result = null;
            var i;
            var methods = window.checkoutConfig.payment.resursbank.methods;

            for (i = 0; i < methods.length; i++) {
                if (methods[i].code === code) {
                    result = methods[i][key];
                }
            }

            return result;
        }

        /**
         * Pushes ID-number to the server to be stored in the session.
         *
         * @param {string} idNum
         * @param {string} country
         * @param {string} customerType
         * @returns {object} $.Deferred().
         */
        function pushIdNumber(idNum, country, customerType) {
            var deferred = $.Deferred();
            var call = getPushIdCall(idNum, country, customerType)
                .onDone(function (response) {
                    if (response.hasOwnProperty('message') &&
                        response.message.hasOwnProperty('error') &&
                        typeof response.message.error === 'string'
                    ) {
                        window.alert(
                            'Resurs Bank Checkout\n\n' +
                            response.message.error
                        );

                        deferred.reject(response);
                    } else {
                        ID_NUMBER(idNum);
                        deferred.resolve(response);
                    }
                })
                .onFail(function(response) {
                    window.alert(
                        'Resurs Bank Checkout\n\n' +
                        'Could not contact the server.'
                    );

                    deferred.reject(response);
                })
                .onAlways(function () {
                    PUSHING_ID_NUMBER(false);
                });

            AJAX_CHAIN.add(call).run();

            PUSHING_ID_NUMBER(true);

            return deferred;
        }

        /**
         * Pushes the customer type to the server where it will be stored in
         * the session.
         *
         * @param {string} type
         * @returns {object} $.Deferred().
         */
        function pushCustomerType(type) {
            var deferred = $.Deferred();
            var call = getPushCustomerTypeCall(type)
                .onDone(function (response) {
                    if (response.hasOwnProperty('message') &&
                        response.message.hasOwnProperty('error') &&
                        typeof response.message.error === 'string'
                    ) {
                        window.alert(
                            'Resurs Bank Checkout\n\n' +
                            response.message.error
                        );

                        deferred.reject(response);
                    } else {
                        CUSTOMER_TYPE(type);
                        deferred.resolve();
                    }
                })
                .onFail(function (response) {
                    window.alert(
                        'Resurs Bank Checkout\n\n' +
                        'Could not contact the server.'
                    );

                    deferred.reject(response);
                })
                .onAlways(function () {
                    PUSHING_CUSTOMER_TYPE(false);
                });

            AJAX_CHAIN.add(call).run();

            PUSHING_CUSTOMER_TYPE(true);

            return deferred;
        }

        /**
         * Pushes a company contact ID-number to the server to be stored in the
         * session.
         *
         * @param {string} idNum
         * @param {string} country
         * @returns {object} $.Deferred().
         */
        function pushContactId(idNum, country) {
            var deferred = $.Deferred();
            var call = getPushContactIdCall(idNum, country)
                .onDone(function (response) {
                    if (response.hasOwnProperty('message') &&
                        response.message.hasOwnProperty('error') &&
                        typeof response.message.error === 'string'
                    ) {
                        window.alert(
                            'Resurs Bank Checkout\n\n' +
                            response.message.error
                        );

                        deferred.reject(response);
                    } else {
                        CONTACT_ID(idNum);
                        deferred.resolve(response);
                    }
                })
                .onFail(function(response) {
                    window.alert(
                        'Resurs Bank Checkout\n\n' +
                        'Could not contact the server.'
                    );

                    deferred.reject(response);
                })
                .onAlways(function () {
                    PUSHING_CONTACT_ID(false);
                });

            AJAX_CHAIN.add(call).run();

            PUSHING_CONTACT_ID(true);

            return deferred;
        }

        /**
         * Pushes a card number to the server to be stored in the session.
         *
         * @param {string} num
         * @returns {object} $.Deferred().
         */
        function pushCardNumber(num) {
            var deferred = $.Deferred();
            var call = getPushCardNumberCall(num)
                .onDone(function (response) {
                    if (response.hasOwnProperty('message') &&
                        response.message.hasOwnProperty('error') &&
                        typeof response.message.error === 'string'
                    ) {
                        window.alert(
                            'Resurs Bank Checkout\n\n' +
                            response.message.error
                        );

                        deferred.reject(response);
                    } else {
                        CARD_NUMBER(num);
                        deferred.resolve(response);
                    }
                })
                .onFail(function(response) {
                    window.alert(
                        'Resurs Bank Checkout\n\n' +
                        'Could not contact the server.'
                    );

                    deferred.reject(response);
                })
                .onAlways(function () {
                    PUSHING_CARD_NUMBER(false);
                });

            AJAX_CHAIN.add(call).run();

            PUSHING_CARD_NUMBER(true);

            return deferred;
        }

        /**
         * Pushes card amount to the server.
         *
         * @param {number} amount
         * @returns {object} $.Deferred().
         */
        function pushCardAmount(amount) {
            var deferred = $.Deferred();
            var call = getPushCardAmountCall(amount)
                .onDone(function (response) {
                    if (response.hasOwnProperty('message') &&
                        response.message.hasOwnProperty('error') &&
                        typeof response.message.error === 'string'
                    ) {
                        window.alert(
                            'Resurs Bank Checkout\n\n' +
                            response.message.error
                        );

                        deferred.reject(response);
                    } else {
                        deferred.resolve(response);
                    }
                })
                .onFail(function(response) {
                    window.alert(
                        'Resurs Bank Checkout\n\n' +
                        'Could not contact the server.'
                    );

                    deferred.reject(response);
                })
                .onAlways(function () {
                    PUSHING_CARD_AMOUNT(false);
                });

            AJAX_CHAIN.add(call).run();

            PUSHING_CARD_AMOUNT(true);

            return deferred;
        }

        /**
         * Pushes selected payment method to server. The method will be applied
         * on the quote, totals will then be re-calculated (to support payment
         * method fees) and the resulting totals returned.
         *
         * @param {string} code
         * @returns {object} $.Deferred().
         */
        function createPushPaymentMethodCall(code) {
            var restUrl = !customer.isLoggedIn() ?
                '/guest-carts/:cartId/resursbank_checkout/collectTotals' :
                '/carts/mine/resursbank_checkout/collectTotals';

            var serviceUrl = urlBuilder.createUrl(restUrl, {
                cartId: quote.getQuoteId()
            });

            var payload = {
                cartId: quote.getQuoteId(),
                paymentMethod: {
                    method: code
                }
            };

            return new Ajax.Dummy()
                .onFire(function(call) {
                    storage.post(
                        serviceUrl,
                        JSON.stringify(payload)
                    ).done(function(response) {
                        call.resolve(response);
                    }).always(function() {
                        PUSHING_PAYMENT_METHOD(false);
                    });

                    PUSHING_PAYMENT_METHOD(true);
                });
        }

        /**
         * Fetches an address from the API.
         *
         * @param {string} idNum
         * @param {string} countryIso
         * @param {string} customerType
         */
        function fetchAddress(idNum, countryIso, customerType) {
            var deferred = $.Deferred();
            var call = getFetchAddressCall(idNum, countryIso, customerType)
                .onDone(function (response) {
                    if (response.hasOwnProperty('message') &&
                        response.message.hasOwnProperty('error') &&
                        typeof response.message.error === 'string'
                    ) {
                        deferred.reject(response, response.message.error);
                    } else {
                        ID_NUMBER(idNum);
                        deferred.resolve(response);
                    }
                })
                .onFail(function(response) {
                    window.alert(
                        'Resurs Bank Checkout\n\n' +
                        'Could not contact the server. Please fill out ' +
                        'form manually.'
                    );

                    deferred.reject(response);
                })
                .onAlways(function () {
                    FETCHING_ADDRESS(false);
                });

            AJAX_CHAIN.add(call).run();

            FETCHING_ADDRESS(true);

            return deferred;
        }

        /**
         * Takes a payment method code and fetches additional data about it, in
         * form of an HTML string.
         *
         * @param {string} code
         * @returns {object} $.Deferred().
         */
        function getCostOfPurchase(code) {
            var deferred = $.Deferred();
            var call = getCostOfPurchaseCall(code)
                .onDone(function (response) {
                    if (response.hasOwnProperty('message') &&
                        response.message.hasOwnProperty('error') &&
                        typeof response.message.error === 'string'
                    ) {
                        window.alert(
                            'Resurs Bank Checkout\n\n' +
                            response.message.error
                        );

                        deferred.reject(response);
                    } else {
                        deferred.resolve(response);
                    }
                })
                .onFail(function(response) {
                    window.alert(
                        'Resurs Bank Checkout\n\n' +
                        'Could not contact the server. Please fill out ' +
                        'form manually.'
                    );

                    deferred.reject(response);
                })
                .onAlways(function () {
                    FETCHING_READ_MORE_DATA(false);
                });

            AJAX_CHAIN.add(call).run();

            FETCHING_READ_MORE_DATA(true);

            return deferred;
        }

        /**
         * Returns the name of the private citizen customer type option.
         *
         * @returns {string}
         */
        function getPrivateCustomer() {
            return CUSTOMER_PRIVATE;
        }

        /**
         * Returns the name of the company customer type option.
         *
         * @returns {string}
         */
        function getCompanyCustomer() {
            return CUSTOMER_COMPANY;
        }

        /**
         * Creates a call to push the given SSN/Org nr. to the server.
         *
         * @param {string} id
         * @param {string} country
         * @param {string} customerType
         * @returns {Call}
         */
        function getPushIdCall(id, country, customerType) {
            return new Ajax.Call({
                options: {
                    method: 'POST',
                    url: Server.createUrl('simplified/setIdNumber'),
                    data: {
                        id_num: id,
                        country: country,
                        customer_type: customerType,
                        form_key: Server.getFormKey()
                    }
                }
            });
        }

        /**
         * Creates a call to fetch address data for the given SSN/Org nr.
         *
         * NOTE: Only swedish customers are able to fetch their address.
         *
         * @param {string} id
         * @param {string} country
         * @param {string} customerType
         * @returns {Call}
         */
        function getFetchAddressCall(id, country, customerType) {
            return new Ajax.Call({
                options: {
                    method: 'POST',
                    url: Server.createUrl('simplified/fetchAddress'),
                    data: {
                        id_num: id,
                        country: country,
                        customer_type: customerType,
                        form_key: Server.getFormKey()
                    }
                }
            });
        }

        /**
         * Creates a call to push the selected customer type to the server.
         *
         * @param {string} customerType
         * @returns {Call}
         */
        function getPushCustomerTypeCall(customerType) {
            return new Ajax.Call({
                options: {
                    method: 'POST',
                    url: Server.createUrl('simplified/setCustomerType'),
                    data: {
                        type: customerType,
                        form_key: Server.getFormKey()
                    }
                }
            });
        }

        /**
         * Creates a call to push the given contact ID to the server.
         *
         * NOTE: Only company customers are allowed to push contact ID.
         *
         * @param {string} id
         * @param {string} country
         * @returns {Call}
         */
        function getPushContactIdCall(id, country) {
            return new Ajax.Call({
                options: {
                    method: 'POST',
                    url: Server.createUrl('simplified/setContactId'),
                    data: {
                        id_num: id,
                        country: country,
                        form_key: Server.getFormKey()
                    }
                }
            });
        }

        /**
         * Creates a call to push the given card number to the server.
         *
         * @param {string} num
         * @returns {Call}
         */
        function getPushCardNumberCall(num) {
            return new Ajax.Call({
                options: {
                    method: 'POST',
                    url: Server.createUrl('simplified/setCardNumber'),
                    data: {
                        card_num: num,
                        form_key: Server.getFormKey()
                    }
                }
            });
        }

        /**
         * Creates a call to retrieve payment information for provided method.
         *
         * @param {string} code
         * @returns {Call}
         */
        function getCostOfPurchaseCall(code) {
            return new Ajax.Call({
                options: {
                    method: 'GET',
                    url: Server.createUrl('simplified/getCostOfPurchase'),
                    data: {
                        code: code,
                        form_key: Server.getFormKey()
                    }
                }
            });
        }

        /**
         * Creates a call to push card amount to the server.
         *
         * @param {number} amount
         * @returns {Call}
         */
        function getPushCardAmountCall(amount) {
            return new Ajax.Call({
                options: {
                    method: 'POST',
                    url: Server.createUrl('simplified/setCardAmount'),
                    data: {
                        card_amount: amount,
                        form_key: Server.getFormKey()
                    }
                }
            });
        }

        /**
         * Returns the chosen country's ISO that has been set in the
         * configuration.
         *
         * @returns {string}
         */
        function getConfigCountryIso() {
            return CONFIG_COUNTRY_ISO;
        }

        /**
         * Initialization function for the module. Calling it more than once
         * will have no effect.
         *
         * @param {object} config
         * @param {string} config.countryIso
         * @param {string} config.customerType
         * @param {string} config.idNumber
         * @param {string} config.contactId
         * @param {string} config.privateCustomerKey
         * @param {string} config.companyCustomerKey
         * @param {string} config.logo
         * @param {number} config.cardAmountInterval
         * @returns {object} The model.
         */
        function init(config) {
            if (!INITIALIZED) {
                ID_NUMBER.subscribe(function(oldValue) {
                    previousIdNumber(oldValue);
                }, null, 'beforeChange');

                CONFIG_COUNTRY_ISO = config.countryIso;
                ID_NUMBER(config.idNumber);
                CONTACT_ID(config.contactId);
                CUSTOMER_TYPE(config.customerType);
                LOGO(config.logo);
                CUSTOMER_PRIVATE = config.privateCustomerKey;
                CUSTOMER_COMPANY = config.companyCustomerKey;
                CARD_AMOUNT_INTERVAL = config.cardAmountInterval;
                INITIALIZED = true;
            }

            return $this;
        }

        $this.init = init;
        $this.setIdNumber = setIdNumber;
        $this.setCustomerType = setCustomerType;
        $this.setContactId = setContactId;
        $this.setCardNumber = setCardNumber;
        $this.setPaymentMethod = setPaymentMethod;
        $this.getPaymentMethod = getPaymentMethod;
        $this.pushIdNumber = pushIdNumber;
        $this.pushCustomerType = pushCustomerType;
        $this.pushContactId = pushContactId;
        $this.pushCardNumber = pushCardNumber;
        $this.pushCardAmount = pushCardAmount;
        $this.fetchAddress = fetchAddress;
        $this.getPrivateCustomer = getPrivateCustomer;
        $this.getCompanyCustomer = getCompanyCustomer;
        $this.getCostOfPurchase = getCostOfPurchase;
        $this.getMethodData = getMethodData;
        $this.createPushPaymentMethodCall = createPushPaymentMethodCall;
        $this.getConfigCountryIso = getConfigCountryIso;
        $this.idNumber = ID_NUMBER;
        $this.contactId = CONTACT_ID;
        $this.cardNumber = CARD_NUMBER;
        $this.customerType = CUSTOMER_TYPE;
        $this.logo = LOGO;
        $this.pushingIdNumber = PUSHING_ID_NUMBER;
        $this.pushingCustomerType = PUSHING_CUSTOMER_TYPE;
        $this.pushingContactId = PUSHING_CONTACT_ID;
        $this.pushingCardNumber = PUSHING_CARD_NUMBER;
        $this.pushingCardAmount = PUSHING_CARD_AMOUNT;
        $this.fetchingAddress = FETCHING_ADDRESS;
        $this.fetchingReadMoreData = FETCHING_READ_MORE_DATA;
        $this.performingRequest = PERFORMING_REQUEST;
        $this.ajaxChain = AJAX_CHAIN;
        $this.ajaxChainTotals = AJAX_CHAIN_TOTALS;
        $this.cardAmountInterval = CARD_AMOUNT_INTERVAL;

        return Object.freeze($this);
    }
);
