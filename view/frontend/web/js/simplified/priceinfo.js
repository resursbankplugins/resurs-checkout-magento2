/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * openPriceInfo (global and unframed support from EComPHP 1.3.36).
 *
 * With Magento2 adaptions:
 *
 * Makes tabs clickable when purchaseinfo is not iframed. Affects visually only DK-based purchaseinfos,
 * since they are multitabbed.
 *
 * @param evt
 * @param methodName
 */
function openPriceInfo(evt, methodName) {
    var i, tabcontent, priceinfotablink;
    tabcontent = document.getElementsByClassName("priceinfotab");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    priceinfotablink = document.getElementsByClassName("priceinfotablink");
    for (i = 0; i < priceinfotablink.length; i++) {
        priceinfotablink[i].className = priceinfotablink[i].className.replace(" active", "");
    }
    // This part may receive an object instead of an element id, so if we have an element
    // ready on the function call, we should use that one instead of looking for it.
    if (typeof methodName === 'string') {
        document.getElementById(methodName).style.display = "block";
        if (null !== evt) {
            evt.currentTarget.className += " active";
        }
    } else {
        methodName.style.display = "block";
        if (null !== evt) {
            evt.currentTarget.className += " active";
        }
    }
}
