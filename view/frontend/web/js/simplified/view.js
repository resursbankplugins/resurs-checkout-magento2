/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'uiRegistry'
    ],
    function (
        uiRegistry
    ) {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/simplified/view
         * @type {object}
         * @readonly
         */
        var EXPORT = {};

        /**
         * Returns the UiClass component for the billing address telephone
         * input, or undefined if it can't be found.
         *
         * Takes an optional payment method code that, if supplied, will make
         * the function try to find the component under the given payment
         * method. If not given, it will assume that the field is in the
         * bottom of the page.
         *
         * NOTE: The placement of the field can be changed in backend:
         * Stores > Configuration > Sales > Checkout > Checkout Options >
         * Display Billing Address On.
         *
         * @param {string} [paymentMethodCode]
         * @returns {UiClass|undefined}
         */
        function getBillingTelephoneField(paymentMethodCode) {
            var result;

            if (typeof paymentMethodCode === 'string') {
                result = uiRegistry.get(
                    'checkout.steps.billing-step.payment.' +
                    'payments-list.' +
                    paymentMethodCode + '-form.' +
                    'form-fields.telephone'
                );
            } else {
                result = uiRegistry.get(
                    'checkout.steps.billing-step.payment.afterMethods.' +
                    'billing-address-form.form-fields.telephone'
                );
            }

            return result;
        }

        /**
         * Returns the UiClass component for the billing address country
         * input, or undefined if it can't be found.
         *
         * Takes an optional payment method code that, if supplied, will make
         * the function try to find the component under the given payment
         * method. If not given, it will assume that the field is in the
         * bottom of the page.
         *
         * NOTE: The placement of the field can be changed in backend:
         * Stores > Configuration > Sales > Checkout > Checkout Options >
         * Display Billing Address On.
         *
         * @param {string} [paymentMethodCode]
         * @returns {UiClass|undefined}
         */
        function getBillingCountryField(paymentMethodCode) {
            var result;

            if (typeof paymentMethodCode === 'string') {
                result = uiRegistry.get(
                    'checkout.steps.billing-step.payment.' +
                    'payments-list.' +
                    paymentMethodCode + '-form.' +
                    'form-fields.country_id'
                );
            } else {
                result = uiRegistry.get(
                    'checkout.steps.billing-step.payment.afterMethods.' +
                    'billing-address-form.form-fields.country_id'
                );
            }

            return result;
        }

        /**
         * Returns the UiClass component for the shipping address telephone
         * input, or undefined if it can't be found.
         *
         * @returns {UiClass|undefined}
         */
        function getShippingTelephoneField() {
            return uiRegistry.get(
                'checkout.steps.shipping-step.shippingAddress.' +
                'shipping-address-fieldset.telephone'
            );
        }

        /**
         * Returns the UiClass component for the shipping address country
         * input, or undefined if it can't be found.
         *
         * @returns {UiClass|undefined}
         */
        function getShippingCountryField() {
            return uiRegistry.get(
                'checkout.steps.shipping-step.shippingAddress.' +
                'shipping-address-fieldset.country_id'
            );
        }

        EXPORT.getBillingTelephoneField = getBillingTelephoneField;
        EXPORT.getBillingCountryField = getBillingCountryField;
        EXPORT.getShippingTelephoneField = getShippingTelephoneField;
        EXPORT.getShippingCountryField = getShippingCountryField;

        return Object.freeze(EXPORT);
    }
);
