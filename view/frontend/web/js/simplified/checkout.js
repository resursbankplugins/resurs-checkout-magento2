/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'Resursbank_Checkout/js/simplified/model',
        'Resursbank_Checkout/js/simplified/server',
        'Resursbank_Checkout/js/simplified/priceinfo'
    ],
    function (Model, Server) {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/simplified/checkout
         * @type {object}
         * @readonly
         */
        var EXPORT = {};

        /**
         * Whether the module has been initialized.
         *
         * @type {boolean}
         */
        var INITIALIZED = false;

        /**
         * Initialization function for the module. Calling it more than once
         * will have no effect.
         *
         * @param {object} config
         * @param {string} config.baseUrl
         * @param {string} config.moduleUrl
         * @param {string} config.formKey
         * @param {string} config.privateCustomerKey - The constant to identify
         * the customer as an individual.
         * @param {string} config.companyCustomerKey - The constant to identify
         * the customer as a company.
         * @param {string} config.idNumber - Default SSN/Org nr.
         * @param {string} [config.contactId] - Optional. Default SSN nr.
         * Exclusive for company customers, they are required to specify both
         * an SSN and Org nr. during checkout.
         * @param {string} config.countryIso - The country ISO, e.g. the ISO
         * for Sweden would be SE.
         * @param {string} config.customerType - Whether the customer is an
         * individual or company.
         * @param {string} config.logo - Path to the Resurs Bank logo image.
         * @param {number} config.cardAmountInterval
         * @returns {Object}
         */
        function init(config) {
            if (!INITIALIZED) {
                Server.init({
                    baseUrl: config.baseUrl,
                    moduleUrl: config.moduleUrl,
                    formKey: config.formKey
                });

                Model.init({
                    countryIso: config.countryIso,
                    customerType: config.customerType,
                    logo: config.logo,
                    idNumber: config.idNumber,
                    contactId: config.contactId,
                    privateCustomerKey: config.privateCustomerKey,
                    companyCustomerKey: config.companyCustomerKey,
                    cardAmountInterval: config.cardAmountInterval
                });

                INITIALIZED = true;
            }

            return EXPORT;
        }

        EXPORT.init = init;

        return Object.freeze(EXPORT);
    }
);
