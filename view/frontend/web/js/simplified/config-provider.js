/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [],
    function () {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/simplified/config-provider
         * @type {object}
         * @readonly
         */
        var EXPORT = {};

        /**
         * Returns an array of objects where each object contains data about
         * a specific payment method coming from Resurs Bank.
         *
         * @returns {array}
         */
        function getPaymentMethods() {
            return window.checkoutConfig.payment.resursbank.methods;
        }

        /**
         * Says whether the billing address fields are attached to each
         * payment method, or at the end of page of the payment step during
         * checkout.
         *
         * @returns {boolean}
         */
        function getIsBillingOnPaymentMethods() {
            return window
                .checkoutConfig
                .payment
                .resursbank
                .isBillingOnPaymentMethod;
        }

        EXPORT.getPaymentMethods = getPaymentMethods;
        EXPORT.getIsBillingOnPaymentMethods = getIsBillingOnPaymentMethods;

        return Object.freeze(EXPORT);
    }
);
