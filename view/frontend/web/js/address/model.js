/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'underscore',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/action/create-billing-address',
        'Magento_Checkout/js/action/create-shipping-address',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Checkout/js/action/select-billing-address'
    ],
    function(
        // Magento.
        _,
        Quote,
        CreateBillingAddress,
        CreateShippingAddress,
        SelectShippingAddress,
        SelectBillingAddress
    ) {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/cart/model
         * @constant {object}
         */
        var EXPORT = {};


        var addedAddress = null;

        /**
         * Sends the billing information to the server with an AJAX call.
         *
         * @param {Address} address
         * @return {object} The module.
         */
        function pushUserInfo(address) {
            var billing;

            if (address.hasOwnProperty('shipping') &&
                typeof address.shipping !== 'undefined'
            ) {
                SelectShippingAddress(CreateShippingAddress(address.shipping));
                SelectBillingAddress(CreateBillingAddress(address.billing));
            }
            else {
                billing = CreateBillingAddress(address.billing);

                SelectShippingAddress(billing);
                SelectBillingAddress(billing);
            }

            addedAddress = address;

            return EXPORT;
        }

        /**
         * Sets the guestEmail property of the quote object.
         *
         * @param {string} email
         * @returns {object} The module.
         */
        function setGuestEmail(email) {
            Quote.guestEmail = email;

            return EXPORT;
        }

        /**
         * Compares an Address object against a previously added Address
         * object.
         *
         * @param {Address} address
         * @returns {*}
         */
        function hasAddress(address) {
            return addedAddress === null
                ? false
                : _.isEqual(address, addedAddress);
        }

        EXPORT.pushUserInfo = pushUserInfo;
        EXPORT.setGuestEmail = setGuestEmail;
        EXPORT.hasAddress = hasAddress;

        return Object.freeze(EXPORT);
    }
);
