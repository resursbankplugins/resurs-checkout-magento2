/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [],
    function() {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/address/address
         * @type {function}
         */
        var EXPORT;

        /**
         * @param {object} config
         * @param {object} config.billing - Billing address.
         * @param {object} [config.shipping] - Optional shipping address.
         * @constructor
         */
        function Address(config) {
            this.billing = config.billing;
            this.shipping = config.shipping;
        }

        EXPORT = Address;

        return Object.freeze(EXPORT);
    }
);
