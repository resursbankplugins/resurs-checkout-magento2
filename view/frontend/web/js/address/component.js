/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/shipping-save-processor/default',
        'Resursbank_Checkout/js/lib/server',
        'Resursbank_Checkout/js/iframe',
        'Resursbank_Checkout/js/address/address',
        'Resursbank_Checkout/js/address/model',
        'Resursbank_Checkout/js/lib/ajax'
    ],
    function(
        // Magento.
        $,
        Quote,
        DefaultProcessor,

        // Resurs Bank.
        Server,
        Iframe,
        Address,
        Model,
        Ajax
    ) {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/address/component
         * @constant {object}
         */
        var EXPORT = {};

        /**
         * Whether the module has been inititalized.
         *
         * @type {boolean}
         * @readonly
         */
        var INITIALIZED = false;

        /**
         * @type {Chain}
         */
        var AJAX_CHAIN = new Ajax.Chain({
            name: 'Address component'
        });

        /**
         * Says whether the module has been initialized.
         *
         * @returns {boolean}
         */
        function isInitialized() {
            return INITIALIZED;
        }

        /**
         * Prepares the users billing information to be sent to the server. Can alter names and add properties
         * that the server expects to get.
         *
         * @param {object} billingData
         * @returns {object} The corrected user information, ready to be sent to the server.
         */
        function prepareBillingInfo(billingData) {
            return correctAddressObject(billingData);
        }

        /**
         * Prepares the users shipping information to be sent to the server.
         * Can alter names and add properties that the server expects to get.
         *
         * @param {object} billingData
         * @param {object} shippingData
         * @returns {object} The corrected user information, ready to be sent
         * to the server.
         */
        function prepareShippingInfo(billingData, shippingData) {
            shippingData.telephone = billingData.telephone;
            shippingData.email = billingData.email;

            return correctAddressObject(shippingData);
        }

        /**
         * Prepares either the shipping or billing address, depending which
         * object was passed to this method.
         *
         * @param addressData
         * @return {object} The new address object.
         */
        function correctAddressObject(addressData) {
            return function (obj) {
                $.each(addressData, function (key, value) {
                    switch (key) {
                        case 'address': obj.street[0] = value; break;
                        case 'addressExtra': obj.street[1] = value; break;
                        default: obj[getCorrectInfoName(key)] = value;
                    }
                });

                return obj;
            }({street: []});
        }

        /**
         * Takes a name of a property of user information and returns the corrected version of that property name.
         *
         * @param {string} name The name of the property to correct.
         * @returns {string}
         */
        function getCorrectInfoName(name) {
            var correctedName = '';

            switch (name) {
                case 'surname': correctedName = 'lastname'; break;
                case 'postal': correctedName = 'postcode'; break;
                case 'countryCode': correctedName = 'country_id'; break;
                default: correctedName = name;
            }

            return correctedName;
        }

        /**
         * Turn an object into an address object.
         *
         * @param {object} data
         * @param {object} data.address - Billing and shipping address.
         * @param {object} [data.delivery] - Optional shipping address.
         * @returns {Address}
         */
        function toAddressObj(data) {
            var obj = {};

            if (data.hasOwnProperty('delivery')) {
                obj.shipping = prepareShippingInfo(data.address, data.delivery);
            }

            obj.billing = prepareBillingInfo(data.address);

            return new Address(obj);
        }

        /**
         * Event handler for when the user information changes in the iframe.
         *
         * @param {object} data
         * @param {object} data.address - Billing and shipping address.
         * @param {object} [data.delivery] - Optional shipping address.
         */
        function onUserInfoChange(data) {
            var address = toAddressObj(data);

            if (isValidAddress(address) && !Model.hasAddress(address)) {
                Model.setGuestEmail(address.billing.email);
                Model.pushUserInfo(address);
            }
        }

        /**
         * Validates an address object.
         *
         * @param {Address} address
         * @return {boolean}
         */
        function isValidAddress(address) {
            var addressObj =
                (address.hasOwnProperty('shipping') &&
                typeof address.shipping !== 'undefined') ?
                address.shipping :
                address.billing;

            return addressObj.firstname !== '' &&
                addressObj.lastname !== '' &&
                addressObj.email !== '' &&
                addressObj.city !== '' &&
                addressObj.postcode !== '' &&
                addressObj.telephone !== '' &&
                addressObj.street[0] !== '';
        }

        /**
         * Initialization function.
         *
         * @returns {object} The module.
         */
        function init() {
            if (!INITIALIZED) {
                Iframe.channel.on('user-info-change', onUserInfoChange);

                Quote.shippingMethod.subscribe(function() {
                    var dummy = AJAX_CHAIN.addDummy();
                    AJAX_CHAIN.run();

                    DefaultProcessor.saveShippingInformation().then(function () {
                        dummy.getCall().resolve();
                    }, function() {
                        dummy.getCall().reject();
                    });
                });

                INITIALIZED = true;
            }

            return EXPORT;
        }

        EXPORT.init = init;
        EXPORT.isInitialized = isInitialized;
        EXPORT.ajaxChain = AJAX_CHAIN;

        return Object.freeze(EXPORT);
    }
);
