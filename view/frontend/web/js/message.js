/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define([
    'jquery'
], function ($) {
    return function (config) {
        var $this = {};

        /**
         * Initializer.
         *
         * @param {Object} [config]
         * @param {String} [config.text]
         * @param {String} [config.wrapperClass]
         * @param {String} [config.messageClass]
         * @param {Element} [config.element]
         */
        var init = function (config) {
            var i, el, wrapper;

            for (i in config) {
                if ($this.hasOwnProperty(i)) {
                    $this[i] = config[i];
                }
            }

            if ($this.element === null) {
                wrapper = document.createElement('div');
                wrapper.className = $this.wrapperClass;

                el = document.createElement('div');
                el.className = $this.messageClass;
                $(el).text($this.text);

                $this.element = wrapper;
            }

            wrapper.appendChild(el);
        };

        /**
         *  The message to display.
         *
         * @type {string}
         */
        $this.text = '';

        /**
         * The message element.
         *
         * @type {null}
         */
        $this.element = null;

        /**
         * The default [className] of the wrapper element.
         *
         * @type {String}
         */
        $this.wrapperClass = 'resursbank-checkout-message-wrapper';

        /**
         * The default [className] of the message element.
         *
         * @type {String}
         */
        $this.messageClass = 'resursbank-checkout-message';

        /**
         * Whether or not the message is displayed.
         *
         * @type {Boolean}
         */
        $this.shown = false;

        /**
         * Displays the message at the specified [element] and [position].
         *
         * @param {Element} element - The element to show the message next to.
         * @param {Object} [config]
         * @param {String} [config.position] - The position to display the message.
         * @param {String} [config.duration] - The amount of time it should be displayed before being removed in
         * milliseconds. Defaults to 5000.
         * @param {Number} [config.time] - The time it takes for the animation to complete in milliseconds. Defaults to
         * 1000.
         * @param {Function} [config.complete] - A callback for when the animation has finished.
         * @returns {$this}
         */
        $this.show = function (element, config) {
            var duration = 5000;
            var position = 'after';
            var el = $(element);
            var time = 1000;

            if (!$this.shown) {
                if (config) {
                    duration = config.duration || duration;
                    position = config.position || position;
                    time = config.time || time;
                }

                if (position === 'before') {
                    el.before($this.element);
                }
                else if (position === 'after') {
                    el.after($this.element);
                }

                $($this.element)
                    .fadeIn(time, function () {
                        $this.shown = true;
                    })
                    .delay(duration)
                    .fadeOut(time, function () {
                        $this.shown = false;
                        $this.element.remove();

                        if (config && typeof config.complete === 'function') {
                            config.complete();
                        }
                    });
            }

            return $this;
        };

        /**
         * Destroys the instance.
         */
        $this.destroy = function () {
            var i;

            $($this.element)
                .stop(true, true)
                .remove();

            for (i in $this) {
                if ($this.hasOwnProperty(i)) {
                    $this[i] = null;
                }
            }

            init = null;
            $this = null;
        };

        init(config);

        return $this;
    };
});
