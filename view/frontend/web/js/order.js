/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * PaymentMethod selection
 * ----------------------------------------------------------------------------
 * Explanation of the selectPaymentMethod call:
 *
 * When the page first loads we receive information from the iframe about the
 * pre-selected payment method.
 *
 * We then use the js/order/payment-method script to select the payment method
 * (ie. applying on our quote which payment method has been selected).
 *
 * Certain actions will reload the list of available payment methods initially
 * collected by Magento, and re-evaluate the currently selected payment method
 * against that list. This process contains a bug which will cause this to fail
 * and reset the currently selected payment method (by setting it to 'null').
 *
 * For example, when you select a shipping method an AJAX request is invoked to
 * apply the selected method on your quote. The response from this request will
 * contain an updated list of available payment methods. This list will then
 * replace the list originally collected by Magento when the page is loaded.
 * This script will then validate the selected payment method against this and
 * will fail then reset the selected payment method to 'null'.
 *
 * When the payment method is set to 'null' we are not able to place the order
 * before selecting another payment method to overwrite the value.
 *
 * However, selecting a payment method won't invoke the same validation
 * process. So right before we place the order we re-select the payment method.
 * Thus ensuring that the payment method will always be selected when you place
 * your order. If the payment method does not exist  on the server the fallback
 * (resursbank_standard) will be used instead automatically.
 *
 * Please note that through several tests the behaviour of how and when this
 * problem occurs has been a bit inconsistent.
 */

define(
    [
        'jquery',
        'mage/translate',
        'Magento_Checkout/js/action/place-order',
        'Magento_Checkout/js/model/quote',
        'Magento_Ui/js/model/messages',
        'Magento_Checkout/js/model/full-screen-loader',
        'Resursbank_Checkout/js/lib/channel',
        'Resursbank_Checkout/js/lib/server',
        'Resursbank_Checkout/js/lib/ajax',
        'Resursbank_Checkout/js/payment-method',
        'Resursbank_Checkout/js/iframe'
    ],
    function(
        // Magento
        $,
        $t,
        PlaceOrderAction,
        Quote,
        Messages,
        FullscreenLoader,

        // Resurs Bank
        Channel,
        Server,
        Ajax,
        PaymentMethod,
        Iframe
    ) {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/order
         * @constant {object}
         */
        var EXPORT = {};

        /**
         * An event channel.
         *
         * @constant {Channel}
         */
        var CHANNEL = new Channel();

        /**
         * @constant {Chain}
         */
        var AJAX_CHAIN = new Ajax.Chain({
            limit: 1,
            name: 'Order'
        });

        /**
         * Base URL for placing orders.
         *
         * @constant {string}
         */
        var BASE_URL = 'order/';

        /**
         * Event handler for when the place order button is clicked in the
         * iframe.
         */
        function bookOrder() {
            // See explanation at the top of the file: PaymentMethod selection.
            PaymentMethod.select(PaymentMethod.getSelected());

            placeOrder();
        }

        /**
         * Creates a call to place an order.
         *
         * @returns {Call}
         */
        function getOrderStatusCall() {
            return new Ajax.Call({
                options: {
                    url: createServerUrl('status'),
                    method: 'POST',
                    contentType: 'application/json'
                }
            });
        }

        /**
         * Creates a URL for a server call.
         *
         * @param {string} path
         * @returns {string}
         */
        function createServerUrl(path) {
            return Server.createUrl(BASE_URL + path);
        }

        /**
         * Attempts to place the order.
         *
         * @returns {Call|undefined}
         */
        function placeOrder() {
            var call;

            if (validateShippingMethod()) {
                startFullscreenLoader();

                call = getOrderStatusCall()
                    .onDone(function (response) {
                        if (response.validSession === true) {
                            PlaceOrderAction(
                                {
                                    'method': getPaymentMethodCode()
                                },
                                new Messages()
                            ).success(function () {
                                if (response.grandTotal <= 0) {
                                    CHANNEL.emit('grand-total-zero');
                                } else {
                                    CHANNEL.emit('order-status-update', {
                                        orderReady: true
                                    });
                                }
                            }).fail(function (orderResponse) {
                                alert(
                                    'An error occured when placing the ' +
                                    'order:\n\n' +
                                    orderResponse.responseJSON.message
                                );

                                CHANNEL.emit('order-status-update', {
                                    orderReady: false
                                });
                            });
                        } else {
                            CHANNEL.emit('order-status-update', {
                                orderReady: false
                            });

                            window.alert(response.invalidSessionError);
                            window.location.reload();
                        }
                    });

                AJAX_CHAIN.add(call).run();
            } else {
                CHANNEL.emit('order-status-update', {
                    orderReady: false
                });
            }

            return call;
        }

        /**
         * Confirm that a shipping method has been selected.
         *
         * @returns {boolean}
         */
        function validateShippingMethod() {
            var result = true;

            if (!Quote.isVirtual()) {
                $('#co-shipping-method-form input[type="radio"]')
                    .each(function (i, box) {
                        result = box.checked;

                        if (result) {
                            return false;
                        }
                    });

                if (!result) {
                    alert($t('Please select a shipping method.'));
                }
            }

            return result;
        }

        /**
         * Retrieve payment method code.
         *
         * @returns string
         */
        function getPaymentMethodCode()
        {
            var result = Quote.paymentMethod();
            var toString = Object.prototype.toString;
            var retValue = result;

            if (
                toString.call(result) === '[object Object]' &&
                result.hasOwnProperty('method')
            ) {
                retValue = result.method;
            }

            return String(retValue);
        }

        /**
         * Cancels the booking of an order.
         */
        function cancelBooking() {
            Iframe.postMessage({
                eventType: 'checkout:order-status',
                orderReady: false
            });
        }

        /**
         * Finalize the booking of an order.
         */
        function finalizeBooking() {
            Iframe.postMessage({
                eventType: 'checkout:order-status',
                orderReady: true
            });
        }

        /**
         * Start the full screen loader.
         *
         * @return {object} The module.
         */
        function startFullscreenLoader() {
            // Need to start the loader to replace any other full screen
            // loaders that other processes might have started, so that we
            // can choose when it will stop.
            FullscreenLoader.startLoader();
            var fullscreenLoadingMask = $('body > .loading-mask')[0];

            if (fullscreenLoadingMask) {
                // In the case that the user have disabled full screen loaders,
                // we enable it here with inline CSS so that in special cases
                // (like rebuilding the cart or finalizing the order) the
                // loader will still show.
                fullscreenLoadingMask.style.setProperty(
                    'display',
                    'block',
                    'important'
                );
            }

            return EXPORT;
        }

        /**
         * Stop the full screen loader.
         *
         * @return {object} The module.
         */
        function stopFullscreenLoader() {
            // See comment in startFullscreenLoader().
            FullscreenLoader.stopLoader();
            var fullscreenLoadingMask = $('body > .loading-mask')[0];

            if (fullscreenLoadingMask) {
                // See comment in startFullscreenLoader().
                fullscreenLoadingMask.style.setProperty(
                    'display',
                    'none'
                );
            }

            return EXPORT;
        }

        EXPORT.bookOrder = bookOrder;
        EXPORT.cancelBooking = cancelBooking;
        EXPORT.finalizeBooking = finalizeBooking;
        EXPORT.startFullscreenLoader = startFullscreenLoader;
        EXPORT.stopFullscreenLoader = stopFullscreenLoader;
        EXPORT.channel = CHANNEL;
        EXPORT.ajaxChain = AJAX_CHAIN;

        return Object.freeze(EXPORT);
    }
);
