/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery',
        'mage/translate',
        'ko',
        'Magento_Checkout/js/model/quote',
        'Resursbank_Checkout/js/lib/ajax',
        'Resursbank_Checkout/js/lib/server'
    ],
    function (
        $,
        $t,
        ko,
        Quote,
        Ajax,
        Server
    ) {
        /**
         * Queue for AJAX calls.
         *
         * @type {Chain}
         */
        var _ajaxChain = new Ajax.Chain({
            name: 'part-payment'
        });

        /**
         * The calculated price the customer is expected to pay each month for
         * the entirety of the duration.
         *
         * @type {Function} - Float, observable.
         * @private
         */
        var _suggestedPrice = ko.observable(0);

        /**
         * How many months the customer is expected to make regular payments
         * for the purchased product.
         *
         * @type {Function} - Integer, observable.
         * @private
         */
        var _duration = ko.observable(0);

        /**
         * Observes the duration.
         *
         * @type {computedObservable|*}
         */
        var duration = ko.computed(function () {
            return _duration();
        });

        /**
         * Whether the model is currently fetching data.
         *
         * @type {Function} - Boolean, observable.
         * @private
         */
        var _isFetchingData = ko.observable(false);

        /**
         * Whether the model has been initialized.
         *
         * @type {boolean}
         * @private
         */
        var _initialized = false;

        /**
         * Observes whether the model is currently fetching data.
         *
         * @type {computedObservable|*}
         */
        var isFetchingData = ko.computed({
            read: function () {
                return _isFetchingData();
            }
        });

        /**
         * Observes the suggested part payment price. Returns a number when
         * read, accepts only numbers when written to.
         *
         * @type {computedObservable|*}
         */
        var suggestedPrice = ko.computed({
            read: function () {
                return _suggestedPrice();
            },
            write: function(value) {
                if (typeof value === 'number') {
                    _suggestedPrice(value);
                }
            }
        });

        /**
         * Takes a payment method code and fetches additional data about it, in
         * form of an HTML string.
         *
         * @param {string} code
         * @returns {Request} Has the original call object and a promise that
         * will be resolved or rejected, depending on the answer from the
         * server.
         */
        function getCostOfPurchase(code) {
            var deferred = $.Deferred();
            var call = getCostOfPurchaseCall(code);
            var result = new Ajax.Request({
                call: call,
                deferred: deferred
            });

            call.onDone(function (response) {
                var status = Ajax.getStatus(response);

                deferred.resolve(status);
            })
            .onFail(function (response) {
                deferred.reject($.mage.__(
                    'Part payment information cannot be retrieved at this time.'
                ));
            })
            .onAlways(function () {
                _isFetchingData(false);
            });

            _ajaxChain.add(call).run();

            _isFetchingData(true);

            return result;
        }

        /**
         * Creates a call to retrieve payment information for provided method.
         *
         * @param {string} code
         * @returns {Call}
         */
        function getCostOfPurchaseCall(code) {
            return new Ajax.Call({
                options: {
                    method: 'GET',
                    url: Server.createUrl('simplified/getCostOfPurchase'),
                    data: {
                        code: code,
                        form_key: Server.getFormKey()
                    }
                }
            });
        }

        /**
         * Returns an object with price format rules for the current product.
         *
         * @returns {Object|null} Object with rules, or null if such an object
         * can't be found.
         */
        function getPriceFormat() {
            return window.hasOwnProperty('checkoutConfig') &&
                window.checkoutConfig.hasOwnProperty('priceFormat') ?
                window.checkoutConfig.priceFormat :
                null;
        }

        /**
         * Returns the selected payment method.
         *
         * @returns {string}
         */
        function getSelectedPaymentMethod() {
            return Quote.paymentMethod();
        }

        /**
         * @param {object} data
         * @param {number} data.suggestedPrice - Float.
         * @param {number} data.duration - Integer.
         */
        function init(data) {
            if (!_initialized) {
                suggestedPrice(data.suggestedPrice);
                _duration(data.duration);
                _initialized = true;

                Quote.totals.subscribe(function () {
                    suggestedPrice(
                        Math.ceil(
                            parseFloat(
                                Quote.totals().base_grand_total / duration()
                            )
                        )
                    )
                });
            }
        }

        return Object.freeze({
            init: init,
            getCostOfPurchase: getCostOfPurchase,
            getCostOfPurchaseCall: getCostOfPurchaseCall,
            getPriceFormat: getPriceFormat,
            getSelectedPaymentMethod: getSelectedPaymentMethod,
            suggestedPrice: suggestedPrice,
            duration: duration,
            isFetchingData: isFetchingData
        });
    }
);
