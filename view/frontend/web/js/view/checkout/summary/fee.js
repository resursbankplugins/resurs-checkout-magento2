/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'ko',
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/totals',
        'Magento_Catalog/js/price-utils',
        'Magento_Checkout/js/model/quote',
        'Resursbank_Checkout/js/simplified/model'
    ],
    function (
        ko,
        Component,
        totals,
        priceUtils,
        quote,
        model
    ) {
        'use strict';

        /**
         * Value bound to amount excl. tax <span> in template.
         *
         * @var {string}
         */
        var totalExcl = ko.observable(getTotalValue());

        /**
         * Value bound to amount incl. tax <span> in template.
         *
         * @var {string}
         */
        var totalIncl = ko.observable(getTotalValue(true));

        /**
         * Whether to display the method fee. If the fee for the selected
         * payment method is zero, it should not be displayed.
         *
         * @type {computedObservable|*}
         */
        var displayFee = ko.computed(function () {
            var method = quote.paymentMethod();

            return method !== null && typeof method !== 'undefined' ?
                getMethodFee(method.method) > 0 :
                false;
        });

        /**
         * Update total in template whenever quote totals changes. This is how
         * we reflect payment method fee when changing between methods.
         */
        quote.totals.subscribe(function () {
            totalExcl(getTotalValue());
            totalIncl(getTotalValue(true));
        });

        /**
         * Retrieve fee value for currently selected payment method (formatted
         * to match configured currency).
         *
         * @param {boolean} includeTax
         * @returns {*|String}
         */
        function getTotalValue(includeTax) {
            includeTax = (includeTax === true);

            var result = totals.getSegment(
                'resursbank_payment_fee'
            ).value;

            if (includeTax) {
                result+= getTaxValue();
            }

            return priceUtils.formatPrice(result, quote.getPriceFormat());
        }

        /**
         * Retrieve tax value.
         *
         * @returns {number}
         */
        function getTaxValue() {
            var value = model.getMethodData(model.getPaymentMethod(), 'feeTax');

            if (value === null) {
                value = 0.0;
            }

            return parseFloat(value);
        }

        /**
         * Retrieve the fee of a payment method.
         *
         * @returns {number}
         */
        function getMethodFee(code) {
            var value = model.getMethodData(code, 'fee');

            if (value === null) {
                value = 0.0;
            }

            return parseFloat(value);
        }

        /**
         * Tax display configuration.
         *
         * 1 = Excl. tax only
         * 2 = Incl. tax only
         * 3 = Both
         *
         * @returns {string}
         */
        function getTaxDisplay() {
            return window.checkoutConfig.payment.resursbank.fee.tax.display;
        }

        /**
         * Whether or not to display fee incl. tax.
         *
         * @returns {boolean}
         */
        function displayInclTax() {
            var display = getTaxDisplay();

            return (display === 2 || display === 3);
        }

        /**
         * Whether or not to display fee excl. tax.
         *
         * @returns {boolean}
         */
        function displayExclTax() {
            var display = getTaxDisplay();

            return (display === 1 || display === 3);
        }

        /**
         * Get title for fee total incl. tax.
         *
         * @returns {string}
         */
        function getTitleIncl() {
            return getTaxDisplay() === 3 ? this.titleIncl : this.title;
        }

        /**
         * Get title for fee total excl. tax.
         *
         * @returns {string}
         */
        function getTitleExcl() {
            return getTaxDisplay() === 3 ? this.titleExcl : this.title;
        }

        return Component.extend({
            defaults: {
                template: 'Resursbank_Checkout/checkout/summary/fee'
            },

            totalExcl: totalExcl,
            totalIncl: totalIncl,
            displayFee: displayFee,
            displayInclTax: displayInclTax,
            displayExclTax: displayExclTax,
            getTitleIncl: getTitleIncl,
            getTitleExcl: getTitleExcl
        });
    }
);
