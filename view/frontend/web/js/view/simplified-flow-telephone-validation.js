/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery',
        'ko',
        'mage/translate',
        'uiRegistry',
        'uiComponent',
        'Magento_Ui/js/lib/validation/validator',
        'Resursbank_Checkout/js/lib/credentials',
        'Resursbank_Checkout/js/simplified/view'
    ],
    function (
        $,
        ko,
        translate,
        uiRegistry,
        Component,
        validator,
        credentials,
        view
    ) {
        'use strict';

        /**
         * Country ID of the chosen country during checkout.
         *
         * @type {string}
         */
        var chosenCountryId = '';

        /**
         * Initialization function for the component. Calling it more than once
         * will have no effect.
         *
         * @returns {Object}
         */
        function init() {
            validator.addRule(
                'resursbank-checkout-telephone',
                function(value) {
                    return credentials.isCountryAllowed(chosenCountryId) ?
                        credentials.validatePhone(value, chosenCountryId) :
                        true;
                },
                $.mage.__(
                    'Please provide a valid phone number for your chosen ' +
                    'country.'
                )
            );

            return {
                initialize: function() {
                    this._super();

                    var countryIdField = view.getShippingCountryField();
                    var telephoneField = view.getShippingTelephoneField();

                    if (typeof countryIdField !== 'undefined') {
                        countryIdField.value.subscribe(function(value) {
                            chosenCountryId = value;
                        });

                        chosenCountryId = countryIdField.value();
                    }

                    if (typeof telephoneField !== 'undefined') {
                        telephoneField.setValidation(
                            'resursbank-checkout-telephone',
                            true
                        );
                    }
                }
            };
        }

        return Component.extend(init());
    }
);
