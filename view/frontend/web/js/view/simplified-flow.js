/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery',
        'ko',
        'mage/translate',
        'uiComponent',
        'Magento_Checkout/js/model/quote',
        'Resursbank_Checkout/js/simplified/model',
        'Resursbank_Checkout/js/lib/credentials'
    ],
    function (
        $,
        ko,
        translate,
        Component,
        quote,
        model,
        Credentials
    ) {
        'use strict';

        /**
         * Whether the customer is, for example, a person or a company.
         *
         * @type {function} ko.observable() - string.
         */
        var selectedCustomerType = ko.computed({
            read: function() {
                return model.customerType();
            },

            write: function(value) {
                model.setCustomerType(value);
            }
        });

        /**
         * Value of the ID-number field.
         *
         * @type {function} ko.observable() - string.
         */
        var selectedIdNumber = ko.observable('');

        /**
         * Failed to fetch address from Resurs Bank API.
         *
         * @type {function} ko.observable() - boolean.
         */
        var failedToFetchAddress = ko.observable(false);

        /**
         * Error message from address fetching.
         *
         * @type {function} ko.observable() - string.
         */
        var failedToFetchAddressError = ko.observable('');

        /**
         * Whether the customer is a company.
         *
         * @type {function} ko.observable() - boolean.
         */
        var isCompanyCustomer = ko.computed(function() {
            return selectedCustomerType() === 'LEGAL';
        });

        /**
         * Whether the address has been fetched.
         *
         * @type {function} ko.observable() - boolean.
         */
        var isAddressFetched = ko.observable(false);

        /**
         * Whether the component should be visible. Should only be visible if
         * the user has selected Sweden as country.
         *
         * @type {function} ko.observable() - boolean.
         */
        var showComponent = ko.computed(function() {
            return Credentials.isSweden(model.getConfigCountryIso());
        });

        /**
         * Whether the "Fetch address" button should be visible. Should only
         * be visible if the user has selected "Sweden" as country.
         *
         * @type {function} ko.observable() - boolean.
         */
        var showFetchAddressBtn = ko.computed(function() {
            return Credentials.isSweden(model.getConfigCountryIso()) && !isAddressFetched();
        });

        /**
         * Whether the "Not you?" button should be visible. Should only
         * be visible if the user has selected "Sweden" as country and an
         * address has been fetched.
         *
         * @type {function} ko.observable() - boolean.
         */
        var showRemoveAddressBtn = ko.computed(function() {
            return Credentials.isSweden(model.getConfigCountryIso()) && isAddressFetched();
        });

        /**
         * Whether the customer type controls should be disabled.
         *
         * @type {function} ko.observable() - boolean.
         */
        var disableCustomerTypeSelection = ko.computed(function() {
            return model.performingRequest() ||
                (Credentials.isSweden(model.getConfigCountryIso()) && isAddressFetched());
        });

        /**
         * Whether the ID-number field should be disabled.
         *
         * @type {function} ko.observable() - boolean.
         */
        var disableIdNumberField = ko.computed(function() {
            return disableCustomerTypeSelection();
        });

        /**
         * Whether the "Fetch address" button should be disabled.
         *
         * @type {function} ko.observable() - boolean.
         */
        var disableFetchBtn = ko.computed(function() {
            return model.performingRequest() || isAddressFetched();
        });

        /**
         * Whether the "Not you?" button should be disabled.
         *
         * @type {function} ko.observable() - boolean.
         */
        var disableRemoveAddressBtn = ko.computed(function() {
            return model.performingRequest() || !isAddressFetched();
        });

        /**
         * Component object.
         *
         * @exports Resursbank_Checkout/js/view/simplified-flow
         * @type {object}
         */
        var $this = {};

        /**
         * Event handler for when the fetch address button is clicked.
         */
        function onFetchBtnClick() {
            var idNum;
            var countryIso;
            var customerType;
            var valid;

            if (!model.fetchingAddress() && !isAddressFetched()) {
                idNum = selectedIdNumber();
                countryIso = model.getConfigCountryIso();
                customerType = selectedCustomerType();
                valid = Credentials.validate(idNum, countryIso, customerType);

                failedToFetchAddress(!valid);

                if (valid) {
                    model.fetchAddress(idNum, countryIso, customerType)
                        .done(function(response) {
                            var inputs = getInputs();
                            var data;

                            if (typeof inputs !== 'undefined' &&
                                response.hasOwnProperty('address')
                            ) {
                                isAddressFetched(true);
                                data = response.address;

                                Object.keys(response.address)
                                    .forEach(function(key) {
                                        var el;

                                        if (inputs.hasOwnProperty(key)) {
                                            el = inputs[key];
                                            el.value = data[key];

                                            ko.utils.triggerEvent(el, 'keyup');
                                        }
                                    });
                            }
                        }).fail(function(response, msg) {
                            failedToFetchAddressError(msg);
                            failedToFetchAddress(true);
                    });
                } else {
                    failedToFetchAddressError(
                        $.mage.__('Wrong SSN/Org. number.')
                    );
                }
            }
        }

        /**
         * Event handler for when the remove address button is clicked.
         */
        function onRemoveAddressBtnClick() {
            if (isAddressFetched()) {
                removeAddress();
            }
        }

        /**
         * Fetches the input fields from the checkout page and returns them
         * in an object where each key is the value of the "name" attribute of
         * the input.
         *
         * @returns {object|undefined}
         */
        function getInputs() {
            var list;
            var els = $(
                '#co-shipping-form .field[name^="shippingAddress"] input'
            );

            if (els.length > 0) {
                list = {};
                els.each(function(i, el) {
                    var filteredName = el.name
                        .replace(/\[/g, '')
                        .replace(/\]/g, '');

                    list[filteredName] = el;
                });
            }

            return list;
        }

        /**
         * Removes the fetched address information from the checkout fields and
         * from the stored id number from the session.
         */
        function removeAddress() {
            var inputs;

            if (isAddressFetched() === true) {
                model.setIdNumber('').done(function () {
                    inputs = getInputs();

                    if (typeof inputs !== 'undefined') {
                        Object.keys(inputs).forEach(function (key) {
                            inputs[key].value = '';
                        });
                    }

                    isAddressFetched(false);
                });
            }
        }

        /**
         * Initialization function for the component. Calling it more than once
         * will have no effect.
         *
         * @returns {Object}
         */
        function init() {
            selectedIdNumber(model.idNumber());

            model.idNumber.subscribe(function(value) {
                selectedIdNumber(value);
            });

            $this = {
                defaults: {
                    template: 'Resursbank_Checkout/simplified-flow'
                },

                selectedIdNumber: selectedIdNumber,
                showFetchAddressBtn: showFetchAddressBtn,
                showRemoveAddressBtn: showRemoveAddressBtn,
                showComponent: showComponent,
                selectedCustomerType: selectedCustomerType,
                disableCustomerTypeSelection: disableCustomerTypeSelection,
                disableIdNumberField: disableIdNumberField,
                failedToFetchAddress: failedToFetchAddress,
                failedToFetchAddressError: failedToFetchAddressError,
                disableFetchBtn: disableFetchBtn,
                disableRemoveAddressBtn: disableRemoveAddressBtn,
                isCompanyCustomer: isCompanyCustomer,
                onFetchBtnClick: onFetchBtnClick,
                onRemoveAddressBtnClick: onRemoveAddressBtnClick
            };

            return $this;
        }

        return Component.extend(init());
    }
);
