/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery',
        'mage/translate',
        'ko',
        'priceBox',
        'Resursbank_Checkout/js/lib/ajax',
        'Resursbank_Checkout/js/lib/server'
    ],
    function(
        $,
        $t,
        ko,
        PriceBox,
        Ajax,
        Server
    ) {
        'use strict';

        /**
         * Queue for AJAX calls.
         *
         * @type {Chain}
         */
        var _ajaxChain = new Ajax.Chain({
            name: 'part-payment'
        });

        /**
         * How many months the customer is expected to make regular payments
         * for the purchased product.
         *
         * @type {Function} - Integer, observable.
         * @private
         */
        var _duration = ko.observable(0);

        /**
         * The calculated price the customer is expected to pay each month for
         * the entirety of the duration.
         *
         * @type {Function} - Float, observable.
         * @private
         */
        var _suggestedPrice = ko.observable(0);

        /**
         * The type (simple, bundle etc.) of the product that the customer is
         * currently viewing.
         *
         * @type {Function} - String, observable.
         * @private
         */
        var _productType = ko.observable('');

        /**
         * The id of the configured payment method in Resurs Bank's settings.
         *
         * @type {Function} - Integer, observable.
         * @private
         */
        var _configuredPaymentMethodId = ko.observable(0);

        /**
         * Whether the model is currently fetching data.
         *
         * @type {Function} - Boolean, observable.
         * @private
         */
        var _isFetchingData = ko.observable(false);

        /**
         * The final price of the product, which includes things like custom
         * options. Can be seen as the grand total of a product page.
         *
         * @type {Function} - Float, observable.
         * @private
         */
        var _finalPrice = ko.observable(0);

        /**
         * HTMLElement of the price box on the product page.
         *
         * @type {jQuery}
         */
        var _priceBoxEl = null;

        /**
         * jQuery instance of the price box element on a product page that
         * will allow us to get the product's prices.
         *
         * @type {Object}
         */
        var _priceBoxInstance = null;

        /**
         * Whether the model has been initialized.
         *
         * @type {boolean}
         * @private
         */
        var _initialized = false;

        /**
         * Observes the suggested part payment price. Returns a number when
         * read, accepts only numbers when written to.
         *
         * @type {computedObservable|*}
         */
        var suggestedPrice = ko.computed({
            read: function () {
                return _suggestedPrice();
            },
            write: function(value) {
                if (typeof value === 'number') {
                    _suggestedPrice(value);
                }
            }
        });

        /**
         * Observes the final price. Returns a number when read, accepts only
         * numbers when written to.
         *
         * @type {computedObservable|*}
         */
        var finalPrice = ko.computed({
            read: function () {
                return _finalPrice();
            },
            write: function(value) {
                if (typeof value === 'number') {
                    _finalPrice(value);
                }
            }
        });

        /**
         * Observes the duration.
         *
         * @type {computedObservable|*}
         */
        var duration = ko.computed(function () {
            return _duration();
        });

        /**
         * Observes the product type.
         *
         * @type {computedObservable|*}
         */
        var productType = ko.computed(function () {
            return _productType();
        });

        /**
         * Observes the configured payment method id.
         *
         * @type {computedObservable|*}
         */
        var configuredPaymentMethodId = ko.computed(function () {
            return _configuredPaymentMethodId();
        });

        /**
         * Observes whether the model is currently fetching data.
         *
         * @type {computedObservable|*}
         */
        var isFetchingData = ko.computed({
            read: function() {
                return _isFetchingData();
            },
            write: function(value) {
                if (typeof value === 'boolean') {
                    _isFetchingData(value);
                }
            }
        });

        /**
         * Creates and executes a call to the server that will fetch a table
         * of part payment information about a given price and payment method.
         * The table is returned as an HTML string.
         *
         * @param {number} price - Float.
         * @returns {Request} Has the original call object and a promise that
         * will be resolved or rejected, depending on the answer from the
         * server.
         */
        function getCostOfPurchase(price) {
            var deferred = $.Deferred();
            var call = getCostOfPurchaseCall(price);
            var result = new Ajax.Request({
                call: call,
                deferred: deferred
            });

            call.onDone(function(response) {
                var status = Ajax.getStatus(response);

                if (status instanceof Ajax.Response.Success) {
                    deferred.resolve(status);
                } else {
                    deferred.resolve(status);
                }
            });

            call.onFail(function() {
                deferred.reject($t(
                    'No part payment information exists for this product.'
                ));
            });

            call.onAlways(function() {
                isFetchingData(false);
            });

            _ajaxChain.add(call).run();
            isFetchingData(true);

            return result;
        }

        /**
         * Creates a call to fetch a table of part payment information for a
         * given price and payment method.
         *
         * @param {number} price - Float.
         * @returns {Call} A call to the server that can be executed later.
         */
        function getCostOfPurchaseCall(price) {
            return new Ajax.Call({
                options: {
                    method: 'POST',
                    url: Server.createUrl('catalog/getCostOfPurchase'),
                    data: {
                        form_key: Server.getFormKey(),
                        methodId: _configuredPaymentMethodId(),
                        price: price
                    }
                }
            });
        }

        /**
         * Get the final price of the current product.
         *
         * @returns {number} Returns -1 if a final price could not be found.
         */
        function getFinalPrice() {
            var returnPrice = -1;

            if (_priceBoxInstance) {
                var displayPrices = _priceBoxInstance.cache.displayPrices;

                if (displayPrices.finalPrice) {
                    returnPrice = displayPrices.finalPrice.amount;
                } else if (displayPrices.basePrice) {
                    returnPrice = displayPrices.basePrice.amount;
                }
            }

            return returnPrice;
        }

        /**
         * Returns an object with price format rules for the current product.
         *
         * @returns {Object|null} Object with rules, or null if such an object
         * can't be found.
         */
        function getPriceFormat() {
            return _priceBoxInstance ?
                (_priceBoxInstance.options.priceConfig &&
                _priceBoxInstance.options.priceConfig.priceFormat) :
                null;
        }

        /**
         * @param {object} data
         * @param {number} data.duration - Integer.
         * @param {number} data.suggestedPrice - Float.
         * @param {string} data.productType
         * @param {number} data.configuredPaymentMethodId - Integer.
         */
        function init(data) {
            if (!_initialized) {
                setTimeout(function() {
                    _duration(data.duration);
                    suggestedPrice(data.suggestedPrice);
                    _productType(data.productType);
                    _configuredPaymentMethodId(data.configuredPaymentMethodId);
                    initPriceBoxEl();

                    _initialized = true;
                }, );
            }
        }

        function initPriceBoxEl() {
            var price;

            _priceBoxEl = productType() === 'bundle' ?
                $('.price-box.price-configured_price') :
                $('.price-box');

            if (_priceBoxEl) {
                _priceBoxInstance = _priceBoxEl.data('magePriceBox');
                _priceBoxEl.on('updatePrice', function() {
                    var price = getFinalPrice();

                    if (price !== -1) {
                        finalPrice(price);
                        suggestedPrice(price / duration());
                    }
                });

                price = getFinalPrice();

                if (price !== -1) {
                    finalPrice(price);
                }
            }
        }

        return Object.freeze({
            init: init,
            getCostOfPurchase: getCostOfPurchase,
            getCostOfPurchaseCall: getCostOfPurchaseCall,
            getPriceFormat: getPriceFormat,
            suggestedPrice: suggestedPrice,
            duration: duration,
            productType: productType,
            configuredPaymentMethod: configuredPaymentMethodId,
            isFetchingData: isFetchingData,
            finalPrice: finalPrice,
            initPriceBoxEl: initPriceBoxEl
        });
    }
);
