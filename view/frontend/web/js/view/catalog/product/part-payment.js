/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'ko',
        'jquery',
        'mage/translate',
        'uiElement',
        'Magento_Catalog/js/price-utils',
        'Resursbank_Checkout/js/lib/ajax',
        'Resursbank_Checkout/js/view/catalog/product/part-payment.model',
        'Resursbank_Checkout/js/part-payment/remodal'
    ],
    function(
        ko,
        $,
        $t,
        Component,
        PriceUtils,
        Ajax,
        Model,
        remodal
    ) {
        'use strict';

        /**
         * ID for the remodal window.
         *
         * @type {string}
         */
        var remodalId = 'resursbank-checkout-part-payment-remodal';

        return Component.extend({
            defaults: {
                template: 'Resursbank_Checkout/catalog/product/part-payment'
            },

            initialize: function () {
                /**
                 * Placeholder for {this}.
                 *
                 * @type {object}
                 */
                var me = this;

                /**
                 * An instance of Remodal, the modal JQuery plugin.
                 *
                 * @type {null|object}
                 */
                var remodalInstance = null;

                /**
                 * The AJAX spinner used when fetching data for the "Read more"
                 * links.
                 *
                 * @type {null|HTMLDivElement}
                 */
                var remodalLoader = null;

                /**
                 * The content for the "Read more" links.
                 *
                 * @type {null|HTMLDivElement}
                 */
                var remodalContent = null;

                /**
                 * A flag that states whether the cart's totals has changed. If
                 * they do we need to fetch new data when clicking "Read more".
                 *
                 * @type {boolean}
                 */
                var totalsHasChanged = false;

                /**
                 * Initialize the Remodal window.
                 *
                 * NOTE: The modal window will open when initialized.
                 */
                function initRemodal() {
                    remodalLoader = $('#' + me.remodalLoaderId)
                        .get(0);

                    remodalContent = $('#' + me.remodalContentId)
                        .get(0);

                    remodalInstance = $(
                        '[data-remodal-id="' + me.remodalId + '"]'
                    ).remodal({
                        hashTracking: false
                    });
                }

                /**
                 * Opens the Remodal window and fetches new data when needed.
                 */
                function openRemodalWindow() {
                    remodalInstance.open();

                    // if (me.remodalData() === null || totalsHasChanged) {
                    Model.initPriceBoxEl();
                    updateRemodalWindow();
                    // }
                }

                /**
                 * Fetches updated part payment information from the server and
                 * updates the remodal content with it.
                 *
                 */
                function updateRemodalWindow() {
                    var request;

                    $(remodalContent).hide();
                    $(remodalLoader).show();

                    request = Model.getCostOfPurchase(Model.finalPrice());
                    request.deferred.done(function(status) {
                        $(remodalLoader).fadeOut(1000, function() {
                            if (status instanceof Ajax.Response.Success) {
                                me.remodalData(status.response.html);
                            } else {
                                me.remodalData(status.error);
                            }

                            totalsHasChanged = false;

                            $(remodalContent).fadeIn(1000);
                        });
                    }).fail(function(error) {
                        $(remodalLoader).fadeOut(1000, function() {
                            me.remodalData(error);

                            totalsHasChanged = false;

                            $(remodalContent).fadeIn(1000);
                        });
                    });
                }

                me._super();

                /**
                 * Whether to hide the Remodal AJAX spinner.
                 *
                 * @type {function} ko.observable() - boolean.
                 */
                me.hideRemodalLoader = ko.computed(function() {
                    return !Model.isFetchingData();
                });

                /**
                 * HTML string with data to be displayed in the "Read more"
                 * modal window.
                 *
                 * @type {Function} ko.observable() - string.
                 */
                me.remodalData = ko.observable(null);

                /**
                 * Called when "Read more" links are clicked to initiate the
                 * Remodal modal windows, and later to open the modal windows.
                 *
                 * NOTE: Can't initiate Remodal windows by checking if the
                 * elements has rendered as the Knockout bindings hasn't been
                 * initiated at that point, so the attributes that Remodal is
                 * looking for aren't there yet.
                 *
                 * @param {object} comp - The view model.
                 * @param {Event} event
                 */
                me.bindRemodal = function(comp, event) {
                    // Prevent link from adding a hashtag to the URL as it will
                    // cause Magento to redirect from checkout.
                    event.preventDefault();

                    if (remodalInstance === null) {
                        initRemodal();
                    }

                    openRemodalWindow();
                };

                Model.finalPrice.subscribe(function() {
                    totalsHasChanged = true;
                });
            },

            title: 'Title',
            readMore: $t('Read More'),
            modalTitle: 'Resurs Bank - ' + $t('Part Payment'),
            modalContent: ko.observable(''),
            remodalId: remodalId,
            remodalLoaderId: remodalId + '-loader',
            remodalContentId: remodalId + '-content',
            duration: Model.duration() + ' ' + $t('months'),
            suggestedPrice: ko.computed(function() {
                var priceFormat = Model.getPriceFormat();

                return PriceUtils.formatPrice(
                    Model.suggestedPrice(),
                    priceFormat !== null ? priceFormat : {}
                );
            })
        });
    }
);
