/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* @api */
define([
    'jquery',
    'ko',
    'uiRegistry',
    'mage/translate',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/view/payment/default',
    'Resursbank_Checkout/js/lib/ajax',
    'Resursbank_Checkout/js/lib/credentials',
    'Resursbank_Checkout/js/simplified/model',
    'Resursbank_Checkout/js/simplified/view',
    'Resursbank_Checkout/js/simplified/config-provider',
    'Resursbank_Checkout/js/part-payment/remodal',
    'Magento_Checkout/js/action/redirect-on-success',
    'mage/url',
    'Magento_Checkout/js/model/totals',
    'Magento_Checkout/js/checkout-data',
    'Magento_Ui/js/lib/validation/validator'
], function (
    $,
    ko,
    uiRegistry,
    translate,
    quote,
    Component,
    Ajax,
    Credentials,
    model,
    view,
    configProvider,
    remodal,
    redirectOnSuccessAction,
    url,
    totals,
    checkoutData,
    validator
) {
    'use strict';

    /**
     * Whether or not the customer is allowed to place the order.
     *
     * @type {function} ko.observable() - boolean.
     */
    var isPlaceOrderAllowed = ko.computed(function() {
        return Component.prototype.isPlaceOrderActionAllowed();
    });

    /**
     * The value of the payment method's ID input.
     *
     * @type {function} ko.observable - string.
     */
    var idNumber = ko.observable(model.idNumber());

    /**
     * The value of the payment method's contact ID input.
     *
     * @type {function} ko.observable - string.
     */
    var contactId = ko.observable(model.contactId());

    /**
     * The value of the payment method's card input.
     *
     * @type {function} ko.observable - string.
     */
    var cardNumber = ko.observable(model.cardNumber());

    /**
     * Whether the ID-number is invalid.
     *
     * @type {function} ko.observable() - boolean.
     */
    var invalidIdNumber = ko.computed(function() {
        return !(Credentials.validate(
            idNumber(),
            model.getConfigCountryIso(),
            model.customerType()
        ));
    });

    /**
     * Whether the contact ID-number is invalid.
     *
     * NOTE: contact id's are always private SSN numbers, never org. numbers.
     *
     * @type {function} ko.observable() - boolean.
     */
    var invalidContactId = ko.computed(function() {
        return !(Credentials.validate(
            contactId(),
            model.getConfigCountryIso(),
            model.getPrivateCustomer()
        ));
    });

    /**
     * Whether the card number is invalid .
     *
     * @type {function} ko.observable() - boolean.
     */
    var invalidCardNumber = ko.computed(function() {
        return typeof cardNumber() !== 'string' ||
            !Credentials.validateCard(cardNumber());
    });

    /**
     * If the customer is a company.
     *
     * @type {function} ko.observable - boolean.
     */
    var isCompanyCustomer = ko.computed(function() {
        return model.customerType() === model.getCompanyCustomer();
    });

    /**
     * The label for the ID-number field of a payment method.
     *
     * @type {function} ko.observable() - string.
     */
    var idFieldLabel = ko.computed(function() {
        return model.customerType() === model.getCompanyCustomer() ?
            $.mage.__('Org. number') :
            $.mage.__('SSN');
    });

    /**
     * Whether to disable the ID-number field.
     *
     * @type {function} ko.observable() - boolean.
     */
    var disableIdNumber = ko.computed(function() {
        return model.performingRequest();
    });

    /**
     * Whether to disable the card number field.
     *
     * @type {function} ko.observable() - boolean.
     */
    var disableCardNumber = ko.computed(function() {
        return model.performingRequest();
    });

    /**
     * Whether to disable the contact ID field.
     *
     * @type {function} ko.observable() - boolean.
     */
    var disableContactId = ko.computed(function() {
        return model.performingRequest();
    });

    /**
     * Checks whether a payment method should have a card input.
     *
     * @param {string} code
     * @returns {boolean}
     */
    function hasCardInput(code) {
        return window.checkoutConfig.payment.resursbank.methods.some(
            function(method) {
                return (
                    method.code === code &&
                    method.type === 'CARD' &&
                    method.disableInput !== true
                );
            }
        );
    }

    /**
     * Checks whether a payment method is provided by an external partner to
     * Resurs Bank.
     *
     * @param {string} code
     * @returns {boolean}
     */
    function isResursInternalMethod(code) {
        return window.checkoutConfig.payment.resursbank.methods.some(
            function(method) {
                return method.code === code
                    && method.type !== 'PAYMENT_PROVIDER';
            }
        );
    }

    /**
     * Checks whether a payment method is connected to Swish.
     *
     * @param {string} code
     * @returns {boolean}
     */
    function isSwishMethod(code) {
        return window.checkoutConfig.payment.resursbank.methods.some(
            function(method) {
                return method.code === code
                    && method.type === 'PAYMENT_PROVIDER'
                    && method.specificType === 'SWISH';
            }
        );
    }

    /**
     * Checks whether a payment method is a credit card.
     *
     * @param {string} code
     * @returns {boolean}
     */
    function isCreditCardMethod(code) {
        return window.checkoutConfig.payment.resursbank.methods.some(
            function(method) {
                return method.code === code
                    && method.type === 'PAYMENT_PROVIDER'
                    && (
                        method.specificType === 'DEBIT_CARD'
                        || method.specificType === 'CREDIT_CARD'
                    );
            }
        );
    }

    /**
     * Checks whether "Read more" needs to be displayed.
     *
     * @param {string} code
     * @returns {boolean}
     */
    function isResursReadMore(code) {
        return window.checkoutConfig.payment.resursbank.methods.some(
            function(method) {
                return (
                    method.code === code &&
                    (
                        method.specificType === 'PART_PAYMENT' ||
                        method.specificType === 'REVOLVING_CREDIT' ||
                        method.specificType === 'INVOICE' ||
                        method.specificType === 'CARD'
                    )
                );
            }
        );
    }

    /**
     * Event handler for when the value of the ID-number field changes.
     */
    function onIdNumberFieldChange() {
        model.setIdNumber(idNumber());
    }

    /**
     * Event handler for when the value of the contact ID field changes.
     */
    function onContactIdFieldChange() {
        model.setContactId(contactId());
    }

    /**
     * Event handler for when the value of the card number field changes.
     */
    function onCardNumberFieldChange() {
        model.setCardNumber(cardNumber());
    }

    /**
     * Event handler for when the value of the card amount field changes.
     */
    function onCardAmountChange(comp, event) {
        comp.cardAmount(parseFloat(event.target.value));
    }

    /**
     * Apply payment method on quote.
     *
     * @param {string} code
     */
    function setPaymentMethod(code) {
        totals.isLoading(true);

        model.setPaymentMethod(code).getCall().done(function (response) {
            quote.setTotals(response.totals);
        }).always(function() {
            if (model.ajaxChainTotals.isQueueEmpty()) {
                totals.isLoading(false);
            }
        });

        model.ajaxChainTotals.run();
    }

    /**
     * Takes a payment method code and checks whether the payment method
     * requires a card number.
     *
     * @param {string} code
     * @returns {boolean}
     */
    function doesRequireCardNumber(code) {
        return false;
        // return window
        //     .checkoutConfig
        //     .payment
        //     .resursbank
        //     .methods
        //     .some(
        //         function(method) {
        //             return code !== '' &&
        //                 method.code === code &&
        //                 method.type === 'CARD' &&
        //                 method.disableInput !== true;
        //         }
        //     );
    }

    /**
     * Creates and returns an object that represents a card amount option in a
     * <select> element.
     *
     * @param {string|number} value
     * @param {string|number} text
     * @returns {object}
     */
    function createCardAmountOption(value, text) {
        return {
            value: value,
            text: text
        }
    }

    /**
     * Takes the name of a payment method and returns an array of credit limit
     * intervals for that payment method.
     *
     * @param {string} code
     * @returns {Array}
     */
    function getCardAmountOptions(code) {
        var i;
        var grandTotal;
        var interval;
        var result = [];
        var maxOrderTotal = parseFloat(
            model.getMethodData(code, 'maxOrderTotal')
        );

        if (!isNaN(maxOrderTotal)) {
            grandTotal = Math.ceil(quote.totals().base_grand_total);
            interval = model.cardAmountInterval;
            i = grandTotal + interval - grandTotal % interval;

            result.push(createCardAmountOption(grandTotal, grandTotal));

            for (i; i <= maxOrderTotal; i += interval) {
                result.push(createCardAmountOption(i, i));
            }
        }

        return result;
    }

    /**
     * Checks whether a payment method has card amount options.
     *
     * @param {string} code
     * @returns {boolean}
     */
    function hasCardAmount(code) {
        var maxOrderTotal = parseFloat(
            model.getMethodData(code, 'maxOrderTotal')
        );

        return model.getMethodData(code, 'type') === 'REVOLVING_CREDIT' &&
            model.getMethodData(code, 'specificType') === 'REVOLVING_CREDIT' &&
            !isNaN(maxOrderTotal) && maxOrderTotal > 0;
    }

    /**
     * Checks whether a payment method has an SSN field.
     *
     * @param {string} code
     * @returns {boolean}
     */
    function hasSsnField(code) {
        return model.getMethodData(code, 'type') !== 'PAYMENT_PROVIDER';
    }

    /**
     * Initialization function.
     *
     * @returns {object} The component object.
     */
    function init() {
        var billingAddressCountryId = '';
        var billingTelephoneField;
        var billingCountryField;

        validator.addRule(
            'resursbank-checkout-telephone',
            function(value) {
                return Credentials.isCountryAllowed(billingAddressCountryId) ?
                    Credentials.validatePhone(value, billingAddressCountryId) :
                    true;
            },
            $.mage.__('Please provide a valid phone number for your chosen country.')
        );

        model.idNumber.subscribe(function(value) {
            idNumber(value);
        });

        model.contactId.subscribe(function(value) {
            contactId(value);
        });

        model.cardNumber.subscribe(function(value) {
            cardNumber(value);
        });

        quote.paymentMethod.subscribe(function(value) {
            var newMethod = value.method;
            var methods = window.checkoutConfig.payment.resursbank.methods;
            var isResursbankMethod = methods.some(function(method) {
                return method.code === newMethod;
            });

            if (!isResursbankMethod) {
                setPaymentMethod(newMethod);
            }
        });

        if (!configProvider.getIsBillingOnPaymentMethods()) {
            billingTelephoneField = view.getBillingTelephoneField();
            billingCountryField = view.getBillingCountryField();

            if (typeof billingTelephoneField !== 'undefined' &&
                typeof billingCountryField !== 'undefined'
            ) {
                billingTelephoneField.setValidation(
                    'resursbank-checkout-telephone',
                    true
                );

                billingCountryField.value.subscribe(function (value) {
                    billingAddressCountryId = value;
                });

                billingAddressCountryId = billingCountryField.value();
            }
        }

        return Component.extend({
            defaults: {
                redirectAfterPlaceOrder: true,
                template: 'Resursbank_Checkout/payment/method/resursbank'
            },

            idNumber: idNumber,
            contactId: contactId,
            cardNumber: cardNumber,
            isCompanyCustomer: isCompanyCustomer,
            invalidIdNumber: invalidIdNumber,
            invalidContactId: invalidContactId,
            invalidCardNumber: invalidCardNumber,
            idFieldLabel: idFieldLabel,
            disableIdNumber: disableIdNumber,
            disableCardNumber: disableCardNumber,
            disableContactId: disableContactId,
            onIdNumberFieldChange: onIdNumberFieldChange,
            onContactIdFieldChange: onContactIdFieldChange,
            onCardNumberFieldChange: onCardNumberFieldChange,
            onCardAmountChange: onCardAmountChange,
            setPaymentMethod: setPaymentMethod,

            /**
             * Initialization method.
             */
            initialize: function() {
                /**
                 * Placeholder for {this}.
                 *
                 * @type {object}
                 */
                var me = this;

                /**
                 * An instance of Remodal, the modal JQuery plugin.
                 *
                 * @type {null|object}
                 */
                var remodalInstance = null;

                /**
                 * The AJAX spinner used when fetching data for the "Read more"
                 * links.
                 *
                 * @type {null|HTMLDivElement}
                 */
                var remodalLoader = null;

                /**
                 * The content for the "Read more" links.
                 *
                 * @type {null|HTMLDivElement}
                 */
                var remodalContent = null;

                /**
                 * A flag that states whether the cart's totals has changed. If
                 * they do we need to fetch new data when clicking "Read more".
                 *
                 * @type {boolean}
                 */
                var totalsHasChanged = false;

                /**
                 * Billing address telephone field for this payment method.
                 *
                 * @type {UiClass|undefined}
                 */
                var billingTelephoneField;

                /**
                 * Billing address country field for this payment method.
                 *
                 * @type {UiClass|undefined}
                 */
                var billingCountryField;

                // If the user change shipping method, or if the store has
                // modules installed that allow the customer to change
                // quantities of items or otherwise change the totals, we need
                // to know so we can fetch new data for "Read more".
                quote.totals.subscribe(function() {
                    totalsHasChanged = true;
                });

                /**
                 * Initialize the Remodal window.
                 *
                 * NOTE: The modal window will open when initialized.
                 */
                function initRemodal() {
                    remodalLoader = $('#' + me.getCode() + '-loader')
                        .get(0);

                    remodalContent = $('#' + me.getCode() + '-content')
                        .get(0);

                    remodalInstance = $(
                        '[data-remodal-id="' + me.remodalId + '"]'
                    ).remodal({
                        hashTracking: false
                    });
                }

                /**
                 * Opens the Remodal window and fetches new data when needed.
                 */
                function openRemodalWindow() {
                    remodalInstance.open();

                    if (me.remodalData() === null || totalsHasChanged) {
                        $(remodalContent).hide();
                        $(remodalLoader).show();

                        model.getCostOfPurchase(me.getCode())
                            .done(function(response) {
                                $(remodalLoader).fadeOut(500, function() {
                                    if (!Array.isArray(response) &&
                                        response.hasOwnProperty('html')
                                    ) {
                                        me.remodalData(response.html);

                                        var tempDiv = document.createElement('div');
                                        tempDiv.innerHTML = response.html;
                                        var tabcontent = tempDiv.getElementsByClassName('priceinfotab');

                                        if (tabcontent.length) {
                                            openPriceInfo(
                                                null,
                                                tabcontent[0].id
                                            );
                                        }
                                    }

                                    totalsHasChanged = false;
                                    $(remodalContent).fadeIn(500);
                                });
                            });
                    }
                }

                this._super();

                if (configProvider.getIsBillingOnPaymentMethods()) {
                    billingTelephoneField = view.getBillingTelephoneField(
                        this.getCode()
                    );

                    billingCountryField = view.getBillingCountryField(
                        this.getCode()
                    );

                    if (typeof billingTelephoneField !== 'undefined' &&
                        typeof billingCountryField !== 'undefined'
                    ) {
                        billingTelephoneField.setValidation(
                            'resursbank-checkout-telephone',
                            true
                        );

                        billingCountryField.value.subscribe(function (value) {
                            billingAddressCountryId = value;
                        });

                        billingAddressCountryId = billingCountryField.value();
                    }
                }

                /**
                 * Whether the payment method has a card amount field.
                 *
                 * @type {boolean}
                 */
                this.hasCardAmount = hasCardAmount(this.getCode());

                // Initialize more properties if the payment method is able to
                // have an option for card amount.
                if (this.hasCardAmount) {

                    /**
                     * The requested card amount.
                     *
                     * @type {Function} ko.observable() - number.
                     */
                    this.cardAmount = ko.observable(
                        quote.totals().base_grand_total
                    );

                    /**
                     * Whether we should disable the card amount field.
                     *
                     * @type {computedObservable} Boolean.
                     */
                    this.disableCardAmount = ko.computed(function() {
                        return model.performingRequest();
                    });

                    /**
                     * Available options for card amount field.
                     *
                     * @type {Function} Array.
                     */
                    this.cardAmountOptions = ko.observable(
                        getCardAmountOptions(this.getCode())
                    );

                    quote.totals.subscribe(function (value) {
                        var paymentMethod =
                            checkoutData.getSelectedPaymentMethod();

                        if (paymentMethod === me.getCode()) {
                            me.cardAmountOptions(
                                getCardAmountOptions(me.getCode())
                            );
                            me.cardAmount(value.base_grand_total);
                        } else {
                            if (me.cardAmount() !== 0) {
                                me.cardAmount(0);
                                me.cardAmountOptions([]);
                            }
                        }
                    });

                    this.cardAmount.subscribe(function(value) {
                        model.pushCardAmount(value);
                    });

                    if (checkoutData.getSelectedPaymentMethod() === this.getCode()) {
                        // Push card amount after page load if this payment
                        // method is preselected.
                        model.pushCardAmount(this.cardAmount());
                    }
                }

                /**
                 * Checks if payment method requires a card number.
                 *
                 * @type {function} ko.observable - boolean.
                 */
                this.requiresCardNumber = ko.observable(
                    doesRequireCardNumber(this.getCode())
                );

                /**
                 * Whether the payment method has an SSN field.
                 *
                 * @type {boolean}
                 */
                this.hasSsnField = hasSsnField(this.getCode());

                // Because not every payment method will have a card input, we
                // set a unique property for every payment method. Every
                // property that is specified outside of this method will be
                // shared among the payment methods.
                this.hasCardNumber = ko.observable(
                    hasCardInput(this.getCode())
                );

                /**
                 * Check whether the method is provided by an external partner
                 * to Resurs Bank.
                 *
                 * @type {function} ko.observable() - boolean.
                 */
                this.isResursInternalMethod = ko.observable(
                    isResursInternalMethod(this.getCode())
                );

                /**
                 * Whether this payment method is a credit card.
                 *
                 * @type {boolean}
                 */
                this.isCreditCardMethod = isCreditCardMethod(this.getCode());

                /**
                 * Whether this payment method is connected to Swish.
                 *
                 * @type {boolean}
                 */
                this.isSwishMethod = isSwishMethod(this.getCode());

                /**
                 * Check whether or not the "Read More" button should be
                 * displayed.
                 *
                 * @type {function} ko.observable() - boolean.
                 */
                this.isResursReadMore = ko.observable(
                    isResursReadMore(this.getCode())
                );

                /**
                 * Whether or not order can be placed based on custom fields for our
                 * payment methods.
                 *
                 * @type {function} ko.observable() - boolean.
                 */
                this.canPlaceOrder = ko.computed(function() {
                    var idResult = (
                        !me.hasSsnField ||
                        (
                            idNumber() !== '' &&
                            !invalidIdNumber()
                        )
                    );

                    var cardNumberResult = !me.requiresCardNumber() ||
                        (cardNumber() !== '' && !invalidCardNumber());

                    var companyResult = !isCompanyCustomer() ||
                        (contactId() !== '' && !invalidContactId());

                    return idResult &&
                        cardNumberResult &&
                        companyResult &&
                        isPlaceOrderAllowed() &&
                        model.performingRequest() === false;
                });

                /**
                 * Whether to hide the Remodal AJAX spinner.
                 *
                 * @type {function} ko.observable() - boolean.
                 */
                this.hideRemodalLoader = ko.computed(function() {
                    return !model.fetchingReadMoreData();
                });

                /**
                 * The ID used to initiate the Remodal instance.
                 *
                 * @type {string}
                 */
                this.remodalId = this.getCode() + '-remodal';

                /**
                 * The url to Resurs Bank logo
                 *
                 * @type {string}
                 */
                this.resursBankLogo = require.toUrl(
                    'Resursbank_Checkout/images/resurs-bank-logo-2.png'
                );

                /**
                 * Path to the logo of a Swish payment method.
                 *
                 * @type {string}
                 */
                this.swishLogo = require.toUrl(
                    'Resursbank_Checkout/images/swish.png'
                );

                /**
                 * Path to the logo of a credit card payment method.
                 *
                 * @type {string}
                 */
                this.creditCardLogo = require.toUrl(
                    'Resursbank_Checkout/images/credit-card-x2.png'
                );

                /**
                 * HTML string with data to be displayed in the "Read more"
                 * modal window.
                 *
                 * @type {function} ko.observable() - string.
                 */
                this.remodalData = ko.observable(null);

                /**
                 * Called when "Read more" links are clicked to initiate the
                 * Remodal modal windows, and later to open the modal windows.
                 *
                 * NOTE: Can't initiate Remodal windows by checking if the
                 * elements has rendered as the Knockout bindings hasn't been
                 * initiated at that point, so the attributes that Remodal is
                 * looking for aren't there yet.
                 *
                 * @param {object} comp - The view model.
                 * @param {Event} event
                 */
                this.bindRemodal = function(comp, event) {
                    // Prevent link from adding a hashtag to the URL as it will
                    // cause Magento to redirect from checkout.
                    event.preventDefault();

                    if (remodalInstance === null) {
                        initRemodal();
                    }

                    openRemodalWindow();
                };
            },

            /**
             * Checks the validity of the ID number in session.
             *
             * @param {object} data - Data that KnockoutJS supplies.
             * @param {object} event
             */
            resursBankPlaceOrder: function(data, event) {
                var me = this;
                var isCompany = isCompanyCustomer();
                var hasCard = this.hasCardNumber();

                var validContactId = isCompany ?
                    Credentials.validate(
                        model.contactId(),
                        model.getConfigCountryIso(),
                        model.getPrivateCustomer()
                    ) : false;

                var validIdNumber = Credentials.validate(
                    model.idNumber(),
                    model.getConfigCountryIso(),
                    model.customerType()
                );

                var validCardNumber = hasCard ?
                    Credentials.validateCard(cardNumber()) :
                    true;

                if (validIdNumber &&
                    validCardNumber &&
                    (!isCompany || validContactId)
                ) {
                    if (model.ajaxChain.isEmpty()) {
                        this.placeOrder(data, event);
                    } else {
                        model.ajaxChain.onEmpty(function() {
                            me.placeOrder(data, event);
                        });
                    }
                }
            },

            /**
             * Action taken after order has successfully been created.
             */
            afterPlaceOrder: function () {
                redirectOnSuccessAction.redirectUrl = url.build(
                    'resursbank_checkout/simplified/redirect'
                );
            },

            /**
             * Update resursbank_payment_fee total value to reflect payment fee
             * of selected payment method.
             */
            updatePaymentFee: function () {
                setPaymentMethod(this.getCode());
            },

            /**
             * Retrieve configured fee for currently selected payment method.
             *
             * @returns {number}
             */
            getPaymentFee: function () {
                var result = model.getMethodData(this.getCode(), 'fee');

                return typeof result === 'number' ? result : 0;
            },

            /**
             * Retrieve configured title for currently selected payment method.
             *
             * @returns {string}
             */
            getTitle: function () {
                var result = model.getMethodData(this.getCode(), 'title');

                return (typeof result === 'string' && result !== '') ?
                    result :
                    this._super();
            }
        });
    }

    return init();
});
