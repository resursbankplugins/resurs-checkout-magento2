/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* @api */
define([
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/payment/renderer-list',
    'Magento_Checkout/js/model/payment/method-list',
    'Magento_Checkout/js/view/payment/list',
    'Magento_Checkout/js/view/payment',
], function (ko, Component, rendererList, paymentMethods, paymentList, paymentComponent) {
    'use strict';

    // Added through config provider, see etc/frontend/di.xml for more info.
    var methods = window.checkoutConfig.payment.resursbank.methods;

    // We loop through our list of dynamic payment methods and create a separate
    // instance for each of them using the same component.
    methods.forEach(
        function (method) {
            rendererList.push({
                type: method.code,
                component: 'Resursbank_Checkout/js/view/payment/method/resursbank'
            });
        }
    );

    /** Add view logic here if needed */
    return Component.extend({});
});
