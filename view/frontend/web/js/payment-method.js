/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'Resursbank_Checkout/js/select-payment-method',
        'Resursbank_Checkout/js/iframe'
    ],
    function(
        // Magento.
        SelectPaymentMethod,

        // Resurs Bank.
        Iframe
    ) {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/payment-method
         * @constant {object}
         */
        var EXPORT = {};

        /**
         * Whether the module has been initialized.
         *
         * @type {boolean}
         * @readonly
         */
        var INITIALIZED = false;

        /**
         * Suffix for each Resurs Bank method.
         *
         * @constant {string}
         */
        var METHOD_PREFIX = 'resursbank';

        /**
         * Suffix for each Resurs Bank method.
         *
         * @type {string}
         * @readonly
         */
        var METHOD_SUFFIX = '';

        /**
         * Selected payment method.
         *
         * @type {string}
         */
        var SELECTED_METHOD = '';

        /**
         * Says whether the module has been initialized.
         *
         * @returns {boolean}
         */
        function isInitialized() {
            return INITIALIZED;
        }

        /**
         * Corrects the name of the payment method that is sent by the iframe.
         *
         * @param method {string} The payment method.
         * @returns {string}
         */
        function correctPaymentMethod(method) {
            return METHOD_PREFIX +
                '_' +
                method.toLowerCase() +
                '_' +
                METHOD_SUFFIX;
        }

        /**
         * Sets the payment method on the quote object.
         *
         * @param {string} method
         * @returns {object} The module
         */
        function select(method) {
            SELECTED_METHOD = method;
            SelectPaymentMethod(correctPaymentMethod(method));

            return EXPORT;
        }

        /**
         *  Returns the selected payment method.
         *
         * @returns {string}
         */
        function getSelected() {
            return SELECTED_METHOD;
        }

        /**
         * Event handler when payment method changes in the iframe.
         *
         * @param {object} data
         * @param {string} data.method
         */
        function onPaymentMethodChange(data) {
            select(data.method);
        }

        /**
         * Initialization function.
         *
         * @returns {object} The module.
         */
        function init(config) {
            if (!INITIALIZED) {
                METHOD_SUFFIX = config.methodSuffix;

                Iframe.channel.on(
                    'payment-method-change',
                    onPaymentMethodChange
                );

                INITIALIZED = true;
            }

            return EXPORT;
        }

        EXPORT.init = init;
        EXPORT.isInitialized = isInitialized;
        EXPORT.select = select;
        EXPORT.getSelected = getSelected;
        EXPORT.correctPaymentMethod = correctPaymentMethod;

        return Object.freeze(EXPORT);
    }
);
