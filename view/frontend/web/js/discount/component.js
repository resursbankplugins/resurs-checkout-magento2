/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery',
        'ko',
        'uiComponent',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'Resursbank_Checkout/js/discount/model'
    ],
    function(
        // Magento.
        $,
        ko,
        Component,
        Quote,
        PriceUtils,

        // Resurs Bank.
        Model
    ) {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/discount/component
         * @constant {object}
         */
        var EXPORT = {};

        /**
         * Not used in here, but keep it around as KnockoutJS will throw
         * if we don't have it.
         */
        var IS_LOADING = ko.observable(false);

        /**
         * Whether the discount input should be disabled.
         *
         * @type {computedObservable}
         */
        var DISABLE_INPUT = ko.computed(function() {
            return Model.performingRequest() || Model.isApplied();
        });

        /**
         * Initialization function.
         *
         * @returns {object} The component object.
         */
        function init() {
            var totals = Quote.getTotals();

            if (totals()) {
                Model.discountCode(
                    totals()['coupon_code'] === null ?
                    '' :
                    totals()['coupon_code']
                );

                Model.discountAmount(
                    PriceUtils.formatPrice(
                        totals()['discount_amount'],
                        Quote.getPriceFormat()
                    )
                );
            }

            Model.isApplied(Model.discountCode() !== '');

            EXPORT.defaults = {
                template: 'Resursbank_Checkout/payment/discount'
            };

            EXPORT.discountCode = Model.discountCode;
            EXPORT.discountAmount = Model.discountAmount;
            EXPORT.isApplied = Model.isApplied;
            EXPORT.disabled = Model.performingRequest;
            EXPORT.disableInput = DISABLE_INPUT;
            EXPORT.isLoading = IS_LOADING;

            /**
             * Applies discount code.
             */
            EXPORT.apply = function() {
                if (this.validate() && !Model.isApplying()) {
                    Model.apply(Model.discountCode());
                }
            };

            /**
             * Removes an applied discount code.
             */
            EXPORT.cancel = function() {
                if (this.validate() && !Model.isCanceling()) {
                    Model.cancel();
                }
            };

            /**
             * Validates the discount form.
             *
             * @returns {*}
             */
            EXPORT.validate = function() {
                var form = $('#discount-form');

                return form.validation() && form.validation('isValid');
            };

            return EXPORT;
        }


        return Component.extend(init());
    }
);
