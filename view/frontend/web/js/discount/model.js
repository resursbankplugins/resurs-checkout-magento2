/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    [
        'jquery',
        'ko',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/resource-url-manager',
        'Magento_Checkout/js/action/get-payment-information',
        'Magento_Catalog/js/price-utils',
        'Resursbank_Checkout/js/cart/item/item',
        'Resursbank_Checkout/js/cart/item/prices',
        'Resursbank_Checkout/js/cart/item/response/change',
        'Resursbank_Checkout/js/cart/item/response/remove',
        'Resursbank_Checkout/js/lib/ajax',
        'Resursbank_Checkout/js/lib/ajax/response',
        'Resursbank_Checkout/js/lib/server'
    ], 
    function(
        // Magento.
        $,
        ko,
        Quote,
        UrlManager,
        GetPaymentInformationAction,
        PriceUtils,

        // Resurs Bank.
        Item,
        Prices, 
        ItemChangeResponse,
        ItemRemoveResponse,
        Ajax,
        Response,
        Server
    ) {
        'use strict';

        /**
         * @exports Resursbank_Checkout/js/discount/model
         * @constant {object}
         */
        var EXPORT = {};

        /**
         * @constant {Chain}
         */
        var AJAX_CHAIN = new Ajax.Chain({
            limit: 1,
            name: 'Discount'
        });

        /**
         * The discount amount of an applied discount code. If none is applied,
         * it will have a value of 0.
         *
         * @constant {function} ko.observable - number.
         */
        var DISCOUNT_AMOUNT = ko.observable(0);

        /**
         * The applied discount code. If none is applied, it will be an empty
         * string.
         *
         * @constant {function} ko.observable - string.
         */
        var DISCOUNT_CODE = ko.observable('');

        /**
         * Whether a discount code is applied.
         *
         * @constant {function} ko.observable - boolean.
         */
        var IS_APPLIED = ko.observable(false);

        /**
         * Whether there's an ongoing request to apply a discount code.
         *
         * @constant {function} ko.observable - boolean.
         */
        var IS_APPLYING = ko.observable(false);

        /**
         * Whether there's an ongoing request to cancel a discount code.
         *
         * @constant {function} ko.observable - boolean.
         */
        var IS_CANCELING = ko.observable(false);

        /**
         * Whether there's an ongoing request.
         *
         * @constant {function} ko.observable - boolean.
         */
        var PERFORMING_REQUEST = ko.computed(function() {
            return IS_APPLYING() || IS_CANCELING();
        });

        /**
         * Pushes a discount code to the server where it will be applied.
         *
         * @param {string} code
         * @returns {Request}
         */
        function apply(code) {
            var call = getApplyCall(code);
            var deferred = $.Deferred();

            deferred.done(function() {
                var totals = Quote.getTotals();

                IS_APPLIED(true);

                DISCOUNT_AMOUNT(
                    PriceUtils.formatPrice(
                        totals()['discount_amount'],
                        Quote.getPriceFormat()
                    )
                );
            });

            deferred.always(function() {
                IS_APPLYING(false);
            });

            call.onDone(function() {
                GetPaymentInformationAction(deferred);
            });

            call.onFail(function() {
                IS_APPLYING(false);
            });

            AJAX_CHAIN.add(call).run();

            IS_APPLYING(true);

            return new Ajax.Request({
                call: call,
                deferred: deferred
            });
        }

        /**
         * Cancels an applied discount code.
         *
         * @returns {Request}
         */
        function cancel() {
            var call = getCancelCall();
            var deferred = $.Deferred();

            deferred.done(function() {
                IS_APPLIED(false);
                DISCOUNT_CODE('');
            });

            deferred.always(function() {
                IS_CANCELING(false);
            });

            call.onDone(function() {
                GetPaymentInformationAction(deferred);
            });

            call.onFail(function() {
                IS_CANCELING(false);
            });

            AJAX_CHAIN.add(call).run();

            IS_CANCELING(true);

            return new Ajax.Request({
                call: call,
                deferred: deferred
            });
        }

        /**
         * Creates a call to apply a discount code.
         *
         * @param {string} code
         * @returns {Call}
         */
        function getApplyCall(code) {
            return new Ajax.Call({
                options: {
                    method: 'PUT',
                    url: Server.getBaseUrl() + createApplyUrl(code),
                    data: {}
                }
            });
        }

        /**
         * Creates a call to cancel an applied discount code.
         *
         * @returns {Call}
         */
        function getCancelCall() {
            return new Ajax.Call({
                options: {
                    method: 'DELETE',
                    url: Server.getBaseUrl() + createCancelUrl(),
                    data: {}
                }
            });
        }

        /**
         * Creates the URL for applying a discount code.
         *
         * @param {string} code
         * @returns {string}
         */
        function createApplyUrl(code) {
            return UrlManager.getApplyCouponUrl(code, Quote.getQuoteId());
        }

        /**
         * Creates the URL for canceling a discount code.
         *
         * @returns {string}
         */
        function createCancelUrl() {
            return UrlManager.getCancelCouponUrl(Quote.getQuoteId());
        }

        EXPORT.apply = apply;
        EXPORT.cancel = cancel;
        EXPORT.ajaxChain = AJAX_CHAIN;
        EXPORT.discountAmount = DISCOUNT_AMOUNT;
        EXPORT.discountCode = DISCOUNT_CODE;
        EXPORT.isApplied = IS_APPLIED;
        EXPORT.isApplying = IS_APPLYING;
        EXPORT.isCanceling = IS_CANCELING;
        EXPORT.performingRequest = PERFORMING_REQUEST;

        return Object.freeze(EXPORT);
    }
);
