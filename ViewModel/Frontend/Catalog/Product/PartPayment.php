<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\ViewModel\Frontend\Catalog\Product;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Helper\Data;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * @package Resursbank\Checkout\ViewModel\Frontend\Catalog\Product
 */
class PartPayment implements ArgumentInterface
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var FormKey
     */
    private $formKey;

    /**
     * @var Data
     */
    private $catalogHelper;

    /**
     * PartPayment constructor.
     *
     * @param FormKey $formKey
     * @param Data $catalogHelper
     */
    public function __construct(
        FormKey $formKey,
        Data $catalogHelper
    ) {
        $this->formKey = $formKey;
        $this->catalogHelper = $catalogHelper;
        $this->product = $this->catalogHelper->getProduct();
    }

    /**
     * Get form key.
     *
     * @return string
     */
    public function getFormKey(): string
    {
        return $this->formKey->getFormKey();
    }

    /**
     * Returns the price of the product, or 0.0 if the product can't be found.
     *
     * @return float
     */
    public function getPrice(): float
    {
        return $this->product->getPrice() !== null ?
            (float) $this->getFinalPrice() :
            0.0;
    }

    /**
     * Returns product final price.
     *
     * @return null|float
     */
    public function getFinalPrice()
    {
        return $this->product
            ->getPriceInfo()
            ->getPrices()
            ->get('final_price')
            ->getAmount()
            ->getValue();
    }

    /**
     * Retrieve product type.
     *
     * @return string
     */
    public function getProductType(): string
    {
        // Avoid type casting here since getTypeId may return an array.
        $result = $this->product->getTypeId();

        return !is_string($result) ? '' : $result;
    }
}
