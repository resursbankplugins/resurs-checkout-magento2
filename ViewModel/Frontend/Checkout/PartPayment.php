<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\ViewModel\Frontend\Checkout;

use Exception;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\Api;

/**
 * @package Resursbank\Checkout\ViewModel\Frontend\Checkout
 */
class PartPayment implements ArgumentInterface
{
    /**
     * @var FormKey
     */
    private $formKey;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var Api
     */
    private $apiModel;

    /**
     * @param FormKey $formKey
     * @param Log $log
     * @param Api $apiModel
     */
    public function __construct(
        FormKey $formKey,
        Log $log,
        Api $apiModel
    ) {
        $this->formKey = $formKey;
        $this->log = $log;
        $this->apiModel = $apiModel;
    }

    /**
     * @return string
     */
    public function getFormKey(): string
    {
        $result = '';

        try {
            $result = $this->formKey->getFormKey();
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Returns quote grand total.
     *
     * @return float
     */
    public function getGrandTotal(): float
    {
        $result = 0;

        try {
            $result = $this->apiModel->getQuote()->getGrandTotal();
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return (float) $result;
    }
}
