<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\ViewModel\Adminhtml\Sales\Order\View\Info;

use Exception;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Sales\Model\Order;
use Resursbank\Checkout\Api\Data\PaymentHistoryInterface;
use Resursbank\Checkout\Api\Data\PaymentHistorySearchResultsInterface;
use Resursbank\Checkout\Helper\Admin;
use Resursbank\Checkout\Helper\Db\PaymentHistory as PaymentHistoryDb;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Version;
use Resursbank\Checkout\Helper\Order as HelperOrder;

/**
 * Injects custom HTML containing payment history information on order view in
 * admin panel.
 *
 * @package Resursbank\Checkout\ViewModel\Adminhtml\Sales\Order\View\Info
 */
class PaymentHistory implements ArgumentInterface
{
    /**
     * @var PaymentHistoryDb
     */
    private $paymentHistoryDb;

    /**
     * @var PaymentHistorySearchResultsInterface
     */
    private $paymentSearchResults;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var Version
     */
    private $version;

    /**
     * @var Admin
     */
    private $admin;

    /**
     * @var HelperOrder
     */
    private $helperOrder;

    /**
     * @param PaymentHistoryDb $paymentHistoryDb
     * @param PaymentHistorySearchResultsInterface $paymentSearchResults
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param Log $log
     * @param Version $version
     * @param Admin $admin
     * @param HelperOrder $helperOrder
     */
    public function __construct(
        PaymentHistoryDb $paymentHistoryDb,
        PaymentHistorySearchResultsInterface $paymentSearchResults,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Log $log,
        Version $version,
        Admin $admin,
        HelperOrder $helperOrder
    ) {
        $this->paymentHistoryDb = $paymentHistoryDb;
        $this->paymentSearchResults = $paymentSearchResults;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->log = $log;
        $this->version = $version;
        $this->admin = $admin;
        $this->helperOrder = $helperOrder;
    }

    /**
     * Fetch payment history events relating to provided order.
     *
     * @param Order $order
     * @return PaymentHistoryInterface[]
     */
    public function getEvents(Order $order): array
    {
        $items = [];

        try {
            $criteria = $this->searchCriteriaBuilder->addFilter(
                PaymentHistoryInterface::PAYMENT_ID,
                $order->getPayment()->getEntityId(),
                'eq'
            )->create();

            $items = $this->paymentHistoryDb
                ->getRepository()
                ->getList($criteria)
                ->getItems();

        } catch (Exception $e) {
            $this->log->error(
                'Could not retrieve list of payment history events for ' .
                'order #' . $order->getIncrementId() . ' : ' . $e->getMessage()
            );
        }

        return $items;
    }

    /**
     * Converts data from a payment history event to presentational data.
     *
     * @param PaymentHistoryInterface $event
     * @return array
     */
    public function eventToTableData(PaymentHistoryInterface $event): array
    {
        $ev = $event->getEvent();
        $user = $event->getUser();

        return [
            'event' => $ev === null ? '' : $event->getEventLabel($ev),
            'user' => $user === null ? '' : $event->getUserLabel($user),
            'timestamp' => $event->getCreatedAt(''),
            'extra' => $event->getExtra(''),
            'state_from' => $event->getStateFrom(''),
            'state_to' => $event->getStateTo(''),
            'status_from' => $event->getStatusFrom(''),
            'status_to' => $event->getStatusTo('')
        ];
    }

    /**
     * Fetch payment history events relating to provided order and convert the
     * data to a presentational form.
     *
     * @param Order $order
     * @return array
     */
    public function getTableDataFromOrder(Order $order): array
    {
        $arr = [];

        foreach ($this->getEvents($order) as $event) {
            $arr[] = $this->eventToTableData($event);
        }

        return $arr;
    }

    /**
     * Returns the incremental ID of the given order.
     *
     * @param Order $order
     * @return string
     */
    public function getOrderNumber(Order $order): string
    {
        return $order->getIncrementId();
    }

    /**
     * Returns the module versions for data, schema and composer.
     *
     * @return array
     */
    public function getModuleVersions(): array
    {
        return $this->version->getAll();
    }

    /**
     * Returns the username of the currently logged in user.
     *
     * @return string
     */
    public function getLoggedInUsername(): string
    {
        return $this->admin->getUserName();
    }

    /**
     * Get the environment that was selected in the Resurs Bank configuration
     * at the time when the order was placed.
     *
     * @param Order $order
     * @return string
     */
    public function getOrderEnvironment(Order $order): string
    {
        return $this->helperOrder->getEnvironment($order);
    }

    /**
     * Check to see if payment history should be visible.
     *
     * @param Order $order
     * @return boolean
     */
    public function visible(Order $order) : bool
    {
        return $this->helperOrder->isResursOrder($order);
    }
}
