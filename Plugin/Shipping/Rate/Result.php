<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Plugin\Shipping\Rate;

use Exception;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address;
use Magento\Shipping\Model\Rate\Result as Original;
use Resursbank\Checkout\Helper\Api;
use Resursbank\Checkout\Helper\Config\Checkout\Advanced as AdvancedConfig;
use Resursbank\Checkout\Helper\Config\Checkout\General as GeneralConfig;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Log;

/**
 * Intercept process where shipping rates are collected to be rendered in
 * checkout.
 *
 * @package Resursbank\Checkout\Plugin\Shipping\Rate
 */
class Result
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var GeneralConfig
     */
    private $generalConfig;

    /**
     * @var AdvancedConfig
     */
    private $advancedConfig;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var Api
     */
    private $api;

    /**
     * @var State
     */
    private $state;

    /**
     * Constructor.
     *
     * @param Session $session
     * @param GeneralConfig $generalConfig
     * @param AdvancedConfig $advancedConfig
     * @param ApiConfig $apiConfig
     * @param Log $log
     * @param Api $api
     * @param State $state
     */
    public function __construct(
        Session $session,
        GeneralConfig $generalConfig,
        AdvancedConfig $advancedConfig,
        ApiConfig $apiConfig,
        Log $log,
        Api $api,
        State $state
    ) {
        $this->session = $session;
        $this->generalConfig = $generalConfig;
        $this->advancedConfig = $advancedConfig;
        $this->apiConfig = $apiConfig;
        $this->log = $log;
        $this->api = $api;
        $this->state = $state;
    }

    /**
     * Intercept the process which collects carrier (shipping)
     *
     * @param Original $subject
     * @return null
     */
    public function beforeGetAllRates(Original $subject)
    {
        try {
            if ($this->isEnabled()) {
                /** @var Quote $quote */
                $quote = $this->api->getQuote();

                /** @var Address $address */
                $address = $quote->getShippingAddress();

                $hasAddress = (
                    ($address instanceof Address) &&
                    $address->getData('street') &&
                    $address->getData('city') &&
                    $address->getData('postcode') &&
                    $address->getData('firstname') &&
                    $address->getData('lastname')
                );

                if (!$hasAddress) {
                    $subject->reset();
                    $address->collectShippingRates();
                }
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return null;
    }

    /**
     * Check if this plugin should execute.
     *
     * @return bool
     * @throws LocalizedException
     * @throws Exception
     */
    protected function isEnabled()
    {
        return (
            $this->state->getAreaCode() !== 'adminhtml' &&
            $this->generalConfig->isEnabled() &&
            $this->apiConfig->isCheckoutFlow() &&
            $this->advancedConfig->isCarrierRatesRequireAddressEnabled()
        );
    }
}
