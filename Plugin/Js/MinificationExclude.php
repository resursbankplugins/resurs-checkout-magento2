<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Plugin\Js;

use Magento\Framework\View\Asset\Minification;

/**
 * Exclude external oc-shop.js file from JS minification process.
 *
 * @package Resursbank\Checkout\Plugin\Js
 */
class MinificationExclude
{
    /**
     * @param Minification $subject
     * @param array $result
     * @param string $contentType
     * @return array
     */
    public function afterGetExcludes(
        Minification $subject,
        array $result,
        $contentType
    ) {
        if ($contentType === 'js') {
            $result[] = 'oc-shop.js';
        }

        return $result;
    }
}
