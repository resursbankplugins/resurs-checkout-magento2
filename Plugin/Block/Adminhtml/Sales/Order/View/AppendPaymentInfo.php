<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Plugin\Block\Adminhtml\Sales\Order\View;

use Exception;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Payment;
use Resursbank\Checkout\Block\Adminhtml\Sales\Order\View\Info\Payment as PaymentBlock;
use Magento\Sales\Block\Adminhtml\Order\View\Info;
use Magento\Framework\ObjectManagerInterface;
use Magento\Sales\Model\Order;

/**
 * Prepends checkout information (invoice information from Resurs Bank) to the
 * information block on the order view.
 */
class AppendPaymentInfo
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Log helper.
     *
     * @var Log
     */
    private $log;

    /**
     * Payment helper.
     *
     * @var Payment
     */
    private $payment;

    /**
     * @param ObjectManagerInterface $objectManager
     * @param Log $log
     * @param Payment $payment
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        Log $log,
        Payment $payment
    ) {
        $this->objectManager = $objectManager;
        $this->log = $log;
        $this->payment = $payment;
    }

    /**
     * Prepend checkout information to order information block.
     *
     * @param Info $subject
     * @param $result
     * @return string
     * @throws Exception
     */
    public function afterToHtml(Info $subject, $result)
    {
        try {
            if ($this->isEnabled($subject->getOrder())) {
                /** @var PaymentBlock $block */
                $block = $this->objectManager->create(
                    '\Resursbank\Checkout\Block\Adminhtml\Sales\Order\View' .
                    '\Info\Payment'
                );

                $result = $block->toHtml() . $result;
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Check whether or not this plugin should execute.
     *
     * @param Order $order
     * @return bool
     * @throws Exception
     */
    private function isEnabled(Order $order)
    {
        return $this->payment->validate($order);
    }
}
