<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Plugin\Quote;

use Exception;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Quote\Model\QuoteManagement as QuoteManagementModel;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Resursbank\Checkout\Helper\Checkout\Onepage;
use Resursbank\Checkout\Helper\Config\Checkout\Advanced;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Payment as PaymentHelper;

/**
 * Remove old order when an error occurs during the checkout process (to avoid
 * dangling, cancelled, orders).
 *
 * @package Resursbank\Checkout\Plugin\Quote
 */
class QuoteManagement
{
    /**
     * @var Onepage
     */
    private $onepage;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var Advanced
     */
    private $configAdvanced;

    /**
     * @var PaymentHelper
     */
    private $paymentHelper;

    /**
     * @var Log
     */
    private $log;

    /**
     * @param Onepage $onepage
     * @param OrderRepositoryInterface $orderRepository
     * @param Advanced $configAdvanced
     * @param PaymentHelper $paymentHelper
     * @param Log $log
     */
    public function __construct(
        Onepage $onepage,
        OrderRepositoryInterface $orderRepository,
        Advanced $configAdvanced,
        PaymentHelper $paymentHelper,
        Log $log
    ) {
        $this->onepage = $onepage;
        $this->orderRepository = $orderRepository;
        $this->configAdvanced = $configAdvanced;
        $this->paymentHelper = $paymentHelper;
        $this->log = $log;
    }

    /**
     * When the order process fails (for example because of a rejected credit
     * application) we want to remove the order which may already have been
     * created (orders are created before the payment at Resurs Bank is
     * completed). If we do not remove the order with the failed payment we may
     * end up with a lot of orders from the same person, due to subsequent
     * attempts to place an order.
     *
     * This method deletes old orders. We can still re-use the increment ID from
     * the initial order attempt, so long as the customer session remains.
     *
     * @param QuoteManagementModel $subject
     * @param $cartId
     * @param PaymentInterface|null $paymentMethod
     * @return null
     */
    public function beforePlaceOrder(
        QuoteManagementModel $subject,
        $cartId,
        PaymentInterface $paymentMethod = null
    ) {
        try {
            if ($this->onepage->hasCurrentOrder()) {
                /** @var Order $order */
                $order = $this->onepage->getCurrentOrder();

                if ($this->isEnabled($order)) {
                    $this->orderRepository->delete($order);
                }
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return null;
    }

    /**
     * Check whether this plugin is enabled.
     *
     * @param Order $order
     * @return bool
     */
    private function isEnabled(Order $order)
    {
        $reservedOrderId = $this->onepage->getSession()->getQuote()
            ->getReservedOrderId();

        return (
            $this->configAdvanced->getReuseErroneouslyCreatedOrders() &&
            $order->getIncrementId() === $reservedOrderId &&
            $this->paymentHelper->validateMethod($order)
        );
    }
}
