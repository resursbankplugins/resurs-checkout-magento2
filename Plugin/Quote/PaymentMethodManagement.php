<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Plugin\Quote;

use \Magento\Quote\Model\PaymentMethodManagement as MagentoPaymentMethodManagement;
use \Magento\Quote\Api\Data\PaymentInterface;
use \Magento\Quote\Api\CartRepositoryInterface;
use \Magento\Quote\Model\Quote;
use \Magento\Framework\Exception\NoSuchEntityException;
use \Resursbank\Checkout\Helper\Db\Account\Method as MethodDb;

/**
 * Intercept process where payment method is applied on quote.
 *
 * @package Resursbank\Checkout\Plugin\Quote
 */
class PaymentMethodManagement
{
    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var MethodDb
     */
    private $methodDb;

    /**
     * Constructor
     *
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param MethodDb $methodDb
     */
    public function __construct(
        CartRepositoryInterface $quoteRepository,
        MethodDb $methodDb
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->methodDb = $methodDb;
    }

    /**
     * We check that the requested payment method (provided through a selection
     * in the checkout iframe) has a corresponding class within our module.
     * Since these are loaded dynamically, and can technically be removed
     * without affecting the module in other ways, we override the selection to
     * use the "resursbank_default" payment method if no class exist for the
     * requested method. This way we ensure that the checkout always works.
     *
     * @param MagentoPaymentMethodManagement $subject
     * @param $cartId
     * @param PaymentInterface $method
     * @return array
     * @throws NoSuchEntityException
     */
    public function beforeSet(
        MagentoPaymentMethodManagement $subject,
        $cartId,
        PaymentInterface $method
    ) {
        if (substr((string) $method->getMethod(), 0, 11) === 'resursbank_') {
            /** @var Quote $quote */
            $quote = $this->quoteRepository->get($cartId);

            // We need this to support the zeroTotalValidator call occurring
            // in vendor/magento/module-quote/Model/PaymentMethodManagement.php
            // :: set(). Otherwise we will always get an Exception when
            // attempting to check out a zero sum order.
            if ((float) $quote->getGrandTotal() === 0.0) {
                $method->setMethod('free');
            } else {
                $model = $this->methodDb->loadByCode($method->getMethod());

                if (!$model->getId() || !$model->isActive()) {
                    $method->setMethod('resursbank_default');
                }
            }
        }

        return [$cartId, $method];
    }
}
