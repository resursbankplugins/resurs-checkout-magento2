<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Plugin\Cart;

use Exception;
use Magento\Checkout\Controller\Cart\Index;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Result\Page;
use Resursbank\Checkout\Helper\Api;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ConfigApi;
use Resursbank\Checkout\Helper\Config\Checkout\General as Config;
use Resursbank\Checkout\Helper\Log;

/**
 * Redirects requests for the cart page to the checkout page.
 *
 * @package Resursbank\Checkout\Plugin\Cart
 */
class Redirect
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var RedirectFactory
     */
    private $redirectFactory;

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @var Api
     */
    private $apiHelper;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var ConfigApi
     */
    private $configApi;

    /**
     * @param Config $config
     * @param RedirectFactory $redirectFactory
     * @param UrlInterface $url
     * @param Api $apiHelper
     * @param Log $log
     * @param ConfigApi $configApi
     */
    public function __construct(
        Config $config,
        RedirectFactory $redirectFactory,
        UrlInterface $url,
        Api $apiHelper,
        Log $log,
        ConfigApi $configApi
    ) {
        $this->config = $config;
        $this->redirectFactory = $redirectFactory;
        $this->url = $url;
        $this->apiHelper = $apiHelper;
        $this->log = $log;
        $this->configApi = $configApi;
    }

    /**
     * Redirect cart to checkout.
     *
     * @param Index $subject
     * @param Page $result
     * @return mixed
     * @throws Exception
     */
    public function afterExecute(Index $subject, Page $result)
    {
        try {
            if ($this->isEnabled()) {
                return $this->redirectFactory
                    ->create()
                    ->setPath($this->getPath());
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }

        return $result;
    }

    /**
     * Check if this plugin should execute.
     *
     * @return bool
     * @throws Exception
     */
    protected function isEnabled()
    {
        return (
            $this->config->isEnabled() &&
            $this->config->isRedirectCartToCheckoutEnabled() &&
            $this->configApi->isCheckoutFlow()
        );
    }

    /**
     * Retrieve redirect URI path.
     *
     * @return string
     * @throws NoSuchEntityException
     */
    protected function getPath()
    {
        return $this->url->getUrl(
            $this->apiHelper->cartIsEmpty() ?
                '/' :
                'checkout/index'
        );
    }
}
