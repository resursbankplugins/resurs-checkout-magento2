<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Plugin\Simplified;

use \Exception;
use \Magento\Checkout\Block\Checkout\LayoutProcessor\Interceptor;
use \Resursbank\Checkout\Helper\Logic\Account\Method;
use \Resursbank\Checkout\Helper\Config\Checkout\Advanced as ConfigAdvanced;

/**
 * Injects 'isBillingAddressRequired' property for all our payment methods in
 * the compiled layout XML. This is to ensure the billing address form section
 * is displayed for all our payment methods without us needing to specify the
 * requirement in the layout XML for each payment method (since the methods are
 * dynamically named for each account this is not a possibility for us).
 *
 * @package Resursbank\Checkout\Plugin\Simplified
 */
class Layout
{
    /**
     * @var Method
     */
    private $method;

    /**
     * @var ConfigAdvanced
     */
    private $configAdvanced;

    /**
     * @param Method $method
     * @param ConfigAdvanced $configAdvanced
     */
    public function __construct(
        Method $method,
        ConfigAdvanced $configAdvanced
    ) {
        $this->method = $method;
        $this->configAdvanced = $configAdvanced;
    }

    /**
     * @param Interceptor $subject
     * @param array $result
     * @return array
     * @throws Exception
     */
    public function beforeProcess(Interceptor $subject, $result)
    {
        if (isset($result['components']['checkout']['children']['steps']
            ['children']['billing-step']['children']
            ['payment']['children'])
        ) {
            $methods = array_filter(
                $this->method->collect(),
                function (array $method) {
                    return isset($method['active']) &&
                        $method['active'] === '1';
                }
            );

            foreach ($methods as $method) {
                $result['components']['checkout']['children']['steps']
                ['children']['billing-step']['children']['payment']['children']
                ['renders']['children']['resursbank']['methods']
                [$method['code']]['isBillingAddressRequired'] = true;
            }
        }

        if (isset($result['components']['checkout']['children']['steps']
            ['children']['shipping-step']['children']['shippingAddress']
            ['children']['shipping-address-fieldset']['children']
            ['resursbank-simplified-telephone-validation'])
        ) {
            if (!$this->configAdvanced->isValidatePhoneNumberEnabled()) {
                unset(
                    $result['components']['checkout']['children']['steps']
                    ['children']['shipping-step']['children']['shippingAddress']
                    ['children']['shipping-address-fieldset']['children']
                    ['resursbank-simplified-telephone-validation']
                );
            }
        }

        return [$result];
    }
}
