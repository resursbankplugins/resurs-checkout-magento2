<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Plugin\Layout\File;

use \Exception;
use \Magento\Framework\View\Layout\File\Collector\Aggregated;
use \Magento\Framework\View\File;
use \Resursbank\Checkout\Helper\Config\Checkout\General as GeneralConfig;
use \Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;

/**
 * This plugin will exclude our overriding layout file for checkout_index_index
 * if the module has been disabled.
 *
 * @package Resursbank\Checkout\Plugin\Layout\File
 */
class Collector
{
    /**
     * @var GeneralConfig
     */
    private $generalConfig;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @param GeneralConfig $generalConfig
     * @param ApiConfig $apiConfig
     */
    public function __construct(
        GeneralConfig $generalConfig,
        ApiConfig $apiConfig
    ) {
        $this->generalConfig = $generalConfig;
        $this->apiConfig = $apiConfig;
    }

    /**
     * @param Aggregated $subject
     * @param array $result
     * @return File[]
     * @throws Exception
     */
    public function afterGetFiles(Aggregated $subject, array $result)
    {
        // NOTE: The nestled if statements here are necessary, check the else
        // clause.
        if ($this->generalConfig->isEnabled()) {
            if ($this->apiConfig->isSimplifiedFlow()) {
                /**
                 * @var int $key
                 * @var File $file
                 */
                foreach ($result as $key => $file) {
                    if ($this->isCheckoutLayout($file)) {
                        $fileName = dirname($file->getFilename()) .
                            '/simplified/checkout_index_index.xml';

                        $newFile = new File(
                            $fileName,
                            $file->getModule(),
                            $file->getTheme(),
                            $file->isBase()
                        );

                        $result[$key] = $newFile;
                    }
                }
            }
        } else {
            /**
             * @var int $key
             * @var File $file
             */
            foreach ($result as $key => $file) {
                if ($this->isCheckoutLayout($file)) {
                    unset($result[$key]);
                }
            }
        }

        return $result;
    }

    /**
     * Check if provided layout file is our checkout_index_index.xml file.
     *
     * @param File $file
     * @return bool
     */
    protected function isCheckoutLayout(File $file)
    {
        return (
            $file->getModule() === 'Resursbank_Checkout' &&
            strpos($file->getFileIdentifier(), 'checkout_index_index.xml')
        );
    }
}
