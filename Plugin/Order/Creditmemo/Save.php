<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Plugin\Order\Creditmemo;

use Exception;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Creditmemo;
use Magento\Sales\Model\Order\Interceptor;
use Resursbank\Checkout\Helper\Admin;
use Resursbank\Checkout\Helper\Api;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Config\Checkout\Ecom as EcomConfig;
use Resursbank\Checkout\Helper\Db\PaymentHistory as PaymentHistoryDbHelper;
use Resursbank\Checkout\Helper\Ecom;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Order as OrderHelper;
use Resursbank\Checkout\Helper\Payment;
use Resursbank\Checkout\Model\Api\Credentials;
use Resursbank\Checkout\Model\Api\Payment\Converter\CreditmemoConverter;
use Resursbank\Checkout\Model\PaymentHistory;
use Resursbank\RBEcomPHP\ResursBank;

/**
 * Credit payment at Resurs Bank when a credit memo for the corresponding order
 * is created in Magento.
 *
 * @package Resursbank\Checkout\Plugin\Order\Creditmemo
 */
class Save
{
    /**
     * @var Ecom
     */
    private $ecom;

    /**
     * @var Api
     */
    private $api;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var Payment
     */
    private $payment;

    /**
     * @var CreditmemoConverter
     */
    private $creditmemoConverter;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @var EcomConfig
     */
    private $ecomConfig;

    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @var PaymentHistoryDbHelper
     */
    private $paymentHistoryDbHelper;

    /**
     * @var Admin
     */
    private $admin;

    /**
     * @param Ecom $ecom
     * @param Api $api
     * @param ManagerInterface $messageManager
     * @param Log $log
     * @param Payment $payment
     * @param CreditmemoConverter $creditmemoConverter
     * @param ApiConfig $apiConfig
     * @param EcomConfig $ecomConfig
     * @param OrderHelper $orderHelper
     * @param PaymentHistoryDbHelper $paymentHistoryDbHelper
     * @param Admin $admin
     */
    public function __construct(
        Ecom $ecom,
        Api $api,
        ManagerInterface $messageManager,
        Log $log,
        Payment $payment,
        CreditmemoConverter $creditmemoConverter,
        ApiConfig $apiConfig,
        EcomConfig $ecomConfig,
        OrderHelper $orderHelper,
        PaymentHistoryDbHelper $paymentHistoryDbHelper,
        Admin $admin
    ) {
        $this->ecom = $ecom;
        $this->api = $api;
        $this->messageManager = $messageManager;
        $this->log = $log;
        $this->payment = $payment;
        $this->creditmemoConverter = $creditmemoConverter;
        $this->apiConfig = $apiConfig;
        $this->ecomConfig = $ecomConfig;
        $this->orderHelper = $orderHelper;
        $this->paymentHistoryDbHelper = $paymentHistoryDbHelper;
        $this->admin = $admin;
    }

    /**
     * When a credit memo is created in Magento we will create a credit payment
     * matching its content.
     *
     * @param Creditmemo $subject
     * @param Interceptor $result
     * @return Interceptor
     * @throws Exception
     */
    public function afterAfterSave(Creditmemo $subject, $result)
    {
        try {
            /** @var Order $order */
            $order = $subject->getOrder();

            if ($this->isEnabled($order)) {
                /** @var string $reference */
                $reference = $this->orderHelper->getPaymentId($order);

                if (!$this->refund($subject, $reference)) {
                    throw new Exception(
                        'An error occurred while communicating with the API.'
                    );
                }

                $this->log->info("Successfully refunded payment {$reference}");

                // Log payment history event.
                $this->logPaymentCreditedEvent($order);
            }
        } catch (Exception $e) {
            // Display generic error message.
            $this->messageManager->addErrorMessage(
                sprintf(__('Failed to credit Resurs Bank payment: %s.'), $e->getMessage())
            );

            // Log actual error.
            $this->log->error($e);

            throw $e;
        }

        return $result;
    }

    /**
     * Refund Resurs Bank payment.
     *
     * @param Creditmemo $memo
     * @param string $reference
     * @return bool
     * @throws Exception
     */
    private function refund(Creditmemo $memo, $reference)
    {
        $this->log->info("Refunding payment {$reference}");

        /** @var ResursBank $ecom */
        $ecom = $this->ecom->getConnection(
            $this->getApiCredentials($memo->getOrder())
        );

        // Set platform / user reference.
        $ecom->setRealClientName('Magento2');
        $ecom->setLoggedInUser($this->admin->getUserName());

        // Without this ECom will lose the payment reference.
        $ecom->setPreferredId($reference);

        $this->creditmemoConverter->convert($ecom, $memo);

        /* Even though we disable payload validation in our call to
        creditPayment below, ECom will perform some validation, which will fail
        for certain order lines (if you part debit a discount a new order line
        is created in the payment at Resurs Bank, it's marked as DEBIT but never
        AUTHORIZE, because of this ECom won't see it in getPaymentDiffAsTable(),
        and thus we cannot credit the new discount order line after it has been
        added). The line below changes what data ECom utilizes to identify order
        lines and is essentially a work-around to this problem (doing this ECom
        will see the initial discount line, which is in AUTHORIZE, and pass the
        validation for our new discount order line).  */
        $ecom->setGetPaymentMatchKeys(
            ['artNo', 'description', 'unitMeasure']
        );

        return $ecom->creditPayment($reference, null, false, true);
    }

    /**
     * Retrieve API credentials relative to the store the order was created in.
     *
     * @param Order $order
     * @return Credentials
     * @throws Exception
     */
    private function getApiCredentials(Order $order)
    {
        return $this->apiConfig->getCredentials($order->getStore()->getCode());
    }

    /**
     * Check whether this plugin should execute.
     *
     * @param Order $order
     * @return bool
     * @throws Exception
     */
    private function isEnabled(Order $order)
    {
        return (
            $this->ecomConfig->isAfterShopEnabled(
                $order->getStore()->getCode()
            ) &&
            $this->payment->validate($order)
        );
    }

    /**
     * Log payment history event when payment has been credited.
     *
     * @param Order $order
     * @return void
     * @throws AlreadyExistsException
     * @throws LocalizedException
     */
    private function logPaymentCreditedEvent(Order $order)
    {
        $this->paymentHistoryDbHelper->addEntry(
            $this->paymentHistoryDbHelper->createEntry(
                (int)$order->getPayment()->getEntityId(),
                PaymentHistory::EVENT_PAYMENT_CREDITED,
                PaymentHistory::USER_CLIENT,
                'Performed by user: ' . $this->admin->getUserName()
            )
        );
    }
}
