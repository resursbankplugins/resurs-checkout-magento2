<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Plugin\Order\Create;

use Exception;
use Magento\Checkout\Controller\Onepage\Failure as OnepageFailure;
use Magento\Checkout\Model\Type\Onepage;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Result\Page;
use Magento\Sales\Model\Order;
use Resursbank\Checkout\Helper\Cart;
use Resursbank\Checkout\Helper\Config\Checkout\General as Config;
use Resursbank\Checkout\Helper\Checkout\Onepage as OnepageHelper;
use Resursbank\Checkout\Helper\Db\PaymentHistory as PaymentHistoryDbHelper;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Payment as PaymentHelper;
use Resursbank\Checkout\Model\PaymentHistory;

/**
 * Cancel the previous order, rebuild the cart and redirect to the cart.
 *
 * @package Resursbank\Checkout\Plugin\Order\Create
 */
class Failure
{
    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @var RedirectFactory
     */
    private $redirectFactory;

    /**
     * @var Onepage
     */
    private $onepage;

    /**
     * @var Cart
     */
    private $cartHelper;

    /**
     * @var OnepageHelper
     */
    private $onepageHelper;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var PaymentHistoryDbHelper
     */
    private $paymentHistoryDbHelper;

    /**
     * @var PaymentHelper
     */
    private $paymentHelper;

    /**
     * @param ManagerInterface $messageManager
     * @param Log $log
     * @param UrlInterface $url
     * @param RedirectFactory $redirectFactory
     * @param Onepage $onepage
     * @param Config $config
     * @param Cart $cartHelper
     * @param OnepageHelper $onepageHelper
     * @param PaymentHistoryDbHelper $paymentHistoryDbHelper
     * @param PaymentHelper $paymentHelper
     */
    public function __construct(
        ManagerInterface $messageManager,
        Log $log,
        UrlInterface $url,
        RedirectFactory $redirectFactory,
        Onepage $onepage,
        Config $config,
        Cart $cartHelper,
        OnepageHelper $onepageHelper,
        PaymentHistoryDbHelper $paymentHistoryDbHelper,
        PaymentHelper $paymentHelper
    ) {
        $this->messageManager = $messageManager;
        $this->log = $log;
        $this->url = $url;
        $this->redirectFactory = $redirectFactory;
        $this->onepage = $onepage;
        $this->cartHelper = $cartHelper;
        $this->onepageHelper = $onepageHelper;
        $this->config = $config;
        $this->paymentHistoryDbHelper = $paymentHistoryDbHelper;
        $this->paymentHelper = $paymentHelper;
    }

    /**
     * Cancel the previous order, rebuild the cart and redirect to the cart.
     *
     * @param OnepageFailure $subject
     * @param Page|Redirect $result
     * @return Redirect
     * @throws Exception
     */
    public function afterExecute(OnepageFailure $subject, $result)
    {
        /** @var Redirect $redirect */
        $redirect = $this->redirectFactory->create();

        try {
            /** @var Order $order */
            $order = $this->onepageHelper->getCurrentOrder();

            if ($this->isEnabled($order)) {
                // Log payment history event.
                $this->logPaymentEvent($order);

                // Rebuild shopping cart from previous quote.
                $this->cartHelper->rebuildCart($order);

                // Add error message explaining the payment failed but they may
                // try a different payment method.
                $this->messageManager->addErrorMessage(__(
                    'The payment failed. Please confirm the cart content ' .
                    'and try a different payment method.'
                ));
            }
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage(__(
                'The payment failed and the cart could not be rebuilt. ' .
                'Please add the items back to your cart manually and try ' .
                'a different payment alternative. We sincerely apologize ' .
                'for this inconvenience.'
            ));

            $this->log->error($e);
        }

        // Redirect to cart page.
        return $redirect->setPath($this->url->getUrl('checkout/cart'));
    }

    /**
     * Check if this plugin should execute.
     *
     * NOTE: Zero sum orders won't have any payment. For that reason we cannot
     * validate using $this->paymentHelper->validate(), instead we just confirm
     * the payment method originates from Resurs Bank using validateMethod().
     *
     * @param Order $order
     * @return bool
     * @throws Exception
     */
    protected function isEnabled(Order $order)
    {
        return (
            $this->config->isEnabled() &&
            $this->paymentHelper->validateMethod($order)
        );
    }

    /**
     * Log payment history event when the customer enters the order failure
     * page.
     *
     * @param Order $order
     * @return void
     * @throws AlreadyExistsException
     * @throws LocalizedException
     * @throws ValidatorException
     */
    private function logPaymentEvent(Order $order)
    {
        if ($order->getPayment() === null) {
            throw new ValidatorException(
                __('Missing payment on order #' .
                    $order->getIncrementId())
            );
        }

        $entityId = $order->getPayment()->getEntityId();

        if ($entityId !== null) {
            $this->paymentHistoryDbHelper->addEntry(
                $this->paymentHistoryDbHelper->createEntry(
                    (int) $entityId,
                    PaymentHistory::EVENT_PURCHASE_DECLINED,
                    PaymentHistory::USER_CUSTOMER
                )
            );
        }
    }
}
