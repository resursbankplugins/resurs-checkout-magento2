<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Plugin\Order\Create;

use Exception;
use Magento\Checkout\Controller\Onepage\Success as OnepageSuccess;
use Magento\Checkout\Model\Session;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Result\Page;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Address;
use Magento\Sales\Model\Order\AddressRepository;
use Resursbank\Checkout\Api\Data\PaymentHistoryInterface;
use Resursbank\Checkout\Exception\InvalidDataException;
use Resursbank\Checkout\Helper\Api as HelperApi;
use Resursbank\Checkout\Helper\Checkout\Onepage;
use Resursbank\Checkout\Helper\Order as OrderHelper;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ConfigApi;
use Resursbank\Checkout\Helper\Config\Checkout\General as Config;
use Resursbank\Checkout\Helper\Db\PaymentHistory as PaymentHistoryDbHelper;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Payment as PaymentHelper;
use Resursbank\Checkout\Model\Api;
use Resursbank\Checkout\Model\Api\Adapter\Simplified\Address as PaymentAddress;
use Resursbank\Checkout\Model\Api\Adapter\Simplified as ApiAdapter;
use Resursbank\Checkout\Model\PaymentHistory;
use Resursbank\Checkout\Model\Callback;

/**
 * Book signed payment (complete a payment which has already been created at
 * Resurs Bank).
 *
 * @package Resursbank\Checkout\Plugin\Order\Create
 */
class Success
{
    /**
     * @var Log
     */
    private $log;

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var ApiAdapter
     */
    private $apiAdapter;

    /**
     * @var HelperApi
     */
    private $helperApi;

    /**
     * @var ConfigApi
     */
    private $configApi;

    /**
     * @var RedirectFactory
     */
    private $redirectFactory;

    /**
     * @var AddressRepository
     */
    private $addressRepository;

    /**
     * @var Onepage
     */
    private $onepage;

    /**
     * @var PaymentHistoryDbHelper
     */
    private $paymentHistoryDbHelper;

    /**
     * @var PaymentHelper
     */
    private $paymentHelper;

    /**
     * @var Callback
     */
    private $callback;

    /**
     * @param Log $log
     * @param UrlInterface $url
     * @param Config $config
     * @param ApiAdapter $apiAdapter
     * @param HelperApi $helperApi
     * @param ConfigApi $configApi
     * @param RedirectFactory $redirectFactory
     * @param AddressRepository $addressRepository
     * @param Onepage $onepage
     * @param PaymentHistoryDbHelper $paymentHistoryDbHelper
     * @param PaymentHelper $paymentHelper
     */
    public function __construct(
        Log $log,
        UrlInterface $url,
        Config $config,
        ApiAdapter $apiAdapter,
        HelperApi $helperApi,
        ConfigApi $configApi,
        RedirectFactory $redirectFactory,
        AddressRepository $addressRepository,
        Onepage $onepage,
        PaymentHistoryDbHelper $paymentHistoryDbHelper,
        PaymentHelper $paymentHelper,
        Callback $callback
    ) {
        $this->log = $log;
        $this->url = $url;
        $this->config = $config;
        $this->apiAdapter = $apiAdapter;
        $this->helperApi = $helperApi;
        $this->configApi = $configApi;
        $this->redirectFactory = $redirectFactory;
        $this->addressRepository = $addressRepository;
        $this->onepage = $onepage;
        $this->paymentHistoryDbHelper = $paymentHistoryDbHelper;
        $this->paymentHelper = $paymentHelper;
        $this->callback = $callback;
    }

    /**
     * @param OnepageSuccess $subject
     * @param Page|Redirect $result
     * @return Redirect
     */
    public function afterExecute(OnepageSuccess $subject, $result)
    {
        try {
            $session = $subject->getOnepage()->getCheckout();
            $order = $session->getLastRealOrder();

            if ($this->isEnabled($order)) {
                if ($this->configApi->isSimplifiedFlow()) {
                    $this->handleSimplifiedFlow($subject, $order);
                }

                // Log payment history event.
                $this->logPaymentEvent();

                // Update billing address on order.
                $this->updateBillingAddress($order);

                // NOTE: This applies to both flows. We do not wish to clear
                // the session information until the order process is
                // successfully concluded.
                if ((bool) $order->getData(OrderHelper::EMAIL_REQUESTED_COLUMN) === false) {
                    // Set custom order status.
                    $this->setOrderStatus($order);
                }

                // Clear session data.
                $this->helperApi->clearPaymentSession();
                $this->helperApi->clearQuoteSession($order);
            }
        } catch (Exception $e) {
            $this->log->error($e);

            /** @var Redirect $redirect */
            $result = $this->redirectFactory->create();

            // Redirect to failure page.
            $result->setPath($this->url->getUrl('checkout/onepage/failure'));
        }

        return $result;
    }

    /**
     * Unique actions for Simplified Flow.
     *
     * @param OnepageSuccess $subject
     * @param Order $order
     * @return void
     * @throws Exception
     */
    private function handleSimplifiedFlow(
        OnepageSuccess $subject,
        Order $order
    ) {
        /** @var Session $session */
        $session = $subject->getOnepage()->getCheckout();

        /** @var string $orderId */
        $paymentId = (string)$session->getData(
            ApiAdapter::SESSION_PAYMENT_ID
        );

        // There will only be a paymentId in our session if the payment was
        // created, see Plugin/Simplified/Order/Create.php
        if ($paymentId !== '') {
            $this->apiAdapter->bookPayment($paymentId, $order);
        }
    }

    /**
     * Try to update the billing address on the order entity to reflect the
     * address applied on the payment at Resurs Bank.
     *
     * NOTE: If there is an Exception during this process we will simply log the
     * error and leave the billing address applied on the order as-is. This is
     * by design since it's not vital for the address information to match
     * between the order and the payment, it's just more proper.
     *
     * @param Order $order
     * @return void
     */
    private function updateBillingAddress(Order $order)
    {
        try {
            /** @var array $payment */
            $payment = $this->apiAdapter->getPayment($order->getIncrementId());

            // Resolve address from Resurs Bank payment.
            $address = $this->resolveAddressFromPayment($payment);

            if ($address instanceof PaymentAddress) {
                // Override billing address with information from Resurs Bank.
                $this->overrideBillingAddress(
                    $address,
                    $order,
                    $payment
                );
            }
        } catch (Exception $e) {
            $this->log->info(
                'Failed to update billing address on order ' .
                $order->getIncrementId() . '. The address on the payment at ' .
                'Resurs Bank may differ. The complete Exception will follow ' .
                'below.'
            );

            $this->log->info($e);
        }
    }

    /**
     * Check whether customer type is "LEGAL" (company) in payment information.
     *
     * @param array $payment
     * @return bool
     */
    private function isCompanyCustomer(array $payment): bool
    {
        $type = ($payment['customer']['type'] ?? Api::CUSTOMER_TYPE_PRIVATE);

        return $type === Api::CUSTOMER_TYPE_COMPANY;
    }

    /**
     * Override billing address on order with information from Resurs Bank
     * payment. This is to ensure the customer's billing address in Magento
     * matches the address resolved by Resurs Bank when the payment is created.
     *
     * @param PaymentAddress $address
     * @param Order $order
     * @param array $payment
     * @return void
     * @throws CouldNotSaveException
     */
    private function overrideBillingAddress(
        PaymentAddress $address,
        Order $order,
        array $payment
    ) {
        /** @var Address $billingAddress */
        $billingAddress = $order->getBillingAddress();

        if ($this->isCompanyCustomer($payment)) {
            $billingAddress->setCompany($address->getCompany());
        } else {
            $billingAddress->setFirstname($address->getFirstName())
                ->setLastname($address->getLastName());
        }

        $billingAddress
            ->setStreet([
                $address->getStreet0(),
                $address->getStreet1()
            ])
            ->setPostcode($address->getPostcode())
            ->setCity($address->getCity())
            ->setCountryId($address->getCountry());

        // Save updates applied on address entity.
        $this->addressRepository->save($billingAddress);

        // Ensure the address is applied on the order entity (without this
        // "bill to name" in the order grid would for example give the previous
        // value).
        $order->setBillingAddress($billingAddress);
    }

    /**
     * Resolve address from payment.
     *
     * @param array $payment
     * @return PaymentAddress|null
     * @throws InvalidDataException
     */
    private function resolveAddressFromPayment(array $payment)
    {
        $result = null;

        if (isset($payment['customer']['address']) &&
            is_array($payment['customer']['address']) &&
            !empty($payment['customer']['address'])
        ) {
            $result = new PaymentAddress(
                ($payment['customer']['address']['firstName'] ?? ''),
                ($payment['customer']['address']['lastName'] ?? ''),
                $payment['customer']['address']['postalArea'],
                $payment['customer']['address']['postalCode'],
                $payment['customer']['address']['country'],
                $payment['customer']['address']['addressRow1'],
                ($payment['customer']['address']['addressRow2'] ?? ''),
                ($payment['customer']['address']['fullName'] ?? '')
            );
        }

        return $result;
    }

    /**
     * Log payment history event when customer enters order success page.
     *
     * @return void
     */
    private function logPaymentEvent()  
    {
        try {
            /** @var Order $order */
            $order = $this->onepage->getCurrentOrder();

            if ($order->getPayment() === null) {
                throw new ValidatorException(
                    __('Missing payment on order #' .
                    $order->getIncrementId())
                );
            }

            $entityId = $order->getPayment()->getEntityId();

            if ($entityId !== null) {
                $this->paymentHistoryDbHelper->addEntry(
                    $this->paymentHistoryDbHelper->createEntry(
                        (int)$entityId,
                        PaymentHistory::EVENT_PAYMENT_COMPLETED,
                        PaymentHistory::USER_RESURS_BANK
                    )
                );
            }
        } catch (Exception $e) {
            $this->log->error($e);
        }
    }

    /**
     * Apply custom status on order to avoid the internal cronjob that removes
     * expired orders after x number of minutes.
     *
     * @param Order $order
     * @return void
     * @throws Exception
     */
    private function setOrderStatus(Order $order)
    {
        $this->callback->syncStatusFromGateway($order);
    }

    /**
     * NOTE: Zero sum orders won't have any payment. For that reason we cannot
     * validate using $this->paymentHelper->validate(), instead we just confirm
     * the payment method originates from Resurs Bank using validateMethod().
     *
     * @param Order $order
     * @return bool
     * @throws Exception
     */
    private function isEnabled(Order $order): bool
    {
        return (
            $this->config->isEnabled() &&
            $this->paymentHelper->validateMethod($order)
        );
    }
}
