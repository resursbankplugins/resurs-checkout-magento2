<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Plugin\Order;

use Exception;
use Magento\Sales\Model\Order;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ConfigApi;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Payment;
use Resursbank\Checkout\Model\Api as ModelApi;

/**
 * When the order is placed we will change the payment reference from the
 * randomised token used when identifying the quote to the increment_id assigned
 * to the order object.
 *
 * We will also disable the outgoing order notification email, this will be sent
 * after the payment has been completed and the client reaches the success page
 * instead.
 *
 * @package Resursbank\Checkout\Plugin\Order
 */
class Create
{
    /**
     * @var ModelApi
     */
    private $apiModel;

    /**
     * @var ConfigApi
     */
    private $apiConfig;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var Payment
     */
    private $paymentHelper;

    /**
     * @param ModelApi $apiModel
     * @param ConfigApi $apiConfig
     * @param Log $log
     * @param Payment $paymentHelper
     */
    public function __construct(
        ModelApi $apiModel,
        ConfigApi $apiConfig,
        Log $log,
        Payment $paymentHelper
    ) {
        $this->apiModel = $apiModel;
        $this->apiConfig = $apiConfig;
        $this->log = $log;
        $this->paymentHelper = $paymentHelper;
    }

    /**
     * Prevent order confirmation email from being sent during order creation
     * and update the Resurs Bank payment reference to correspond with the newly
     * created order's increment_id.
     *
     * @param Order $subject
     * @param Order $result
     * @return Order
     * @throws Exception
     */
    public function afterBeforeSave(Order $subject, Order $result): Order
    {
        try {
            if ($this->isEnabled($subject)) {
                // Avoid sending an order confirmation email until the payment
                // has been accepted by Resurs Bank.
                $subject->setCanSendNewEmailFlag(false);

                // In Checkout Flow we need to update the id of the payment
                // session to correspond with the order increment_id.
                if ($this->apiConfig->isCheckoutFlow()) {
                    // Change payment reference from a random token string to
                    // the actual order id.
                    $this->apiModel->updatePaymentReference(
                        $this->apiModel->getPaymentReference(),
                        $subject->getIncrementId()
                    );
                }
            }
        } catch (Exception $e) {
            $this->log->error($e);
            throw $e;
        }

        return $result;
    }

    /**
     * Check whether or not this plugin should execute.
     *
     * @param Order $order
     * @return bool
     * @throws Exception
     */
    private function isEnabled(Order $order): bool
    {
        return (
            $order->isObjectNew() &&
            !$order->getOriginalIncrementId() &&
            $this->paymentHelper->validateMethod($order) &&
            $order->getGrandTotal() > 0
        );
    }
}
