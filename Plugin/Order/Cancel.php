<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Plugin\Order;

use Exception;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Controller\Adminhtml\Order\Cancel as Original;
use Magento\Sales\Model\Order;
use Resursbank\Checkout\Helper\Admin;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Config\Checkout\Ecom as EcomConfig;
use Resursbank\Checkout\Helper\Db\PaymentHistory as PaymentHistoryDbHelper;
use Resursbank\Checkout\Helper\Ecom;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Order as OrderHelper;
use Resursbank\Checkout\Helper\Payment;
use Resursbank\Checkout\Model\Api\Credentials;
use Resursbank\Checkout\Model\PaymentHistory;
use Resursbank\RBEcomPHP\ResursBank;

/**
 * Annul payment at Resurs Bank when the corresponding order is cancelled in
 * Magento.
 *
 * @package Resursbank\Checkout\Plugin\Order
 */
class Cancel
{
    /**
     * @var Ecom
     */
    private $ecom;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var Payment
     */
    private $payment;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @var EcomConfig
     */
    private $ecomConfig;

    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var Admin
     */
    private $admin;

    /**
     * @var PaymentHistoryDbHelper
     */
    private $paymentHistoryDbHelper;

    /**
     * @param Ecom $ecom
     * @param ManagerInterface $messageManager
     * @param RequestInterface $request
     * @param Log $log
     * @param Payment $payment
     * @param ApiConfig $apiConfig
     * @param EcomConfig $ecomConfig
     * @param OrderHelper $orderHelper
     * @param OrderRepositoryInterface $orderRepository
     * @param Admin $admin
     * @param PaymentHistoryDbHelper $paymentHistoryDbHelper
     */
    public function __construct(
        Ecom $ecom,
        ManagerInterface $messageManager,
        RequestInterface $request,
        Log $log,
        Payment $payment,
        ApiConfig $apiConfig,
        EcomConfig $ecomConfig,
        OrderHelper $orderHelper,
        OrderRepositoryInterface $orderRepository,
        Admin $admin,
        PaymentHistoryDbHelper $paymentHistoryDbHelper
    ) {
        $this->ecom = $ecom;
        $this->messageManager = $messageManager;
        $this->request = $request;
        $this->log = $log;
        $this->payment = $payment;
        $this->apiConfig = $apiConfig;
        $this->ecomConfig = $ecomConfig;
        $this->orderHelper = $orderHelper;
        $this->orderRepository = $orderRepository;
        $this->admin = $admin;
        $this->paymentHistoryDbHelper = $paymentHistoryDbHelper;
    }

    /**
     * After an order has been cancelled we will cancel its payment session.
     *
     * @param Original $subject
     * @param Redirect $result
     * @return Redirect
     * @throws Exception
     */
    public function afterExecute(Original $subject, Redirect $result): Redirect
    {
        try {
            /** @var Order $order */
            $order = $this->orderRepository->get(
                (int) $this->request->getParam('order_id')
            );

            if ($this->isEnabled($order)) {
                /** @var string $reference */
                $reference = $this->orderHelper->getPaymentId($order);

                if (!$this->cancel($order, $reference)) {
                    throw new Exception(
                        'An error occurred while communicating with the API.'
                    );
                }

                $this->log->info("Successfully cancelled payment {$reference}");

                $this->orderRepository->save($order);

                // Log payment history event.
                $this->logPaymentCanceledEvent($order);
            }
        } catch (Exception $e) {
            // Display generic error.
            $this->messageManager->addErrorMessage(
                sprintf(__('Failed to cancel Resurs Bank payment: %s.'), $e->getMessage())
            );

            // Log actual error.
            $this->log->error($e);

            throw $e;
        }

        return $result;
    }

    /**
     * Cancel Resurs Bank payment.
     *
     * @param Order $order
     * @param string $reference
     * @return bool
     * @throws Exception
     */
    private function cancel(Order $order, string $reference): bool
    {
        $this->log->info("Cancelling payment {$reference}");

        /** @var ResursBank $ecom */
        $ecom = $this->ecom->getConnection($this->getApiCredentials($order));

        // Set platform / user reference.
        $ecom->setRealClientName('Magento2');
        $ecom->setLoggedInUser($this->admin->getUserName());

        // Without this ECom will lose the payment reference.
        $ecom->setPreferredId($reference);

        return $ecom->annulPayment($reference);
    }

    /**
     * Retrieve API credentials relative to the store the order was created in.
     *
     * @param Order $order
     * @return Credentials
     * @throws Exception
     */
    private function getApiCredentials(Order $order): Credentials
    {
        return $this->apiConfig->getCredentials($order->getStore()->getCode());
    }

    /**
     * Check whether this plugin should execute.
     *
     * @param Order $order
     * @return bool
     * @throws Exception
     */
    private function isEnabled(Order $order)
    {
        return (
            $this->ecomConfig->isAfterShopEnabled(
                $order->getStore()->getCode()
            ) &&
            $this->payment->validate($order)
        );
    }

    /**
     * Log payment history event when payment has been annulled.
     *
     * @param Order $order
     * @return void
     * @throws AlreadyExistsException
     * @throws LocalizedException
     */
    private function logPaymentCanceledEvent(Order $order)
    {
        $this->paymentHistoryDbHelper->addEntry(
            $this->paymentHistoryDbHelper->createEntry(
                (int)$order->getPayment()->getEntityId(),
                PaymentHistory::EVENT_PAYMENT_ANNULLED,
                PaymentHistory::USER_CLIENT,
                'Performed by user: ' . $this->admin->getUserName()
            )
        );
    }
}
