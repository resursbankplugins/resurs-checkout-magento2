<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Plugin\Config\Structure;

use Exception;
use Magento\Config\Model\Config\Structure\Converter as Original;
use Magento\Framework\App\RequestInterface;
use Resursbank\Checkout\Helper\Config\Checkout\Api;
use Resursbank\Checkout\Helper\Db\Account;
use Resursbank\Checkout\Helper\Db\Account\Method as MethodDb;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Logic\Account\Method;
use Resursbank\Checkout\Model\Account\Method as MethodModel;
use Resursbank\Checkout\Model\Account as AccountModel;
use Resursbank\Checkout\Model\Api\Credentials;
use Resursbank\Checkout\Model\ResourceModel\Account\Method\Collection;

/**
 * Create custom configuration sections for all dynamic payment methods.
 *
 * We need to create the sections this way since we do not know what payment
 * methods will be available until the client fetches them from the API.
 *
 * @package Resursbank\Checkout\Plugin\Config\Structure
 */
class Converter
{
    /**
     * @var Method
     */
    private $method;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var Api
     */
    private $api;

    /**
     * @var MethodDb
     */
    private $methodDb;

    /**
     * @var Account
     */
    private $account;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @param Method $method
     * @param Log $log
     * @param Api $api
     * @param MethodDb $methodDb
     * @param Account $account
     * @param RequestInterface $request
     */
    public function __construct(
        Method $method,
        Log $log,
        Api $api,
        MethodDb $methodDb,
        Account $account,
        RequestInterface $request
    ) {
        $this->method = $method;
        $this->log = $log;
        $this->api = $api;
        $this->methodDb = $methodDb;
        $this->account = $account;
        $this->request = $request;
    }

    /**
     * Build and append configuration sections.
     *
     * @param Original $subject
     * @param array $result
     * @return array
     * @throws Exception
     */
    public function afterConvert(Original $subject, array $result): array
    {
        try {
            if ($this->hasConfigElement($result)) {
                $collection = $this->getMethodsCollection();

                // Amend array structure for our payment methods.
                $methods = &$result['config']['system']['sections']
                ['resursbank_methods']['children']['methods'];

                if (!isset($methods['children']) ||
                    !is_array($methods['children'])
                ) {
                    $methods['children'] = [];
                }

                /** @var MethodModel $method */
                foreach ($collection as $method) {
                    /** @var AccountModel $account */
                    $account = $this->account->getModel()
                        ->load($method->getAccountId());

                    $this->addConfig($methods, $method, $account);
                }
            }
        } catch (Exception $e) {
            $this->log->error($e, true);

            throw $e;
        }

        return $result;
    }

    /**
     * Retrieve collection of payment methods for the active account.
     *
     * @return Collection
     * @throws Exception
     */
    private function getMethodsCollection(): Collection
    {
        return $this->methodDb->getCollection()
            ->addFieldToFilter('active', true);
    }

    /**
     * Add payment method config section.
     *
     * @param array $config
     * @param MethodModel $method
     * @param AccountModel $account
     */
    private function addConfig(
        array &$config,
        MethodModel $method,
        AccountModel $account
    ) {
        $type = $method->getRawValue('type');
        $specificType = $method->getRawValue('specificType');

        // Applies to all methods.
        $this->addBasicConfig($config, $method, $account);

        // "Fee" config. For all Resurs Bank methods without external partners.
        if ($type !== 'PAYMENT_PROVIDER') {
            $this->addFeeConfig($config, $method);
        }

        // Disable Input field, for "Resurs Bank - Card" method.
        if ($type === 'CARD' && $specificType === 'CARD') {
            $this->addDisableInputConfig($config, $method);
        }
    }

    /**
     * Appends basic configuration structure to system config.
     *
     * NOTE: This creates the required array keys for all conditional config
     * options (such as "Payment Fee"). Be sure to always execute this before
     * any subsequent method to further expand the config structure.
     *
     * @param array $config
     * @param MethodModel $method
     * @param AccountModel $account
     */
    private function addBasicConfig(
        array &$config,
        MethodModel $method,
        AccountModel $account
    ) {
        $config['children'][$method->getCode()] = [
            'id' => $method->getCode(),
            'translate' => 'label',
            'sortOrder' => 0,
            'showInDefault' => 1,
            'showInWebsite' => 1,
            'showInStore' => 1,
            'label' => $this->getMethodLabel($method, $account),
            '_elementType' => 'group',
            'path' => 'resursbank_methods',
            'children' => [
                'title' => [
                    'id' => 'title',
                    'translate' => 'label',
                    'type' => 'text',
                    'sortOrder' => 0,
                    'showInDefault' => 1,
                    'showInWebsite' => 1,
                    'showInStore' => 1,
                    'label' => 'Title',
                    '_elementType' => 'field',
                    'path' => "resursbank_methods/{$method->getCode()}"
                ],
                'sort_order' => [
                    'id' => 'sort_order',
                    'translate' => 'label',
                    'type' => 'text',
                    'sortOrder' => 1,
                    'showInDefault' => 1,
                    'showInWebsite' => 1,
                    'showInStore' => 1,
                    'label' => 'Sort Order',
                    '_elementType' => 'field',
                    'path' => "resursbank_methods/{$method->getCode()}"
                ]
            ]
        ];
    }

    /**
     * Appends configuration options for payment fee to config structure.
     *
     * NOTE: Always execute addBasicConfig prior to this to ensure all required
     * array keys exist.
     *
     * @param array $config
     * @param MethodModel $method
     */
    private function addFeeConfig(
        array &$config,
        MethodModel $method
    ) {
        $config['children'][$method->getCode()]['children']['fee'] = [
            'id' => 'fee',
            'translate' => 'label',
            'type' => 'text',
            'sortOrder' => 2,
            'showInDefault' => 1,
            'showInWebsite' => 1,
            'showInStore' => 1,
            'label' => 'Fee',
            '_elementType' => 'field',
            'path' => "resursbank_methods/{$method->getCode()}"
        ];
    }

    /**
     * Appends config options to disable input fields to config structure.
     *
     * NOTE: Always execute addBasicConfig prior to this to ensure all required
     * array keys exist.
     *
     * @param array $config
     * @param MethodModel $method
     */
    private function addDisableInputConfig(
        array &$config,
        MethodModel $method
    ) {
        $config['children'][$method->getCode()]['children']['disable_input'] = [
            'id' => 'disable_input',
            'translate' => 'label',
            'type' => 'select',
            'source_model' => 'Magento\Config\Model\Config\Source\Yesno',
            'sortOrder' => 3,
            'showInDefault' => 1,
            'showInWebsite' => 1,
            'showInStore' => 1,
            'label' => 'Disable Input',
            'comment' => (
                'Allow payments without card / account number. The ' .
                'government ID will fetch card / account number through ' .
                'signing. This will allow clients to proceed through ' .
                'checkout faster, but it requires some settings to be ' .
                'applied for your API account at Resurs Bank. Please contact ' .
                'us before you apply this setting.'
            ),
            '_elementType' => 'field',
            'path' => "resursbank_methods/{$method->getCode()}"
        ];
    }

    /**
     * Check if config array includes element for payment methods.
     *
     * @param array $result
     * @return bool
     */
    private function hasConfigElement(array $result): bool
    {
        return (
            isset(
                $result['config']['system']['sections']['resursbank_methods']
            ) &&
            is_array(
                $result['config']['system']['sections']['resursbank_methods']
            )
        );
    }

    /**
     * @param MethodModel $method
     * @param AccountModel $account
     * @return string
     */
    private function getMethodLabel(
        MethodModel $method,
        AccountModel $account
    ): string {
        /** @var Credentials $credentials */
        $credentials = $this->account->getCredentials($account);

        return sprintf(
            "%s (%s :: %s)",
            $method->getTitle(),
            $account->getUsername(),
            $credentials->getEnvironmentCode()
        );
    }
}
