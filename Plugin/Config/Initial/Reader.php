<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Plugin\Config\Initial;

use Exception;
use DomainException;
use Magento\Framework\App\Config\Initial\Reader as Original;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Helper\Logic\Account\Method;

/**
 * Inject our dynamic payment methods into the initially assembled config.
 *
 * @package Resursbank\Checkout\Plugin\Config\Initial
 */
class Reader
{
    /**
     * @var Method
     */
    private $method;

    /**
     * @var Log
     */
    private $log;

    /**
     * @param Method $method
     * @param Log $log
     */
    public function __construct(
        Method $method,
        Log $log
    ) {
        $this->method = $method;
        $this->log = $log;
    }

    /**
     * Method injector.
     *
     * @param Original $subject
     * @param array $result
     * @return array
     * @throws Exception
     * @noinspection PhpUnusedParameterInspection
     */
    public function afterRead(Original $subject, array $result): array
    {
        try {
            if ($this->hasPaymentElement($result)) {
                // Retrieve complete config of all dynamic methods.
                $collection = $this->method->collect();

                // Inject method config.
                if (is_array($collection) && !empty($collection)) {
                    $result['data']['default']['payment'] = array_merge(
                        $result['data']['default']['payment'],
                        $collection
                    );
                }
            }
        } catch (DomainException $e) {
            // Do nothing. This kind of Exception will be thrown whenever we
            // generate static content without a database present.
        } catch (Exception $e) {
            // NOTE: In certain situations an error is expected, for example
            // in the build portion of a pipeline where the database may be
            // absent since it's not required.
            $this->log->error($e, true);
        }

        return $result;
    }

    /**
     * Check if config array includes element for payment methods.
     *
     * @param array $result
     * @return bool
     */
    private function hasPaymentElement(array $result): bool
    {
        return (
            isset($result['data']['default']['payment']) &&
            is_array($result['data']['default']['payment'])
        );
    }
}
