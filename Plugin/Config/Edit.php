<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Resursbank\Checkout\Plugin\Config;

use Exception;
use Magento\Config\Controller\Adminhtml\System\Config\Edit as MagentoEdit;
use Magento\Framework\Message\ManagerInterface;
use Resursbank\Checkout\Helper\Callback as CallbackHelper;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Model\Api;

/**
 * Run code before the config page has been loaded.
 *
 * Class Edit
 * @package Resursbank\Checkout\Plugin\Config\Edit
 */
class Edit
{
    /**
     * Used to fetch available payment methods.
     *
     * @var Api
     */
    private $api;

    /**
     * @var ManagerInterface
     */
    private $messages;

    /**
     * @var CallbackHelper
     */
    private $callback;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * Config constructor.
     *
     * @param Api $api
     * @param CallbackHelper $callback
     * @param ApiConfig $apiConfig
     * @param ManagerInterface $messages
     */
    public function __construct(
        Api $api,
        CallbackHelper $callback,
        ApiConfig $apiConfig,
        ManagerInterface $messages
    ) {
        $this->api = $api;
        $this->messages = $messages;
        $this->callback = $callback;
        $this->apiConfig = $apiConfig;
    }

    /**
     * Check for errors before the config page loads. Because we use the message
     * bag for displaying errors, we need to check certain values before the
     * page loads so that the error(s) displays when its loaded. Otherwise, the
     * error messages will be a page load behind, and show when you reload the
     * page a second time.
     *
     * @param MagentoEdit $subject
     * @return null
     */
    public function beforeExecute(MagentoEdit $subject)
    {
        $section = $subject->getRequest()->getParam('section');

        if ($section === 'resursbank_checkout') {
            try {
                $this->callback->getCallbacks(
                    $this->apiConfig->getCredentials()
                );
            } catch (Exception $e) {
                $this->messages->addErrorMessage($e->getMessage());
            }
        }

        return null;
    }
}
