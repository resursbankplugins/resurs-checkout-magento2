<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Plugin\Payment;

use Exception;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\MethodInterface;
use Resursbank\Checkout\Helper\Config\Checkout\General as Config;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\Payment\Standard;

/**
 * When Magento creates an instance of the configured model for a payment method
 * (at the time of writing, this is always
 * \Resursbank\Checkout\Model\Payment\Standard in our case since all our payment
 * methods use the same model class) the 'code' Magento later uses to identify
 * the method will always be "resursbank_default", because it's a private
 * variable on the model class with a static value.
 *
 * This plugin overwrites the $_code property on the model class instance
 * with the correct value, to ensure methods can always be accurately
 * identified.
 *
 * @package Resursbank\Checkout\Plugin\Payment
 */
class CorrectMethodInstance
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var Log
     */
    private $log;

    /**
     * @param Config $config
     * @param Log $log
     */
    public function __construct(
        Config $config,
        Log $log
    ) {
        $this->config = $config;
        $this->log = $log;
    }

    /**
     * @param Data $subject
     * @param MethodInterface $result
     * @param $code
     * @return MethodInterface
     * @throws Exception
     */
    public function afterGetMethodInstance(
        Data $subject,
        MethodInterface $result,
        $code
    ) {
        try {
            if ($this->isEnabled($result)) {
                $result->setCode($code);
            }
        } catch (Exception $e) {
            $this->log->error($e);
            throw $e;
        }

        return $result;
    }

    /**
     * Check whether or not this plugin should execute.
     *
     * NOTE: refer to the documentation of this class for more information about
     * the weird static 'code' and class instance checks.
     *
     * @param MethodInterface $method
     * @return bool
     * @throws Exception
     */
    private function isEnabled(MethodInterface $method)
    {
        return (
            $this->config->isEnabled() &&
            $method->getCode() === 'resursbank_default' &&
            ($method instanceof Standard)
        );
    }
}
