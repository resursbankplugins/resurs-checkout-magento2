<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Plugin\PaymentSession;

use Exception;
use Magento\Checkout\Controller\Index\Index;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Resursbank\Checkout\Helper\Api as HelperApi;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Config\Checkout\General as Config;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\Api as ModelApi;

/**
 * Initialize payment session.
 *
 * @package Resursbank\Checkout\Plugin\PaymentSession
 */
class Init
{
    /**
     * @var ModelApi
     */
    private $apiModel;

    /**
     * @var HelperApi
     */
    private $apiHelper;

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @param ModelApi $apiModel
     * @param HelperApi $apiHelper
     * @param ResponseFactory $responseFactory
     * @param UrlInterface $url
     * @param ManagerInterface $messageManager
     * @param Log $log
     * @param Config $config
     * @param ApiConfig $apiConfig
     */
    public function __construct(
        ModelApi $apiModel,
        HelperApi $apiHelper,
        ResponseFactory $responseFactory,
        UrlInterface $url,
        ManagerInterface $messageManager,
        Log $log,
        Config $config,
        ApiConfig $apiConfig
    ) {
        $this->apiModel = $apiModel;
        $this->apiHelper = $apiHelper;
        $this->url = $url;
        $this->responseFactory = $responseFactory;
        $this->messageManager = $messageManager;
        $this->log = $log;
        $this->config = $config;
        $this->apiConfig = $apiConfig;
    }

    /**
     * Initialize payment session before the checkout page loads (predispatch of
     * checkout_index_index).
     *
     * @param Index $subject
     * @return null
     * @throws Exception
     */
    public function beforeExecute(Index $subject)
    {
        try {
            if ($this->isEnabled()) {
                $this->apiModel->initPaymentSession();
            }
        } catch (Exception $e) {
            // Log error information.
            $this->log->error($e);
        }

        return null;
    }

    /**
     * Check if this plugin should execute.
     *
     * @return bool
     * @throws Exception
     */
    protected function isEnabled()
    {
        return (
            $this->config->isEnabled() &&
            $this->apiConfig->isCheckoutFlow() &&
            !$this->apiModel->paymentSessionInitialized()
        );
    }
}
