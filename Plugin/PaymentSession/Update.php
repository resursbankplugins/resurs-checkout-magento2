<?php
/**
 * Copyright 2016 Resurs Bank AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Resursbank\Checkout\Plugin\PaymentSession;

use Exception;
use Magento\Quote\Model\Quote;
use Resursbank\Checkout\Helper\Api as HelperApi;
use Resursbank\Checkout\Helper\Config\Checkout\Api as ApiConfig;
use Resursbank\Checkout\Helper\Config\Checkout\General as Config;
use Resursbank\Checkout\Helper\Log;
use Resursbank\Checkout\Model\Api as ModelApi;

/**
 * Update existing payment session.
 *
 * @package Resursbank\Checkout\Plugin\PaymentSession
 */
class Update
{
    /**
     * @var ModelApi
     */
    private $apiModel;

    /**
     * @var HelperApi
     */
    private $apiHelper;

    /**
     * @var ApiConfig
     */
    private $apiConfig;

    /**
     * @var Log
     */
    private $log;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param ModelApi $apiModel
     * @param HelperApi $apiHelper
     * @param ApiConfig $apiConfig
     * @param Log $log
     * @param Config $config
     */
    public function __construct(
        ModelApi $apiModel,
        HelperApi $apiHelper,
        ApiConfig $apiConfig,
        Log $log,
        Config $config
    ) {
        $this->apiModel = $apiModel;
        $this->apiHelper = $apiHelper;
        $this->apiConfig = $apiConfig;
        $this->log = $log;
        $this->config = $config;
    }

    /**
     * Update Resurs Bank payment session after the quote has been saved.
     *
     * @param Quote $subject
     * @param Quote $result
     * @return Quote
     * @throws Exception
     */
    public function afterAfterSave(Quote $subject, $result)
    {
        try {
            if ($this->isEnabled()) {
                if ($subject->getItemsCount() > 0) {
                    $this->apiModel->updatePaymentSession();
                } else {
                    $this->apiHelper->clearPaymentSession();
                }
            }
        } catch (Exception $e) {
            $this->log->error($e);
            throw $e;
        }

        return $result;
    }

    /**
     * Check if this plugin should execute.
     *
     * @return bool
     * @throws Exception
     */
    protected function isEnabled()
    {
        return (
            $this->config->isEnabled() &&
            $this->apiConfig->isCheckoutFlow() &&
            $this->apiModel->paymentSessionInitialized()
        );
    }
}
